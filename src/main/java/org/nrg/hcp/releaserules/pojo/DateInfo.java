package org.nrg.hcp.releaserules.pojo;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DateInfo {
	
	private Date date = null;
	private Boolean dateProblem = false;
	private String dateMessage = "";

}
