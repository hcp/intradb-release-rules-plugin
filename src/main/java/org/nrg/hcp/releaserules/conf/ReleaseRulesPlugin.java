package org.nrg.hcp.releaserules.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbReleaseRulesPlugin",
			name = "Intradb Release Rules Plugin"
		)
@ComponentScan({ })
public class ReleaseRulesPlugin {
	
	public static Logger logger = Logger.getLogger(ReleaseRulesPlugin.class);

	public ReleaseRulesPlugin() {
		logger.info("Configuring Intradb Release Rules plugin");
	}
	
}
