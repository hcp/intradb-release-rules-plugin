package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import org.nrg.ccf.sessionbuilding.releaserules.transformers.DefaultSeriesDescAndSessionLabelTransformer;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public class FilenameTransformer_CCF_PHCP_7T extends DefaultSeriesDescAndSessionLabelTransformer {
	
	@Override
	public String transformFilename(String fileName, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		fileName = super.transformFilename(fileName, srcSession, destSession, srcSessionScan, destSessionScan);
		// NOTE:  PHCP is sharing exact visit dates, so we don't need to rename files.  ALSO!!!  Sometimes
		// there are multiple eyetracker files for a single scan, and this collapsing will cause
		// only a single file to show up!
		// COMMENTING OUT THE TRANSFORMATIONS TO PREVENT ELIMINATION OF EYETRACKER FILES
		//if (fileName.endsWith(".txt") || fileName.endsWith(".edf")) {
		//	fileName = fileName.replaceFirst("_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]","");
		//	fileName = fileName.replaceFirst("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]","");
		//}
		return fileName;
	}

}
