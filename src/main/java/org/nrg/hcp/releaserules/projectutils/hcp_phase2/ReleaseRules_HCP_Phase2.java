package org.nrg.hcp.releaserules.projectutils.hcp_phase2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.ccf.sessionbuilding.abst.AbstractCcfReleaseRules;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.HcpToolboxdata;
import org.nrg.xdat.om.NtScores;
import org.nrg.xdat.om.HcpvisitHcpvisitdata;
import org.nrg.xdat.om.HcpSubjectmetadata;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

//@HcpReleaseRules
@CcfReleaseRules(description = "HCP_Phase2 Session Building Release Rules")
//public class ReleaseRules_HCP_Phase2 extends AbstractCcfReleaseRules implements HcpReleaseRulesI, CcfReleaseRulesI {
public class ReleaseRules_HCP_Phase2 extends AbstractCcfReleaseRules implements CcfReleaseRulesI {
	
	public static final String  LOCALIZER_RGX = "^Localizer.*", 
								SCOUT_RGX = "^AAHScout.*",
								T1WMEG_RGX = "^T1w_MEG.*",
								BIAS_RGX = "(?i)^BIAS_.*", 
								FIELDMAP_RGX = "^FieldMap.*",
								ANYFIELDMAP_RGX = "(?i)^.*FieldMap.*",
								GEFIELDMAP_RGX = "(?i)^FieldMap_((Ma)|(Ph)).*",
								SEFIELDMAP_RGX = "^FieldMap_SE_.*",
								SPINECHO_RGX = "(?i)^SpinEchoFieldMap.*",
								STRUCT_RGX = "(?i)^T[12]w.*",
								T1_RGX = "(?i)^T1w.*",
								T2_RGX = "(?i)^T2w.*", 
								T1MPR_RGX = "(?i)^T1w_MPR[0-9]*.*$",
								T2SPC_RGX = "(?i)^T2w_SPC[0-9]*.*$",
								TFMRI_RGX = "(?i)^tfMRI.*", 
								RFMRI_RGX = "(?i)^rfMRI.*", 
								DWI_RGX = "(?i)^DWI.*", 
								DMRI_RGX = "(?i)^dMRI.*", 
								BOLD_RGX = "(?i)^BOLD.*", 
								BOLDONLY_RGX = "(?i)^BOLD", 
								SBREF_RGX = "(?i)^.*_SBRef$", 
								LR_RGX = "(?i)^.*_LR(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
								RL_RGX = "(?i)^.*_RL(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
								LRRL_RGX = "(?i)_((RL)|(LR))(($)|(_.*$))", 
								LRRLONLY_RGX = "(?i)_((LR)|(RL))_", 
								TFMRINO_RGX = "(?i)[0-9]_((LR)|(RL)).*$", 
								MAINSCAN_RGX = "(?i)((T[12]w)|(tfMRI)|(rfMRI)|(dMRI))",
								STRUCTMAIN_RGX = "(?i)((T[12]w))",
								MAINSCANDESC_RGX = "(?i)^((T[12]w)|(tfMRI)|(rfMRI)|(DWI)).*$",
								STRUCTSCANDESC_RGX = "(?i)^((T[12]w)).*$",
								OTHER_RGX = "(?i)^.*MPR.*Collection.*$",
								SESSION_LABEL_EXT = "_MR"
								;
	
	public static final String[] validQualityRatings = {"undetermined","usable","unusable","excellent","good","fair","poor"};
	public static final String[] structuralQualityRatings = {"excellent","good","fair","poor"};
	
	public final List<String> errorList = Lists.newArrayList();
	public final List<String> warningList = Lists.newArrayList();
	final List<XnatMrsessiondata> subjectSessions = new ArrayList<>();
	private boolean errorOverride;
	private static final Logger _logger = LoggerFactory.getLogger(ReleaseRules_HCP_Phase2.class);
	
	@Override
	public List<String> applyRulesToScans(
			List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions,
			Map<String,String> params, boolean errorOverride)
			throws ReleaseRulesException {
		// NOTE:  Currently HCP_Phase2 does not use the selectionCriteria parameter.  All sessions are combined into a single 3T session.
		return applyRulesToScans(subjectScans, subjectSessions, errorOverride);
	}

	//@Override
	//public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta,
	//		SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap,
	//		String selectionCriteria, HcpToolboxdata tboxexpt, NtScores ntexpt,
	//		HcpvisitHcpvisitdata visitexpt, boolean errorOverride)
	//		throws ReleaseRulesException {
	//	// NOTE:  Currently HCP_Phase2 does not use the selectionCriteria parameter.  All sessions are combined into a single 3T session.
	//	applyRulesToSubjectMetaData(subjMeta,scanMap,tboxexpt,ntexpt,visitexpt,errorOverride);
	//	
	//}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(List<XnatMrsessiondata> projectExpts,
			Map<String,String> params, UserI user) throws ReleaseRulesException {
		// NOTE:  Currently HCP_Phase2 does not use the selectionCriteria parameter.  All sessions are combined into a single 3T session.
		return projectExpts;
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + SESSION_LABEL_EXT;
	}
	
	@Override
	public boolean requireScanUidAnonymization() {
		return true;
	}
	
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		// NOTE:  Order of these calls is VERY important
		this.errorOverride=errorOverride;
		nullPriorValues(subjectScans);
		setCountScanAndSession(subjectScans);
		setTargetForReleaseAndScanID(subjectScans);
		setTypeDescriptionAndOthers(subjectScans);
		setGroupValues(subjectScans);
		updateT1T2InclusionAndDesc(subjectScans);
		updaterfMRIDescAndFlagError(subjectScans);
		useLaterBiasGroupIfMissing(subjectScans);
		excludeUnneededBiasScans(subjectScans);
		rearrangeDWIDesc(subjectScans);
		setScanOrderAndScanComplete(subjectScans);
		flagDwiError(subjectScans);
		flagTaskError(subjectScans);
		hardCodedValues(subjectScans);
		fixBackslashProblem(subjectScans);
		if (!(errorList.isEmpty() || (errorOverride && 
				_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
			throw new ReleaseRulesException(errorList);
		}
		return warningList;

	}
	
	private List<String> getErrorOrWarningList() {
		return (errorOverride) ? warningList : errorList;
	}

	private void fixBackslashProblem(
			List<XnatMrscandata> subjectScans) {
		for (XnatMrscandata scan : subjectScans) {
			// Replace multiple backslash instances with a single backslash
			if (scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains("\\\\")) {
				String imageType = scan.getParameters_imagetype().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_imagetype(imageType);
			}
			if (scan.getParameters_seqvariant()!=null && scan.getParameters_seqvariant().contains("\\\\")) {
				String seqVariant = scan.getParameters_seqvariant().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_seqvariant(seqVariant);
			}
			if (scan.getParameters_scansequence()!=null && scan.getParameters_scansequence().contains("\\\\")) {
				String scanSequence = scan.getParameters_scansequence().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scansequence(scanSequence);
			}
			if (scan.getParameters_scanoptions()!=null && scan.getParameters_scanoptions().contains("\\\\")) {
				String scanOptions = scan.getParameters_scanoptions().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scanoptions(scanOptions);
			}
		}
	}

	private void nullPriorValues(List<XnatMrscandata> subjectScans) {
		errorList.clear();
		warningList.clear();
		for (XnatMrscandata scan : subjectScans) {
			scan.setSubjectsessionnum(null);
			scan.setDatarelease(null);
			scan.setDbsession(null);
			scan.setDbid(null);
			scan.setDbtype(null);
			scan.setDbdesc(null);
			scan.setReleasecountscan(null);
			scan.setReleasecountscanoverride(null);
			scan.setTargetforrelease(null);
			// IMPORTANT:  Do not null out releaseOverride.  It is set by users, not by this class to force publish/non-publish.
			scan.setParameters_protocolphaseencodingdir(null);
			scan.setParameters_shimgroup(null);
			scan.setParameters_sefieldmapgroup(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_gefieldmapgroup(null);
			scan.setParameters_perotation(null);
			scan.setParameters_peswap(null);
			scan.setParameters_pedirection(null);
			scan.setParameters_readoutdirection(null);
			scan.setParameters_eprimescriptnum(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_scanorder(null);
			scan.setScancomplete(null);
			scan.setViewscan(null);
		}
	}

	private void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		ArrayList<Object> dayList = new ArrayList<Object>();
		HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (XnatMrscandata scan : subjectScans) {
			XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				_logger.error("ERROR: Returned session for scan should not be null!!!!");
				continue;
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getType().matches(T1WMEG_RGX) || scan.getSeriesDescription().matches(T1WMEG_RGX) || 
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			if (isStructuralScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorOrWarningList().add("RULESERROR:  Structural scan quality rating is invalid or scan has not been rated (SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorOrWarningList().add("RULESERROR:  Scan quality rating is invalid (SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			scan.setReleasecountscan(true);
		}
	}

	private void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
				scan.setTargetforrelease(false);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					scan.getQuality().equalsIgnoreCase("poor") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					(scan.getQuality().equalsIgnoreCase("fair") && scan.getType().matches(STRUCT_RGX)) ||
					// 4)  COUNTSCAN - Exclude GE FieldMaps except for structural sessions
					(scan.getSeriesDescription().matches(FIELDMAP_RGX) && notStructuralSession(getScanSession(scan, subjectSessions)))
				) {
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		if (!hasValidT1AndT2(subjectScans)) {
			for (XnatMrscandata scan : subjectScans) {
				scan.setTargetforrelease(false);
			}
		}
	}

	private boolean hasValidT1AndT2(List<XnatMrscandata> subjectScans) {
		boolean hasT1 = false;
		boolean hasT2 = false;
		for (XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan)) {
				if (scan.getType().matches(T1_RGX)) {
					hasT1 = true;
				} else if (scan.getType().matches(T2_RGX)) {
					hasT2 = true;
				}
				if (hasT1 && hasT2) {
					return true;
				}
			}
		}
		return false;
	}

	private void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Currently no changes for scan type
			scan.setDbtype(scan.getType());
			// SpinEchoFieldmaps
			String currSeriesDesc = scan.getSeriesDescription();
			// Initially set Dbdesc to current series description
			scan.setDbdesc(currSeriesDesc);
			if (currSeriesDesc.equalsIgnoreCase("BOLD_RL_SB_SE")) {
				scan.setDbdesc("SpinEchoFieldMap_RL");
				setPEFields(scan,currSeriesDesc);
			} else if (currSeriesDesc.equalsIgnoreCase("BOLD_LR_SB_SE")) {
				scan.setDbdesc("SpinEchoFieldMap_LR");
				setPEFields(scan,currSeriesDesc);
			}
			if (scan.getType().matches(TFMRI_RGX)) {
				Matcher mtch =  Pattern.compile(TFMRINO_RGX).matcher(currSeriesDesc);
				final String tfmrino = mtch.find() ? mtch.group() : "";
				if (tfmrino.length()>1) {
					scan.setDbdesc(currSeriesDesc.replaceFirst(BOLDONLY_RGX, "tfMRI").replace(tfmrino,tfmrino.substring(1)));
				}
				setPEFields(scan,currSeriesDesc);
				if (tfmrino.length()>1) {
					try {
						scan.setParameters_eprimescriptnum(Integer.parseInt(tfmrino.replaceFirst("_.*$","")));
					} catch (NumberFormatException e) {	
						// Do nothing for now.
					}
				}
			}
			if (scan.getType().matches(RFMRI_RGX)) {
				scan.setDbdesc(currSeriesDesc.replaceFirst(BOLDONLY_RGX, "rfMRI"));
				setPEFields(scan,currSeriesDesc);
			}
			if (scan.getType().matches(DMRI_RGX)) {
				setPEFields(scan,currSeriesDesc);
			}
			scan.setViewscan(!scan.getType().matches("(" + ANYFIELDMAP_RGX + ")|(" + BIAS_RGX + ")|(" + SBREF_RGX + ")"));
		}
	}

	private void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int sessionRestCount = 0;
		int countOfRestSessions = 0;
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						sessionRestCount=1;
						countOfRestSessions++;
						if (countOfRestSessions>2) {
							getErrorOrWarningList().add("RULESERROR:  More than two sessions contain usable Resting State scans");
						}
					} else {
						sessionRestCount++;
						if (sessionRestCount>2) {
							final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
							getErrorOrWarningList().add("RULESERROR:  Session contains more than two usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
				}
				inRest=true;
				// Update series description based on restCount
				scan.setDbdesc(currDbdesc.replaceFirst("[0-9]+",Integer.toString(restCount)));
			} else if (!currDbdesc.matches(RFMRI_RGX) && (scan.getViewscan()!=null && scan.getViewscan()) && inRest) {
				inRest=false;
				restCount++;
			}
		}
	}
	
	private void updateT1T2InclusionAndDesc(List<XnatMrscandata> subjectScans) {
		// Target T1, T2 from session that has both
		ArrayList<Integer> t1List = new ArrayList<Integer>();
		ArrayList<Integer> t2List = new ArrayList<Integer>();
		ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (XnatMrscandata scan : subjectScans) {
			String currSeriesDesc = scan.getSeriesDescription();
			if  (currSeriesDesc.matches(T1MPR_RGX) && isTargetOrOverrideStruc(scan)) {
				Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if  (currSeriesDesc.matches(T2SPC_RGX) && isTargetOrOverrideStruc(scan)) {
				Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getErrorOrWarningList().add("RULESERROR:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (XnatMrscandata scan : subjectScans) {
				String currSeriesDesc = scan.getSeriesDescription();
				if ((currSeriesDesc.matches(T1MPR_RGX) || currSeriesDesc.matches(T2SPC_RGX)) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
					}
				}
			}
		}
		// Now, keep based on shim values
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		char shimGroup = Character.MIN_VALUE;
		for (XnatMrscandata scan : subjectScans) {
			String currSeriesDesc = scan.getSeriesDescription();
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (currSeriesDesc.matches(T1MPR_RGX) && isTargetOrOverrideStruc(scan)) {
				if (t1Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t1Count++;
				}
				if (t1Count!=0) {
					if (shimGroup == Character.MIN_VALUE) {
						shimGroup = getShimGroup(scan);
					}
					if (t1Count>2 || shimGroup!=getShimGroup(scan)) {
						t1Count--;
						setNoRelease(scan);
					} else {
						String part1 = "T1w_MPR";
						String part2 = currSeriesDesc.length()>part1.length() ? String.valueOf(t1Count) + currSeriesDesc.substring(part1.length()+1) : String.valueOf(t1Count);
						scan.setDbdesc(part1+part2);
					    setRDFields(scan);
					}
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && isTargetOrOverrideStruc(scan)) {
				if (t2Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t2Count++;
				}
				if (t2Count!=0) {
					if (shimGroup == Character.MIN_VALUE) {
						shimGroup = getShimGroup(scan);
					}
					if (t2Count>2 || shimGroup!=getShimGroup(scan)) {
						t2Count--;
						setNoRelease(scan);
					} else {
						String part1 = "T2w_SPC";
						String part2 = currSeriesDesc.length()>part1.length() ? String.valueOf(t2Count) + currSeriesDesc.substring(part1.length()+1) : String.valueOf(t2Count);
						scan.setDbdesc(part1+part2);
					    setRDFields(scan);
					}
				}
			}
		}
		int t1CountMax=0;
		int t2CountMax=0;
		t1Count=0;
		t2Count=0;
		shimGroup = Character.MIN_VALUE;
		// Initial pass through to find max number so these don't wind up reverse numbered (bugfix).  There's got to be a better way to do this,
		// but this is a quick fix.
		for (XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			String currSeriesDesc = scan.getSeriesDescription();
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (currSeriesDesc.matches(T1MPR_RGX) && isTargetOrOverrideStruc(scan)) {
				t1CountMax++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				if (t1CountMax>2 || shimGroup!=getShimGroup(scan)) {
					t1CountMax--;
				} else {
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && isTargetOrOverrideStruc(scan)) {
				t2CountMax++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				if (t2CountMax>2 || shimGroup!=getShimGroup(scan)) {
					t2CountMax--;
				} else {
				}
			}
		}
		for (XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			String currSeriesDesc = scan.getSeriesDescription();
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (currSeriesDesc.matches(T1MPR_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				if (t1Count>2 || shimGroup!=getShimGroup(scan)) {
					t1Count--;
					setNoRelease(scan);
				} else {
					String part1 = "T1w_MPR";
					String part2 = currSeriesDesc.length()>part1.length() ? String.valueOf(t1CountMax+1-t1Count) + currSeriesDesc.substring(part1.length()+1) : String.valueOf(t1CountMax+1-t1Count);
					scan.setDbdesc(part1+part2);
				    setRDFields(scan);
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				if (t2Count>2 || shimGroup!=getShimGroup(scan)) {
					t2Count--;
					setNoRelease(scan);
				} else {
					String part1 = "T2w_SPC";
					String part2 = currSeriesDesc.length()>part1.length() ? String.valueOf(t2CountMax+1-t2Count) + currSeriesDesc.substring(part1.length()+1) : String.valueOf(t2CountMax+1-t2Count);
					scan.setDbdesc(part1+part2);
				    setRDFields(scan);
				}
			}
		}
	}		
	
	
	private boolean scanShimMatchesFieldMaps(List<XnatMrscandata> subjectScans, XnatMrscandata comparescan) {
		for (XnatMrscandata scan : getReversedScanList(subjectScans)) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) &&
						scan.getParameters_gefieldmapgroup().equals(comparescan.getParameters_gefieldmapgroup())) {
				if (scan.getParameters_shimgroup().equals(comparescan.getParameters_shimgroup())) {
					return true;
				} else {
					return false;
				}
			} 
		}
		return false;
	}

	private boolean isTargetOrOverrideAll(XnatMrscandata scan) {
		if (scan.getSeriesDescription()!=null && scan.getSeriesDescription().matches(STRUCTSCANDESC_RGX)) {
			if (isTargetOrOverrideStruc(scan)) {
				return true;
			}
		} else if (scan.getTargetforrelease()!=null) {
			return scan.getTargetforrelease();
		}
		return false;
	}
	
	private boolean isTargetOrOverrideStruc(XnatMrscandata scan) {
		if ( (scan.getTargetforrelease()!=null && scan.getTargetforrelease() && (scan.getReleaseoverride()==null || !(scan.getReleaseoverride()<0))) ||
				(scan.getTargetforrelease()!=null && !scan.getTargetforrelease() && scan.getReleaseoverride()!=null && (scan.getReleaseoverride()>0))
			) {
			return true;
		}
		return false;
	}

	private void setNoRelease(XnatMrscandata scan) {
		// Do this instead of just setting targetforrelease to false to clear out other fields that
		// may have been set (Note that a few aren't included here that should remain)
		scan.setTargetforrelease(false);
		scan.setSubjectsessionnum(null);
		scan.setDatarelease(null);
		scan.setDbsession(null);
		scan.setDbid(null);
		scan.setDbtype(null);
		scan.setDbdesc(null);
		scan.setParameters_protocolphaseencodingdir(null);
		scan.setParameters_sefieldmapgroup(null);
		scan.setParameters_gefieldmapgroup(null);
		scan.setParameters_perotation(null);
		scan.setParameters_peswap(null);
		scan.setParameters_pedirection(null);
		scan.setParameters_readoutdirection(null);
		scan.setParameters_eprimescriptnum(null);
		scan.setParameters_biasgroup(null);
	}

	private char getShimGroup(XnatMrscandata scan) {
		try {
			if (scan.getParameters_shimgroup() == null || scan.getParameters_shimgroup().length()<1) {
				return '0';
			}
			return scan.getParameters_shimgroup().charAt(0);
		} catch (Exception e) {
			return '0';
		}
	}
	
	private void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		int geFieldmapGroup = 0;
		int geSession = 0;
		boolean inGEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (XnatMrscandata scan : subjectScans) {
			
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			
			// BIAS FIELD GROUP
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && !inBiasGroup) {
				inBiasGroup = true;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				biasFieldGroup++;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
				inBiasGroup = false;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (scan.getReleasecountscan() && inBiasGroup && scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && !scan.getType().matches(BIAS_RGX) &&
						isTargetOrOverrideAll(scan) && scan.getSubjectsessionnum().intValue() == seSession) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup && scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			
			// GE FIELDMAP GROUP (STEP 1 - SET VALUES FOR FIELDMAP SCANS)
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inGEFieldmapGroup) {
				inGEFieldmapGroup = true;
				geFieldmapGroup++;
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_gefieldmapgroup(geFieldmapGroup);
			} else if (!scan.getSeriesDescription().matches(GEFIELDMAP_RGX)) {
				inGEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inGEFieldmapGroup && scan.getSubjectsessionnum().intValue() == geSession) {
				scan.setParameters_gefieldmapgroup(geFieldmapGroup);
			}
			
		}
		
		// GE FieldMap group (STEP 2 - ASSIGN GROUP VALUE TO T1/T2 SCANS (uses reverse sort data)
		geFieldmapGroup = 0;
		String shimGroup = null;
		for (XnatMrscandata scan : getReversedScanList(subjectScans)) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan)) {
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				geFieldmapGroup = scan.getParameters_gefieldmapgroup();
				shimGroup = scan.getParameters_shimgroup();
			} else if (scan.getType().matches(STRUCT_RGX) && (scan.getParameters_gefieldmapgroup()==null || scan.getParameters_gefieldmapgroup()<=0) &&
					isTargetOrOverrideAll(scan) && scan.getSubjectsessionnum().intValue() == geSession) {
				if (geFieldmapGroup>0) {
					scan.setParameters_gefieldmapgroup(geFieldmapGroup);
				}
			}
		}
		
		// GE FieldMap group (STEP 3 - ASSIGN GROUP VALUE TO LATER T1/T2 SCANS if shim values match (DB-1841))
		geFieldmapGroup = 0;
		shimGroup = null;
		for (XnatMrscandata scan : subjectScans) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan)) {
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				geFieldmapGroup = scan.getParameters_gefieldmapgroup();
				shimGroup = scan.getParameters_shimgroup();
			} else if (scan.getType().matches(STRUCT_RGX) && (scan.getParameters_gefieldmapgroup()==null || scan.getParameters_gefieldmapgroup()<=0) &&
					isTargetOrOverrideAll(scan) && scan.getSubjectsessionnum().intValue() == geSession) {
				if (geFieldmapGroup>0 && shimGroup != null && shimGroup.equals(scan.getParameters_shimgroup())) {
					scan.setParameters_gefieldmapgroup(geFieldmapGroup);
				}
			}
		}
		
		// GE FieldMap group (STEP 4 - ASSIGN GROUP VALUE TO T1/T2 SCANS (uses reverse sort data) - ORIGINAL ASSIGNMENT CODE - DOESN'T LOOK AT SHIM GROUPS
		// This step can probably be removed, as it hopefully won't match and set anything, but it is being left in so session building behavior 
		// isn't unexpectedly changed.  Prior iterations will select optimal fieldmap based on shim group.  This will select when there is no
		// matching shim group, which would have happened originally.
		geFieldmapGroup = 0;
		for (XnatMrscandata scan : getReversedScanList(subjectScans)) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan)) {
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				geFieldmapGroup = scan.getParameters_gefieldmapgroup();
			} else if (scan.getType().matches(STRUCT_RGX) && isTargetOrOverrideAll(scan) && scan.getSubjectsessionnum().intValue() == geSession) {
				if (geFieldmapGroup>0) {
					scan.setParameters_gefieldmapgroup(geFieldmapGroup);
				}
			}
		}
		
	}		

	private void rearrangeDWIDesc(List<XnatMrscandata> subjectScans) {
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || !currDbdesc.matches(DWI_RGX)) {
				continue;
			}
			if (currDbdesc.indexOf("_LR_")>0) {
				currDbdesc = currDbdesc.replaceFirst("_LR_","_");
				currDbdesc = currDbdesc.matches(SBREF_RGX) ? currDbdesc.replaceFirst("(?i)_SBRef","_LR_SBRef") : currDbdesc + "_LR";
			}
			if (currDbdesc.indexOf("_RL_")>0) {
				currDbdesc = currDbdesc.replaceFirst("_RL_","_");
				currDbdesc = currDbdesc.matches(SBREF_RGX) ? currDbdesc.replaceFirst("(?i)_SBRef","_RL_SBRef") : currDbdesc + "_RL";
			}
			scan.setDbdesc(currDbdesc);
		}
	}
	
	private void useLaterBiasGroupIfMissing(List<XnatMrscandata> subjectScans) {
		// This method is to fix an issue first discovered with session 429040_3T where the initial set of bias scans
		// was unusable and a set was captured after the main scans.  These, having occurred in the same session,
		// should be usable, so when bias group is missing for a main scan, we'll use the later bias group in the
		// same session
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan)	) {
				Integer currBias = scan.getParameters_biasgroup();
				if (currBias != null ) {
					continue;
				}
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && (compareDbdesc.matches(BIAS_RGX) || compareDbdesc.equalsIgnoreCase("AFI")) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (compareBias!=null) {
							scan.setParameters_biasgroup(compareBias);
							break;
						}
					}
				}
			}
		}
	}
	
	private void excludeUnneededBiasScans(List<XnatMrscandata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if ((currDbdesc.matches(BIAS_RGX) || currDbdesc.equalsIgnoreCase("AFI")) && isTargetOrOverrideAll(scan)) {
				Integer currBias = scan.getParameters_biasgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
			if (currDbdesc.matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan)) {
				Integer currGE = scan.getParameters_gefieldmapgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareGE = scan2.getParameters_gefieldmapgroup();
						if (currGE!=null && compareGE!=null && compareGE.equals(currGE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
			if (currDbdesc.matches(SPINECHO_RGX) && isTargetOrOverrideAll(scan)) {
				Integer currSE = scan.getParameters_sefieldmapgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareSE = scan2.getParameters_sefieldmapgroup();
						if (currSE!=null && compareSE!=null && compareSE.equals(currSE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}

	private void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (XnatMrscandata scan : subjectScans) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DWI_RGX)) ? "DWI" : currDbdesc.replaceFirst(LRRL_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                String scanComplete = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=1200) ? "Y" : "N";
               		scanPct = (double)frames/1200;
                } else if (currDbdesc.matches(DWI_RGX)) { 
                	String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		int dirNum = Integer.parseInt(dirVal);
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_GAMBLING")) {
                		scanComplete = (frames>=253) ? "Y" : "N";
                		scanPct = (double)frames/253;
                	} else if (currDbdesc.contains("_MOTOR")) {
                		scanComplete = (frames>=284) ? "Y" : "N";
                		scanPct = (double)frames/284;
                	} else if (currDbdesc.contains("_LANGUAGE")) {
                		scanComplete = (frames>=316) ? "Y" : "N";
                		scanPct = (double)frames/316;
                	} else if (currDbdesc.contains("_SOCIAL")) {
                		scanComplete = (frames>=274) ? "Y" : "N";
                		scanPct = (double)frames/274;
                	} else if (currDbdesc.contains("_RELATIONAL")) {
                		scanComplete = (frames>=232) ? "Y" : "N";
                		scanPct = (double)frames/232;
                	} else if (currDbdesc.contains("_EMOTION")) {
                		scanComplete = (frames>=176) ? "Y" : "N";
                		scanPct = (double)frames/176;
                	} else if (currDbdesc.contains("_WM")) {
                		scanComplete = (frames>=405) ? "Y" : "N";
                		scanPct = (double)frames/405;
                	}
                }
                // Per Greg, tfMRI should have at least 75% of frames to have been considered usable.
                if (scanPct<.75) {
                	getErrorOrWarningList().add("RULESERROR:  tfMRI, rfMRI and dMRI scans should have at least 75% of frames to have been marked usable (SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                }
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        for (XnatMrscandata scan : subjectScans) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	Double currScanPct = pctMap.get(currDbdesc);
	            String thatDbdesc = (currDbdesc.matches(LR_RGX)) ? currDbdesc.replace("_LR","_RL") : currDbdesc.replace("_RL","_LR");  
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            }
            }
        }
	}
	
	private void setRDFields(XnatMrscandata scan) {
		try {
			Float ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			if (scan.getParameters_orientation().equals("Sag") && scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
					ipe_rotation>=-0.53 && ipe_rotation<=0.53) {
				scan.setParameters_readoutdirection("+z");
				return;
			}
		} catch (Exception e) {
			// Do nothing
		}
		getErrorOrWarningList().add("RULESERROR:  Values do not match expected for setting READOUT DIRECTION (SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
	}
	
	
	private void setPEFields(XnatMrscandata scan,String seriesDesc) {
		try {
			final Float ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			final Integer ipe_dirpos = Integer.parseInt(scan.getParameters_inplanephaseencoding_directionpositive());
			if (scan.getParameters_orientation().equals("Tra")) {
				if (scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
						ipe_rotation>=1.05 && ipe_rotation<=2.10) {
					if (ipe_dirpos==0)	{
						scan.setParameters_pedirection("-x");
						checkPESeriesDesc(scan);
						return;
					} else if (ipe_dirpos==1) {
						scan.setParameters_pedirection("+x");
						checkPESeriesDesc(scan);
						return;
					}
				} else if (scan.getParameters_inplanephaseencoding_direction().equals("COL") &&
						ipe_rotation>=-0.52 && ipe_rotation<=0.52) {
					if (ipe_dirpos==0)	{
						scan.setParameters_pedirection("+y");
						checkPESeriesDesc(scan);
						return;
					} else if (ipe_dirpos==1) {
						scan.setParameters_pedirection("-y");
						checkPESeriesDesc(scan);
						return;
					}
				}
			}
		} catch (Exception e) {
			// Do nothing
		}
		getErrorOrWarningList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SCAN=" + scan.getId() + ", DESC=" + seriesDesc + ")");
	}

	private void checkPESeriesDesc(XnatMrscandata scan) {
		if (!(scan.getParameters_pedirection().equals("+x") && scan.getSeriesDescription().contains("_RL") ||
			scan.getParameters_pedirection().equals("-x") && scan.getSeriesDescription().contains("_LR") ||
			scan.getParameters_pedirection().equals("+y") && scan.getSeriesDescription().contains("_PA") ||
			scan.getParameters_pedirection().equals("-y") && scan.getSeriesDescription().contains("_AP"))) {
			getErrorOrWarningList().add("RULESERROR:  Series Description does not reflect PHASE ENCODING DIRECTION (SCAN=" + scan.getId() +
					", DESC=" + scan.getSeriesDescription() + ",PE=" + scan.getParameters_pedirection() + ")");
		}
	}

	public boolean isStructuralScan(XnatImagescandataI scan) {
		if (scan.getType().matches(STRUCT_RGX)) {
			return true;
		} 
		return false;
	}

	private boolean notStructuralSession(XnatImagesessiondata session) {
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			if (scan.getType().matches(STRUCT_RGX)) {
				return false;
			}
		}
		return true;
	}
	
	private void hardCodedValues(List<XnatMrscandata> subjectScans) {
		for (XnatMrscandata scan : subjectScans) {
			XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null || currSess.getLabel() == null || scan.getSeriesDescription() == null) {
				continue;
			}
			if (currSess.getLabel().equalsIgnoreCase("119833_fncb") && scan.getSeriesDescription().contains("RELATIONAL1") &&
					scan.getParameters_eprimescriptnum()!=null && scan.getParameters_eprimescriptnum()==1) {
				// This one should have been RELATIONAL3
				scan.setParameters_eprimescriptnum(3);
			}
		}
	}

	private void flagDwiError(List<XnatMrscandata> subjectScans) {
		// Flag error if multiple bias groups in TO BE RELEASED DWI scans 
		Integer prevBias = null; 
		for (XnatMrscandata scan : subjectScans) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(DWI_RGX)) { 
            	Integer thisBias = scan.getParameters_biasgroup();
            	if (prevBias!=null && thisBias!=null && !thisBias.equals(prevBias)) {
           			getErrorOrWarningList().add("RULESERROR:  DWI contains multiple bias groups in scans to be released");
            	} else if (prevBias==null && thisBias!=null) {
            		prevBias = thisBias;
            	}
            }
		}
	}

	private void flagTaskError(List<XnatMrscandata> subjectScans) {
        HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        HashMap<String,Integer> sessionMap = new HashMap<String,Integer>();
        HashMap<String,String> dirMap = new HashMap<String,String>();
		for (XnatMrscandata scan : subjectScans) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(TFMRI_RGX)) { 
               	String part1=currDbdesc.replaceFirst(LRRL_RGX,"");
               	// Too many scans to be released
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                	if (mapval>2) {
               			getErrorOrWarningList().add("RULESERROR:  Session contains more than two task scans targeted for release (" + part1 + ")");
                	}
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
               	// Multiple sessions containing scans
                if (sessionMap.containsKey(part1) && scan.getSubjectsessionnum()!=null) {
                	if (!sessionMap.get(part1).equals(scan.getSubjectsessionnum())) {
                		getErrorOrWarningList().add("RULESERROR:  Multiple sessions contain scans for the same task (" + part1 + ")");
                	}
                } else {
                	if (scan.getSubjectsessionnum()!=null) {
                		sessionMap.put(part1, scan.getSubjectsessionnum());
                	}
                }
               	// Multiple sessions containing same direction
                String direction = (currDbdesc.matches(LR_RGX)) ? "LR" : "RL";
                if (dirMap.containsKey(part1) && direction!=null) {
                	if (dirMap.get(part1).equals(direction)) {
                		getErrorOrWarningList().add("RULESERROR:  Multiple sessions for same phase encoding direction for task (" + part1 + ")");
                	}
                } else {
                	if (direction!=null) {
                		dirMap.put(part1,direction);
                	}
                }
            }
		}
	}
	
	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta, List<XnatMrscandata> scanList,
				HcpToolboxdata tboxexpt, NtScores ntexpt, HcpvisitHcpvisitdata visitexpt, boolean errorOverride) throws ReleaseRulesException {
		
		// Initialize values
		subjMeta.setCompleteness_imaging_fullprotocol(false);
		subjMeta.setCompleteness_imaging_t1count(0);
		subjMeta.setCompleteness_imaging_t2count(0);
		subjMeta.setCompleteness_imaging_rfmriAllscans(false);
		subjMeta.setCompleteness_imaging_rfmriComplete(false);
		subjMeta.setCompleteness_imaging_rfmriCount(0);
		subjMeta.setCompleteness_imaging_tfmriFullAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriFullComplete(false);
		subjMeta.setCompleteness_imaging_tfmriWmAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriWmComplete(false);
		subjMeta.setCompleteness_imaging_tfmriGambAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriGambComplete(false);
		subjMeta.setCompleteness_imaging_tfmriMotorAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriMotorComplete(false);
		subjMeta.setCompleteness_imaging_tfmriLangAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriLangComplete(false);
		subjMeta.setCompleteness_imaging_tfmriSocAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriSocComplete(false);
		subjMeta.setCompleteness_imaging_tfmriRelAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriRelComplete(false);
		subjMeta.setCompleteness_imaging_tfmriEmotAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriEmotComplete(false);
		subjMeta.setCompleteness_imaging_dmriAllscans(false);
		subjMeta.setCompleteness_imaging_dmriCount(0);
		subjMeta.setCompleteness_imaging_dmriPctcomplete(0.0);
		subjMeta.setCompleteness_imaging_dmriComplete(false);
		
		subjMeta.setCompleteness_nontoolbox_fullbattery(false);
		subjMeta.setCompleteness_nontoolbox_hcppnp(false);
		subjMeta.setCompleteness_nontoolbox_ddisc(false);
		subjMeta.setCompleteness_nontoolbox_neo(false);
		subjMeta.setCompleteness_nontoolbox_spcptnl(false);
		subjMeta.setCompleteness_nontoolbox_cpw(false);
		subjMeta.setCompleteness_nontoolbox_pmat24a(false);
		subjMeta.setCompleteness_nontoolbox_vsplot24(false);
		subjMeta.setCompleteness_nontoolbox_er40(false);
		subjMeta.setCompleteness_nontoolbox_asrsyndrome(false);
		subjMeta.setCompleteness_nontoolbox_asrdsm(false);
		
		subjMeta.setCompleteness_toolbox_fullbattery(false);
		subjMeta.setCompleteness_toolbox_cognition(false);
		subjMeta.setCompleteness_toolbox_emotion(false);
		subjMeta.setCompleteness_toolbox_motor(false);
		subjMeta.setCompleteness_toolbox_sensory(false);
		
		subjMeta.setCompleteness_alertness_fullbattery(false);
		subjMeta.setCompleteness_alertness_mmse(false);
		subjMeta.setCompleteness_alertness_psqi(false);
		
		// In case session not up-to-date, apply release rules to scans.  The meta-data rules rely on
		// fields set during processing for the combined session, as applied in the applyRulesToScans method.
		applyRulesToScans(scanList, subjectSessions, errorOverride);
		
		setImagingCompleteness(subjMeta,scanList);
		setToolboxCompleteness(subjMeta,tboxexpt);
		setNontoolboxCompleteness(subjMeta,ntexpt);
		setAlertnessCompleteness(subjMeta,visitexpt);
		
	}
	
	private void setImagingCompleteness(HcpSubjectmetadata subjMeta,List<XnatMrscandata> subjectScans) {
		
		if (subjectScans == null) {
			return;
		}
		
		int t1count=0, t2count=0;
		int rfmri_lr_count=0, rfmri_rl_count=0;
		int dmri_count=0;
		double dir95_paircomp=0, dir96_paircomp=0, dir97_paircomp=0;
		boolean rfmri_complete = true; 
		boolean sameDwiBiasGroup = true; 
		Integer prevBiasGroup = null; 
		
		// Here, we're assuming standardized series descriptions (which should be the case) and we're depending on the "scanComplete" 
		// field we're setting when building the combined session.
		ArrayList<String> allscansDesc = new ArrayList<String>();
		ArrayList<String> completeDesc = new ArrayList<String>();
		for (XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan) && scan.getDbdesc()!=null) {
				String currDbdesc = scan.getDbdesc();
               	allscansDesc.add(currDbdesc);
                if ((currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) && !currDbdesc.matches(SBREF_RGX)) {
                	// Only add to completeDesc if scanComplete for TFMRI, RFMRI 
                	if (scan.getScancomplete()!=null && scan.getScancomplete().equalsIgnoreCase("Y") &&
                			scan.getParameters_biasgroup()!=null && scan.getParameters_biasgroup()>0 &&
                			scan.getParameters_sefieldmapgroup()!=null && scan.getParameters_sefieldmapgroup()>0) {
                		completeDesc.add(currDbdesc);
                	}
                } else if ((currDbdesc.matches(DWI_RGX) && !currDbdesc.matches(SBREF_RGX))) {
                	if (scan.getScancomplete()!=null && scan.getScancomplete().equalsIgnoreCase("Y") &&
                			scan.getParameters_biasgroup()!=null && scan.getParameters_biasgroup()>0) {
                		completeDesc.add(currDbdesc);
                	}
                } else {
                	completeDesc.add(currDbdesc);
                } 
			}
		}
		for (XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan)) {
				// T1, T2 counts
				if (scan.getType().matches(T1_RGX)) {
					t1count++;
					continue;
				}
				if (scan.getType().matches(T2_RGX)) {
					t2count++;
					continue;
				}
				String currDbdesc = scan.getDbdesc();
                String direction = (currDbdesc!=null && currDbdesc.matches(LR_RGX)) ? "LR" : "RL";
				// rfMRI Counts
                if (currDbdesc != null && currDbdesc.matches(RFMRI_RGX) && !currDbdesc.matches(SBREF_RGX)) {
                	if (direction.equals("LR")) {
                		rfmri_lr_count++;
                	} else if (direction.equals("RL")) {
                		rfmri_rl_count++;
                	}
                	rfmri_complete = (rfmri_complete && completeDesc.contains(currDbdesc) && completeDesc.contains(currDbdesc + "_SBRef")) ? true : false;
      			}
				// check that all dMRI sessions are from same bias group
                if (currDbdesc != null && currDbdesc.matches(DWI_RGX) && !currDbdesc.matches(SBREF_RGX)) {
                	dmri_count++;
                	if (currDbdesc.contains("95") && scan.getPctpaircomplete()!=null && dir95_paircomp<=0) {
                		dir95_paircomp=scan.getPctpaircomplete();
                	} if (currDbdesc.contains("96") && scan.getPctpaircomplete()!=null && dir96_paircomp<=0) {
                		dir96_paircomp=scan.getPctpaircomplete();
                	} if (currDbdesc.contains("97") && scan.getPctpaircomplete()!=null && dir97_paircomp<=0) {
                		dir97_paircomp=scan.getPctpaircomplete();
                	}
                	if (prevBiasGroup!=null) {
                		if (scan.getParameters_biasgroup()==null || !prevBiasGroup.equals(scan.getParameters_biasgroup())) {
                			sameDwiBiasGroup = false;
                		}
                	} else if (scan.getParameters_biasgroup()!=null) {
                		prevBiasGroup = scan.getParameters_biasgroup();
                	}
                }
			}
		}
		subjMeta.setCompleteness_imaging_t1count(t1count);
		subjMeta.setCompleteness_imaging_t2count(t2count);
		subjMeta.setCompleteness_imaging_rfmriAllscans((rfmri_lr_count>=2 && rfmri_rl_count>=2));
		subjMeta.setCompleteness_imaging_rfmriComplete((rfmri_complete && rfmri_lr_count>=2 && rfmri_rl_count>=2));
		
		subjMeta.setCompleteness_imaging_rfmriCount(rfmri_lr_count + rfmri_rl_count);
		subjMeta.setCompleteness_imaging_tfmriWmAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_WM_RL_SBRef","tfMRI_WM_RL","tfMRI_WM_LR_SBRef","tfMRI_WM_LR"})));
		subjMeta.setCompleteness_imaging_tfmriWmComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_WM_RL_SBRef","tfMRI_WM_RL","tfMRI_WM_LR_SBRef","tfMRI_WM_LR"})));
		subjMeta.setCompleteness_imaging_tfmriGambAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_GAMBLING_RL_SBRef","tfMRI_GAMBLING_RL","tfMRI_GAMBLING_LR_SBRef","tfMRI_GAMBLING_LR"})));
		subjMeta.setCompleteness_imaging_tfmriGambComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_GAMBLING_RL_SBRef","tfMRI_GAMBLING_RL","tfMRI_GAMBLING_LR_SBRef","tfMRI_GAMBLING_LR"})));
		subjMeta.setCompleteness_imaging_tfmriMotorAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_MOTOR_RL_SBRef","tfMRI_MOTOR_RL","tfMRI_MOTOR_LR_SBRef","tfMRI_MOTOR_LR"})));
		subjMeta.setCompleteness_imaging_tfmriMotorComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_MOTOR_RL_SBRef","tfMRI_MOTOR_RL","tfMRI_MOTOR_LR_SBRef","tfMRI_MOTOR_LR"})));
		subjMeta.setCompleteness_imaging_tfmriLangAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_LANGUAGE_RL_SBRef","tfMRI_LANGUAGE_RL","tfMRI_LANGUAGE_LR_SBRef","tfMRI_LANGUAGE_LR"})));
		subjMeta.setCompleteness_imaging_tfmriLangComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_LANGUAGE_RL_SBRef","tfMRI_LANGUAGE_RL","tfMRI_LANGUAGE_LR_SBRef","tfMRI_LANGUAGE_LR"})));
		subjMeta.setCompleteness_imaging_tfmriSocAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_SOCIAL_RL_SBRef","tfMRI_SOCIAL_RL","tfMRI_SOCIAL_LR_SBRef","tfMRI_SOCIAL_LR"})));
		subjMeta.setCompleteness_imaging_tfmriSocComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_SOCIAL_RL_SBRef","tfMRI_SOCIAL_RL","tfMRI_SOCIAL_LR_SBRef","tfMRI_SOCIAL_LR"})));
		subjMeta.setCompleteness_imaging_tfmriRelAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_RELATIONAL_RL_SBRef","tfMRI_RELATIONAL_RL","tfMRI_RELATIONAL_LR_SBRef","tfMRI_RELATIONAL_LR"})));
		subjMeta.setCompleteness_imaging_tfmriRelComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_RELATIONAL_RL_SBRef","tfMRI_RELATIONAL_RL","tfMRI_RELATIONAL_LR_SBRef","tfMRI_RELATIONAL_LR"})));
		subjMeta.setCompleteness_imaging_tfmriEmotAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_EMOTION_RL_SBRef","tfMRI_EMOTION_RL","tfMRI_EMOTION_LR_SBRef","tfMRI_EMOTION_LR"})));
		subjMeta.setCompleteness_imaging_tfmriEmotComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_EMOTION_RL_SBRef","tfMRI_EMOTION_RL","tfMRI_EMOTION_LR_SBRef","tfMRI_EMOTION_LR"})));
		subjMeta.setCompleteness_imaging_tfmriFullAllscans(subjMeta.getCompleteness_imaging_tfmriWmAllscans() && subjMeta.getCompleteness_imaging_tfmriGambAllscans() && subjMeta.getCompleteness_imaging_tfmriMotorAllscans() &&
													subjMeta.getCompleteness_imaging_tfmriLangAllscans() && subjMeta.getCompleteness_imaging_tfmriSocAllscans() &&
													subjMeta.getCompleteness_imaging_tfmriRelAllscans() && subjMeta.getCompleteness_imaging_tfmriEmotAllscans());
		subjMeta.setCompleteness_imaging_tfmriFullComplete(subjMeta.getCompleteness_imaging_tfmriWmComplete() && subjMeta.getCompleteness_imaging_tfmriGambComplete() && subjMeta.getCompleteness_imaging_tfmriMotorComplete() &&
													subjMeta.getCompleteness_imaging_tfmriLangComplete() && subjMeta.getCompleteness_imaging_tfmriSocComplete() &&
													subjMeta.getCompleteness_imaging_tfmriRelComplete() && subjMeta.getCompleteness_imaging_tfmriEmotComplete());
		subjMeta.setCompleteness_imaging_dmriAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"DWI_dir95_RL_SBRef","DWI_dir95_RL","DWI_dir95_LR_SBRef","DWI_dir95_LR",
																									"DWI_dir96_RL_SBRef","DWI_dir96_RL","DWI_dir96_LR_SBRef","DWI_dir96_LR",
																									"DWI_dir97_RL_SBRef","DWI_dir97_RL","DWI_dir97_LR_SBRef","DWI_dir97_LR"})));
		subjMeta.setCompleteness_imaging_dmriComplete(completeDesc.containsAll(Arrays.asList(new String[]{"DWI_dir95_RL_SBRef","DWI_dir95_RL","DWI_dir95_LR_SBRef","DWI_dir95_LR",
																									"DWI_dir96_RL_SBRef","DWI_dir96_RL","DWI_dir96_LR_SBRef","DWI_dir96_LR",
																									"DWI_dir97_RL_SBRef","DWI_dir97_RL","DWI_dir97_LR_SBRef","DWI_dir97_LR"})) && sameDwiBiasGroup);
		subjMeta.setCompleteness_imaging_fullprotocol(subjMeta.getCompleteness_imaging_t1count()>0 && subjMeta.getCompleteness_imaging_t2count()>0 &&
														subjMeta.getCompleteness_imaging_rfmriComplete() && subjMeta.getCompleteness_imaging_tfmriFullComplete() &&
														subjMeta.getCompleteness_imaging_dmriComplete());
		
		subjMeta.setCompleteness_imaging_dmriCount(dmri_count);
		subjMeta.setCompleteness_imaging_dmriPctcomplete(((dir95_paircomp*95)+(dir96_paircomp*96)+(dir97_paircomp*97))/(95+96+97));
	}

	private void setNontoolboxCompleteness(HcpSubjectmetadata subjMeta, NtScores ntexpt) {
		
		if (ntexpt == null) {
			return;
		}
		
		if (ntexpt.getHcppnp_marsFinal()!=null && ntexpt.getHcppnp_marsErrs()!=null && ntexpt.getHcppnp_marsLogScore()!=null) {
			subjMeta.setCompleteness_nontoolbox_hcppnp(true);
		}
		if (ntexpt.getDdisc_auc200()!=null && ntexpt.getDdisc_auc200()>=0 && ntexpt.getDdisc_auc40000()!=null && ntexpt.getDdisc_auc40000()>=0) {
			subjMeta.setCompleteness_nontoolbox_ddisc(true);
		}
		if (ntexpt.getNeo_neo()!=null && ntexpt.getNeo_neo()>0) {
			subjMeta.setCompleteness_nontoolbox_neo(true);
		}
		if (ntexpt.getSpcptnl_scptSen()!=null && ntexpt.getSpcptnl_scptSen()>=0 &&
		    ntexpt.getSpcptnl_scptSpec()!=null && ntexpt.getSpcptnl_scptSpec()>=0 &&
		    ntexpt.getSpcptnl_scptTprt()!=null && ntexpt.getSpcptnl_scptTprt()>=0 &&
		    ntexpt.getSpcptnl_scptLrnr()!=null && ntexpt.getSpcptnl_scptLrnr()>=0) {
			subjMeta.setCompleteness_nontoolbox_spcptnl(true);
		}
		if (ntexpt.getCpw_iwrdRtc()!=null && ntexpt.getCpw_iwrdRtc()>=0 &&
				ntexpt.getCpw_iwrdTot()!=null && ntexpt.getCpw_iwrdTot()>=0) {
			subjMeta.setCompleteness_nontoolbox_cpw(true);
		}
		if (ntexpt.getPmat24a_pmat24Acr()!=null && ntexpt.getPmat24a_pmat24Acr()>=0 &&
				ntexpt.getPmat24a_pmat24Asi()!=null && ntexpt.getPmat24a_pmat24Asi()>=0 &&
				ntexpt.getPmat24a_pmat24Artcr()!=null && ntexpt.getPmat24a_pmat24Artcr()>=0) {
			subjMeta.setCompleteness_nontoolbox_pmat24a(true);
		}
		if (ntexpt.getVsplot24_vsplotCrte()!=null && ntexpt.getVsplot24_vsplotCrte()>=0 &&
		    ntexpt.getVsplot24_vsplotOff()!=null && ntexpt.getVsplot24_vsplotOff()>=0 &&
		    ntexpt.getVsplot24_vsplotTc()!=null && ntexpt.getVsplot24_vsplotTc()>=0) {
			subjMeta.setCompleteness_nontoolbox_vsplot24(true);
		}
		if (ntexpt.getEr40_er40Cr()!=null && ntexpt.getEr40_er40Cr()>=0 &&
				ntexpt.getEr40_er40Crt()!=null && ntexpt.getEr40_er40Crt()>=0 &&
				ntexpt.getEr40_er40ang()!=null && ntexpt.getEr40_er40ang()>=0 &&
				ntexpt.getEr40_er40fear()!=null && ntexpt.getEr40_er40fear()>=0 &&
				ntexpt.getEr40_er40hap()!=null && ntexpt.getEr40_er40hap()>=0 &&
				ntexpt.getEr40_er40noe()!=null && ntexpt.getEr40_er40noe()>=0 &&
				ntexpt.getEr40_er40sad()!=null && ntexpt.getEr40_er40sad()>=0) {
			subjMeta.setCompleteness_nontoolbox_er40(true);
		}
		if (ntexpt.getAsr_asrsyndromescores_asrCmpTotalRaw()!=null && ntexpt.getAsr_asrsyndromescores_asrCmpTotalRaw()>=0 &&
				ntexpt.getAsr_asrsyndromescores_asrCmpTotalT()!=null && ntexpt.getAsr_asrsyndromescores_asrCmpTotalT()>0) {
			subjMeta.setCompleteness_nontoolbox_asrsyndrome(true);
		}
		if (ntexpt.getAsr_asrdsmscores_dsmDepRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmDepRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAnxRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAnxRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmSomRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmSomRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAvoidRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAvoidRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAdhRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAdhRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmInattRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmInattRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmHypRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmHypRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAsocRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAsocRaw()>=0) {
			subjMeta.setCompleteness_nontoolbox_asrdsm(true);
		}
		subjMeta.setCompleteness_nontoolbox_fullbattery(subjMeta.getCompleteness_nontoolbox_hcppnp() &&
														subjMeta.getCompleteness_nontoolbox_ddisc() &&
														subjMeta.getCompleteness_nontoolbox_neo() &&
														subjMeta.getCompleteness_nontoolbox_spcptnl() &&
														subjMeta.getCompleteness_nontoolbox_cpw() &&
														subjMeta.getCompleteness_nontoolbox_pmat24a() &&
														subjMeta.getCompleteness_nontoolbox_vsplot24() &&
														subjMeta.getCompleteness_nontoolbox_er40() &&
														subjMeta.getCompleteness_nontoolbox_asrsyndrome() &&
														subjMeta.getCompleteness_nontoolbox_asrdsm());
	}

	private void setToolboxCompleteness(HcpSubjectmetadata subjMeta,
			HcpToolboxdata tboxexpt) {
		
		if (tboxexpt == null) {
			return;
		}
		
		if (tboxexpt.getCogDimCardUscr()!=null && tboxexpt.getCogDimCardUscr()>=0 &&
		    tboxexpt.getCogDimCardAscr()!=null && tboxexpt.getCogDimCardAscr()>=0 &&
		    tboxexpt.getCogFlankerUscr()!=null && tboxexpt.getCogFlankerUscr()>=0 &&
		    tboxexpt.getCogFlankerAscr()!=null && tboxexpt.getCogFlankerAscr()>=0 &&
		    tboxexpt.getCogListSortUscr()!=null && tboxexpt.getCogListSortUscr()>=0 &&
		    tboxexpt.getCogListSortAscr()!=null && tboxexpt.getCogListSortAscr()>=0 &&
		    tboxexpt.getCogOralReadUscr()!=null && tboxexpt.getCogOralReadUscr()>=0 &&
		    tboxexpt.getCogOralReadAscr()!=null && tboxexpt.getCogOralReadAscr()>=0 &&
		    tboxexpt.getCogPatternUscr()!=null && tboxexpt.getCogPatternUscr()>=0 &&
		    tboxexpt.getCogPatternAscr()!=null && tboxexpt.getCogPatternAscr()>=0 &&
		    tboxexpt.getCogPicSeqUscr()!=null && tboxexpt.getCogPicSeqUscr()>=0 &&
		    tboxexpt.getCogPicSeqAscr()!=null && tboxexpt.getCogPicSeqAscr()>=0 &&
		    tboxexpt.getCogPicVocabUscr()!=null && tboxexpt.getCogPicVocabUscr()>=0 &&
		    tboxexpt.getCogPicVocabAscr()!=null && tboxexpt.getCogPicVocabAscr()>=0) {
		    subjMeta.setCompleteness_toolbox_cognition(true);
		}
		
		if (tboxexpt.getEmoFearSomTscr()!=null && tboxexpt.getEmoFearSomTscr()>=0 && 
		    tboxexpt.getEmoFriendTscr()!=null && tboxexpt.getEmoFriendTscr()>=0 && 
		    tboxexpt.getEmoLifeSatTscr()!=null && tboxexpt.getEmoLifeSatTscr()>=0 && 
		    tboxexpt.getEmoInstSupTscr()!=null && tboxexpt.getEmoInstSupTscr()>=0 && 
		    tboxexpt.getEmoLonelyTscr()!=null && tboxexpt.getEmoLonelyTscr()>=0 && 
		    tboxexpt.getEmoMeaningTscr()!=null && tboxexpt.getEmoMeaningTscr()>=0 && 
		    tboxexpt.getEmoHostilTscr()!=null && tboxexpt.getEmoHostilTscr()>=0 && 
		    tboxexpt.getEmoRejectTscr()!=null && tboxexpt.getEmoRejectTscr()>=0 &&
		    tboxexpt.getEmoStressTscr()!=null && tboxexpt.getEmoStressTscr()>=0 && 
		    tboxexpt.getEmoPosAffectTscr()!=null && tboxexpt.getEmoPosAffectTscr()>=0 && 
		    tboxexpt.getEmoSadnessTscr()!=null && tboxexpt.getEmoSadnessTscr()>=0 && 
		    tboxexpt.getEmoSelfEffTscr()!=null && tboxexpt.getEmoSelfEffTscr()>=0 && 
		    tboxexpt.getEmoAngerAffTscr()!=null && tboxexpt.getEmoAngerAffTscr()>=0 && 
		    tboxexpt.getEmoAngerHostilTscr()!=null && tboxexpt.getEmoAngerHostilTscr()>=0 && 
		    tboxexpt.getEmoAngerPhysTscr()!=null && tboxexpt.getEmoAngerPhysTscr()>=0 && 
		    tboxexpt.getEmoSupportTscr()!=null && tboxexpt.getEmoSupportTscr()>=0 && 
		    tboxexpt.getEmoFearAffTscr()!=null && tboxexpt.getEmoFearAffTscr()>=0) {
		    subjMeta.setCompleteness_toolbox_emotion(true);
		}
		
		if (tboxexpt.getMotGripUscr()!=null && tboxexpt.getMotGripUscr()>=0 &&
		    tboxexpt.getMotGripAscr()!=null && tboxexpt.getMotGripAscr()>=0 &&
		    tboxexpt.getMotWalk2minUscr()!=null && tboxexpt.getMotWalk2minUscr()>=0 &&
		    tboxexpt.getMotWalk2minAscr()!=null && tboxexpt.getMotWalk2minAscr()>=0 &&
		    tboxexpt.getMotWalk4mCscr()!=null && tboxexpt.getMotWalk4mCscr()>=0 && 
		    tboxexpt.getMotPegboardUscr()!=null && tboxexpt.getMotPegboardUscr()>=0 &&
		    tboxexpt.getMotPegboardAscr()!=null && tboxexpt.getMotPegboardAscr()>=0) {
		    subjMeta.setCompleteness_toolbox_motor(true);
		}
		
		if (tboxexpt.getSenOdorUscr()!=null && tboxexpt.getSenOdorUscr()>=0 &&
		    tboxexpt.getSenOdorAscr()!=null && tboxexpt.getSenOdorAscr()>=0 &&
		    tboxexpt.getSenPainTscr()!=null && tboxexpt.getSenPainTscr()>=0 &&
		    tboxexpt.getSenTasteUscr()!=null && tboxexpt.getSenTasteUscr()>=0 &&
		    tboxexpt.getSenTasteAscr()!=null && tboxexpt.getSenTasteAscr()>=0 && 
		    tboxexpt.getSenNoiseCscr()!=null && tboxexpt.getSenNoiseCscr()>=0) {
		    subjMeta.setCompleteness_toolbox_sensory(true);
		}
		
		subjMeta.setCompleteness_toolbox_fullbattery(subjMeta.getCompleteness_toolbox_motor() &&
													 subjMeta.getCompleteness_toolbox_emotion() &&
													 subjMeta.getCompleteness_toolbox_motor() &&
													 subjMeta.getCompleteness_toolbox_sensory());
		   
	}

	private void setAlertnessCompleteness(HcpSubjectmetadata subjMeta, HcpvisitHcpvisitdata visitexpt) {
		
		if (visitexpt == null) {
			return;
		}
		
		if (visitexpt.getHcpmmse_totalscore() !=null && visitexpt.getHcpmmse_totalscore()>=0) {
		    subjMeta.setCompleteness_alertness_mmse(true);
		}
		if (visitexpt.getHcppsqi_totalscore() !=null && visitexpt.getHcppsqi_totalscore()>=0) {
		    subjMeta.setCompleteness_alertness_psqi(true);
		}
		
		subjMeta.setCompleteness_alertness_fullbattery(subjMeta.getCompleteness_alertness_mmse() &&
													 subjMeta.getCompleteness_alertness_psqi());
		
	}

}


