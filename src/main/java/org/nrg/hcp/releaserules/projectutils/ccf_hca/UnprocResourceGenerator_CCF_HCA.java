package org.nrg.hcp.releaserules.projectutils.ccf_hca;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sessionbuilding.abst.AbstractUnprocResourceGenerator;
import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesReportException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.pojo.ResourceValidationResults;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CcfResourceGenerator(description = "CCF_HCA Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_HCA extends AbstractUnprocResourceGenerator {
	
	@SuppressWarnings("unused")
	protected static String[] biasScanTypes = { "Bias_Receive" };
	protected static String[] tseHiresScanTypes = { "TSE_HiResHp" };
	@SuppressWarnings("unused")
	protected static String[] tseHiresSeriesDescriptions = { "TSE_HiResHp" };
	protected static String[] tseHiresNormSeriesDescriptions = { "TSE_Norm_HiResHp", "TSE_Dis3D_HiResHp" };
	protected static String[] pcaslScanTypes = { "mbPCASL", "mbPCASLhr" };
	@SuppressWarnings("unused")
	protected static String[] fieldmapSeriesDescriptions = { "SpinEchoFieldMap_AP", "SpinEchoFieldMap_PA" };
	protected static String[] fieldmapScanTypes = { "FieldMap_SE_EPI" };
	protected static String[] t1t2SeriesDescriptions = { "T1w_MPR_vNav_4e_RMS", "T2w_SPC_vNav" };
	protected static String[] fmriScanTypes = { "tfMRI", "rfMRI" };
	protected static String[] dmriScanTypes = { "dMRI" };
	public static String EVENT_ACTION = "Created unproc resources";
	public static String[] INCLUDED_RESOURCES = { CommonConstants.NIFTI_RESOURCE, CommonConstants.LINKED_DATA_RESOURCE };
	// NOTE:  The resources listed in INCLUDED_RESOURCE_LABELS will include the resource label in the resource path.  Otherwise it's not included.
	public static String[] INCLUDED_RESOURCE_LABELS = { CommonConstants.LINKED_DATA_RESOURCE };
	
	protected final Map<String, List<String>> subdirMap = new HashMap<>();

	public UnprocResourceGenerator_CCF_HCA(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
		subdirMap.put("OTHER_FILES", new ArrayList<String>());
		subdirMap.get("OTHER_FILES").add("(?i).*(_setter|_4e_e|_InitialFrames|_Norm|_Dis3D).*");
	}

	@Override
	public void generateResources() throws ReleaseResourceException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			createT1T2Resources(ci);
			createHiresResources(ci);
			createPcaslResources(ci);
			createFmriResources(ci);
			createDmriResources(ci);
			addReportToT1Resource(ci);
		} catch (Exception e) {
			throw new ReleaseResourceException("ERROR:  Could not create unproc resources", e);
		}
	}

	protected void addReportToT1Resource(EventMetaI ci) {
		final List<XnatAbstractresourceI> resourceList = _combSession.getAllResources();
		String reportCSV = null;
		for (final XnatAbstractresourceI resource : resourceList) {
			if (resource.getLabel().toUpperCase().contains("T1W")) {
				final ArrayList<StoredFile> fws = new ArrayList<>();
				try {
					reportCSV = (reportCSV!=null) ? reportCSV : getReportCSV(); 
					final Path filePath = Files.createTempFile("session_report", ".csv");
					final File tempFile = filePath.toFile();
					FileUtils.writeStringToFile(tempFile, reportCSV);
					fws.add(new StoredFile(tempFile,false));
					getSessionModifier(ci).addFile(fws, resource.getLabel(), null, "OTHER_FILES/session_report.csv", new XnatResourceInfo(getUser(), new Date(), new Date()), false);
				} catch (Exception e) {
					log.error("ERROR:  Couldn't write session_report.csv", e);
				}
			}
		}
	}

	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
				}
			}
		}
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, subdirMap, ci);
	}

	protected void createHiresResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(tseHiresScanTypes)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getType())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedHiresScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
				}
			}
		}
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, subdirMap, ci);
	}
	
	public Collection<? extends XnatImagescandataI> getAssociatedHiresScans(XnatImagescandataI scan) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : getSession().getScans_scan()) {
			if (!mrScan.equals(cScan)) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				if (mrScan.getParameters_sefieldmapgroup().equals(((XnatMrscandata)cScan).getParameters_sefieldmapgroup()) &&
						Arrays.asList(tseHiresNormSeriesDescriptions).contains(cScan.getSeriesDescription())) {
					returnList.add(cScan);
				}
			}
		}
		return returnList;
	}

	protected void createPcaslResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(pcaslScanTypes)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getType())) {
					scanList.add(scan);
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
				}
			}
		}
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, subdirMap, ci);
	}

	protected void createFmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(fmriScanTypes)) {
				if (st.equals(scan.getType())) {
					final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, scan.getSeriesDescription());
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
				}
			}
		}
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, subdirMap, ci);
	}

	protected void createDmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		List<XnatImagescandataI> scanList = new ArrayList<>(); 
		assignMap.put("Diffusion", scanList);
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(dmriScanTypes)) {
				if (st.equals(scan.getType())) {
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
				}
			}
		}
		final Map<String, List<String>> dmriSubdirMap = new HashMap<>();
		dmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		
		dmriSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, dmriSubdirMap, ci);
	}

	protected List<XnatImagescandataI> getScanListFromMap(Map<String, List<XnatImagescandataI>> assignMap, String str) {
		if (assignMap.get(str)==null) {
			assignMap.put(str, new ArrayList<XnatImagescandataI>());
		}
		return assignMap.get(str);
	}
	
	protected String getReportCSV() throws ReleaseRulesReportException {
		final List<XnatMrsessiondata> mrSessions = new ArrayList<>();
		for (final XnatImagesessiondata session : _srcSessions) {
			if (session instanceof XnatMrsessiondata) {
				mrSessions.add((XnatMrsessiondata)session);
			}
		}
		return CcfReleaseRulesUtils.getReportCSV(mrSessions, _releaseRules, _releaseRules.getReportColMap(), true);
	}

	@Override
	public ResourceValidationResults validateUnprocResources() throws ReleaseResourceException {
		final ResourceValidationResults results = ResourceValidationResults.newValidInstance();
		final String diffusionMessage = "Missing Resource:  Diffusion_unproc";
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			final List<String> sdCheckList = new ArrayList<>(Arrays.asList(t1t2SeriesDescriptions));
			sdCheckList.addAll(Arrays.asList(fmriScanTypes));
			for (final String st : sdCheckList) {
				if (st.equals(scan.getType()) && !ResourceUtils.hasResource(getSession(), scan.getSeriesDescription() + "_unproc")) {
					results.setIsValid(false);
					results.getErrorList().add("Missing Resource:  " + scan.getSeriesDescription() + "_unproc");
				}
			}
			if (results.getErrorList().contains(diffusionMessage)) {
				continue;
			}
			for (final String st : Arrays.asList(dmriScanTypes)) {
				if (st.equals(scan.getType()) && !ResourceUtils.hasResource(getSession(), "Diffusion_unproc")) {
					results.setIsValid(false);
					results.getErrorList().add(diffusionMessage);
					break;
				}
			}
		}
		return results;
	}

}
