package org.nrg.hcp.releaserules.projectutils.dmcc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sessionbuilding.abst.AbstractUnprocResourceGenerator;
import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.pojo.ResourceValidationResults;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "DMCC Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_DMCC extends AbstractUnprocResourceGenerator {
	
	@SuppressWarnings("unused")
	private final static String[] biasSeriesDescriptions = { "BIAS_BC", "BIAS_32CH" };
	private final static String[] biasScanTypes = { "Bias_Receive" };
	@SuppressWarnings("unused")
	private final static String[] fieldmapSeriesDescriptions = { "SpinEchoFieldMap_AP", "SpinEchoFieldMap_PA" };
	private final static String[] fieldmapScanTypes = { "FieldMap_SE_EPI" };
	private final static String[] t1t2SeriesDescriptions = { "T1w_MPR", "T2w_SPC" };
	private final static String[] fmriScanTypes = { "tfMRI", "rfMRI" };
	protected final static String[] dmriScanTypes = { "dMRI" };
	public static final String EVENT_ACTION = "Created unproc resources";
	public static final String[] INCLUDED_RESOURCES = { CommonConstants.NIFTI_RESOURCE, CommonConstants.LINKED_DATA_RESOURCE };
	// NOTE:  The resources listed in INCLUDED_RESOURCE_LABELS will include the resource label in the resource path.  Otherwise it's not included.
	public static final String[] INCLUDED_RESOURCE_LABELS = { CommonConstants.LINKED_DATA_RESOURCE };

	public UnprocResourceGenerator_DMCC(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	}

	@Override
	public void generateResources() throws ReleaseResourceException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			createT1T2Resources(ci);
			createFmriResources(ci);
		} catch (Exception e) {
			throw new ReleaseResourceException("ERROR:  Could not create unproc resources", e);
		}
	}

	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		// Shims often don't match fieldmaps for DMCC, so we'll include them as "OTHER" files.
		final Map<String, List<String>> strucSubdirMap = new HashMap<>();
		strucSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(MPR|SPC)(?!.*_Norm).*$");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, strucSubdirMap, ci);
	}
	
	private void createFmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(fmriScanTypes)) {
				if (st.equals(scan.getType())) {
					final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, scan.getSeriesDescription());
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
				}
			}
		}
		final Map<String, List<String>> fmriSubdirMap = new HashMap<>();
		fmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		fmriSubdirMap.get("OTHER_FILES").add("(?i).*(Bias).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, fmriSubdirMap, ci);
	}

	private List<XnatImagescandataI> getScanListFromMap(Map<String, List<XnatImagescandataI>> assignMap, String str) {
		if (assignMap.get(str)==null) {
			assignMap.put(str, new ArrayList<XnatImagescandataI>());
		}
		return assignMap.get(str);
	}
	

	@Override
	public ResourceValidationResults validateUnprocResources() throws ReleaseResourceException {
		final ResourceValidationResults results = ResourceValidationResults.newValidInstance();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			final List<String> sdCheckList = new ArrayList<>(Arrays.asList(t1t2SeriesDescriptions));
			sdCheckList.addAll(Arrays.asList(fmriScanTypes));
			for (final String st : sdCheckList) {
				if (st.equals(scan.getType()) && !ResourceUtils.hasResource(getSession(), scan.getSeriesDescription() + "_unproc")) {
					results.setIsValid(false);
					results.getErrorList().add("Missing Resource:  " + scan.getSeriesDescription() + "_unproc");
				}
			}
			for (final String st : Arrays.asList(dmriScanTypes)) {
				if (st.equals(scan.getType()) && !ResourceUtils.hasResource(getSession(), "Diffusion_unproc")) {
					results.setIsValid(false);
					results.getErrorList().add("Missing Resource:  Diffusion_unproc");
					break;
				}
			}
		}
		return results;
	}

}
