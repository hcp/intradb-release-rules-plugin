package org.nrg.hcp.releaserules.projectutils.ccf_hcd;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseFileHandler_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_HCD Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_HCD extends ReleaseFileHandler_CCF_HCA {
	
	public ReleaseFileHandler_CCF_HCD(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
	}

}
