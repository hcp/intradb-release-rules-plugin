package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_PHCP Project Session Building Release Rules")
public class ReleaseRules_CCF_PHCP_7T extends ReleaseRules_CCF_HCA {
	
	private Object _lock = new Object();
	
	public ReleaseRules_CCF_PHCP_7T() {
		super();
		//SELECTION_CRITERIA = new String[] {"3T","7T","MRS"};
		SESSION_LABEL_EXT = "";
		SELECTION_CRITERIA = new String[] {"V2_7A","V2_7B", "V3_7Z"};
	    //SELECTION_CRITERIA = new String[] { "01" };
	}
	
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: 3T\n" +
				"    label: Select Session Type\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	if (newCriteria == '3T') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_7T)/,'_3T');\n" +
				"                 	}else if (newCriteria == '7T') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_3T)/,'_7T');\n" +
				"                 	}else if (newCriteria == 'MRS') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_3T|_7T)/,'_MRS');\n" +
				"                 	}\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        '3T': '3T'\n" +
				"        '7T': '7T'\n" +
				//"        'MRS': 'MRS'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String sc = params.get("selectionCriteria");
		for (final XnatMrsessiondata session : projectExpts) {
			final String lcLabel = session.getLabel().toLowerCase();
			final String scanner = session.getScanner();
			final String sessionType = session.getSessionType();
			boolean isMRS = scanner.contains("7TAS") && (lcLabel.contains("mrs") || sessionType.endsWith("MRS"));
			boolean is7T = scanner.contains("7TAS") && !isMRS;
			boolean is3T = scanner.equals("TRIOC");
			if (!(is3T || is7T || isMRS)) {
				log.warn("WARNING:  Session " + session.getLabel() + " does not match any build type.");
				continue;
			}
			if (is3T) {
				//filteredExpts.add(session);
			} else if (is7T && session.getLabel().contains(sc.replaceAll("T_",""))) {
				filteredExpts.add(session);
			} else if (isMRS && session.getLabel().contains(sc.replaceAll("_MRS_","_7"))) {
				//filteredExpts.add(session);
			}
		}
		return filteredExpts;
	}
	
	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride, boolean skipClearList) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			if (!skipClearList) {
				clearWarningAndErrorLists();
			}
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			// PHCP 7T Difference.  No Normalized
			//checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			// PHCP TODO:  Not yet.  GE fieldmaps are not in pairs.
			//fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			// PHCP 7T Difference.  No T2s.   Let's not do any checking other tha duplicate scans (later)
			//updateT1T2InclusionAndDesc(subjectScans); 
			// PHCP 7T Difference.  Doesn't contain both normalized and non-normalized scans
			//updateNonNormalizedInclusionAndQuality(subjectScans);
			// PHCP 7T Difference.  No T2s.  Let's not do any checking.  We'll only check for duplicate scans (later)
			//flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			// PHCP 7T Difference.  Let's skip shim checks.
			//flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			// PHCP 7T Difference.  Let's skip fieldmap checks.
			//flagScansWithMissingFieldmapGroup(subjectScans);
			// PHCP 7T Difference.  No dMRIMRI
			//flagIfNotAllDmriInSameShimGroup(subjectScans);
			// PHCP 7T Difference.  Doesn't contain the normal scans
			//markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			setScanOrderAndScanComplete(subjectScans); 
			// PHCP 7T Difference:  no PCASL
			//updatePCASLInclusion(subjectScans);
			// PHCP TODO:  Not yet. 
			//removeFieldmapScansWithoutMainScan(subjectScans);
			// PHCP 7T Difference
			//removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			// PHCP 7T Difference.  No HiRes
			//flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride && 
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		String sc = params.get("selectionCriteria");
		if (sc == null) {
			sc = "3T";
		}
		String appendGroup = sc;
		if (sc.contains("_MRS_")) {
			appendGroup = sc.replace("_MRS_","_7") + "MRS";
		} else if (sc.contains("_7T_")) {
			appendGroup = sc.replace("_7T_", "_7");  
		}
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + "_" + appendGroup;
	}
	
	@Override
	protected void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				log.error("ERROR: scan (key) should not be null!!!");
				log.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				log.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getSeriesDescription().matches(PHYSIO_RGX) ||
					(scan.getSeriesDescription().matches(SETTER_RGX) && !scan.getParameters_imagetype().contains("MOSAIC")) ||
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
					// 3)  Others per MP
					|| scan.getSeriesDescription().toUpperCase().contains("CSSLOC")
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// Let's set everything else to release count scan for 7T.
			scan.setReleasecountscan(true);
		}
	}

	@Override
	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			scan.setTargetforrelease(true);
		}
	}
	

	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		boolean hasOppPe = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			if (scan.getDbdesc().equalsIgnoreCase("FieldMap")) {
				if (scan.getType().contains("_Magnitude")) {
					scan.setDbdesc("FieldMap_Magnitude");
				} else if (scan.getType().contains("_Phase")) {
					scan.setDbdesc("FieldMap_Phase");
				}
			}
			String dbDesc = scan.getDbdesc()
					.replace("_AP_2", "_AP")
					.replace("_PA_2", "_PA")
					;
			if (scan.getSeriesDescription().contains("oppPE_")) {
				if (hasOppPe) {
					dbDesc = dbDesc.replace("oppPE_","oppPE2_");
				} else {
					dbDesc = dbDesc.replace("oppPE_","oppPE1_");
				}
				
			}
			scan.setDbdesc(dbDesc);
			if (scan.getSeriesDescription().contains("oppPE") && !scan.getSeriesDescription().contains("SBRef")) {
				hasOppPe = true;
			}
		}
	}

	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
		// Not doing anything yet
		return;
		/*
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=488) ? "Y" : "N";
               		scanPct = (double)frames10/(488-10);
                	scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_VISMOTOR")) {
                		scanComplete = (frames>=194) ? "Y" : "N";
                		//scanPct = (double)frames10/(194-10);
                		if (frames<155) {
                			getErrorList().add("RULESERROR:  tfMRI VISMOTOR scans should have at least 155 frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_CARIT")) {
                		scanComplete = (frames>=300) ? "Y" : "N";
                		scanPct = (double)frames10/(300-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI CARIT scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_FACENAME")) {
                		scanComplete = (frames>=345) ? "Y" : "N";
                		//scanPct = (double)frames10/(345-10);
                		if (frames<315) {
                			getErrorList().add("RULESERROR:  tfMRI FACENAME scans should have at least 315 frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } else if (currDbdesc.matches(PCASL_RGX)) { 
                	if (currDbdesc.contains("hr_")) {
                		scanComplete = (frames>=90) ? "Y" : "N";
                		scanPct = (double)frames/90;
                	} else {
                		scanComplete = (frames>=70) ? "Y" : "N";
                		scanPct = (double)frames/70;
                	} 
                }
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
		*/
	}

	
}

