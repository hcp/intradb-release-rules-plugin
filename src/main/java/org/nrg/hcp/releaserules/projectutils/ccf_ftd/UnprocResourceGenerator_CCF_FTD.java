package org.nrg.hcp.releaserules.projectutils.ccf_ftd;

import java.util.List;


import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_mdp.UnprocResourceGenerator_CCF_MDP;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_FTD Project Unprocessed Resource Generator")
// NOTE:  Starting out with CCF_MDP project unprocessed generator since series descriptions are the same for FTD
public class UnprocResourceGenerator_CCF_FTD extends UnprocResourceGenerator_CCF_MDP {
	
	protected final static String[] t1t2SeriesDescriptions = { "T1w_MPR", "T2w_SPC" };
	
	public UnprocResourceGenerator_CCF_FTD(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	}
	
}
