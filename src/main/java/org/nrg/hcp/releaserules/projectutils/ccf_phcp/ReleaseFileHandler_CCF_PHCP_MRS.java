package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.exception.ReleaseTransferException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectScanResourceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CcfReleaseFileHandler(description = "CCF_PHCP Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_PHCP_MRS extends AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler {
	
	final static protected String MRS_RESOURCE_LABEL = "MR_SPECTROSCOPY_DATA";
	
	{
		getPathMatchRegex().add("^.*$");
	}
	
	public ReleaseFileHandler_CCF_PHCP_MRS(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_PHCP_MRS() , user);
	}
	

	@Override
	public void transferFiles() throws ReleaseTransferException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getDestSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			//List<String> noInitialFramesFilesRegex = new ArrayList<>();
			// Exclude InitialFrames files.
			//noInitialFramesFilesRegex.add("^((?!InitialFrames).)*$");
			//transferFilesForResourceWithHandlingForJsonFiles(SOURCE_SESSION_NIFTI_RESOURCE, DEST_SESSION_NIFTI_RESOURCE, true, true, noInitialFramesFilesRegex, ci);
			transferFilesForResourceWithHandlingForJsonFiles(SOURCE_SESSION_NIFTI_RESOURCE, DEST_SESSION_NIFTI_RESOURCE, null, _transformer, ci);
			transferFilesForResource(SOURCE_SESSION_LINKED_DATA_RESOURCE, DEST_SESSION_LINKED_DATA_RESOURCE, pathMatchRegex, _transformer, ci);
			transferMrsFiles(ci);
		} catch (Exception e) {
			throw new ReleaseTransferException("ERROR:  Could not transfer files", e);
		}
	}
	
	
	private void transferMrsFiles(EventMetaI ci) throws ReleaseTransferException {
		final XnatImagesessiondata combSess = getDestSession();
		for (final XnatImagesessiondata expt : getSrcSessions()) {
			for (final XnatImagescandataI scan : expt.getScans_scan()) {
				if (scan == null || scan instanceof XnatMrsessiondata) {
					continue;
				}
				if (scan.getSeriesDescription() == null) {
					continue;
				}
				final String lcSeriesDesc = scan.getSeriesDescription().toLowerCase();
				// Per site, drop the calibration and check scans
				if (scan == null || scan.getSeriesDescription() == null || 
						scan.getSeriesDescription().toLowerCase().contains("_cal_") || 
						scan.getSeriesDescription().toLowerCase().contains("_check") || 
						scan.getSeriesDescription().toLowerCase().contains(" check") || 
						scan.getSeriesDescription().toLowerCase().contains("_ws_check")) {
					continue;
				}
				//// END TEMPORARY
				for (final XnatAbstractresourceI resourceI : scan.getFile()) {
					if (!((resourceI.getLabel().equalsIgnoreCase("MRS_NIFTI")) 
							&& resourceI instanceof XnatResource)) {
						continue;
					}
					final XnatResource resource = (XnatResource)resourceI;
					if (resource instanceof XnatResourcecatalog) {
						// Let's be sure to reread files from filesystem for resource catalogs.
						((XnatResourcecatalog)resource).clearFiles();
						
					}
					if (resource.getFileResources(CommonConstants.ROOT_PATH) == null || resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
						continue;
					}
					final XnatImagescandata combScan = getTargetStructuralScan(combSess,scan);
					if (combScan == null) {
						log.warn("ERROR:  Could not locate and open destination session scan (SCAN=" + scan.getDbid() + ")");
						continue;
					}
					
					final DirectScanResourceImpl scanModifier = new DirectScanResourceImpl(combScan, combSess, true, _user, ci);
					XnatResourcecatalog destResource = null;
					for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
						String resourcePath = resource.getFullPath(CommonConstants.ROOT_PATH);
						resourcePath = resourcePath.replaceFirst("[/]$", "");
						resourcePath = resourcePath.substring(0,resourcePath.lastIndexOf(File.separator));
						final File file = rFile.getF();
						if (pathMatchRegex!=null) {
							boolean hasMatch = false;
							for (final String regex : pathMatchRegex) {
								if (rFile.getAbsolutePath().matches(regex)) {
									hasMatch = true;
									continue;
								}
							}
							if (!hasMatch) {
								continue;
							}
						}
						// Initialize resource now.  We only want to create it if we're going to save to it.
						if (destResource == null) {
							destResource = getOrCreateResource(combScan, scanModifier, MRS_RESOURCE_LABEL, scan, ci);
							if (destResource==null) {
								continue;
							}
							destResource.clearCountAndSize();
							destResource.clearFiles();
						}
						File tempFile = null;
						try {
							tempFile = File.createTempFile("CCFSessionBuilding", "fil");
							FileUtils.copyFile(file, tempFile);
							final ArrayList<StoredFile> fws = new ArrayList<>();
							fws.add(new StoredFile(tempFile, false));
							String newName = file.getName();
							if (_transformer != null) {
								newName = _transformer.transformFilename(newName,expt,combSess,scan,combScan);
							}
							//String newDir = file.getParentFile().getAbsolutePath().replace(resourcePath, "") + 
							//		File.separator + scan.getSeriesDescription();
							String newDir = file.getParentFile().getAbsolutePath().replace(resourcePath, "");
							//if (transformer != null) {
							//	newDir = transformer.transformParentDirectoryPath(newDir,expt,combSess,scan,combScan);
							//}
							final String newPath = (newDir != null && newDir.length()>0) ? newDir + File.separator + newName : newName;
							scanModifier.addFile(fws, MRS_RESOURCE_LABEL, null, newPath, new XnatResourceInfo(_user, new Date(), new Date()), false);
						} catch (Exception e) {
							throw new ReleaseTransferException("Error copying file", e);
						} finally {
							if (tempFile != null && tempFile.exists()) {
								tempFile.delete();
							}
						}
					}
					if (destResource == null) {
						continue;
					}
					destResource.calculate(CommonConstants.ROOT_PATH);
					destResource.setFileCount(destResource.getCount(CommonConstants.ROOT_PATH));
					destResource.setFileSize(destResource.getSize(CommonConstants.ROOT_PATH));
					try {
						//SaveItemHelper.authorizedSave(combScan,_user,false,true,ci);
						SaveItemHelper.authorizedSave(destResource,_user,false,true,ci);
					} catch (Exception e) {
						// Do nothing.  Only the file counts are affected.
					}
				}
			}
		}
		
	}

	private XnatImagescandata getTargetStructuralScan(final XnatImagesessiondata combSess, final XnatImagescandataI scan) {
		String strStart = "XXX_XX";
		if (scan.getSeriesDescription().toUpperCase().startsWith("OCC_")) {
			strStart="OCC_T1";
		} else if (scan.getSeriesDescription().toUpperCase().startsWith("PFC_")) {
			strStart="PFC_T1";
		}
		for (final XnatImagescandataI cScan : combSess.getScans_scan()) {
			if (cScan instanceof XnatMrscandata) {
				if (cScan.getSeriesDescription().toUpperCase().startsWith(strStart)) {
					return (XnatImagescandata)cScan;
				}
			}
		}
		return null;
	}

}
