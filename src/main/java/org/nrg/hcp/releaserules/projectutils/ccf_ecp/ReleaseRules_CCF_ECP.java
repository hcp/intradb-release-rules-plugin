package org.nrg.hcp.releaserules.projectutils.ccf_ecp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_bwh.ReleaseRules_CCF_BWH;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_ECP Project Session Building Release Rules")
public class ReleaseRules_CCF_ECP extends ReleaseRules_CCF_BWH {
	
	//**************************************************************************************************************//
	//**************************************************************************************************************//
	// NOTE:  The ECP project does not have normalized structural scans.  This class may be a better starting point // 
	//        for projects without normalized structurals than the HCA class                                        //
	//**************************************************************************************************************//
	//**************************************************************************************************************//
	
	//private Object _lock = new Object();
	
	public final String BIAS_RGX = "(?i)^BIAS_.*"; 
	
	
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		//rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	public ReleaseRules_CCF_ECP() {
		super();
		SESSION_LABEL_EXT = "";
		AP_RGX = "(?i)^.*_AP(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$"; 
		PA_RGX = "(?i)^.*_PA(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$"; 
		APPA_RGX = "(?i)_((AP)|(PA))(($)|(_.*$))"; 
		APPAONLY_RGX = "(?i)_((PA)|(AP))_"; 
		TFMRINO_RGX = "(?i)[0-9]_((PA)|(AP)).*$";
		SEFIELDMAP_RGX = "(?i)^FieldMap_SE.*";
		SELECTION_CRITERIA = new String[] {"BASELINE","FOLLOWUP", "CONTROL"};
	}
	
	private Object _lock = new Object();
	
	
	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		return applyRulesToScans(subjectScans, subjectSessions, errorOverride, false);
		
	}
	
	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride, boolean skipClearList) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			if (!skipClearList) {
				clearWarningAndErrorLists();
			}
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			// NOT FOR ECP - No Normalized Scans
			//checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			updateT1T2InclusionAndDesc(subjectScans); 
			// NOT FOR ECP - No Normalized Scans and this method is for sessions that have them
			//updateNonNormalizedInclusionAndQuality(subjectScans);
			flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			flagScansWithMissingFieldmapGroup(subjectScans);
			flagIfNotAllDmriInSameShimGroup(subjectScans);
			markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			useLaterBiasGroupIfMissing(subjectScans);
			excludeUnneededBiasScans(subjectScans);
			setScanOrderAndScanComplete(subjectScans); 
			updatePCASLInclusion(subjectScans);
			removeFieldmapScansWithoutMainScan(subjectScans);
			removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			//flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride && 
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
	}

	
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: BASELINE\n" +
				"    label: Select Session Type\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	if (newCriteria == 'BASSELINE') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/_[FC]_MR/,'_B_MR');\n" +
				"                 	}else if (newCriteria == 'FOLLOWUP') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/_[BC]_MR/,'_F_MR');\n" +
				"                 	}else if (newCriteria == 'CONTROL') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/_[BF]_MR/,'_C_MR');\n" +
				"                 	}\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        'BASELINE': 'BASELINE'\n" +
				"        'FOLLOWUP': 'FOLLOWUP'\n" +
				"        'CONTROL': 'CONTROL'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String sc = params.get("selectionCriteria");
		for (final XnatMrsessiondata session : projectExpts) {
			final boolean isControl = isControl(projectExpts);
			final boolean isBaseline = session.getLabel().matches("^.*_[12]_.*$");
			final boolean isFollowup = session.getLabel().matches("^.*_3_.*$");
			if (!(isBaseline || isFollowup || isControl)) {
				log.warn("WARNING:  Session " + session.getLabel() + " does not match any build type.");
				continue;
			}
			if ((sc.equals("BASELINE") && (isBaseline && !isControl)) || (sc.equals("FOLLOWUP") && isFollowup) || 
					(sc.equals("CONTROL") && (isControl && isBaseline))) {
				filteredExpts.add(session);
			}
		}
		return filteredExpts;
	}
	
	private boolean isControl(List<XnatMrsessiondata> projectExpts) {
		for (XnatMrsessiondata exp : projectExpts) {
			if (EcpUtils.isControl(exp.getLabel())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		String sc = params.get("selectionCriteria");
		if (sc == null) {
			sc = (EcpUtils.isControl(subj.getLabel())) ? "CONTROL" : "BASELINE";
		}
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + "_" + sc.substring(0,1) + "_MR"; 
	}
	
	
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		final List<Integer> groupList = new ArrayList<>();
		for (final XnatMrscandata scan : subjectScans) {
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				// No shim values reported in GE scanner (ECP)
				scan.setParameters_shimgroup("A");
			}
			
			// BIAS FIELD GROUP
			
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && !inBiasGroup) {
				inBiasGroup = true;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				biasFieldGroup++;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
				inBiasGroup = false;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (scan.getReleasecountscan() && inBiasGroup && scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(BIAS_RGX) && isTargetOrOverrideAll(scan) && inBiasGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("Bias_", "Bias" + biasFieldGroup + "_"));
			}
			
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				groupList.add(seFieldmapGroup);
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
					 scan.getSubjectsessionnum().intValue() == seSession) ||
							(seSession == 0 && seFieldmapGroup == 0))
					) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				} else {
					// If we haven't hit any fieldmaps yet, put with group 1.  For ECP, some scans are collected prior
					// to any fieldmaps being collected and the shim values don't normally match
					scan.setParameters_sefieldmapgroup(1);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
							(seSession > 0 && scan.getSubjectsessionnum() > seSession)
					))) {
				inSEFieldmapGroup = false;
				// For ECP, scans in sessions often appear before the fieldmaps.  Let's assign the next fieldmap
				scan.setParameters_sefieldmapgroup(seFieldmapGroup+1);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan) && scan.getParameters_sefieldmapgroup() != null &&
					!groupList.contains(scan.getParameters_sefieldmapgroup())) {
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				getErrorList().add("RULESERROR:  A fieldmap group has been assigned a value not represented by any fieldmap" + 
					" (SESSION=" + ((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
			}
		}
	}		
	
	
	@Override
	protected void updateT1T2InclusionAndDesc(List<XnatMrscandata> subjectScans) {
		// Target T1, T2 from session that has both
		final ArrayList<Integer> t1List = new ArrayList<Integer>();
		final ArrayList<Integer> t2List = new ArrayList<Integer>();
		final ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(T1_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if (scan.getType().matches(T2_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getErrorList().add("RULESERROR:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getType().matches(T1_RGX) || scan.getType().matches(T2_RGX)) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
						// No normalized scans for ECP
						//final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						//if (isNormalizedScan(nextScan)) {
						//	setNoRelease(nextScan);
						//}
					}
				}
			}
		}
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		// MRH 2022-03-17:  Change per Mike Harms.  We no longer care that T1/T2s have matching shims if we're not using the
		// fieldmaps.  MH explicitly prefers picking highest quality over matching shims per 03/16/22-03/17/22 e-mail thread.
		// We'll split shimGroup to t1ShimGroup and t2ShimGroup and just handle those separately.
		//char shimGroup = Character.MIN_VALUE;
		char t1ShimGroup = Character.MIN_VALUE;
		char t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : subjectScans) {
			// Keeping only 1 T1/T2, Flag error if more than one 
			// Keeping second only if in same shim group as first
			if (scan.getType().matches(T1_RGX) && isTargetOrOverrideStruc(scan)) {
				if (t1Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t1Count++;
				}
				if (t1Count!=0) {
					if (t1ShimGroup == Character.MIN_VALUE) {
						t1ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
						if (t1ShimGroup == getShimGroup(scan)) {
							final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
							getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
									"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
						t1Count--;
						setNoRelease(scan);
						//if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						//	setNoRelease(prevScan);
						//}
					} else {
						// No scan values extracted for ECP.  Not currently calculating readout direction
					    //setRDFields(scan);
						//if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						//	setRDFields(prevScan);
						//}
					}
				}
			}
			if (scan.getType().matches(T2_RGX) && isTargetOrOverrideStruc(scan)) {
				if (t2Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t2Count++;
				}
				if (t2Count!=0) {
					if (t2ShimGroup == Character.MIN_VALUE) {
						t2ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
						if (t2ShimGroup == getShimGroup(scan)) {
							final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
							getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
									"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
						t2Count--;
						setNoRelease(scan);
						//if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						//	setNoRelease(prevScan);
						//}
					} else {
						// No scan values extracted for ECP.  Not currently calculating readout direction
					    //setRDFields(scan);
						//if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						//	setRDFields(prevScan);
						//}
					}
				}
			}
		}
		t1Count=0;
		t2Count=0;
		t1ShimGroup = Character.MIN_VALUE;
		t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (scan.getType().matches(T1_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1ShimGroup == Character.MIN_VALUE) {
					t1ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
					if (t1ShimGroup == getShimGroup(scan)) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
									subjectSession.getLabel() +
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
					t1Count--;
					setNoRelease(scan);
					//if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
					//	setNoRelease(prevScan);
					//}
				} else {
					// No scan values extracted for ECP.  Not currently calculating readout direction
				    //setRDFields(scan);
					//if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
					//	setRDFields(prevScan);
					//	setRDEchoScans(subjectScans, prevScan);
					//}
				}
			}
			if (scan.getType().matches(T2_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2ShimGroup == Character.MIN_VALUE) {
					t2ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
					if (t2ShimGroup == getShimGroup(scan)) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
					t2Count--;
					setNoRelease(scan);
					//if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
					//	setNoRelease(prevScan);
					//}
				} else {
					// No scan values extracted for ECP.  Not currently calculating readout direction
				    //setRDFields(scan);
					//if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
					//	setRDFields(prevScan);
					//	setRDEchoScans(subjectScans, prevScan);
					//}
				}
			}
		}
	}		


	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			
			final String sd = scan.getSeriesDescription().toUpperCase();
			
			if (sd.contains("RSFMRI") && sd.contains("PE1")) {
				scan.setDbdesc("rfMRI_REST_PA");
			} else if (sd.contains("RSFMRI") && sd.contains("PE2")) {
				scan.setDbdesc("rfMRI_REST_AP");
			
			} else if (sd.contains("REST_") && sd.contains("PE1")) {
				scan.setDbdesc("rfMRI_REST_PA");
			} else if (sd.contains("REST_") && sd.contains("PE2")) {
				scan.setDbdesc("rfMRI_REST_AP");
				
			} else if (sd.contains("rsfMRI") && sd.contains("PE1")) {
				scan.setDbdesc("rfMRI_REST_PA");
			} else if (sd.contains("rsfMRI") && sd.contains("PE2")) {
				scan.setDbdesc("rfMRI_REST_AP");
				
			} else if (sd.contains("SEM_") && sd.contains("PE1")) {
				scan.setDbdesc("tfMRI_SEM_PA");
			} else if (sd.contains("SEM_") && sd.contains("PE2")) {
				scan.setDbdesc("tfMRI_SEM_AP");
				
			} else if (sd.contains("SOC_") && sd.contains("PE1")) {
				scan.setDbdesc("tfMRI_SOC_PA");
			} else if (sd.contains("SOC_") && sd.contains("PE2")) {
				scan.setDbdesc("tfMRI_SOC_AP");
				
			} else if (sd.contains("LANG_") && sd.contains("PE1")) {
				scan.setDbdesc("tfMRI_LANG_PA");
			} else if (sd.contains("LANG_") && sd.contains("PE2")) {
				scan.setDbdesc("tfMRI_LANG_AP");
				
			} else if (sd.contains("EMOT_") && sd.contains("PE1")) {
				scan.setDbdesc("tfMRI_EMOT_PA");
			} else if (sd.contains("EMOT_") && sd.contains("PE2")) {
				scan.setDbdesc("tfMRI_EMOT_AP");
				
			} else if (sd.contains("SE_MAP") && sd.contains("PE1")) {
				scan.setDbdesc("SpinEchoFieldMap_PA");
			} else if (sd.contains("SE_MAP") && sd.contains("PE2")) {
				scan.setDbdesc("SpinEchoFieldMap_AP");
				
			} else if (sd.contains("75D") && sd.contains("PE1")) {
				scan.setDbdesc("dMRI_dir75_PA");
			} else if (sd.contains("75D") && sd.contains("PE2")) {
				scan.setDbdesc("dMRI_dir75_AP");
			} else if (sd.contains("76D") && sd.contains("PE1")) {
				scan.setDbdesc("dMRI_dir76_PA");
			} else if (sd.contains("76D") && sd.contains("PE2")) {
				scan.setDbdesc("dMRI_dir76_AP");
				
			} else if (sd.contains("MPRAGE")) {
				scan.setDbdesc("T1w_MPRAGE");
			} else if (sd.contains("CUBE_T2")) {
				scan.setDbdesc("T2w_CUBE");
			}
			// Add _norm to series descriptions for normalized scans
			/*
			if (scan.getType().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					if (scan.getDbdesc().contains("_4e")) {
						scan.setDbdesc(scan.getDbdesc().replace("_4e", "_Norm_4e"));
					} else {
						scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
					}
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("TSE_")) {
					scan.setDbdesc(scan.getDbdesc().replace("TSE_", "TSE_Norm_"));
				}
			}
			*/
		}
	}
	

	@Override
	protected void flagIfNotAllDmriInSameShimGroup(List<XnatMrscandata> subjectScans) {
		// Do nothing for ECP - No Shims!!!
	}
	
	
	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		// Do nothing for ECP - No Shims!!!
	}
	
	@Override
	protected void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				log.error("ERROR: scan (key) should not be null!!!");
				log.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				log.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getSeriesDescription().matches(PHYSIO_RGX) ||
					(scan.getSeriesDescription().matches(SETTER_RGX) && !scan.getParameters_imagetype().contains("MOSAIC")) ||
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// Per M.Harms, 2018/01/12, let's only include specific scan types, so random ones that are sometimes collected
			// are excluded.
			if (!(isBiasScan(scan) ||
					scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(MAINSCANDESC_RGX))) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if (isMainStructuralScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  " + ((isMainStructuralScan(scan)) ? "Structural" : "TSE HiRes")  +
								" normalized scan quality rating is invalid or scan has not been rated (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Scan quality rating is invalid (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			scan.setReleasecountscan(true);
		}
	}


	@Override
	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					(scan.getQuality().equalsIgnoreCase("poor") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					scan.getQuality().equalsIgnoreCase("fair")) && isMainStructuralScan(scan)) {
           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
           		scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
						isMainStructuralScan(scan)) {
					if (isT1Type(scan,false)) {
						hasGoodT1 = true;
					} else if (isT2Type(scan,false)) {
						hasGoodT2 = true;
					}
					//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
					//	prevScan.setTargetforrelease(true);
					//	prevScan.setQuality(scan.getQuality());
					//}
				}
			}
			if (!hasGoodT1 || !hasGoodT2) {
				for (final XnatMrscandata scan : subjectScans) {
					if ((!hasGoodT1 && isT1Type(scan,false)) || (!hasGoodT2 && isT2Type(scan,false))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							scan.setTargetforrelease(true);
							//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
							//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
							//	prevScan.setTargetforrelease(true);
							//	prevScan.setQuality(scan.getQuality());
							//}
						}
					}
				}
			}
		}
		// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		if (!hasValidT1AndT2(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
           		setNoRelease(scan);
			}
		}
	}

	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
				} 
				if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
					prevRestSession = scan.getSubjectsessionnum();
					intradbSessionRestCount=1;
					if (currDbdesc.matches(AP_RGX)) {
						if (groupHasAP) {
							multiGroupAP = true;
						}
						groupHasAP = true;
						
					} else if (currDbdesc.matches(PA_RGX)) {
						if (groupHasPA) {
							multiGroupPA = true;
						}
						groupHasPA = true;
					}
					if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
						restGroupCount++;
					}
				} else {
					intradbSessionRestCount++;
					if (currDbdesc.matches(AP_RGX)) {
						if (groupHasAP) {
							multiGroupAP = true;
						}
						groupHasAP = true;
						
					} else if (currDbdesc.matches(PA_RGX)) {
						if (groupHasPA) {
							multiGroupPA = true;
						}
						groupHasPA = true;
					}
					if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
						restGroupCount++;
					}
					if (restCount>4) {
						getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
					}
					if (intradbSessionRestCount>4) {
						getErrorList().add("RULESERROR:  Intradb session contains more than four usable Resting State scans (SESSION=" + 
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
					}
					if (restGroupCount>2) {
						getErrorList().add("RULESERROR:  Intradb session group contains more than two groups of usable Resting State scans (SESSION=" + 
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
					}
				}
				inRest=true;
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}
	
	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
		// NOTE:  This section needs work for ECP!!!!!
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 0;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=27110) ? "Y" : "N";
               		scanPct = (double)frames10/(27110-0);
               		if (scanPct<.75) {
               			getErrorList().add("RULESERROR:  rfMRI scans should have at least 75% of frames to have been marked usable (SESSION=" +
               					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
               					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
               		}
               		//scan100 = (frames10>=100) ? "Y" : "N";
                	//if (scan100.equals("N")) {
                	//	getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                	//			"frames (110 total) to have been marked usable (SESSION=" +
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                	//			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	//}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA/HCD, it looks like we're collecting 1 more frame than directions.
                		if (dirVal.equals("75")) {
                		   int dirNum = 8064;
                		   scanComplete = (frames>=dirNum) ? "Y" : "N";
                		   scanPct = (double)frames/dirNum;
                		} else if (dirVal.equals("76")) {
                		   int dirNum = 8160;
                		   scanComplete = (frames>=dirNum) ? "Y" : "N";
                		   scanPct = (double)frames/dirNum;
                		}
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.toUpperCase().contains("_EMOT")) {
                		scanComplete = (frames>=11880) ? "Y" : "N";
                		scanPct = (double)frames10/(11880-0);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI EMOT scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_SEM")) {
                		scanComplete = (frames>=35280) ? "Y" : "N";
                		scanPct = (double)frames10/(35280-0);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI SEM scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_SOC")) {
                		scanComplete = (frames>=18432) ? "Y" : "N";
                		scanPct = (double)frames10/(18432-0);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI SOC scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_LANG")) {
                		scanComplete = (frames>=27216) ? "Y" : "N";
                		scanPct = (double)frames10/(27216-0);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI SOC scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } 
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("CARIT")) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_PA","_AP") : currDbdesc.replace("_AP","_PA");  
	            final String dbMin = currDbdesc.replace("_PA","").replace("_AP","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	
	
	


	@Override
	protected boolean hasValidT1AndT2(List<XnatMrscandata> subjectScans) {
		boolean hasT1 = false;
		boolean hasT2 = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan)) {
				if (isT1Type(scan,false)) {
					hasT1 = true;
				} else if (isT2Type(scan,false)) {
					hasT2 = true;
				}
				if (hasT1 && hasT2) {
					return true;
				}
			}
		}
		return false;
	}
	

	@Override
	protected void flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(List<XnatMrscandata> subjectScans)
			throws ReleaseRulesException {
		Integer compFieldmapGroup = null;
		String compShimGroup = null;
		XnatMrscandata compScan = null;
		int releaseCount = 0;
		int requiresOverrideCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && willBeReleased(scan)) {
				releaseCount++;
				if (releaseCount>3) {
					hardErrorList.add("RULESERROR:  Too many structural scans are marked for release.  Please " +
							"modify the release flag or usability for one or more scans. This error should not be overridden.");
				}
				if (requiresOverride(scan)) {
					requiresOverrideCount++;
				}
				if (compFieldmapGroup == null || compShimGroup == null) {
					compFieldmapGroup = scan.getParameters_sefieldmapgroup();
					compShimGroup = scan.getParameters_shimgroup();
					compScan = scan;
				} else {
					final XnatMrsessiondata compSession = getScanSession(compScan, subjectSessions);
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					final Integer fmGroup = scan.getParameters_sefieldmapgroup();
					final String shimGroup = scan.getParameters_shimgroup();
					if (fmGroup != null && !fmGroup.equals(compFieldmapGroup)) {
						getErrorList().add("RULESERROR:  Structural scans to be released have different SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								scan.getParameters_sefieldmapgroup() + ")");
					} else if (fmGroup == null) {
						getErrorList().add("RULESERROR:  Structural scan to be released has null SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								fmGroup + ")");
					}
					if (shimGroup != null && !shimGroup.equals(compShimGroup)) {
						getErrorList().add("RULESERROR:  Structural scans to be released have different ShimGroup values (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								scan.getParameters_shimgroup() + ")");
					} else if (shimGroup == null) {
						getErrorList().add("RULESERROR:  Structural scan to be released has null ShimGroup value (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								shimGroup + ")");
					}
				}
			}
		}
		if (releaseCount<2) {
			hardErrorList.add("RULESERROR:  This session does not contain a complete T1/T2 pair marked for release.  " +
							"Note that this error cannot be overridden.  Please edit scan quality or use the release " +
							"override flags to mark scans for release.");
		} else if (requiresOverrideCount>0) {
			getErrorList().add("RULESERROR:  T1/T2 pair marked for release requires a release scan-level override flag to be released. " +
									"This could be due to insufficient scan quality or mismatched shim or fieldmap groups.  The override flag" +
							        " has been supplied but the session errorOverride flag should be used to submit this session so this notice is logged.");
		}
	}
	

	@Override
	protected void markAssociatedScansWithUnreleasedStructuralsAsUnreleased(
			List<XnatMrscandata> subjectScans) {
		// First find scans that will be released and keep their fieldmap values
		ArrayList<Integer> fmList = new ArrayList<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && willBeReleased(scan)) {
				Integer fieldMapGroup = scan.getParameters_sefieldmapgroup();
				if (!fmList.contains(fieldMapGroup)) { 
					fmList.add(fieldMapGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(STRUCT_RGX) && willBeReleased(scan) && 
					(scan.getParameters_sefieldmapgroup()==null || !fmList.contains(scan.getParameters_sefieldmapgroup()))) {
				setNoRelease(scan);
			}
		}
	}
	
	
	@Override
	public boolean isTargetOrOverrideAll(XnatMrscandata scan) {
		if (isMainStructuralScan(scan)) {
			if (isTargetOrOverrideStruc(scan)) {
				return true;
			}
		} else if (scan.getTargetforrelease()!=null) {
			return scan.getTargetforrelease();
		}
		return false;
	}
	
}

