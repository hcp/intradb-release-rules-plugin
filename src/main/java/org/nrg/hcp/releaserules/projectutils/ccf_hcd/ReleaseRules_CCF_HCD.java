package org.nrg.hcp.releaserules.projectutils.ccf_hcd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@HcpReleaseRules
@CcfReleaseRules(description = "CCF_HCD Project Session Building Release Rules")
public class ReleaseRules_CCF_HCD extends ReleaseRules_CCF_HCA {
	
	private static final Logger _logger = LoggerFactory.getLogger(ReleaseRules_CCF_HCD.class);
	private Object _lock = new Object();
	private boolean _is5to7 = false;
	
	public ReleaseRules_CCF_HCD() {
		super();
	    SELECTION_CRITERIA = new String[] {"V1","V2","V3"};
	}

	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: V1\n" +
				"    label: Select Visit\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	sessionLabel = sessionLabel.replace(/_V[123]_/,'_' + newCriteria + '_');\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        'V1': 'V1'\n" +
				"        'V2': 'V2'\n" +
				"        'V3': 'V3'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	
	@Override
	public List<String> applyRulesToScans(
			final List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions,
			final Map<String,String> params, final boolean errorOverride)
			throws ReleaseRulesException {
		return this.applyRulesToScans(subjectScans, subjectSessions, errorOverride);
	}

	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	if (!_is5to7) {
	                	scanComplete = (frames>=488) ? "Y" : "N";
	               		scanPct = (double)frames10/(488-10);
	               		scan100 = (frames10>=100) ? "Y" : "N";
	                	if (scan100.equals("N")) {
	                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
	                				"frames (110 total) to have been marked usable (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
	                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
	                	}
                	} else {
	                	scanComplete = (frames>=263) ? "Y" : "N";
	               		scanPct = (double)frames10/(263-10);
	               		scan100 = (frames10>=100) ? "Y" : "N";
	                	if (scan100.equals("N")) {
	                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
	                				"frames (110 total) to have been marked usable (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
	                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
	                	}
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA/HCD, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_GUESSING")) {
                		scanComplete = (frames>=280) ? "Y" : "N";
                		scanPct = (double)frames10/(280-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI GUESSING scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_CARIT")) {
                		scanComplete = (frames>=300) ? "Y" : "N";
                		scanPct = (double)frames10/(300-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI CARIT scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_EMOTION")) {
                		scanComplete = (frames>=178) ? "Y" : "N";
                		//scanPct = (double)frames10/(178-10);
                		if (frames<150) {
                			getErrorList().add("RULESERROR:  tfMRI EMOTION scans should have at least 150 frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } else if (currDbdesc.matches(PCASL_RGX)) { 
                	if (currDbdesc.contains("hr_")) {
                		scanComplete = (frames>=90) ? "Y" : "N";
                		scanPct = (double)frames/90;
                	} else {
                		scanComplete = (frames>=70) ? "Y" : "N";
                		scanPct = (double)frames/70;
                	} 
                }
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("EMOTION")) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        _logger.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			clearWarningAndErrorLists();
		    this.errorOverride = errorOverride;
		    issueErrorOrWarningFor5to7s(subjectScans,subjectSessions);
		    checkThatGuessingAndCaritAreFromSameSession(subjectScans,subjectSessions);
		    return super.applyRulesToScans(subjectScans, subjectSessions, errorOverride, true);
			
		}
		
	}

	private void issueErrorOrWarningFor5to7s(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions) {
		
	    // We don't yet support session building for 5 - 7 year olds (needs rules implemented for differences in protocol)
    	Integer subjectAge = null;
	    for (final XnatMrsessiondata mrSession : subjectSessions) {
	    	final String subjectAgeStr = mrSession.getSubjectAge();
	    	try {
	    		subjectAge = Integer.valueOf(subjectAgeStr);
	    	} catch (Exception e) {
	    		// Do nothing, we'll look at resting state scans;
	    	}
	    	if (subjectAge != null) {
	    		break;
	    	}
	    }
	    boolean olderFrames = false;
	    boolean hasResting = false;
	    if (subjectAge == null) {
	    	for (final XnatMrscandata mrScan : subjectScans) {
	    		if (mrScan.getType().matches(RFMRI_RGX)) {
	    			hasResting = true;
	    			if (mrScan.getFrames()>263) {
	    				olderFrames = true;
	    			}
	    		}
	    	}
	    	if (hasResting && !olderFrames) {
	    		_is5to7 = true;
	    		//getErrorList().add("ERROR:  This appears to be a 5 - 7 year old subject.  Session building is currently " +
	    		//		"not supported for younger subjects.  If overridden this will build a session for older kids.");
	    	}
	    } else if (subjectAge<=7) {
	    	_is5to7 = true;
    		//getErrorList().add("ERROR:  This appears to be a 5 - 7 year old subject.  Session building is currently " +
    		//		"not supported for younger subjects.  If overridden this will build a session for older kids.");
	    }
		
	}
	
	private void checkThatGuessingAndCaritAreFromSameSession(List<XnatMrscandata> subjectScans,
			List<XnatMrsessiondata> subjectSessions) {
		boolean hasGuessing = false;
		boolean hasCarit = false;
		String guessingSession = "";
		String caritSession = "";
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                continue;
            }
            if (!currDbdesc.matches(TFMRI_RGX)) { 
            	continue;
            }
            if (currDbdesc.contains("_GUESSING") && !hasGuessing) {
            	hasGuessing = true;
            	guessingSession = scan.getImageSessionId();
            } else if (currDbdesc.contains("_CARIT") && !hasCarit) {
            	hasCarit = true;
            	caritSession = scan.getImageSessionId();
            } 
        }
        if (hasGuessing && hasCarit && (!guessingSession.equals(caritSession))) {
    		getErrorList().add("RULESERROR:  Carit scans originate from a different session than guessing scans");
        }
	}
	
	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		if (!_is5to7) {
			boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
			int restCount = 1;
			Integer prevRestSession = null;
			int intradbSessionRestCount = 0;
			int restGroupCount = 0;
			for (final XnatMrscandata scan : subjectScans) {
				final String currDbdesc = scan.getDbdesc();
				if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
					continue;
				}
				XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
				if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
					if (!currDbdesc.matches(SBREF_RGX)) {
						if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
							// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
							restCount++;
							restGroupCount=0;
							multiGroupAP = false;
							multiGroupPA = false;
							groupHasAP = false;
							groupHasPA = false;
						} 
						if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
							prevRestSession = scan.getSubjectsessionnum();
							intradbSessionRestCount=1;
							if (currDbdesc.matches(AP_RGX)) {
								if (groupHasAP) {
									multiGroupAP = true;
								}
								groupHasAP = true;
								
							} else if (currDbdesc.matches(PA_RGX)) {
								if (groupHasPA) {
									multiGroupPA = true;
								}
								groupHasPA = true;
							}
							if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
								restGroupCount++;
							}
						} else {
							intradbSessionRestCount++;
							if (currDbdesc.matches(AP_RGX)) {
								if (groupHasAP) {
									multiGroupAP = true;
								}
								groupHasAP = true;
								
							} else if (currDbdesc.matches(PA_RGX)) {
								if (groupHasPA) {
									multiGroupPA = true;
								}
								groupHasPA = true;
							}
							if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
								restGroupCount++;
							}
							if (restCount>2) {
								getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
										((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
							}
							if (intradbSessionRestCount>2) {
								getErrorList().add("RULESERROR:  Intradb session contains more than two usable Resting State scans (SESSION=" + 
										((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
							}
							if (restGroupCount>1) {
								getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
										((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
							}
						}
						inRest=true;
					} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest) {
						inRest=false;
						restCount++;
						restGroupCount=0;
						groupHasAP = false;
						groupHasPA = false;
						multiGroupAP = false;
						multiGroupPA = false;
					}
					scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
				} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
			}
		} else {
			boolean inRest = false, groupHasAP1 = false, groupHasAP2 = false, groupHasPA1 = false, groupHasPA2 = false,
					multiGroupAP = false, multiGroupPA = false;
			int restCount = 1;
			Integer prevRestSession = null;
			int intradbSessionRestCount = 0;
			int restGroupCount = 0;
			for (final XnatMrscandata scan : subjectScans) {
				final String currDbdesc = scan.getDbdesc();
				if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
					continue;
				}
				final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
				if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
					if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP2) || (currDbdesc.matches(PA_RGX) && groupHasPA2))) {
						// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
						// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
						restCount++;
						restGroupCount=0;
						multiGroupAP = false;
						multiGroupPA = false;
						groupHasAP1 = false;
						groupHasAP2 = false;
						groupHasPA1 = false;
						groupHasPA2 = false;
					} 
					if (!currDbdesc.matches(SBREF_RGX)) {
						if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
							prevRestSession = scan.getSubjectsessionnum();
							intradbSessionRestCount=1;
							if (currDbdesc.matches(AP_RGX)) {
								if (groupHasAP1 && groupHasAP2) {
									multiGroupAP = true;
								}
								if (!groupHasAP1) {
									groupHasAP1 = true;
								} else {
									groupHasAP2 = true;
								}
								
							} else if (currDbdesc.matches(PA_RGX)) {
								if (groupHasPA1 && groupHasPA2) {
									multiGroupPA = true;
								}
								if (!groupHasPA1) {
									groupHasPA1 = true;
								} else {
									groupHasPA2 = true;
								}
							}
							if ((groupHasAP1 && groupHasPA1 && groupHasPA2) || (groupHasAP1 && groupHasAP2 && groupHasPA1) ||
									multiGroupAP || multiGroupPA) {
								restGroupCount++;
							}
						} else {
							intradbSessionRestCount++;
							if (currDbdesc.matches(AP_RGX)) {
								if (groupHasAP1 && (groupHasAP2 || groupHasPA2)) {
									multiGroupAP = true;
								}
								if (!groupHasAP1) {
									groupHasAP1 = true;
								} else {
									groupHasAP2 = true;
								}
								
							} else if (currDbdesc.matches(PA_RGX)) {
								if (groupHasPA1 && (groupHasPA2 || groupHasAP2)) {
									multiGroupPA = true;
								}
								if (!groupHasPA1) {
									groupHasPA1 = true;
								} else {
									groupHasPA2 = true;
								}
							}
							if ((groupHasAP1 && groupHasPA1 && groupHasPA2) || 
									(groupHasAP1 && groupHasAP2 && groupHasPA1) || 
									multiGroupAP || multiGroupPA) {
								restGroupCount++;
							}
							if (restCount>2) {
								getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
										((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
							}
							if (intradbSessionRestCount>3) {
								getErrorList().add("RULESERROR:  Intradb 5 to 7 session contains more than three usable Resting State scans (SESSION=" + 
										((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
							}
							if (restGroupCount>1) {
								getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
										((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
							}
						}
						inRest=true;
					} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest) {
						inRest=false;
						restCount++;
						restGroupCount=0;
						intradbSessionRestCount=0;
						groupHasAP1 = false;
						groupHasAP2 = false;
						groupHasPA1 = false;
						groupHasPA2 = false;
						multiGroupAP = false;
						multiGroupPA = false;
					}
					String newDbDesc = currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount));
					if (intradbSessionRestCount==3 || (groupHasPA1 && currDbdesc.matches(SBREF_RGX))) {
						newDbDesc = newDbDesc.replaceFirst("1_PA","1b_PA").replaceFirst("2_PA", "2b_PA");
					} else {
						newDbDesc = newDbDesc.replaceFirst("1_PA","1a_PA").replaceFirst("2_PA", "2a_PA");
					}
					if (intradbSessionRestCount==3 || (groupHasAP1 && currDbdesc.matches(SBREF_RGX))) {
						newDbDesc = newDbDesc.replaceFirst("1_AP","1b_AP").replaceFirst("2_AP", "2b_AP");
					} else {
						newDbDesc = newDbDesc.replaceFirst("1_AP","1a_AP").replaceFirst("2_AP", "2a_AP");
					}
					scan.setDbdesc(newDbDesc);
				} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					intradbSessionRestCount=0;
					groupHasAP1 = false;
					groupHasAP2 = false;
					groupHasPA1 = false;
					groupHasPA2 = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
			}
			for (final XnatMrscandata scan : subjectScans) {
				final String currDbdesc = scan.getDbdesc();
				final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
				if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
					continue;
				}
				int _5AP = 0, _5PA = 0, _8AP = 0, _8PA = 0;
				if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
					if (currDbdesc.matches(SBREF_RGX)) {
						continue;
					}
					if (currDbdesc.matches(".*[ab]_AP.*")) {
						_5AP+=1;
					} else if (currDbdesc.matches(".*[ab]_PA.*")) {
						_5PA+=1;
					} else if (currDbdesc.matches(".*[12]_AP.*")) {
						_8AP+=1;
					} else if (currDbdesc.matches(".*[12]_PA.*")) {
						_8PA+=1;
					}
				}
				if (_5AP>3) {
					getErrorList().add("RULESERROR:  Intradb 5 to 7 session contains more than 3 REST AP scans (SESSION=" + 
							((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
				}
				if (_5PA>3) {
					getErrorList().add("RULESERROR:  Intradb 5 to 7 session contains more than 3 REST PA scans (SESSION=" + 
							((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
				}
				if (_8AP>2) {
					getErrorList().add("RULESERROR:  Intradb 8yo+ session contains more than 2 REST AP scans (SESSION=" + 
							((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
				}
				if (_8PA>2) {
					getErrorList().add("RULESERROR:  Intradb 8yo+ session contains more than 2 REST PA scans (SESSION=" + 
							((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
				}
			}
		}
	}
	
}

