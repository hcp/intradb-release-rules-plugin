package org.nrg.hcp.releaserules.projectutils.ccf_mdd;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.UnprocResourceGenerator_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_MDD Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_MDD extends UnprocResourceGenerator_CCF_HCA {
	
	public UnprocResourceGenerator_CCF_MDD(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	}

}
