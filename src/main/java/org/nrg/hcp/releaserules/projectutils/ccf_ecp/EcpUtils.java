package org.nrg.hcp.releaserules.projectutils.ccf_ecp;

import java.util.Arrays;

public class EcpUtils {
	
	public final static String[] CONTROL_SUBJECTS = new String[] {
			"EC1006", "EC1008", "EC1011", "EC1012", "EC1015", "EC1018", "EC1022", "EC1023", "EC1032", "EC1035",
			"EC1036", "EC1037", "EC1038", "EC1039", "EC1041", "EC1040", "EC1055", "EC1069", "EC1147", "EC2001",
			"EC2002", "EC2004", "EC2005", "EC2006", "EC2007", "EC2008", "EC2009", "EC2010", "EC2012", "EC2013",
			"EC2015", "EC2016", "EC2017", "EC2018", "EC2020", "EC2021", "EC2023", "EC2030", "EC2041", "EC2048",
			"EC2064", "EC2067", "EC2068", "EC2069", "EC2071", "EC2073", "EC2075", "EC2079", "EC2080", "EC2086",
			"EC2087", "EC2089", "EC2092", "EC2098", "EC2102", "EC2116", "EC2117", "EC2118", "EC2121", "EC2123",
			"EC2124"
	};
	
	public final static boolean isControl(String ins) {
		for (final String ctrl : Arrays.asList(CONTROL_SUBJECTS)) {
			if (ins.startsWith(ctrl)) {
				return true;
			}
		}
		return false;
	}

}
