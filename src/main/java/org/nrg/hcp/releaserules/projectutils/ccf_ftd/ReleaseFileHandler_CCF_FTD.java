package org.nrg.hcp.releaserules.projectutils.ccf_ftd;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.FilenameTransformer_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

import com.google.gson.JsonObject;


@CcfReleaseFileHandler(description = "CCF_FTD Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_FTD extends AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler {
	
	{
		getPathMatchRegex().add("^.*/EVs/.*.txt");
	}
	
	public ReleaseFileHandler_CCF_FTD(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_HCA() , user);
	}
	
	@Override
	protected void handleJsonObject(final JsonObject jsonObj, final XnatImagescandataI scan) {
		anonymizeSeriesInstanceUidValue(jsonObj, scan);
		pullAdditionalValuesFromScan(jsonObj, scan);
		removeSpecifiedValuesFromScan(jsonObj, scan);
		removeFtdValuesFromScan(jsonObj, scan);
	}

	private void removeFtdValuesFromScan(JsonObject jsonObj, XnatImagescandataI scan) {
		removeOrAnonymizeJsonField(jsonObj,"AccessionNumber",true);
		removeOrAnonymizeJsonField(jsonObj,"AcquisitionDateTime",false);
		removeOrAnonymizeJsonField(jsonObj,"DeviceSerialNumber",false);
		removeOrAnonymizeJsonField(jsonObj,"ImageComments",true);
		removeOrAnonymizeJsonField(jsonObj,"InstitutionAddress",false);
		removeOrAnonymizeJsonField(jsonObj,"InstitutionName",false);
		removeOrAnonymizeJsonField(jsonObj,"PatientBirthDate",true);
		removeOrAnonymizeJsonField(jsonObj,"PatientID",true);
		removeOrAnonymizeJsonField(jsonObj,"PatientName",true);
		removeOrAnonymizeJsonField(jsonObj,"PatientSex",true);
		removeOrAnonymizeJsonField(jsonObj,"PatientWeight",true);
		removeOrAnonymizeJsonField(jsonObj,"ProcedureStepDescription",false);
		removeOrAnonymizeJsonField(jsonObj,"ReferringPhysicianName",true);
		removeOrAnonymizeJsonField(jsonObj,"SeriesInstanceUID",false);
		removeOrAnonymizeJsonField(jsonObj,"StationName",false);
		removeOrAnonymizeJsonField(jsonObj,"StudyID",false);
		removeOrAnonymizeJsonField(jsonObj,"StudyInstanceUID",false);
	}

	private void removeOrAnonymizeJsonField(JsonObject jsonObj, String fieldStr, boolean isRemove) {
		if (jsonObj.has(fieldStr)) {
			jsonObj.remove(fieldStr);
			if (!isRemove) {
				jsonObj.addProperty(fieldStr, "ANONYMIZED");
			}
		}
	}

}
