package org.nrg.hcp.releaserules.projectutils.ccf_ecp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_bwh.UnprocResourceGenerator_CCF_BWH;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.security.UserI;


@CcfResourceGenerator(description = "CCF_ECP Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_ECP extends UnprocResourceGenerator_CCF_BWH {
	
	protected final static String[] t1t2SeriesDescriptions = { "T1w_MPRAGE", "T2w_CUBE" };
	
	public UnprocResourceGenerator_CCF_ECP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	}
	

	@Override
	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		// ECP specific change.  Don't know about shims ECP, so we'll include them as "OTHER" files.
		final Map<String, List<String>> strucSubdirMap = new HashMap<>();
		strucSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(_InitialFrames).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, strucSubdirMap, ci);
	}
	
}
