package org.nrg.hcp.releaserules.projectutils.ccf_aabc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.utils.ScanUtils;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;


import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_AABC Project Session Building Release Rules")
public class ReleaseRules_CCF_AABC extends ReleaseRules_CCF_HCA {
	
	//private static final Logger _logger = LoggerFactory.getLogger(ReleaseRules_CCF_AABC.class);
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		//rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		//rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	
	public ReleaseRules_CCF_AABC() {
		super();
		HIRESSCANTYPE_RGX = "(?i)^TSE(_Norm|_Dis3D)*_HiResHp.*$"; 
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String selectionCriteria = params.get("selectionCriteria");
		log.debug("SelectionCriteria Parameter Value:  " + selectionCriteria);
		final List<String> validCriteria = Arrays.asList(SELECTION_CRITERIA);
		for (final XnatMrsessiondata exp : projectExpts) {
			if (exp.getLabel().contains("_7T") || exp.getLabel().contains("MRS")) {
				continue;
			}
			for (final String criteria : validCriteria) {
				if (selectionCriteria != null && selectionCriteria.equals(criteria) && exp.getLabel().contains("_" + selectionCriteria + "_")) {
					log.debug("Adding experiment to filteredExpts list:  " + exp.getLabel());
					filteredExpts.add(exp);
				}
			}
			if (selectionCriteria == null || selectionCriteria.length()<1) {
				// Currently, let's default to V1 session if no parameter is passed.  Soon we may want to require the parameter.
				if (exp.getLabel().toUpperCase().contains("_V1_")) {
					filteredExpts.add(exp);
				}
			} else if (!validCriteria.contains(selectionCriteria)) {
				throw new ReleaseRulesException("ERROR:  Invalid selection criteria value (" + selectionCriteria + ")");
			}
		}
		return filteredExpts;
	}
	

	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		super.setTypeDescriptionAndOthers(subjectScans);
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getSeriesDescription().contains("_ND")) {
				scan.setDbdesc(scan.getDbdesc().replace("_ND", ""));
			} else if (scan.getType().contains("TSE_Dis3D_")) {
				scan.setDbdesc(scan.getType());
			}
		}
	}	
	

	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
            final boolean isXa30Scan = ScanUtils.isXa30Scan(scan);
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                int FmriMultiplier = (isXa30Scan) ? 72 : 1;
                int DmriMultiplier = (isXa30Scan) ? 92 : 1;
                int AslMultiplier = (isXa30Scan) ? 60 : 1;
                int frames10 = frames - (10*FmriMultiplier);
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=(488*FmriMultiplier)) ? "Y" : "N";
               		scanPct = (double)frames10/((488-10)*FmriMultiplier);
                	scan100 = (frames10>=(100*FmriMultiplier)) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		if (!isXa30Scan) {
	                		getErrorList().add("RULESERROR:  VE11 rfMRI scans should have at least 100 frames not counting the 10 initial " +
	                				"frames (110 total) to have been marked usable (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
	                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		} else {
	                		getErrorList().add("RULESERROR:  XA30 rfMRI scans should have at least 7200 frames not counting the 720 initial " +
	                				"frames (7920 total) to have been marked usable (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
	                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=(dirNum*DmriMultiplier)) ? "Y" : "N";
                		scanPct = (double)frames/(dirNum*DmriMultiplier);
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_VISMOTOR")) {
                		scanComplete = (frames>=(194*FmriMultiplier)) ? "Y" : "N";
                		scanPct = (double)frames10/((194-10)*FmriMultiplier);
                		if (frames<(155*FmriMultiplier)) {
                			getErrorList().add("RULESERROR:  tfMRI VISMOTOR scans should have at least " + 
                					155*FmriMultiplier + " frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_CARIT")) {
                		scanComplete = (frames>=(300*FmriMultiplier)) ? "Y" : "N";
                		scanPct = (double)frames10/((300-10)*FmriMultiplier);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI CARIT scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_FACENAME")) {
                		scanComplete = (frames>=(345*FmriMultiplier)) ? "Y" : "N";
                		scanPct = (double)frames10/((345-10)*FmriMultiplier);
                		if (frames<(315*FmriMultiplier)) {
                			getErrorList().add("RULESERROR:  tfMRI FACENAME scans should have at least " +
                					315*FmriMultiplier + " frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } else if (currDbdesc.matches(PCASL_RGX)) { 
                	if (currDbdesc.contains("hr_")) {
                		scanComplete = (frames>=(90*AslMultiplier)) ? "Y" : "N";
                		scanPct = (double)frames/(90*AslMultiplier);
                	} else {
                		scanComplete = (frames>=(70*AslMultiplier)) ? "Y" : "N";
                		scanPct = (double)frames/(70*AslMultiplier);
                	} 
                }
                if (scanPct>0) {
                	scanPct = ((scanPct*100)>100) ? 1 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}

	
}

