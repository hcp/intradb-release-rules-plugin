package org.nrg.hcp.releaserules.projectutils.ccf_acp;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseFileHandler_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_ACP Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_ACP extends ReleaseFileHandler_CCF_HCA {
	
	{
		getPathMatchRegex().add("^.*/LINKED_DATA/PSYCHOPY/.*.txt");
		getPathMatchRegex().add("^.*/LINKED_DATA/PSYCHOPY/.*.edat2");
	}
	
	public ReleaseFileHandler_CCF_ACP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
	}

}
