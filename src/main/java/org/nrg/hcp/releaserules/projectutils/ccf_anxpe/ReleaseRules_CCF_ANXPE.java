package org.nrg.hcp.releaserules.projectutils.ccf_anxpe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_ANXPE Project Session Building Release Rules")
public class ReleaseRules_CCF_ANXPE extends ReleaseRules_CCF_HCA {
	
	public String FMRI_RGX = "(?i)^.*fMRI.*"; 
	
	public ReleaseRules_CCF_ANXPE() {
		super();
		SESSION_LABEL_EXT = "_MR";
	    SELECTION_CRITERIA = new String[] { "" };
	}

	@Override
	public List<String> getParametersYaml() {
		return ImmutableList.of();
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		// Currently just one MR per subject and occasionally an optional EXTRA session for ANXPE.  We'll just add them all.
		filteredExpts.addAll(projectExpts);
		return filteredExpts;
	}
	
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + SESSION_LABEL_EXT; 
	}
	
	
	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("TSE_Norm_")) {
					// For ANXPE, we're renaming the series descriptions for the HiResHp scans to TSE_Norm_HiResHp (the scan type).
					scan.setDbdesc(scan.getType());
				}
			}
			String dbDesc = scan.getDbdesc()
					.replace("_AP1", "1_AP")
					.replace("_AP2", "2_AP")
					.replace("_PA1", "1_PA")
					.replace("_PA2", "2_PA")
					;
			scan.setDbdesc(dbDesc);
		}
	}
	

	@Override
	protected void checkForNormalizedScans(final List<XnatMrscandata> subjectScans) {
		// Throw error if Structural scan doesn't contain a normalized scan as the following scan
		for (final XnatMrscandata scan : subjectScans) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if (isStructuralScan(scan) && !isNormalizedScan(scan) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
				if (!(isNormalizedScan(nextScan))) {
					getErrorList().add("RULESERROR:  Structural scan is not followed by a normalized scan (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
				}
			} 
			/*
			 * ANXPE difference:  ANXPE doesn't have/keep non-normalized HiResHp scan.
			 * 
			if (isHiresScan(scan) && isNormalizedScan(scan) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				final XnatMrscandata previousScan = getPreviousScan(subjectScans,scan);
				if (!isHiresScan(previousScan) || isNormalizedScan(previousScan)) {
					getErrorList().add("RULESERROR:  Normalized TSE HiRes scan is not preceeded " +
								"by a non-normalized HiRes scan (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
				}
			}
			*/
		}
	}
	
	
	@Override
	protected void updateNonNormalizedInclusionAndQuality(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
				final String quality = scan.getQuality();
				final Boolean targetForRelease = scan.getTargetforrelease();
				final Integer releaseOverride = scan.getReleaseoverride();
				final XnatMrscandata prevScan = getPreviousScan(subjectScans, scan); 
				// ANXPE difference.  There is no non-normalized HiRes scan, so we don't set quality on previous scan.
				if ((isMainStructuralScan(prevScan)) && isNormalizedScan(scan)) {
					prevScan.setQuality(quality);
					prevScan.setTargetforrelease(targetForRelease);
					prevScan.setReleaseoverride(releaseOverride);
				}
			}
		}
	}
	
	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            // NOTE HERE:  Let's not skip this code altogether so fieldmap still gets picked based on shims.  Let's just skip
            // outputting the error for non-rfMRI
			//if (currDbdesc == null || !(currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
			//	// For ANXPE, only shim values of fmri scans normally match fieldmaps.
			//	continue;
			//}
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					if (scan.getType().matches(FIELDMAP_RGX)) {
 						getErrorList().add("RULESERROR:  Fieldmap pair has non-matching shim values (" +
 							"SESSION=" + 
 								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
 								", FieldMapGroup=" + fmGroup +")");
 						continue;
					}
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							// For ANXPE, only shim values of fmri scans normally match fieldmaps.
							if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
								getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
										"so a new fieldmap group has been chosen. (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
										", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							}
						}
					}
					if (!newGroup) {
						// For ANXPE, only shim values of fmri scans normally match fieldmaps.
						if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
							getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
					}
				}
			}
		}
	}
	

	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
					} else {
						intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
						if (restCount>2) {
							getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						// ANXPE difference.  Sessions normally have two groups of scans.
						//if (intradbSessionRestCount>2) {
						//	getErrorList().add("RULESERROR:  Intradb session contains more than two usable Resting State scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
						if (restGroupCount>1) {
							getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
					inRest=true;
				} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}
	
	
	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=420) ? "Y" : "N";
               		scanPct = (double)frames10/(420-10);
               		scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA/HCD, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.toUpperCase().contains("_CONFLICT")) {
                		scanComplete = (frames>=290) ? "Y" : "N";
                		scanPct = (double)frames10/(290-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Conflict scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_GAMBLING")) {
                		scanComplete = (frames>=228) ? "Y" : "N";
                		scanPct = (double)frames10/(228-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Gambling scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_FACES")) {
                		scanComplete = (frames>=340) ? "Y" : "N";
                		scanPct = (double)frames10/(340-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI FaceMatching scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	/** Wrong protocol.  Scans set to unusable.
                	} else if (currDbdesc.toUpperCase().contains("_REWARD")) {
                		scanComplete = (frames>=450) ? "Y" : "N";
                		scanPct = (double)frames10/(450-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Reward scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                		**/
                	} 
                } 
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("CARIT")) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	
	
	/*
	protected void setPEFields(XnatMrscandata scan,String seriesDesc) {
		try {
			//Float ipe_rotation = null;
			// 2018-08-13 - We are now pulling PE direction from scan NIFTI JSON rather than calculating it.
			// Per e-mails with Mike Harms, Leah, et. al, we will throw an error if ipe_rotation is missing, but if we 
			// override the error, we will treat the ipe_rotation value as zero
			for (final XnatAbstractresourceI resourceI : scan.getFile()) {
				if (!(resourceI instanceof XnatResource && resourceI.getLabel().equalsIgnoreCase(CommonConstants.NIFTI_RESOURCE))) {
					continue;
				}
				final XnatResource resource = (XnatResource)resourceI;
				if (resource == null || resource.getFileResources(CommonConstants.ROOT_PATH) == null || 
						resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
					continue;
				}
				for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
					final File f = rFile.getF();
					if (!f.getName().toLowerCase().endsWith(".json")) {
						continue;
					}
					final String json = FileUtils.readFileToString(f);
					Map<String,Object> jsonMap = gson.fromJson(json, new TypeToken<Map<String,Object>>(){}.getType());
					if (jsonMap.containsKey(PHASE_ENCODING_DIRECTION_KEY)) {
						final Object peDirection = jsonMap.get(PHASE_ENCODING_DIRECTION_KEY);
						if (peDirection != null) {
							scan.setParameters_pedirection(peDirection.toString());
							checkPESeriesDesc(scan);
							return;
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown retrieving PhaseEncodingDirection - " + e.toString());
		}
		if (scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(FMRI_RGX) || scan.getType().matches(DMRI_RGX)) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			//getErrorList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SESSION=" + 
			getErrorList().add("RULESERROR:  PHASE ENCODING DIRECTION IS NULL (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_phaseencodingdirection() + ")");
		}
	}
	*/
	
}

