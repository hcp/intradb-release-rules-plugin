package org.nrg.hcp.releaserules.projectutils.ccf_hca;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.abst.AbstractCcfReleaseRules;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.framework.node.XnatNode;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.HcpToolboxdata;
import org.nrg.xdat.om.NtScores;
import org.nrg.xdat.om.HcpvisitHcpvisitdata;
import org.nrg.xdat.om.HcpSubjectmetadata;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_HCA Project Session Building Release Rules")
//public class ReleaseRules_CCF_HCA extends AbstractCcfReleaseRules implements HcpReleaseRulesI, CcfReleaseRulesI {
public class ReleaseRules_CCF_HCA extends AbstractCcfReleaseRules implements CcfReleaseRulesI {
	
	//final Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
	public final Gson gson = new GsonBuilder().create();
	public final XnatNode _xnatNode = XDAT.getContextService().getBean(XnatNode.class);
	public String[] PHASE_ENCODING_DIRECTION_KEYS = { "PhaseEncodingDirection" };
	
	public String  LOCALIZER_RGX = "^Localizer.*", 
							SCOUT_RGX = "^AAHScout.*",
							FIELDMAP_RGX = "^FieldMap.*",
							ANYFIELDMAP_RGX = "(?i)^.*FieldMap.*",
							GEFIELDMAP_RGX = "(?i)^FieldMap_((Ma)|(Ph)).*",
							SEFIELDMAP_RGX = "^FieldMap_SE_.*",
							PCASL_RGX = "^.*PCASL.*$",
							SPINECHO_RGX = "(?i)^SpinEchoFieldMap.*",
							STRUCT_RGX = "(?i)^T[12]w.*",
							T1_RGX = "(?i)^T1w$",
							T1_NORM_RGX = "(?i)^T1w_Norm$",
							T1_ANY = "(?i)^T1w.*$",
							T2_RGX = "(?i)^T2w$", 
							T2_NORM_RGX = "(?i)^T2w_Norm$", 
							T2_ANY = "(?i)^T2w.*$", 
							NORM_RGX = "(?i)^_Norm.*", 
							NORMTYPE_ENDING = "_Norm", 
							NORMPARM_ENDING = "NORM", 
							T1MPR_RGX = "(?i)^T1w_MPR[0-9]*.*$",
							T2SPC_RGX = "(?i)^T2w_SPC[0-9]*.*$",
							TFMRI_RGX = "(?i)^tfMRI.*", 
							RFMRI_RGX = "(?i)^rfMRI.*", 
							DMRI_RGX = "(?i)^dMRI.*", 
							SBREF_RGX = "(?i)^.*_SBRef$", 
							AP_RGX = "(?i)^.*_AP(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
							PA_RGX = "(?i)^.*_PA(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
							APPA_RGX = "(?i)_((PA)|(AP))(($)|(_.*$))", 
							APPAONLY_RGX = "(?i)_((AP)|(PA))_", 
							TFMRINO_RGX = "(?i)[0-9]_((AP)|(PA)).*$", 
							MAINSCAN_RGX = "(?i)((T[12]w)|(tfMRI)|(rfMRI)|(dMRI))",
							STRUCTMAIN_RGX = "(?i)((T[12]w))",
							MAINSCANDESC_RGX = "(?i)^((T[12]w)|(tfMRI)|(rfMRI)|(dMRI)).*$",
							STRUCTSCANDESC_RGX = "(?i)^((T[12]w)).*$",
							PHYSIO_RGX = "(?i)^.*_PhysioLog.*$", 
							SETTER_RGX = "(?i)^.*_setter$", 
							MAINSTRUCSCANTYPE_RGX = "(?i)^T[12]w(_Norm)*$", 
							OTHERSTRUCSCANTYPE_RGX = "(?i)^T[12]w(_Norm)*_4e$", 
							HIRESSCANTYPE_RGX = "(?i)^TSE(_Norm)*_HiResHp.*$", 
							OTHER_RGX = "(?i)^.*MPR.*Collection.*$",
							SESSION_LABEL_EXT = "_V1_MR"
					;
	
	public final String[] validQualityRatings = {"undetermined","usable","unusable","excellent","good","fair","poor"};
	public String[] structuralQualityRatings = {"excellent","good","fair","poor"};
	protected String[] SELECTION_CRITERIA = {"V1","V2","V3","V4"};
	protected String[] SINGLETON_CRITERIA = {"ALL"};

	public final List<String> softErrorList = new ArrayList<>();
	public final List<String> hardErrorList = new ArrayList<>();
	public final List<String> warningList = new ArrayList<>();
	public final List<XnatMrsessiondata> subjectSessions = new ArrayList<>();
	private Object _lock = new Object();
	protected boolean errorOverride;
	
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		//rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		//rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: V1\n" +
				"    label: Select Visit\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	sessionLabel = sessionLabel.replace(/_V[123]_/,'_' + newCriteria + '_');\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        'V1': 'V1'\n" +
				"        'V2': 'V2'\n" +
				"        'V3': 'V3'\n" +
				"        'V4': 'V4'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<String> applyRulesToScans(
			final List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions,
			final Map<String,String> params, final boolean errorOverride)
			throws ReleaseRulesException {
		// Remove especially bad scans up front
		final Iterator<XnatMrscandata> i = subjectScans.iterator();
		while (i.hasNext()) {
			final XnatMrscandata scan = i.next();
			if (scan == null || scan.getType() == null || scan.getSeriesDescription() == null) {
				i.remove();
			}
		}
		return applyRulesToScans(subjectScans, subjectSessions, errorOverride);
	}

	//@Override
	//public void applyRulesToSubjectMetaData(final HcpSubjectmetadata subjMeta,
	//		final SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap,
	//		final String selectionCriteria, final HcpToolboxdata tboxexpt, final NtScores ntexpt,
	//		final HcpvisitHcpvisitdata visitexpt, final boolean errorOverride)
	//		throws ReleaseRulesException {
	//	// NOTE:  Currently LS_Phase1a does not use the selectionCriteria parameter.  All sessions are combined into a single 3T session.
	//	applyRulesToSubjectMetaData(subjMeta,scanMap,tboxexpt,ntexpt,visitexpt,errorOverride);
	//	
	//}

	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String selectionCriteria = params.get("selectionCriteria");
		log.debug("SelectionCriteria Parameter Value:  " + selectionCriteria);
		final List<String> validCriteria = Arrays.asList(SELECTION_CRITERIA);
		for (final XnatMrsessiondata exp : projectExpts) {
			for (final String criteria : validCriteria) {
				if (selectionCriteria != null && selectionCriteria.equals(criteria) && exp.getLabel().contains("_" + selectionCriteria + "_")) {
					log.debug("Adding experiment to filteredExpts list:  " + exp.getLabel());
					filteredExpts.add(exp);
				}
			}
			if (selectionCriteria == null || selectionCriteria.length()<1) {
				// Currently, let's default to V1 session if no parameter is passed.  Soon we may want to require the parameter.
				if (exp.getLabel().toUpperCase().contains("_V1_")) {
					filteredExpts.add(exp);
				}
			} else if (!validCriteria.contains(selectionCriteria)) {
				throw new ReleaseRulesException("ERROR:  Invalid selection criteria value (" + selectionCriteria + ")");
			}
		}
		return filteredExpts;
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		final String sc = params.get("selectionCriteria");
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + 
				((sc != null && sc.length()>0 && 
					!Arrays.asList(SINGLETON_CRITERIA).contains(sc)) ? "_" + sc + "_MR" : SESSION_LABEL_EXT);
	}
	
	@Override
	public boolean requireScanUidAnonymization() {
		return true;
	}
	
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		return applyRulesToScans(subjectScans, subjectSessions, errorOverride, false);
		
	}
	
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride, boolean skipClearList) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			if (!skipClearList) {
				clearWarningAndErrorLists();
			}
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			updateT1T2InclusionAndDesc(subjectScans); 
			updateNonNormalizedInclusionAndQuality(subjectScans);
			flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			flagScansWithMissingFieldmapGroup(subjectScans);
			flagIfNotAllDmriInSameShimGroup(subjectScans);
			markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			setScanOrderAndScanComplete(subjectScans); 
			updatePCASLInclusion(subjectScans);
			removeFieldmapScansWithoutMainScan(subjectScans);
			removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			checkSbrefScanUnusableIfMainScanUnusable(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride && 
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
	}

	protected List<String> getErrorList() {
		return (errorOverride) ? softErrorList : hardErrorList;
	}

	protected List<String> getWarningList() {
		return warningList;
	}

	protected List<String> getWarningAndErrorSummaryList() {
		final List<String> returnList = new ArrayList<>(hardErrorList);
		returnList.addAll(softErrorList);
		returnList.addAll(warningList);
		return returnList;
	}

	protected void clearWarningAndErrorLists() {
		warningList.clear();
		softErrorList.clear();
		hardErrorList.clear();
	}

	protected void fixBackslashProblem(final List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			// Replace multiple backslash instances with a single backslash
			if (scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains("\\\\")) {
				final String imageType = scan.getParameters_imagetype().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_imagetype(imageType);
			}
			if (scan.getParameters_seqvariant()!=null && scan.getParameters_seqvariant().contains("\\\\")) {
				final String seqVariant = scan.getParameters_seqvariant().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_seqvariant(seqVariant);
			}
			if (scan.getParameters_scansequence()!=null && scan.getParameters_scansequence().contains("\\\\")) {
				final String scanSequence = scan.getParameters_scansequence().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scansequence(scanSequence);
			}
			if (scan.getParameters_scanoptions()!=null && scan.getParameters_scanoptions().contains("\\\\")) {
				final String scanOptions = scan.getParameters_scanoptions().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scanoptions(scanOptions);
			}
		}
	}

	protected void nullPriorValues(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			scan.setSubjectsessionnum(null);
			scan.setDatarelease(null);
			scan.setDbsession(null);
			scan.setDbid(null);
			scan.setDbtype(null);
			scan.setDbdesc(null);
			scan.setReleasecountscan(null);
			scan.setReleasecountscanoverride(null);
			scan.setTargetforrelease(null);
			// IMPORTANT:  Do not null out releaseOverride.  It is set by users, not by this class to force publish/non-publish.
			scan.setParameters_protocolphaseencodingdir(null);
			scan.setParameters_shimgroup(null);
			scan.setParameters_sefieldmapgroup(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_gefieldmapgroup(null);
			scan.setParameters_perotation(null);
			scan.setParameters_peswap(null);
			scan.setParameters_pedirection(null);
			scan.setParameters_readoutdirection(null);
			scan.setParameters_eprimescriptnum(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_scanorder(null);
			scan.setScancomplete(null);
			scan.setViewscan(null);
		}
	}

	protected void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				log.error("ERROR: scan (key) should not be null!!!");
				log.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				log.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getSeriesDescription().matches(PHYSIO_RGX) ||
					(scan.getSeriesDescription().matches(SETTER_RGX) && !scan.getParameters_imagetype().contains("MOSAIC")) ||
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// Per M.Harms, 2018/01/12, let's only include specific scan types, so random ones that are sometimes collected
			// are excluded.
			if (!(isPcaslScan(scan) || isSetter(scan) || isHiresScan(scan) || 
					scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(MAINSCANDESC_RGX))) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  " + ((isMainStructuralScan(scan)) ? "Structural" : "TSE HiRes")  +
								" normalized scan quality rating is invalid or scan has not been rated (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Scan quality rating is invalid (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			// If scan specifically set to Don't release, then let's count it but not release it
			if (isDontRelease(scan)) {
				scan.setReleasecountscan(true);
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setReleasecountscan(true);
		}
	}

	protected void checkForNormalizedScans(final List<XnatMrscandata> subjectScans) {
		// Throw error if Structural scan doesn't contain a normalized scan as the following scan
		for (final XnatMrscandata scan : subjectScans) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if (isStructuralScan(scan) && !isNormalizedScan(scan) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
				if (nextScan != null && !(isNormalizedScan(nextScan))) {
					getErrorList().add("RULESERROR:  Structural scan is not followed by a normalized scan (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
				}
			} if (isHiresScan(scan) && isNormalizedScan(scan) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				final XnatMrscandata previousScan = getPreviousScan(subjectScans,scan);
				if (!isHiresScan(previousScan) || isNormalizedScan(previousScan)) {
					getErrorList().add("RULESERROR:  Normalized TSE HiRes scan is not preceeded " +
								"by a non-normalized HiRes scan (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
				}
			}
		}
	}

	protected boolean isNormalizedScan(XnatMrscandata scan) {
		if (scan == null || scan.getType() == null || scan.getParameters_imagetype() == null) {
		    return false;
		}
		if (scan.getType().contains("_Norm") && scan.getParameters_imagetype().contains("NORM")) {
			return true;
		}
		return false;
	}

	protected XnatMrscandata getNextScan(List<XnatMrscandata> subjectScans, XnatMrscandata scan) {
		final XnatMrscandata[] scanArray = subjectScans.toArray(new XnatMrscandata[subjectScans.size()]);
		// NOTE:  Returns null for last scan in array
		for (int i=0; i<(scanArray.length-1); i++) {
			if (scan.equals(scanArray[i])) {
				return scanArray[i+1];
			}
		}
		return null;
	}

	protected XnatMrscandata getPreviousScan(List<XnatMrscandata> subjectScans, XnatMrscandata scan) {
		final XnatMrscandata[] scanArray = subjectScans.toArray(new XnatMrscandata[subjectScans.size()]);
		// NOTE:  Returns null for first scan in array
		for (int i=1; i<scanArray.length; i++) {
			if (scan.equals(scanArray[i])) {
				return scanArray[i-1];
			}
		}
		return null;
	}

	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting 
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					(scan.getQuality().equalsIgnoreCase("poor") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					scan.getQuality().equalsIgnoreCase("fair")) && ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan))
				) {
           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
           		scan.setTargetforrelease(false);
				continue;
			}
			if (isDontRelease(scan)) { 
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
						(isMainStructuralScan(scan) && isNormalizedScan(scan))) {
					if (isT1NormalizedScan(scan)) {
						hasGoodT1 = true;
					} else if (isT2NormalizedScan(scan)) {
						hasGoodT2 = true;
					}
					//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
					//	prevScan.setTargetforrelease(true);
					//	prevScan.setQuality(scan.getQuality());
					//}
				}
			}
			if (!hasGoodT1 || !hasGoodT2) {
				for (final XnatMrscandata scan : subjectScans) {
					if (isDontRelease(scan)) { 
						continue;
					}
					if ((!hasGoodT1 && isT1NormalizedScan(scan)) || (!hasGoodT2 && isT2NormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							scan.setTargetforrelease(true);
							//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
							//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
							//	prevScan.setTargetforrelease(true);
							//	prevScan.setQuality(scan.getQuality());
							//}
						}
					}
				}
			}
		}
		if (!hasValidHiresScan(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
				if (isDontRelease(scan)) { 
					continue;
				}
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("fair")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		if (!hasValidHiresScan(subjectScans)) {
			// 2018-08-13.  We'll release even poor HiRes scans if we don't have one of higher quality.
			for (final XnatMrscandata scan : subjectScans) {
				if (isDontRelease(scan)) { 
					continue;
				}
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("poor")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		if (!hasValidT1AndT2(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
           		setNoRelease(scan);
			}
		}
	}
	
	protected boolean isT1NormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T1_NORM_RGX)) {
			return true;
		}
		return false;
	}
	
	protected boolean isT1Type(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(T1_RGX) || 
				(normAsStruc && isT1NormalizedScan(scan))) {
			return true;
		}
		return false;
	}
	
	protected boolean isT1Any(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T1_ANY)) {
			return true;
		}
		return false;
	}
	
	protected boolean isT2NormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T2_NORM_RGX)) {
			return true;
		}
		return false;
	}
	
	protected boolean isT2Type(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(T2_RGX) ||
				(normAsStruc && isT2NormalizedScan(scan))) {
			return true;
		}
		return false;
	}
	
	protected boolean isT2Any(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T2_ANY)) {
			return true;
		}
		return false;
	}

	protected boolean hasValidHiresScan(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan) && isNormalizedScan(scan)) {
				if (isHiresScan(scan)) {
					return true;
				}
			}
		}
		return false;
	}

	protected boolean hasValidT1AndT2(List<XnatMrscandata> subjectScans) {
		boolean hasT1 = false;
		boolean hasT2 = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan) && isNormalizedScan(scan)) {
				if (isT1NormalizedScan(scan)) {
					hasT1 = true;
				} else if (isT2NormalizedScan(scan)) {
					hasT2 = true;
				}
				if (hasT1 && hasT2) {
					return true;
				}
			}
		}
		return false;
	}

	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					if (scan.getDbdesc().contains("_4e")) {
						scan.setDbdesc(scan.getDbdesc().replace("_4e", "_Norm_4e"));
					} else {
						scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
					}
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("TSE_")) {
					scan.setDbdesc(scan.getDbdesc().replace("TSE_", "TSE_Norm_"));
				}
			}
		}
	}

	protected void fieldmapsMustBeReleasedInPairs(List<XnatMrscandata> subjectScans) {
		if (true) {
			//return;
		}
		final List<XnatMrscandata> apList = new ArrayList<>();
		final List<XnatMrscandata> paList = new ArrayList<>();
		boolean hasReleasableAP = false;
		boolean hasReleasablePA = false;
		boolean inFieldmapGroup = false;
		Iterator<XnatMrscandata> i = subjectScans.iterator();
		while (i.hasNext()) {
		    final XnatMrscandata scan = i.next();
			if (scan.getType().matches(FIELDMAP_RGX)) {
				inFieldmapGroup = true;
				if (scan.getSeriesDescription().matches(AP_RGX)) {
					apList.add(scan);
					if (willBeReleased(scan)) {
						hasReleasableAP=true;
					}
				}else if (scan.getSeriesDescription().matches(PA_RGX)) {
					paList.add(scan);
					if (willBeReleased(scan)) {
						hasReleasablePA=true;
					}
				}
			}
			if (inFieldmapGroup && (!scan.getType().matches(FIELDMAP_RGX) || !i.hasNext())) {
				inFieldmapGroup = false;
				if (hasReleasableAP!=hasReleasablePA) {
					if (hasReleasableAP) {
						for (final XnatMrscandata apScan : apList) {
							setNoRelease(apScan);
						}
					} else if (hasReleasablePA) {
						for (final XnatMrscandata paScan : paList) {
							setNoRelease(paScan);
						}
					}
				}
				hasReleasableAP = false;
				hasReleasablePA = false;
				apList.clear();
				paList.clear();
			}
		}
		return;
	}

	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
					} else {
						intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
						if (restCount>2) {
							getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (intradbSessionRestCount>2) {
							getErrorList().add("RULESERROR:  Intradb session contains more than two usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (restGroupCount>1) {
							getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
					inRest=true;
				} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}
	
	protected void updateT1T2InclusionAndDesc(List<XnatMrscandata> subjectScans) {
		// Target T1, T2 from session that has both
		final ArrayList<Integer> t1List = new ArrayList<Integer>();
		final ArrayList<Integer> t2List = new ArrayList<Integer>();
		final ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getErrorList().add("RULESERROR:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getType().matches(T1_NORM_RGX) || scan.getType().matches(T2_NORM_RGX)) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (nextScan != null && isNormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					}
				}
			}
		}
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		// MRH 2022-03-17:  Change per Mike Harms.  We no longer care that T1/T2s have matching shims if we're not using the
		// fieldmaps.  MH explicitly prefers picking highest quality over matching shims per 03/16/22-03/17/22 e-mail thread.
		// We'll split shimGroup to t1ShimGroup and t2ShimGroup and just handle those separately.
		//char shimGroup = Character.MIN_VALUE;
		char t1ShimGroup = Character.MIN_VALUE;
		char t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : subjectScans) {
			// Keeping only 1 T1/T2, Flag error if more than one 
			// Keeping second only if in same shim group as first
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				if (t1Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t1Count++;
				}
				if (t1Count!=0) {
					if (t1ShimGroup == Character.MIN_VALUE) {
						t1ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
						if (t1ShimGroup == getShimGroup(scan)) {
							final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
							getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
									"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
						t1Count--;
						setNoRelease(scan);
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setNoRelease(prevScan);
						}
					} else {
					    setRDFields(scan);
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				if (t2Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t2Count++;
				}
				if (t2Count!=0) {
					if (t2ShimGroup == Character.MIN_VALUE) {
						t2ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
						if (t2ShimGroup == getShimGroup(scan)) {
							final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
							getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
									"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
						t2Count--;
						setNoRelease(scan);
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setNoRelease(prevScan);
						}
					} else {
					    setRDFields(scan);
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
		}
		t1Count=0;
		t2Count=0;
		//shimGroup = Character.MIN_VALUE;
		t1ShimGroup = Character.MIN_VALUE;
		t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1ShimGroup == Character.MIN_VALUE) {
					t1ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
					if (t1ShimGroup == getShimGroup(scan)) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
									subjectSession.getLabel() +
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
					t1Count--;
					setNoRelease(scan);
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
					}
				} else {
				    setRDFields(scan);
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2ShimGroup == Character.MIN_VALUE) {
					t2ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
					if (t2ShimGroup == getShimGroup(scan)) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
					t2Count--;
					setNoRelease(scan);
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
					}
				} else {
				    setRDFields(scan);
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
		}
	}		

	protected void setRDEchoScans(List<XnatMrscandata> subjectScans, XnatMrscandata prevScan) {
		final XnatMrscandata prevPrevScan = getPreviousScan(subjectScans,prevScan);
		if (prevPrevScan != null && is4eScan(prevPrevScan)) {
			setRDFields(prevPrevScan);
			final XnatMrscandata prevPrevPrevScan = getPreviousScan(subjectScans,prevPrevScan);
			if (is4eScan(prevPrevPrevScan)) {
				setRDFields(prevPrevPrevScan);
			}
		}
	}

	protected boolean is4eScan(XnatMrscandata scan) {
		final String scanType = scan.getType();
		return scanType != null && scanType.endsWith("_4e");
	}
	
	protected void updateNonNormalizedInclusionAndQuality(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
				final String quality = scan.getQuality();
				final Boolean targetForRelease = scan.getTargetforrelease();
				final Integer releaseOverride = scan.getReleaseoverride();
				final XnatMrscandata prevScan = getPreviousScan(subjectScans, scan); 
				if ((isMainStructuralScan(prevScan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
					prevScan.setQuality(quality);
					prevScan.setTargetforrelease(targetForRelease);
					prevScan.setReleaseoverride(releaseOverride);
				}
			}
		}
	}

	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				final String currDbdesc = scan.getDbdesc();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					if (scan.getType().matches(FIELDMAP_RGX)) {
 						getErrorList().add("RULESERROR:  Fieldmap pair has non-matching shim values (" +
 							"SESSION=" + 
 								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
 								", FieldMapGroup=" + fmGroup +")");
 						continue;
					}
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
									"so a new fieldmap group has been chosen. (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
					}
					if (!newGroup) {
						if (!currDbdesc.matches(DMRI_RGX)) {
 							getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
 									"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
 									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						} else {
							getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap (diffusion scan = warning only), and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
					}
				}
			}
		}
	}


	protected void flagScansWithMissingFieldmapGroup(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				final String currDbdesc = scan.getDbdesc();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_sefieldmapgroup(thisFm);
						getWarningList().add("RULESWARNING:  Scan's nornally assigned fieldmap either doesn't exist or is unusable, " +
								"so a new fieldmap group has been chosen. (SESSION=" +
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					if (currDbdesc != null && !currDbdesc.matches(DMRI_RGX)) {
						getErrorList().add("RULESERROR:  Scan's normally selected fieldmap either doesn't exist or is unusable, " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					} else if (currDbdesc != null) {
						getWarningList().add("RULESWARNING:  Scan's normally selected fieldmap either doesn't exist or is unusable " +
								"(diffusion scan = warning only), " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}

	protected void flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(List<XnatMrscandata> subjectScans)
			throws ReleaseRulesException {
		Integer compFieldmapGroup = null;
		String compShimGroup = null;
		XnatMrscandata compScan = null;
		int releaseCount = 0;
		int requiresOverrideCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				releaseCount++;
				if (requiresOverride(scan)) {
					requiresOverrideCount++;
				}
				if (releaseCount>3) {
					hardErrorList.add("RULESERROR:  Too many structural scans are marked for release.  Please " +
							"modify the release flag or usability for one or more scans.  This error should not be overridden.");
				}
				if (compFieldmapGroup == null || compShimGroup == null) {
					compFieldmapGroup = scan.getParameters_sefieldmapgroup();
					compShimGroup = scan.getParameters_shimgroup();
					compScan = scan;
				} else {
					final XnatMrsessiondata compSession = getScanSession(compScan, subjectSessions);
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					final Integer fmGroup = scan.getParameters_sefieldmapgroup();
					final String shimGroup = scan.getParameters_shimgroup();
					if (fmGroup != null && !fmGroup.equals(compFieldmapGroup)) {
						getErrorList().add("RULESERROR:  Structural scans to be released have different SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								scan.getParameters_sefieldmapgroup() + ")");
					} else if (fmGroup == null) {
						getErrorList().add("RULESERROR:  Structural scan to be released has null SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								fmGroup + ")");
					}
					if (shimGroup != null && !shimGroup.equals(compShimGroup)) {
						getErrorList().add("RULESERROR:  Structural scans to be released have different ShimGroup values (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								scan.getParameters_shimgroup() + ")");
					} else if (shimGroup == null) {
						getErrorList().add("RULESERROR:  Structural scan to be released has null ShimGroup value (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								shimGroup + ")");
					}
				}
			}
		}
		if (releaseCount<2) {
			hardErrorList.add("RULESERROR:  This session does not contain a complete T1/T2 pair marked for release.  " +
							"Note that this error requires a hard override in order to override.  Please edit scan " +
							"quality or use the release override flags to mark scans for release.");
		} else if (requiresOverrideCount>0) {
			getErrorList().add("RULESERROR:  T1/T2 pair marked for release requires a release scan-level override flag to be released. " +
							"This could be due to insufficient scan quality or mismatched shim or fieldmap groups.  The override flag" +
					        " has been supplied but the session errorOverride flag should be used to submit this session so this notice is logged.");
		}
	}

	protected void flagIfNotAllDmriInSameShimGroup(List<XnatMrscandata> subjectScans) {
		String compShimGroup = null;
		XnatMrscandata compScan = null;
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(DMRI_RGX)) { 
				if (compShimGroup == null) {
					compShimGroup = scan.getParameters_shimgroup();
					compScan = scan;
				} else {
					final XnatMrsessiondata compSession = getScanSession(compScan, subjectSessions);
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					final String shimGroup = scan.getParameters_shimgroup();
					if (shimGroup != null && !shimGroup.equals(compShimGroup)) {
						getErrorList().add("RULESERROR:  One or more diffusion scans to be released have different ShimGroup values (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								scan.getParameters_shimgroup() + ")");
					} else if (shimGroup == null) {
						getErrorList().add("RULESERROR:  Diffusion scan to be released has null ShimGroup value (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								shimGroup + ")");
					}
				}
			}
		}
	}

	protected void markAssociatedScansWithUnreleasedStructuralsAsUnreleased(
			List<XnatMrscandata> subjectScans) {
		// First find scans that will be released and keep their fieldmap values
		ArrayList<Integer> fmList = new ArrayList<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				Integer fieldMapGroup = scan.getParameters_sefieldmapgroup();
				if (!fmList.contains(fieldMapGroup)) { 
					fmList.add(fieldMapGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(STRUCT_RGX) && willBeReleased(scan) && 
					(scan.getParameters_sefieldmapgroup()==null || !fmList.contains(scan.getParameters_sefieldmapgroup()))) {
				setNoRelease(scan);
			}
		}
	}
				
	protected boolean scanShimMatchesFieldMaps(List<XnatMrscandata> subjectScans, XnatMrscandata comparescan) {
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) {
			if (scan.getSeriesDescription().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) &&
						scan.getParameters_sefieldmapgroup().equals(comparescan.getParameters_sefieldmapgroup())) {
				if (scan.getParameters_shimgroup().equals(comparescan.getParameters_shimgroup())) {
					return true;
				} else {
					return false;
				}
			} 
		}
		return false;
	}
	
	public boolean isTargetOrOverrideAll(XnatMrscandata scan) {
		if (isMainStructuralScan(scan) && isNormalizedScan(scan)) {
			if (isTargetOrOverrideStruc(scan)) {
				return true;
			}
		} else if (scan.getTargetforrelease()!=null) {
			return scan.getTargetforrelease();
		}
		return false;
	}
	
	public boolean isTargetOrOverrideStruc(XnatMrscandata scan) {
		final Boolean targetForRelease = scan.getTargetforrelease();
		final Integer releaseOverride = scan.getReleaseoverride();
		if ( (targetForRelease!=null && targetForRelease && (releaseOverride==null || !(releaseOverride<0))) ||
				(targetForRelease!=null && !targetForRelease && releaseOverride!=null && (releaseOverride>0))
			) {
			return true;
		}
		return false;
	}

	public boolean requiresOverride(XnatMrscandata scan) {
		final boolean targetForRelease = scan.getTargetforrelease();
		final Integer releaseOverride = scan.getReleaseoverride();
		return (targetForRelease==false && (releaseOverride!=null && releaseOverride>0));
	}

	public boolean willBeReleased(XnatMrscandata scan) {
		final boolean targetForRelease = scan.getTargetforrelease();
		final Integer releaseOverride = scan.getReleaseoverride();
		return (targetForRelease==true && (releaseOverride==null || releaseOverride>=0)) ||
				(targetForRelease==false && (releaseOverride!=null && releaseOverride>0));
	}

	public boolean isDontRelease(XnatMrscandata scan) {
		final Integer releaseOverride = scan.getReleaseoverride();
		return (releaseOverride!=null && releaseOverride<0);
	}

	public boolean isNoRelease(XnatMrscandata scan) {
		final boolean targetForRelease = scan.getTargetforrelease();
		final Integer releaseOverride = scan.getReleaseoverride();
		return (targetForRelease==false && (releaseOverride==null || releaseOverride<=0)) || 
				(targetForRelease==true && (releaseOverride!=null && releaseOverride<0));
	}

	protected void setNoRelease(XnatMrscandata scan) {
		// Do this instead of just setting targetforrelease to false to clear out other fields that
		// may have been set (Note that a few aren't included here that should remain)
		scan.setTargetforrelease(false);
		// If the override flag is set, we need to not clear out all these fields, especially Dbid, which
		// will cause exception to be thrown when saving session.
		if (scan.getReleaseoverride() == null || !(scan.getReleaseoverride() > 0)) {
			// Don't set SubjectSessionNum to null. It's used in logic.
			////scan.setSubjectsessionnum(null);
			scan.setDatarelease(null);
			scan.setDbsession(null);
			scan.setDbid(null);
			scan.setDbtype(null);
			scan.setDbdesc(null);
			scan.setParameters_protocolphaseencodingdir(null);
			scan.setParameters_sefieldmapgroup(null);
			scan.setParameters_gefieldmapgroup(null);
			scan.setParameters_perotation(null);
			scan.setParameters_peswap(null);
			scan.setParameters_pedirection(null);
			scan.setParameters_readoutdirection(null);
			scan.setParameters_eprimescriptnum(null);
			scan.setParameters_biasgroup(null);
		}
	}

	protected char getShimGroup(XnatMrscandata scan) {
		try {
			if (scan.getParameters_shimgroup() == null || scan.getParameters_shimgroup().length()<1) {
				return '0';
			}
			return scan.getParameters_shimgroup().charAt(0);
		} catch (Exception e) {
			return '0';
		}
	}
	
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int seFieldmapGroup = 0;
		int pcaslFieldmapGroup = 90;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		boolean inPCASLFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			// SE FIELDMAP GROUP
			if (!isPcaslScan(scan)) {
				if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
					inSEFieldmapGroup = true;
					seFieldmapGroup++;
					seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
						isTargetOrOverrideAll(scan) &&
						((scan.getSubjectsessionnum() != null &&
						 scan.getSubjectsessionnum().intValue() == seSession) ||
								(seSession == 0 && seFieldmapGroup == 0))
						) {
					inSEFieldmapGroup = false;
					if (seFieldmapGroup>0) {
						scan.setParameters_sefieldmapgroup(seFieldmapGroup);
					} else {
						// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
						// to any fieldmaps being collected and the shim values don't normally match
						scan.setParameters_sefieldmapgroup(1);
					}
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
					inSEFieldmapGroup = false;
				} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
						scan.getSubjectsessionnum() != null &&
						scan.getSubjectsessionnum().intValue() == seSession) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
				// Update series description for SE FieldMap Group
				if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
					if (!isPcaslScan(scan)) {
						scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
					}
				}
			} else {
				if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inPCASLFieldmapGroup) {
					inPCASLFieldmapGroup = true;
					pcaslFieldmapGroup++;
					seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
					scan.setParameters_sefieldmapgroup(pcaslFieldmapGroup);
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
						isTargetOrOverrideAll(scan) &&
						scan.getSubjectsessionnum() != null &&
						scan.getSubjectsessionnum().intValue() == seSession) {
					inPCASLFieldmapGroup = false;
					if (pcaslFieldmapGroup>0) {
						scan.setParameters_sefieldmapgroup(pcaslFieldmapGroup);
					}
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
					inPCASLFieldmapGroup = false;
				} else if (isTargetOrOverrideAll(scan) && inPCASLFieldmapGroup &&
						scan.getSubjectsessionnum() != null &&
						scan.getSubjectsessionnum().intValue() == seSession) {
					scan.setParameters_sefieldmapgroup(pcaslFieldmapGroup);
				}
			}
			/*
			if (scan.getType().matches(SEFIELDMAP_RGX) && !isPcaslScan(scan) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && !isPcaslScan(scan) && 
						isTargetOrOverrideAll(scan) &&
						scan.getSubjectsessionnum() != null &&
						 scan.getSubjectsessionnum().intValue() == seSession) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && !isPcaslScan(scan) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
						scan.getSubjectsessionnum() != null &&
			 			scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && !isPcaslScan(scan) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
					scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
			}
			// Let's set the fieldmap group to 99 for PCASL scans.
			if (isPcaslScan(scan)) {
				scan.setParameters_sefieldmapgroup(99);
			}
			*/
		}
	}		
	
	public boolean isPcaslScan(XnatMrscandata scan) {
		return scan.getSeriesDescription().toUpperCase().contains("PCASL");
	}

	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=488) ? "Y" : "N";
               		scanPct = (double)frames10/(488-10);
                	scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_VISMOTOR")) {
                		scanComplete = (frames>=194) ? "Y" : "N";
                		//scanPct = (double)frames10/(194-10);
                		if (frames<155) {
                			getErrorList().add("RULESERROR:  tfMRI VISMOTOR scans should have at least 155 frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_CARIT")) {
                		scanComplete = (frames>=300) ? "Y" : "N";
                		scanPct = (double)frames10/(300-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI CARIT scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_FACENAME")) {
                		scanComplete = (frames>=345) ? "Y" : "N";
                		//scanPct = (double)frames10/(345-10);
                		if (frames<315) {
                			getErrorList().add("RULESERROR:  tfMRI FACENAME scans should have at least 315 frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } else if (currDbdesc.matches(PCASL_RGX)) { 
                	if (currDbdesc.contains("hr_")) {
                		scanComplete = (frames>=90) ? "Y" : "N";
                		scanPct = (double)frames/90;
                	} else {
                		scanComplete = (frames>=70) ? "Y" : "N";
                		scanPct = (double)frames/70;
                	} 
                }
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}

	protected void updatePCASLInclusion(List<XnatMrscandata> subjectScans) {
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
            if (!currDbdesc.matches(PCASL_RGX)) { 
            	continue;
            }
            // Per MHarms, 2018-08-13, we'll only include the more recent PCASL scans (PCASLhr) and only if they're complete.
            if (!currDbdesc.contains("hr_")) {
            	setNoRelease(scan);
            } else {
            	final String scanComplete = scan.getScancomplete();
            	if (scanComplete == null || !scanComplete.equals("Y")) {
            		setNoRelease(scan);
            	}
            }
        }
	}
	
	protected void setRDFields(XnatMrscandata scan) {
		try {
			final Float ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			if (scan.getParameters_orientation().equals("Sag") && scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
					ipe_rotation>=-0.78 && ipe_rotation<=0.78) {
				scan.setParameters_readoutdirection("k");
				return;
			}
		} catch (Exception e) {
			// Do nothing
		}
     	final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
		getErrorList().add("RULESERROR:  Values do not match expected for setting READOUT DIRECTION (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ",RD=" + scan.getParameters_readoutdirection() + ")");
	}
	
	
	protected void setPEFields(XnatMrscandata scan,String seriesDesc) {
		boolean hasNiftiResource = false;
		try {
			//Float ipe_rotation = null;
			// 2018-08-13 - We are now pulling PE direction from scan NIFTI JSON rather than calculating it.
			// Per e-mails with Mike Harms, Leah, et. al, we will throw an error if ipe_rotation is missing, but if we 
			// override the error, we will treat the ipe_rotation value as zero
			for (final XnatAbstractresourceI resourceI : scan.getFile()) {
				if (!(resourceI instanceof XnatResource && resourceI.getLabel().equalsIgnoreCase(CommonConstants.NIFTI_RESOURCE))) {
					continue;
				}
				final XnatResource resource = (XnatResource)resourceI;
				if (resource == null || resource.getFileResources(CommonConstants.ROOT_PATH) == null || resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
					continue;
				}
				hasNiftiResource = true;
				for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
					final File f = rFile.getF();
					if (!f.getName().toLowerCase().endsWith(".json")) {
						continue;
					}
					final String json = FileUtils.readFileToString(f);
					Map<String,Object> jsonMap = gson.fromJson(json, new TypeToken<Map<String,Object>>(){}.getType());
					for (final String phaseEncodingDirectionKey : PHASE_ENCODING_DIRECTION_KEYS) {
						if (jsonMap.containsKey(phaseEncodingDirectionKey)) {
							final Object peDirection = jsonMap.get(phaseEncodingDirectionKey);
							if (peDirection != null) {
								scan.setParameters_pedirection(peDirection.toString());
								checkPESeriesDesc(scan);
								return;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown retrieving PhaseEncodingDirection - " + e.toString());
		}
		if (scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(TFMRI_RGX) ||
				scan.getType().matches(RFMRI_RGX) || scan.getType().matches(DMRI_RGX) ||
				scan.getType().matches(PCASL_RGX) || scan.getType().matches(HIRESSCANTYPE_RGX)) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			//getErrorList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SESSION=" + 
			// Often, during development, we don't yet have NIFTI built.  Let's issue a warning in this
			// case, since this should be caught during production as a sanity check failure
			if (!hasNiftiResource) {
				getWarningList().add("RULESWARNING:  PHASE ENCODING DIRECTION IS NULL - NO NIFTI RESOURCE - " +
									"Assuming Development Phase (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_pedirection() + ")");
				return;
			}
			getErrorList().add("RULESERROR:  PHASE ENCODING DIRECTION IS NULL (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_pedirection() + ")");
		}
	}

	protected void checkPESeriesDesc(XnatMrscandata scan) {
		if (!(scan.getSeriesDescription().contains("_RL") ||
			scan.getSeriesDescription().contains("_LR") ||
			scan.getSeriesDescription().contains("_PA") ||
			scan.getSeriesDescription().contains("_AP"))) {
			return;
		}
		if (!(scan.getParameters_pedirection().equals("i") && scan.getSeriesDescription().contains("_RL") ||
			scan.getParameters_pedirection().equals("i-") && scan.getSeriesDescription().contains("_LR") ||
			scan.getParameters_pedirection().equals("j") && scan.getSeriesDescription().contains("_PA") ||
			scan.getParameters_pedirection().equals("j-") && scan.getSeriesDescription().contains("_AP"))) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			getErrorList().add("RULESERROR:  Series Description does not reflect PHASE ENCODING DIRECTION (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
					", PE=" + scan.getParameters_pedirection() + ")");
		}
	}

	public boolean isSetter(XnatImagescandataI scan) {
		if (scan.getSeriesDescription().matches(SETTER_RGX)) {
			return true;
		} 
		return false;
	}

	public boolean isStructuralScan(XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && (scan.getType().matches(MAINSTRUCSCANTYPE_RGX) || scan.getType().matches(OTHERSTRUCSCANTYPE_RGX))) {
			return true;
		} 
		return false;
	}

	public boolean isHiresScan(XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && (scan.getType().matches(HIRESSCANTYPE_RGX))) {
			return true;
		} 
		return false;
	}

	public boolean isMainStructuralScan(XnatImagescandataI scan) {
		// NOTE:  Important here to include nav scans so override is considered for release
		if (scan instanceof XnatMrscandata && scan.getType().matches(MAINSTRUCSCANTYPE_RGX)) {
			return true;
		} 
		return false;
	}

	/*
	protected boolean notStructuralSession(XnatImagesessiondata session) {
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			if (scan instanceof XnatMrscandata && structNotVnav((XnatMrscandata)scan,false)) {
				return false;
			}
		}
		return true;
	}
	*/
	
	protected void flagTaskError(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Integer> sessionMap = new HashMap<String,Integer>();
        final HashMap<String,String> dirMap = new HashMap<String,String>();
		for (final XnatMrscandata scan : subjectScans) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(TFMRI_RGX)) { 
               	final String part1=currDbdesc.replaceFirst(APPA_RGX,"");
               	// Too many scans to be released
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                	if (mapval>2) {
               			getErrorList().add("RULESERROR:  Session contains more than two task scans targeted for release (" + part1 + ")");
                	}
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
               	// Multiple sessions containing scans
                if (sessionMap.containsKey(part1) && scan.getSubjectsessionnum()!=null) {
                	if (!sessionMap.get(part1).equals(scan.getSubjectsessionnum())) {
               			getErrorList().add("RULESERROR:  Multiple sessions contain scans for the same task (" + part1 + ")");
                	}
                } else {
                	if (scan.getSubjectsessionnum()!=null) {
                		sessionMap.put(part1, scan.getSubjectsessionnum());
                	}
                }
               	// Multiple sessions containing same direction
                final String direction = (currDbdesc.matches(AP_RGX)) ? "AP" : "PA";
                if (dirMap.containsKey(part1) && direction!=null) {
                	if (dirMap.get(part1).equals(direction)) {
               			getErrorList().add("RULESERROR:  Multiple sessions for same phase encoding direction for task (" + part1 + ")");
                	}
                } else {
                	if (direction!=null) {
                		dirMap.put(part1,direction);
                	}
                }
            }
		}
	}
	
	/*
	//// MRH:  2017/07/07  Let's keep these for now.  They're not completely tied to the structurals.
	protected void removeHiresScansWithoutT1T2(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			if (isHiresScan(scan)) {
				boolean hasT1=false;
				final Integer hiResFmg = scan.getParameters_sefieldmapgroup();
				for (final XnatMrscandata scan2 : subjectScans.keySet()) {
					if (isMainStructuralScan(scan2) && willBeReleased(scan2) && scan2.getParameters_sefieldmapgroup().equals(hiResFmg)) {
						hasT1=true;
						break;
					}
				}
				if (!hasT1) {
					setNoRelease(scan);
				}
			}
		}
	}
	*/
	
	/*
	//// MRH:  2017/07/07 - We want to include the HiRes scans as part of the release, but we don't want to require them.  
    //// The pipelines currently don't use them and would likely never require them.
	protected void flagT1T2withoutAssociatedHiresScan(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			if (isMainStructuralScan(scan) && willBeReleased(scan)) {
				boolean hasHires=false;
				final Integer t1Fmg = scan.getParameters_sefieldmapgroup();
				for (final XnatMrscandata scan2 : subjectScans.keySet()) {
					if (isHiresScan(scan2) && willBeReleased(scan2) && scan2.getParameters_sefieldmapgroup().equals(t1Fmg)) {
						hasHires=true;
						break;
					}
				}
				if (!hasHires) {
					getErrorList().add("RULESERROR:  Structurals marked for release do not have associated releasable TSE HiRes scans (SESSION=" +
							subjectScans.get(scan).getLabel() + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
				}
			}
		}
	}
	*/
	
	protected void removeFieldmapScansWithoutMainScan(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(ANYFIELDMAP_RGX) && willBeReleased(scan)) {
				boolean hasMain = false;
				final Integer fmFmg = scan.getParameters_sefieldmapgroup();
				for (final XnatMrscandata scan2 : subjectScans) {
					final Integer s2FmFmg = scan2.getParameters_sefieldmapgroup();
					if (!scan2.getType().matches(ANYFIELDMAP_RGX) && willBeReleased(scan2) && 
							s2FmFmg!=null && s2FmFmg.equals(fmFmg)) {
						hasMain = true;
						break;
					}
				}
				if (!hasMain) {
					setNoRelease(scan);
				}
			}
		}
	}

	protected void removeSettersAndOtherStructuralsWithoutMainStructural(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(OTHERSTRUCSCANTYPE_RGX) || scan.getType().matches(SETTER_RGX)) {
				XnatMrscandata nextScan = getNextScan(subjectScans, scan);
				boolean hasReleasableMainScan = false;
				while (true) {
					if (nextScan == null) {
						break;
					}
					if (isMainStructuralScan(nextScan)) {
						// If we're in a T1 and hit a T2 or in a T2 and hit a T1, let's not continue;
						if (!((isT1Any(scan) && isT1Type(nextScan,true)) || (isT2Any(scan) && isT2Type(nextScan,true)))) {
							break;
						}
						if (!this.isNoRelease(nextScan)) {
							hasReleasableMainScan = true;
							break;
						}
					}
					if (!(isStructuralScan(nextScan) || (isSetter(nextScan) && nextScan.getParameters_imagetype().contains("MOSAIC")))) {
						// Quit the loop once we hit anything other than a structural scan or a mosaic setter.  If we hit a non-mosaic
						// setter, we'll want to use that instead.
						break;
					}
					nextScan = getNextScan(subjectScans, nextScan);
				}
				if (!hasReleasableMainScan) {
					setNoRelease(scan);
				}
			}
		}
	}
	
	protected void flagDuplicateScans(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			// field maps are excluded from this check, along with scans not planned for released, we do release multiple resting state
			// scans, but they will be named differently.
			//if (scan.getType().matches(ANYFIELDMAP_RGX) || !willBeReleased(scan) || scan.getSeriesDescription().matches(RFMRI_RGX)) {
			
			if (!willBeReleased(scan)) {
				continue;
			}
			for (final XnatMrscandata scan2 : subjectScans) {
				if (!scan.equals(scan2) && scan.getDbdesc() != null && scan2.getDbdesc() != null && 
						scan.getDbdesc().equals(scan2.getDbdesc()) &&
						scan.getType().equals(scan2.getType()) && willBeReleased(scan2)) {
               		getErrorList().add("RULESERROR:  Multiple matching scans slated for release (" +
               				scan.getDbdesc() + ", " + getScanSession(scan, subjectSessions).getLabel() + ", " + scan.getId() + ", FRAMES=" + scan.getFrames() + ")");
				}
			}
		}
	}

	protected void flagIfNoHiresScanMarkedForRelease(List<XnatMrscandata> subjectScans) {
		boolean hasHires = false;
		boolean releasedHires = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (!isHiresScan(scan)) {
				continue;
			}
			hasHires = true;
			if (!willBeReleased(scan)) {
				continue;
			}
			releasedHires = true;
		}
		if (hasHires && !releasedHires) {
			getErrorList().add("RULESERROR:  No HiRes scan marked for release");
		}
	}

	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta, SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap,
				HcpToolboxdata tboxexpt, NtScores ntexpt, HcpvisitHcpvisitdata visitexpt, boolean errorOverride) throws ReleaseRulesException {
		// No meta-data has been defined for this project.
	}

	public void checkSbrefScanUnusableIfMainScanUnusable(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			final String seriesDesc = scan.getSeriesDescription();
			final String quality = scan.getQuality();
			if (seriesDesc == null || quality == null) {
				continue;
			}
			if (seriesDesc.matches(SBREF_RGX) && !quality.toLowerCase().contains("unusable")) {
				XnatMrscandata nextScan = getNextScan(subjectScans,scan);
				final String nextSeriesDesc = (nextScan != null) ? nextScan.getSeriesDescription() : null;
				final String nextQuality = (nextScan != null) ? nextScan.getQuality() : null;
				if (nextScan == null || nextSeriesDesc == null || (nextQuality != null && nextScan.getQuality().toLowerCase().contains("unusable")) ||
						!nextSeriesDesc.equals(seriesDesc.replace("_SBRef",""))) {
               		getErrorList().add("RULESERROR:  SBRef scans should be marked unusable if their main scan is unusable or missing (" +
               				scan.getDbdesc() + ", " + getScanSession(scan, subjectSessions).getLabel() + ", " + scan.getId() + ")");
				}
			}
		}
	}

	@Override
	public List<String> getSelectionCriteria() {
		return Arrays.asList(SELECTION_CRITERIA);
	}

}

