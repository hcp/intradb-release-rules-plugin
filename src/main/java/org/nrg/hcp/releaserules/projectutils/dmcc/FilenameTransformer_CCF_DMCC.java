package org.nrg.hcp.releaserules.projectutils.dmcc;

import java.io.File;

import org.nrg.ccf.sessionbuilding.releaserules.transformers.DefaultSeriesDescAndSessionLabelTransformer;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public class FilenameTransformer_CCF_DMCC extends DefaultSeriesDescAndSessionLabelTransformer {
	
	@Override
	public String transformParentDirectoryPath(String fileDirectory, File sessionFile, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		fileDirectory = super.transformFilename(fileDirectory, srcSession, destSession, srcSessionScan, destSessionScan);
		final String fileName = sessionFile.getName();
		if (sessionFile.getPath() .contains("LINKED_DATA") &&
				!(  sessionFile.getPath() .contains("EPRIME") ||
					sessionFile.getPath() .contains("PHYSIO") ||
					sessionFile.getPath() .contains("AUDIO") ||
					sessionFile.getPath() .contains("EYETRACKER")
						)) {
			if (fileName.endsWith(".txt")) {
				fileDirectory = "EPRIME";
			}
			
		}
		return fileDirectory;
	}

}
