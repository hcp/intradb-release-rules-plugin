package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_PHCP Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_PHCP_7T extends AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler {
	
	{
		getPathMatchRegex().add("^.*/PSYCHOPY/.*txt");
		getPathMatchRegex().add("^.*/EYE_TRACKER/.*edf");
		getPathMatchRegex().add("^.*/EYE_TRACKER/.*txt");
	}
	
	public ReleaseFileHandler_CCF_PHCP_7T(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_PHCP_7T() , user);
	}

}
