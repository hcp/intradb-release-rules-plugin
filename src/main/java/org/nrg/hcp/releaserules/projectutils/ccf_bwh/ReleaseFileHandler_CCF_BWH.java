package org.nrg.hcp.releaserules.projectutils.ccf_bwh;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseFileHandler_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_BWH Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_BWH extends ReleaseFileHandler_CCF_HCA {
	
	public ReleaseFileHandler_CCF_BWH(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
	}

}
