package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.io.File;

import org.nrg.ccf.sessionbuilding.releaserules.transformers.DefaultSeriesDescAndSessionLabelTransformer;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public class FilenameTransformer_CCF_PHCP extends DefaultSeriesDescAndSessionLabelTransformer {
	
	@Override
	public String transformParentDirectoryPath(String fileDirectory, File sessionFile, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		fileDirectory = super.transformFilename(fileDirectory, srcSession, destSession, srcSessionScan, destSessionScan);
		fileDirectory = fileDirectory.replaceFirst("PSYCHOPY","TASK_TIMING");
		return fileDirectory;
	}
	
	@Override
	public String transformFilename(String fileName, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		fileName = super.transformFilename(fileName, srcSession, destSession, srcSessionScan, destSessionScan);
		if (fileName.endsWith(".mp4")) {
			fileName = fileName.replaceFirst("[(][a-z0-9]+[)]","");
		}
		return fileName;
	}

}
