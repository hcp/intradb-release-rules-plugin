package org.nrg.hcp.releaserules.projectutils.ccf_anxpe;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseFileHandler_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_ANXPE Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_ANXPE extends ReleaseFileHandler_CCF_HCA {
	
	{
		getPathMatchRegex().add("^.*/PHYSIO/.*tsv");
		getPathMatchRegex().add("^.*/TASK_TIMING/EVs/.*");
	}
	
	public ReleaseFileHandler_CCF_ANXPE(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
	}

}
