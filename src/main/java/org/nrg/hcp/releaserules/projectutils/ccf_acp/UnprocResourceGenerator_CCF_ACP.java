package org.nrg.hcp.releaserules.projectutils.ccf_acp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.UnprocResourceGenerator_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_ACP Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_ACP extends UnprocResourceGenerator_CCF_HCA {
	
	
	public UnprocResourceGenerator_CCF_ACP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
		t1t2SeriesDescriptions = new String[] { "T1w_MPR", "T2w_SPC",
				"T1w_MPR_eyes_closed", "T1w_MPR_eyes_closed_PM", "T1w_MPR_PM", "T2w_SPC_eyes_closed",
				"T2w_SPC_eyes_closed_PM", "T2w_SPC_PM"
		};
	    tseHiresScanTypes = new String[] { };
		fieldmapScanTypes = new String[] { "FieldMap_SE_EPI", "FieldMap_SE_EPI_Pha" };
	    biasScanTypes = new String[] { "Bias_Receive", "BIAS_Receive" };
	}
	
	
	@Override
	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		// BANDA/BWH/ANXPE/MDP-specific change.  Shims don't match fieldmaps for BANDA/BWH/ANXPE/MDP, so we'll include them as "OTHER" files.
		final Map<String, List<String>> strucSubdirMap = new HashMap<>();
		strucSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(_Alt).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(_setter|_InitialFrames).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(MPR|SPC)(?!.*_Norm).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, strucSubdirMap, ci);
	}
	
	@Override
	protected void createHiresResources(EventMetaI ci) throws ReleaseResourceException {
		// No HiRes Resources for MDP
		return;
	}
	
	@Override
	protected void createFmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(fmriScanTypes)) {
				if (st.equals(scan.getType())) {
					final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, scan.getSeriesDescription());
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		final Map<String, List<String>> fmriSubdirMap = new HashMap<>();
		fmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());		
		fmriSubdirMap.get("OTHER_FILES").add("(?i).*(Bias).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, fmriSubdirMap, ci);
	}

	@Override
	protected void createDmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		List<XnatImagescandataI> scanList = new ArrayList<>(); 
		assignMap.put("Diffusion", scanList);
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(dmriScanTypes)) {
				if (st.equals(scan.getType())) {
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		final Map<String, List<String>> dmriSubdirMap = new HashMap<>();
		dmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());		
		dmriSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, dmriSubdirMap, ci);
	}

}
