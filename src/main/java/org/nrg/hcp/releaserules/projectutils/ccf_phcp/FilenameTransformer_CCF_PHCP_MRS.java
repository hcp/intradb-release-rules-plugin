package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import org.nrg.ccf.sessionbuilding.releaserules.transformers.DefaultSeriesDescAndSessionLabelTransformer;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public class FilenameTransformer_CCF_PHCP_MRS extends DefaultSeriesDescAndSessionLabelTransformer {

	@Override
	public String transformFilename(String fileName, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		if (fileName.contains(".nii") && !fileName.startsWith(destSession.getLabel())) {
			fileName = destSession.getLabel() + "_" + fileName;
		}
		return fileName;
	}
	
}
