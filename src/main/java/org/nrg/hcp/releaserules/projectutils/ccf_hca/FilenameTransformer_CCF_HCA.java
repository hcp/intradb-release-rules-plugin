package org.nrg.hcp.releaserules.projectutils.ccf_hca;

import org.nrg.ccf.sessionbuilding.releaserules.transformers.DefaultSeriesDescAndSessionLabelTransformer;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public class FilenameTransformer_CCF_HCA extends DefaultSeriesDescAndSessionLabelTransformer {

	@Override
	public String transformFilename(String fileName, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		fileName = super.transformFilename(fileName, srcSession, destSession, srcSessionScan, destSessionScan);
		fileName = fileName.replaceFirst("[-_]*20[0-3][0-9]-[01][0-9]-[0-3][0-9]([_-][0-9]+)*","");
		return fileName;
	}

}
