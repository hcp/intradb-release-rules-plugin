package org.nrg.hcp.releaserules.projectutils.ccf_ecp;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseFileHandler_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_ECP Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_ECP extends ReleaseFileHandler_CCF_HCA {
	
	//{
		//getPathMatchRegex().add("^.*/PSYCHOPY/.*_Resting_Run.*.csv");
		//getPathMatchRegex().add("^.*/PSYCHOPY/.*task.*.csv");
		//getPathMatchRegex().add("^.*/PSYCHOPY/TSV/.*");
		//getPathMatchRegex().add("^.*/PSYCHOPY/EVs/.*");
	//}
	
	public ReleaseFileHandler_CCF_ECP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
	}

}
