package org.nrg.hcp.releaserules.projectutils.ccf_cba;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.UnprocResourceGenerator_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_CBA Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_CBA extends UnprocResourceGenerator_CCF_HCA {
	
	protected static String[] seFieldmapScanTypes = { "FieldMap_SE_EPI" };
	protected static String[] geFieldmapScanTypes = new String[] { "FieldMap_Magnitude", "FieldMap_Phase" };
	
	public UnprocResourceGenerator_CCF_CBA(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	    t1t2SeriesDescriptions = new String[] { "T1w_MPR", "T2w_SPC" };
	    biasScanTypes = new String[] { "Bias_Receive" };
	    fieldmapScanTypes = new String[] { "FieldMap_SE_EPI", "FieldMap_Magnitude", "FieldMap_Phase" };
	}
	
	
	
	@Override
	public void generateResources() throws ReleaseResourceException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			createT1T2Resources(ci);
			createFmriResources(ci);
			createDmriResources(ci);
			addReportToT1Resource(ci);
			createOtherScansResources(ci);
		} catch (Exception e) {
			throw new ReleaseResourceException("ERROR:  Could not create unproc resources", e);
		}
	}

	@Override
	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingGefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		// CBA-specific change.  Not moving SE fieldmaps to OTHER_FILES because we want distortion correction
		// for this project
		final Map<String, List<String>> strucSubdirMap = new HashMap<>();
		strucSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(_setter|_4e_e|_InitialFrames).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(Bias|_FieldMap).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(MPR|SPC)[.].*$");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(FLAIR).*$");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, strucSubdirMap, ci);
	}
	

	@Override
	protected void createFmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(fmriScanTypes)) {
				if (st.equals(scan.getType())) {
					final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, scan.getSeriesDescription());
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, seFieldmapScanTypes));
					scanList.addAll(getMatchingGefieldmapScansByType(scan, geFieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		final Map<String, List<String>> fmriSubdirMap = new HashMap<>();
		fmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());		
		fmriSubdirMap.get("OTHER_FILES").add("(?i).*(Bias|_FieldMap).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, fmriSubdirMap, ci);
	}

	@Override
	protected void createDmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		List<XnatImagescandataI> scanList = new ArrayList<>(); 
		assignMap.put("Diffusion", scanList);
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(dmriScanTypes)) {
				if (st.equals(scan.getType())) {
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, seFieldmapScanTypes));
					scanList.addAll(getMatchingGefieldmapScansByType(scan, geFieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		final Map<String, List<String>> dmriSubdirMap = new HashMap<>();
		dmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());		
		dmriSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, dmriSubdirMap, ci);
	}

	protected void createOtherScansResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		List<XnatImagescandataI> scanList = new ArrayList<>(); 
		assignMap.put("OtherScans", scanList);
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			final String sd = scan.getSeriesDescription();
			if (sd != null && (
					sd.contains("ASL") ||
					sd.contains("mIP_") ||
					sd.contains("Perfusion_Weighted") ||
					sd.contains("SWI_")
					)) {
				scanList.add(scan);
			}
		}
		final Map<String, List<String>> otherSubdirMap = new HashMap<>();
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, otherSubdirMap, ci);
	}


}
