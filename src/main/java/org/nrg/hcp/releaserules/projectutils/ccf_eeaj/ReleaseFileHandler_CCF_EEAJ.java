package org.nrg.hcp.releaserules.projectutils.ccf_eeaj;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.FilenameTransformer_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

import com.google.gson.JsonObject;


@CcfReleaseFileHandler(description = "CCF_EEAJ Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_EEAJ extends AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler {
	
	{
		//getPathMatchRegex().add("^.*/EVs/.*.txt");
	}
	
	public ReleaseFileHandler_CCF_EEAJ(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_HCA() , user);
	}

}
