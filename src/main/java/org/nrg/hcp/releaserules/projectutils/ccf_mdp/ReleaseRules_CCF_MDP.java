package org.nrg.hcp.releaserules.projectutils.ccf_mdp;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.LocalDate;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_MDP Project Session Building Release Rules")
public class ReleaseRules_CCF_MDP extends ReleaseRules_CCF_HCA {
	
	private static final Logger _logger = LoggerFactory.getLogger(ReleaseRules_CCF_MDP.class);
	public final String BIAS_RGX = "(?i)^BIAS_.*"; 
	private Object _lock = new Object();
	// MDP difference (TEMPORARY):  Allowing usable and undetermined scan ratings - Hopefully all will get rated.
	public final String[] structuralQualityRatings = {"usable", "undetermined", "excellent","good","fair","poor"};
	
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		//rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	public ReleaseRules_CCF_MDP() {
		super();
		//SELECTION_CRITERIA = new String[] {"3T","7T","MRS"};
		SESSION_LABEL_EXT = "_MR";
	SELECTION_CRITERIA = new String[] { "ALL" };
	}
	
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: ALL\n" +
				"    label: Select Session Type\n" +
				//"    after:\n" +
				//"        script:\n" +
				//"            tag: 'script'\n" +
				//"            content: >\n" +
				//"                 $('#selectionCriteria').change(function() {\n" +
				//"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				//"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				//"                 	if (newCriteria == 'ALL') {\n" +
				//"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_7T)/,'_3T');\n" +
				//"                 	}else if (newCriteria == '7T') {\n" +
				//"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_3T)/,'_7T');\n" +
				//"                 	}else if (newCriteria == 'MRS') {\n" +
				//"                 	   sessionLabel = sessionLabel.replace(/(_3T|_7T)/,'_MRS');\n" +
				//"                 	}\n" +
				//"                 	$('#sessionLabel').val(sessionLabel);\n" +
				//"                 });\n" +
				"    options:\n" +
				"        'ALL': 'ALL'\n" +
				//"        '7T': '7T'\n" +
				//"        'MRS': 'MRS'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		String sc = params.get("selectionCriteria");
		if (sc == null || sc.equals("ALL")) {
			sc="ALL";
		}
		// Currently BWH isn't building longitudinal datasets, so this may be null.  They're building 01 datasets only.
		for (final XnatMrsessiondata session : projectExpts) {
			//final String lcLabel = session.getLabel().toLowerCase();
			//final String scanner = session.getScanner();
			//final String sessionType = session.getSessionType();
			//boolean isMRS = scanner.contains("7TAS") && (lcLabel.contains("mrs") || sessionType.endsWith("MRS"));
			//boolean is7T = scanner.contains("7TAS") && !isMRS;
			//boolean is3T = scanner.equals("TRIOC");
			//if (!(is3T || is7T || isMRS)) {
			//	log.warn("WARNING:  Session " + session.getLabel() + " does not match any build type.");
			//	continue;
			//}
			//if ((sc.equals("3T") && is3T) || (sc.equals("7T") && is7T) || (sc.equals("MRS") && isMRS)) {
				filteredExpts.add(session);
			//}
		}
		return filteredExpts;
	}
	

	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			clearWarningAndErrorLists();
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			updateT1T2InclusionAndDesc(subjectScans); 
			updateNonNormalizedInclusionAndQuality(subjectScans);
			flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			flagScansWithMissingFieldmapGroup(subjectScans);
			flagIfNotAllDmriInSameShimGroup(subjectScans);
			markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			useLaterBiasGroupIfMissing(subjectScans);
			excludeUnneededBiasScans(subjectScans);
			setScanOrderAndScanComplete(subjectScans); 
			//updatePCASLInclusion(subjectScans);
			removeFieldmapScansWithoutMainScan(subjectScans);
			//removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			//flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride &&
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
		
	}
	
	public boolean isBiasScan(XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && (scan.getSeriesDescription().matches(BIAS_RGX))) {
			return true;
		} 
		return false;
	}
	
	
    @Override
	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					(scan.getQuality().equalsIgnoreCase("poor") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					scan.getQuality().equalsIgnoreCase("fair")) && ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan))
				) {
           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
           		scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
						(isMainStructuralScan(scan) && isNormalizedScan(scan))) {
					if (isT1NormalizedScan(scan)) {
						hasGoodT1 = true;
					} else if (isT2NormalizedScan(scan)) {
						hasGoodT2 = true;
					}
					//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
					//	prevScan.setTargetforrelease(true);
					//	prevScan.setQuality(scan.getQuality());
					//}
				}
			}
			if (!hasGoodT1 || !hasGoodT2) {
				boolean hasFairT1 = false, hasFairT2 = false;
				for (final XnatMrscandata scan : subjectScans) {
					if ((!hasGoodT1 && isT1NormalizedScan(scan)) || (!hasGoodT2 && isT2NormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							if (isT1NormalizedScan(scan)) {
								hasFairT1 = true;
							} else if (isT2NormalizedScan(scan)) {
								hasFairT2 = true;
							}
							scan.setTargetforrelease(true);
							//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
							//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
							//	prevScan.setTargetforrelease(true);
							//	prevScan.setQuality(scan.getQuality());
							//}
						}
					}
				}
				if (!(hasGoodT1 || hasFairT1) || !(hasGoodT2 || hasFairT2)) {
					for (final XnatMrscandata scan : subjectScans) {
						if ((!(hasGoodT1 || hasFairT1) && isT1NormalizedScan(scan)) || 
								(!(hasGoodT2 || hasFairT2) && isT2NormalizedScan(scan))) {
							// MDP difference (TEMPORARY):  Allowing usable and undetermined scan ratings - Hopefully all will get rated.
							if (scan.getQuality().equalsIgnoreCase("poor")
									|| scan.getQuality().equalsIgnoreCase("usable")
									|| scan.getQuality().equalsIgnoreCase("undetermined")
									) {
								scan.setTargetforrelease(true);
							}
						}
					}
				}
			}
		}
		/*
		if (!hasValidHiresScan(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("fair")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		if (!hasValidHiresScan(subjectScans)) {
			// 2018-08-13.  We'll release even poor HiRes scans if we don't have one of higher quality.
			for (final XnatMrscandata scan : subjectScans) {
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("poor")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		*/
		// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		if (!hasValidT1AndT2(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
           		setNoRelease(scan);
			}
		}
	}
	
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		String sc = params.get("selectionCriteria");
		if (sc == null) {
			sc = "ALL";
		}
		//return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + "_" + sc; 
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + SESSION_LABEL_EXT;
	}
	

	@Override
	protected void markAssociatedScansWithUnreleasedStructuralsAsUnreleased(
			List<XnatMrscandata> subjectScans) {
		// First find scans that will be released and keep their fieldmap values
		ArrayList<Integer> fmList = new ArrayList<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				Integer fieldMapGroup = scan.getParameters_sefieldmapgroup();
				if (!fmList.contains(fieldMapGroup)) { 
					fmList.add(fieldMapGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(STRUCT_RGX) && willBeReleased(scan) && 
					(scan.getParameters_sefieldmapgroup()==null || !fmList.contains(scan.getParameters_sefieldmapgroup()))) {
				// MDP Difference:  Release structurals without matching fieldmap
				// FYI:  This method doesn't seem to be doing what the method name would suggest.....  I'm effectively undoing it here.
				//setNoRelease(scan);
			}
		}
	}
	
	
	@Override
	protected void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		//final ArrayList<Object> dayList = new ArrayList<Object>();
		//final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		// MDP Difference:  Calculate sessionday differently.  They have a lot of space between sessions.
		Date baselineDate = null;
		String prevSessionId = "";
		for (final XnatMrscandata scan : subjectScans) {
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				continue;
			} 
			if (currSess.getId().equals(prevSessionId)) {
				continue;
			}
			prevSessionId = currSess.getId();
			final Date currSessionDate = getSessionDate(currSess.getDate());
			if (baselineDate == null || (currSessionDate != null && currSessionDate.before(baselineDate))) {
				baselineDate = currSessionDate;
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				_logger.error("ERROR: scan (key) should not be null!!!");
				_logger.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				_logger.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				_logger.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				//final Object currSessionDate = currSess.getDate();
				//boolean containsDate = false;
				//for (final Object compareDate : dayList) {
				//	if (currSessionDate.equals(compareDate)) {
				//		containsDate = true;
				//		dayMap.put(currSess, dayList.indexOf(compareDate));
				//		break;
				//	}
				//}
				//if (!containsDate) {
				//	dayList.add(currSessionDate);
				//	dayMap.put(currSess, dayList.indexOf(currSessionDate));
				//}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday(getDateDifference(baselineDate,getSessionDate(currSess.getDate())));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getSeriesDescription().matches(PHYSIO_RGX) ||
					(scan.getSeriesDescription().matches(SETTER_RGX) && !scan.getParameters_imagetype().contains("MOSAIC")) ||
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// Per M.Harms, 2018/01/12, let's only include specific scan types, so random ones that are sometimes collected
			// are excluded.
			if (!(isPcaslScan(scan) || isSetter(scan) || isHiresScan(scan) || isBiasScan(scan) || 
					scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(MAINSCANDESC_RGX))) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  " + ((isMainStructuralScan(scan)) ? "Structural" : "TSE HiRes")  +
								" normalized scan quality rating is invalid or scan has not been rated (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Scan quality rating is invalid (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			scan.setReleasecountscan(true);
		}
	}

	private Date getSessionDate(Object date) {
		try {
			if (date == null) {
				return null;
			} else if (date instanceof Date) {
				return (Date)date;
			} else if (date instanceof LocalDate) {
				return Date.from(((LocalDate)date).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			} else {
				return Date.from(LocalDate.parse(date.toString()).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	private String getDateDifference(Date baselineDate, Date sessionDate) {
		if (baselineDate == null || sessionDate == null) {
			return "";
		}
		// NOTE:  Adding one to date difference to start at Day 1.
		return new Long(TimeUnit.DAYS.convert(sessionDate.getTime()-baselineDate.getTime(), TimeUnit.MILLISECONDS)+1).toString();
	}


	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("TSE_Norm_")) {
					// For ANXPE, we're renaming the series descriptions for the HiResHp scans to TSE_Norm_HiResHp (the scan type).
					scan.setDbdesc(scan.getType());
				}
			}
			String dbDesc = scan.getDbdesc()
					.replace("_Redo", "")
					.replace("_rerun", "")
					.replace("MOVIE_", "tfMRI_MOVIE_")
					.replace("RETINO_", "tfMRI_RETINO_")
					.replace("_AP_run1", "1_AP")
					.replace("_AP_run2", "2_AP")
					.replace("_AP_run3", "3_AP")
					.replace("_AP_run4", "4_AP")
					.replace("_AP_run5", "5_AP")
					.replace("_PA_run1", "1_PA")
					.replace("_PA_run2", "2_PA")
					.replace("_PA_run3", "3_PA")
					.replace("_PA_run4", "4_PA")
					.replace("_PA_run5", "5_PA")
					.replace("_Gamble_", "_Gambling_")
					.replace("_WM_", "_WorkMem_")
					.replace("_REST_", "_RESTXX_")
					.replace("_REST1_", "_REST01_")
					.replace("_REST2_", "_REST02_")
					;
			scan.setDbdesc(dbDesc);
		}
	}
	
	
	@Override
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			
			// BIAS FIELD GROUP
			
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && !inBiasGroup) {
				inBiasGroup = true;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				biasFieldGroup++;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
				inBiasGroup = false;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (scan.getReleasecountscan() && inBiasGroup && scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getDbdesc() != null) {
				// MDP Difference.  MDP has one bias group per session.  Let's add Bias group to series description.
				String dbDesc = scan.getDbdesc()
					.replace("BIAS_", "BIAS" + biasFieldGroup + "_")
					;
				scan.setDbdesc(dbDesc);
			}
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
			}
		}
	}		
	
	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		Integer prevRestSession = null;
		int restCount = 1;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			//XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
					} else {
						intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
						// MDP difference:
						// For now, let's undo all error checks for MDP REST scans 
						//if (restCount>2) {
						//	getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
						//if (intradbSessionRestCount>2) {
						//	getErrorList().add("RULESERROR:  Intradb session contains more than two usable Resting State scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
						//if (restGroupCount>1) {
						//	getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
					}
					inRest=true;
				} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				// MDP difference:
				// For now, let's undo *MOST* re-numbering of MDP resting state scans.  They're doing a good job of numbering for scans that have them.
				//scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_RESTXX","rfMRI_REST0" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}

	@Override
	public boolean requireScanUidAnonymization() {
		// MDP is allowing visit dates
		return false;
	}

	protected void flagScansWithMissingFieldmapGroup(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				// MDP Difference:  No fieldmaps are fine for bias scans
				if (isBiasScan(scan)) {
					continue;
				}
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				final String currDbdesc = scan.getDbdesc();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_sefieldmapgroup(thisFm);
						getWarningList().add("RULESWARNING:  Scan's nornally assigned fieldmap either doesn't exist or is unusable, " +
								"so a new fieldmap group has been chosen. (SESSION=" +
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					if (currDbdesc != null && !(currDbdesc.matches(DMRI_RGX))) {
						getErrorList().add("RULESERROR:  Scan's normally selected fieldmap either doesn't exist or is unusable, " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					} else if (currDbdesc != null) {
						getWarningList().add("RULESERROR:  Scan's normally selected fieldmap either doesn't exist or is unusable " +
								"(diffusion scan = warning only), " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}

	
	//@Override
	//protected void flagIfNotAllDmriInSameShimGroup(List<XnatMrscandata> subjectScans) {
	//	// PHCP MODIFICATION!!!:  We're not flagging different shims for diffusion because for much of data collection 
	//	// dir98 and dir99 diffusion scans were collected in different sessions with different shims.
	//	return;
	//}
	
	
	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=420) ? "Y" : "N";
               		scanPct = (double)frames10/(420-10);
               		scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For MDP, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.toUpperCase().contains("MOVIE")) {
                		scanComplete = (frames>=420) ? "Y" : "N";
                		scanPct = (double)frames10/(420-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Movie scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("RETINO")) {
                		scanComplete = (frames>=420) ? "Y" : "N";
                		scanPct = (double)frames10/(420-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Retino scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_EMOTION")) {
                		scanComplete = (frames>=176) ? "Y" : "N";
                		scanPct = (double)frames10/(176-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Emotion scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_FULLFIELD")) {
                		scanComplete = (frames>=420) ? "Y" : "N";
                		scanPct = (double)frames10/(420-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI FullField scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_GAMBL")) {
                		scanComplete = (frames>=253) ? "Y" : "N";
                		scanPct = (double)frames10/(253-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Gambling scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_PRLLOC")) {
                		scanComplete = (frames>=375) ? "Y" : "N";
                		scanPct = (double)frames10/(375-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI PRLloc scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_SOCCOG")) {
                		scanComplete = (frames>=274) ? "Y" : "N";
                		scanPct = (double)frames10/(274-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI SocCog scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_WORKMEM")) {
                		scanComplete = (frames>=405) ? "Y" : "N";
                		scanPct = (double)frames10/(405-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI WorkMem scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_WM")) {
                		scanComplete = (frames>=405) ? "Y" : "N";
                		scanPct = (double)frames10/(405-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI WorkMem scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } 
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("CARIT")) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	


	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				String currDbdesc = scan.getDbdesc();
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					if (scan.getType().matches(FIELDMAP_RGX)) {
 						getErrorList().add("RULESERROR:  Fieldmap pair has non-matching shim values (" +
 							"SESSION=" + 
 								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
 								", FieldMapGroup=" + fmGroup +")");
 						continue;
					}
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							// NOTE:  For MDP, Structurals often don't have matching shims
							if (!isStructuralScan(scan)) { 
								getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
										"so a new fieldmap group has been chosen. (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
										", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							}
						}
					}
					// NOTE:  For MDP, Structurals often don't have matching shims
					if (!newGroup && !isStructuralScan(scan)) { 
						// For MDP, there are lots of shim problems.  Let's make all of them warnings.
						//getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
						//		"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
						//			((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
						//		", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected " +
								"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}
	
	
	@Override
	protected void flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(List<XnatMrscandata> subjectScans)
			throws ReleaseRulesException {
		Integer compFieldmapGroup = null;
		String compShimGroup = null;
		XnatMrscandata compScan = null;
		int releaseCount = 0;
		int requiresOverrideCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				releaseCount++;
				if (requiresOverride(scan)) {
					requiresOverrideCount++;
				}
				if (releaseCount>3) {
					hardErrorList.add("RULESERROR:  Too many structural scans are marked for release.  Please " +
							"modify the release flag or usability for one or more scans.  This error should not be overridden.");
				}
				if (compFieldmapGroup == null || compShimGroup == null) {
					compFieldmapGroup = scan.getParameters_sefieldmapgroup();
					compShimGroup = scan.getParameters_shimgroup();
					compScan = scan;
				} else {
					final XnatMrsessiondata compSession = getScanSession(compScan, subjectSessions);
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					final Integer fmGroup = scan.getParameters_sefieldmapgroup();
					final String shimGroup = scan.getParameters_shimgroup();
					if (fmGroup != null && !fmGroup.equals(compFieldmapGroup)) {
						getErrorList().add("RULESERROR:  Structural scans to be released have different SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								scan.getParameters_sefieldmapgroup() + ")");
					} else if (fmGroup == null) {
						getErrorList().add("RULESERROR:  Structural scan to be released has null SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								fmGroup + ")");
					}
					if (shimGroup != null && !shimGroup.equals(compShimGroup)) {
						// MDP Difference.  This happens often in MDP.  We'll just make it a warning. 
						getWarningList().add("RULESWARNING:  Structural scans to be released have different ShimGroup values (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								scan.getParameters_shimgroup() + ")");
					} else if (shimGroup == null) {
						getErrorList().add("RULESERROR:  Structural scan to be released has null ShimGroup value (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								shimGroup + ")");
					}
				}
			}
		}
		if (releaseCount<2) {
			hardErrorList.add("RULESERROR:  This session does not contain a complete T1/T2 pair marked for release.  " +
							"Note that this error requires a hard override in order to override.  Please edit scan " +
							"quality or use the release override flags to mark scans for release.");
		} else if (requiresOverrideCount>0) {
			getErrorList().add("RULESERROR:  T1/T2 pair marked for release requires a release scan-level override flag to be released. " +
							"This could be due to insufficient scan quality or mismatched shim or fieldmap groups.  The override flag" +
					        " has been supplied but the session errorOverride flag should be used to submit this session so this notice is logged.");
		}
	}
	
	
	
	@Override
	protected void flagIfNotAllDmriInSameShimGroup(List<XnatMrscandata> subjectScans) {
		String compShimGroup = null;
		XnatMrscandata compScan = null;
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(DMRI_RGX)) { 
				if (compShimGroup == null) {
					compShimGroup = scan.getParameters_shimgroup();
					compScan = scan;
				} else {
					final XnatMrsessiondata compSession = getScanSession(compScan, subjectSessions);
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					final String shimGroup = scan.getParameters_shimgroup();
					if (shimGroup != null && !shimGroup.equals(compShimGroup)) {
						// MDP Difference:  This occurs regularly for MDP.  Let's make it a warning.
						getWarningList().add("RULESWARNING:  One or more diffusion scans to be released have different ShimGroup values (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								scan.getParameters_shimgroup() + ")");
					} else if (shimGroup == null) {
						getErrorList().add("RULESERROR:  Diffusion scan to be released has null ShimGroup value (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								shimGroup + ")");
					}
				}
			}
		}
	}
	
	protected void excludeUnneededBiasScans(List<XnatMrscandata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan)) {
				Integer currBias = scan.getParameters_biasgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}
	
	
	protected void useLaterBiasGroupIfMissing(List<XnatMrscandata> subjectScans) {
		// This method is to fix an issue first discovered with session 429040_3T where the initial set of bias scans
		// was unusable and a set was captured after the main scans.  These, having occurred in the same session,
		// should be usable, so when bias group is missing for a main scan, we'll use the later bias group in the
		// same session
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan)	) {
				Integer currBias = scan.getParameters_biasgroup();
				if (currBias != null ) {
					continue;
				}
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (compareBias!=null) {
							scan.setParameters_biasgroup(compareBias);
							break;
						}
					}
				}
			}
		}
	}

	
}

