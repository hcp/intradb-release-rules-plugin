package org.nrg.hcp.releaserules.projectutils.dmcc;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.abst.AbstractCcfReleaseRules;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.framework.node.XnatNode;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.HcpToolboxdata;
import org.nrg.xdat.om.NtScores;
import org.nrg.xdat.om.HcpvisitHcpvisitdata;
import org.nrg.xdat.om.HcpSubjectmetadata;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "DMCC Project Session Building Release Rules")
//public class ReleaseRules_DMCC extends AbstractCcfReleaseRules implements HcpReleaseRulesI, CcfReleaseRulesI {
public class ReleaseRules_DMCC extends AbstractCcfReleaseRules implements CcfReleaseRulesI {
	
	//final Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
	public final Gson gson = new GsonBuilder().create();
	public final XnatNode _xnatNode = XDAT.getContextService().getBean(XnatNode.class);
	public String[] PHASE_ENCODING_DIRECTION_KEYS = { "PhaseEncodingDirection" };
	
	public final String  LOCALIZER_RGX = "^Localizer.*", 
								SCOUT_RGX = "^AAHScout.*",
								T1WMEG_RGX = "^T1w_MEG.*",
								BIAS_RGX = "(?i)^BIAS_.*", 
								FIELDMAP_RGX = "^FieldMap.*",
								ANYFIELDMAP_RGX = "(?i)^.*FieldMap.*",
								GEFIELDMAP_RGX = "(?i)^FieldMap_((Ma)|(Ph)).*",
								SEFIELDMAP_RGX = "^FieldMap_SE_.*",
								SPINECHO_RGX = "(?i)^SpinEchoFieldMap.*",
								STRUCT_RGX = "(?i)^T[12]w.*",
								T1_RGX = "(?i)^T1w.*",
								T2_RGX = "(?i)^T2w.*", 
								NORM_RGX = "(?i)^_Norm.*", 
								NORMTYPE_ENDING = "_Norm", 
								NORMPARM_ENDING = "NORM", 
								T1MPR_RGX = "(?i)^T1w_MPR[0-9]*.*$",
								T2SPC_RGX = "(?i)^T2w_SPC[0-9]*.*$",
								TFMRI_RGX = "(?i)^tfMRI.*", 
								RFMRI_RGX = "(?i)^rfMRI.*", 
								DWI_RGX = "(?i)^DWI.*", 
								DMRI_RGX = "(?i)^dMRI.*", 
								SBREF_RGX = "(?i)^.*_SBRef$", 
								AP_RGX = "(?i)^.*_AP(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
								PA_RGX = "(?i)^.*_PA(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
								APPA_RGX = "(?i)_((PA)|(AP))(($)|(_.*$))", 
								APPAONLY_RGX = "(?i)_((AP)|(PA))_", 
								TFMRINO_RGX = "(?i)[0-9]_((AP)|(PA)).*$", 
								MAINSCAN_RGX = "(?i)((T[12]w)|(tfMRI)|(rfMRI)|(dMRI))",
								STRUCTMAIN_RGX = "(?i)((T[12]w))",
								MAINSCANDESC_RGX = "(?i)^((T[12]w)|(tfMRI)|(rfMRI)|(DWI)).*$",
								MAINSTRUCSCANTYPE_RGX = "(?i)^T[12]w(_Norm)*$",
								STRUCTSCANDESC_RGX = "(?i)^((T[12]w)).*$",
								VNAV_RGX = "(?i)^((T[12]w)).*_vNav_.*$",
								OTHER_RGX = "(?i)^.*MPR.*Collection.*$",
								SESSION_LABEL_EXT = "_MR"
								;
	
	public final String[] validQualityRatings = {"undetermined","usable","unusable","excellent","good","fair","poor"};
	public final String[] structuralQualityRatings = {"excellent","good","fair","poor","usable","undetermined"};
	protected String[] SELECTION_CRITERIA = {"PHASE2","PHASE3","PHASE4"};

	public final List<String> softErrorList = new ArrayList<>();
	public final List<String> hardErrorList = new ArrayList<>();
	public final List<String> warningList = Lists.newArrayList();
	private boolean errorOverride;

	private final List<XnatMrsessiondata> subjectSessions = new ArrayList<>();
	private Object _lock = new Object();
	
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/parameters/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/parameters/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/parameters/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: PHASE2\n" +
				"    label: Select Phase\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	sessionLabel = sessionLabel.replace(/_PHASE[234]_/,'_' + newCriteria + '_');\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        'PHASE2': 'PHASE2'\n" +
				"        'PHASE3': 'PHASE3'\n" +
				"        'PHASE4': 'PHASE4'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}

	@Override
	public List<String> applyRulesToScans(
			List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions,
			Map<String,String> params, boolean errorOverride)
			throws ReleaseRulesException {
		// NOTE:  Currently LS_Phase1a does not use the selectionCriteria parameter.  All sessions are combined into a single 3T session.
		return applyRulesToScans(subjectScans, subjectSessions, errorOverride);
	}

	//@Override
	//public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta,
	//		SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap,
	//		String selectionCriteria, HcpToolboxdata tboxexpt, NtScores ntexpt,
	//		HcpvisitHcpvisitdata visitexpt, boolean errorOverride)
	//		throws ReleaseRulesException {
	//	// NOTE:  Currently LS_Phase1a does not use the selectionCriteria parameter.  All sessions are combined into a single 3T session.
	//	applyRulesToSubjectMetaData(subjMeta,scanMap,tboxexpt,ntexpt,visitexpt,errorOverride);
	//	
	//}

	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String selectionCriteria = params.get("selectionCriteria");
		log.debug("SelectionCriteria Parameter Value:  " + selectionCriteria);
		final List<String> validCriteria = Arrays.asList(SELECTION_CRITERIA);
		for (final XnatMrsessiondata exp : projectExpts) {
			for (final String criteria : validCriteria) {
				if (selectionCriteria != null && selectionCriteria.equals(criteria) && exp.getLabel().contains("_" + selectionCriteria + "_")) {
					log.debug("Adding experiment to filteredExpts list:  " + exp.getLabel());
					filteredExpts.add(exp);
				}
			}
			if (selectionCriteria == null || selectionCriteria.length()<1) {
				throw new ReleaseRulesException("ERROR:  Selection criteria value not provided");
			} else if (!validCriteria.contains(selectionCriteria)) {
				throw new ReleaseRulesException("ERROR:  Invalid selection criteria value (" + selectionCriteria + ")");
			}
		}
		return filteredExpts;
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		final String sc = params.get("selectionCriteria");
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + 
				((sc != null && sc.length()>0) ? "_" + sc + "_MR" : "_MR"); 
	}
	
	@Override
	public boolean requireScanUidAnonymization() {
		return true;
	}
	
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		return applyRulesToScans(subjectScans, subjectSessions, errorOverride, false);
		
	}

	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions,
			boolean errorOverride, boolean skipClearList) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			if (!skipClearList) {
				clearWarningAndErrorLists();
			}
			
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans);
			setCountScanAndSession(subjectScans);
			checkForNormalizedScans(subjectScans);
			setTargetForReleaseAndScanID(subjectScans);
			setTypeDescriptionAndOthers(subjectScans);
			setGroupValues(subjectScans);
			updateNonNormalizedInclusionAndQuality(subjectScans);
			updateT1T2InclusionAndDesc(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans);
			useLaterBiasGroupIfMissing(subjectScans);
			excludeUnneededBiasScans(subjectScans);
			setScanOrderAndScanComplete(subjectScans);
			flagTaskError(subjectScans);
			hardCodedValues(subjectScans);
			fixBackslashProblem(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride && 
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
	}
	
	protected List<String> getErrorList() {
		return (errorOverride) ? softErrorList : hardErrorList;
	}

	protected List<String> getWarningList() {
		return warningList;
	}

	protected List<String> getWarningAndErrorSummaryList() {
		final List<String> returnList = new ArrayList<>(hardErrorList);
		returnList.addAll(softErrorList);
		returnList.addAll(warningList);
		return returnList;
	}

	protected void clearWarningAndErrorLists() {
		warningList.clear();
		softErrorList.clear();
		hardErrorList.clear();
	}
	
	private void fixBackslashProblem(
			final List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			// Replace multiple backslash instances with a single backslash
			if (scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains("\\\\")) {
				final String imageType = scan.getParameters_imagetype().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_imagetype(imageType);
			}
			if (scan.getParameters_seqvariant()!=null && scan.getParameters_seqvariant().contains("\\\\")) {
				final String seqVariant = scan.getParameters_seqvariant().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_seqvariant(seqVariant);
			}
			if (scan.getParameters_scansequence()!=null && scan.getParameters_scansequence().contains("\\\\")) {
				final String scanSequence = scan.getParameters_scansequence().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scansequence(scanSequence);
			}
			if (scan.getParameters_scanoptions()!=null && scan.getParameters_scanoptions().contains("\\\\")) {
				final String scanOptions = scan.getParameters_scanoptions().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scanoptions(scanOptions);
			}
		}
	}

	private void nullPriorValues(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			scan.setSubjectsessionnum(null);
			scan.setDatarelease(null);
			scan.setDbsession(null);
			scan.setDbid(null);
			scan.setDbtype(null);
			scan.setDbdesc(null);
			scan.setReleasecountscan(null);
			scan.setReleasecountscanoverride(null);
			scan.setTargetforrelease(null);
			// IMPORTANT:  Do not null out releaseOverride.  It is set by users, not by this class to force publish/non-publish.
			scan.setParameters_protocolphaseencodingdir(null);
			scan.setParameters_shimgroup(null);
			scan.setParameters_sefieldmapgroup(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_gefieldmapgroup(null);
			scan.setParameters_perotation(null);
			scan.setParameters_peswap(null);
			scan.setParameters_pedirection(null);
			scan.setParameters_readoutdirection(null);
			scan.setParameters_eprimescriptnum(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_scanorder(null);
			scan.setScancomplete(null);
			scan.setViewscan(null);
		}
	}

	private void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR:  Retrieved session should not be null!!!");
				continue;
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getType().matches(T1WMEG_RGX) || scan.getSeriesDescription().matches(T1WMEG_RGX) || 
					scan.getType().matches(VNAV_RGX) ||  scan.getSeriesDescription().matches(VNAV_RGX) || 
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// DMCC Does not want to use the "test" scans
			if (scan.getSeriesDescription().contains("Test")) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			if (isStructuralScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Structural scan quality rating is invalid or scan has not been rated (SESSION=" +
								currSess.getLabel() + ", SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Structural quality rating is invalid (SESSION=" +
								currSess.getLabel() + ", SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			// If scan specifically set to Don't release, then let's count it but not release it
			if (isDontRelease(scan)) {
				scan.setReleasecountscan(true);
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setReleasecountscan(true);
		}
	}

	private void checkForNormalizedScans(final List<XnatMrscandata> subjectScans) {
		// Throw error if Structural scan doesn't contain a normalized scan as the following scan
		for (final XnatMrscandata scan : subjectScans) {
			if (structNotVnav(scan,false) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
				if (!(isNormalizedScan(nextScan) && scan.getQuality().equalsIgnoreCase(nextScan.getQuality()))) {
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					getErrorList().add("RULESERROR:  Structural scan is not followed by a normalized scan of the same quality (SESSION=" +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) +
								", SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
				}
			}
		}
	}
	
	public boolean isDontRelease(XnatMrscandata scan) {
		final Integer releaseOverride = scan.getReleaseoverride();
		return (releaseOverride!=null && releaseOverride<0);
	}

	private boolean isNormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(STRUCT_RGX) && !scan.getType().matches(VNAV_RGX) && !scan.getSeriesDescription().matches(VNAV_RGX) &&
				scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().endsWith(NORMPARM_ENDING)) {
			return true;
		}
		return false;
	}

	private XnatMrscandata getNextScan(List<XnatMrscandata> subjectScans, XnatMrscandata scan) {
		final XnatMrscandata[] scanArray = subjectScans.toArray(new XnatMrscandata[subjectScans.size()]);
		// NOTE:  Returns null for last scan in array
		for (int i=0; i<(scanArray.length-1); i++) {
			if (scan.equals(scanArray[i])) {
				return scanArray[i+1];
			}
		}
		return null;
	}
	
	protected XnatMrscandata getPreviousScan(List<XnatMrscandata> subjectScans, XnatMrscandata scan) {
		final XnatMrscandata[] scanArray = subjectScans.toArray(new XnatMrscandata[subjectScans.size()]);
		// NOTE:  Returns null for first scan in array
		for (int i=1; i<scanArray.length; i++) {
			if (scan.equals(scanArray[i])) {
				return scanArray[i-1];
			}
		}
		return null;
	}

	protected void updateNonNormalizedInclusionAndQuality(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan)) {
				final String quality = scan.getQuality();
				final Boolean targetForRelease = scan.getTargetforrelease();
				final Integer releaseOverride = scan.getReleaseoverride();
				final XnatMrscandata prevScan = getPreviousScan(subjectScans, scan); 
				if (isMainStructuralScan(prevScan) && isNormalizedScan(scan)) {
					prevScan.setQuality(quality);
					prevScan.setTargetforrelease(targetForRelease);
					prevScan.setReleaseoverride(releaseOverride);
				}
			}
		}
	}

	private void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					(scan.getQuality().equalsIgnoreCase("poor") ||
					// ACP difference:  Not mandating scan ratings.		
					 scan.getQuality().equalsIgnoreCase("usable") ||
					 scan.getQuality().equalsIgnoreCase("undetermined") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					scan.getQuality().equalsIgnoreCase("fair")) && (isMainStructuralScan(scan) && isNormalizedScan(scan))
				) {
           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
           		scan.setTargetforrelease(false);
				continue;
			}
			if (isDontRelease(scan)) { 
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
						(isMainStructuralScan(scan) && isNormalizedScan(scan))) {
					if (isT1NormalizedScan(scan)) {
						hasGoodT1 = true;
					} else if (isT2NormalizedScan(scan)) {
						hasGoodT2 = true;
					}
					//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
					//	prevScan.setTargetforrelease(true);
					//	prevScan.setQuality(scan.getQuality());
					//}
				}
			}
			if (!hasGoodT1 || !hasGoodT2) {
				boolean hasFairT1 = false, hasFairT2 = false;
				for (final XnatMrscandata scan : subjectScans) {
					if (isDontRelease(scan)) { 
						continue;
					}
					if ((!hasGoodT1 && isT1NormalizedScan(scan)) || (!hasGoodT2 && isT2NormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							if (isT1NormalizedScan(scan)) {
								hasFairT1 = true;
							} else if (isT2NormalizedScan(scan)) {
								hasFairT2 = true;
							}
							scan.setTargetforrelease(true);
							//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
							//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
							//	prevScan.setTargetforrelease(true);
							//	prevScan.setQuality(scan.getQuality());
							//}
						}
					}
				}
				if (!(hasGoodT1 || hasFairT1) || !(hasGoodT2 || hasFairT2)) {
					for (final XnatMrscandata scan : subjectScans) {
						if (isDontRelease(scan)) { 
							continue;
						}
						if ((!(hasGoodT1 || hasFairT1) && isT1NormalizedScan(scan)) || 
								(!(hasGoodT2 || hasFairT2) && isT2NormalizedScan(scan))) {
							// DMCC difference (TEMPORARY):  Allowing usable and undetermined scan ratings - Hopefully all will get rated.
							if (scan.getQuality().equalsIgnoreCase("poor")
									|| scan.getQuality().equalsIgnoreCase("usable")
									|| scan.getQuality().equalsIgnoreCase("undetermined")
									) {
								scan.setTargetforrelease(true);
							}
						}
					}
				}
			}
		}
		// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		if (!hasValidT1AndT2(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
				scan.setTargetforrelease(false);
			}
		}
	}
	
	public boolean isMainStructuralScan(XnatImagescandataI scan) {
		// NOTE:  Important here to include nav scans so override is considered for release
		if (scan instanceof XnatMrscandata && scan.getType().matches(MAINSTRUCSCANTYPE_RGX)) {
			return true;
		} 
		return false;
	}
	
	private boolean isT1NormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T1_RGX) &&
				scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().endsWith(NORMPARM_ENDING)) {
			return true;
		}
		return false;
	}
	
	private boolean isT1Type(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(T1_RGX) &&
				(normAsStruc ||  
				(scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().endsWith(NORMPARM_ENDING))
				)) {
			return true;
		}
		return false;
	}
	
	private boolean isT2NormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T2_RGX) &&
				scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().endsWith(NORMPARM_ENDING)) {
			return true;
		}
		return false;
	}
	
	private boolean isT2Type(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(T2_RGX) &&
				(normAsStruc ||  
				(scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().endsWith(NORMPARM_ENDING))
				)) {
			return true;
		}
		return false;
	}
	
	private boolean structNotVnav(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(STRUCT_RGX) &&
				(scan.getType()==null || !scan.getType().matches(VNAV_RGX)) && 
				(scan.getSeriesDescription()==null || !scan.getSeriesDescription().matches(VNAV_RGX)) && 
				(normAsStruc ||		
				((scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().endsWith(NORMPARM_ENDING))))) {
			return true;
		}
		return false;
	}
	
	private boolean mainNotVnav(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getDbdesc()!=null && scan.getDbdesc().matches(MAINSCANDESC_RGX) &&
				(scan.getType()==null || !scan.getType().matches(VNAV_RGX)) && 
				(scan.getSeriesDescription()==null || !scan.getSeriesDescription().matches(VNAV_RGX)) && 
				(normAsStruc ||		
				((scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().endsWith(NORMPARM_ENDING))))) {
			return true;
		}
		return false;
	}

	private boolean hasValidT1AndT2(List<XnatMrscandata> subjectScans) {
		boolean hasT1 = false;
		boolean hasT2 = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (isTargetOrOverrideAll(scan)) {
				if (isT1NormalizedScan(scan)) {
					hasT1 = true;
				} else if (isT2NormalizedScan(scan)) {
					hasT2 = true;
				}
				if (hasT1 && hasT2) {
					return true;
				}
			}
		}
		return false;
	}

	private void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			final XnatMrsessiondata mrSession = getScanSession(scan, subjectSessions);
			if (mrSession == null) {
				log.error("ERROR:  Returned session should not be null!!!");
				continue;
			}
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Currently no changes for scan type
			scan.setDbtype(scan.getType());
			// SpinEchoFieldmaps
			final String currSeriesDesc = scan.getSeriesDescription();
			// Initially set Dbdesc to current series description
			scan.setDbdesc(currSeriesDesc);
			if (currSeriesDesc.equalsIgnoreCase("BOLD_PA_SB_SE")) {
				scan.setDbdesc("SpinEchoFieldMap_PA");
				setPEFields(scan,currSeriesDesc);
			} else if (currSeriesDesc.equalsIgnoreCase("BOLD_AP_SB_SE")) {
				scan.setDbdesc("SpinEchoFieldMap_AP");
				setPEFields(scan,currSeriesDesc);
			} else if (currSeriesDesc.contains("SpinEchoFieldMap")) {
				scan.setDbdesc(currSeriesDesc);
				setPEFields(scan,currSeriesDesc);
			}
			if (scan.getType().matches(TFMRI_RGX)) {
				//Matcher mtch =  Pattern.compile(TFMRINO_RGX).matcher(currSeriesDesc);
				//final String tfmrino = mtch.find() ? mtch.group() : "";
				final String revisedSeriesDesc = currSeriesDesc.replaceAll("[0-9]_PA", "_PA").replaceAll("[0-9]_AP", "_AP");
				scan.setDbdesc(revisedSeriesDesc);
				setPEFields(scan,revisedSeriesDesc);
				/*
				if (tfmrino!=null && tfmrino.length()>1) {
					try {
						scan.setParameters_eprimescriptnum(Integer.parseInt(tfmrino.replaceFirst("_.*$","")));
					} catch (NumberFormatException e) {	
						// Do nothing for now.
					}
				}
				*/
			}
			if (scan.getType().matches(RFMRI_RGX)) {
				final String sessionLabel = mrSession.getLabel().toLowerCase();
				final String substParam = (sessionLabel.contains("basel")) ? "Bas" : (sessionLabel.contains("react")) ? "Rea" : (sessionLabel.contains("proac")) ? "Pro" : "Unk";
				final String revisedSeriesDesc = currSeriesDesc.replaceAll("[0-9]_PA", substParam + "_PA").replaceAll("[0-9]_AP", substParam + "_AP");
				scan.setDbdesc(revisedSeriesDesc);
				setPEFields(scan,revisedSeriesDesc);
			}
			if (scan.getType().matches(DMRI_RGX)) {
				setPEFields(scan,currSeriesDesc);
			}
			scan.setViewscan(!scan.getType().matches("(" + ANYFIELDMAP_RGX + ")|(" + BIAS_RGX + ")|(" + SBREF_RGX + ")"));
		}
	}

	private void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						restGroupCount++;
						if (currDbdesc.matches(AP_RGX)) {
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							groupHasPA = true;
						}
					} else {
						intradbSessionRestCount++;
						restGroupCount++;
						if (currDbdesc.matches(AP_RGX)) {
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							groupHasPA = true;
						}
						if (restCount>6) {
							getErrorList().add("RULESERROR:  CDB session contains more than six usable groups of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId())+ ")");
						}
						if (intradbSessionRestCount>6) {
							getErrorList().add("RULESERROR:  Intradb session contains more than six usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (restGroupCount>3) {
							getErrorList().add("RULESERROR:  Intradb session group contains more than three groups of usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
				}
				inRest=true;
				// Update series description based on restCount
				// NOTE:  We're not using scan numbers in the series descriptions for DMCC
				//scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
				//scan.setDbdesc(currDbdesc);
			} else if ((!currDbdesc.matches(RFMRI_RGX) && 
					((scan.getViewscan()!=null && scan.getViewscan() && inRest) || 
						(restGroupCount>1 && scan.getType().matches("(" + ANYFIELDMAP_RGX + ")|(" + BIAS_RGX + ")")))) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
			}
		}
	}
	
	private void updateT1T2InclusionAndDesc(List<XnatMrscandata> subjectScans) {
		// Target T1, T2 from session that has both
		final ArrayList<Integer> t1List = new ArrayList<Integer>();
		final ArrayList<Integer> t2List = new ArrayList<Integer>();
		final ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (final XnatMrscandata scan : subjectScans) {
			String currSeriesDesc = scan.getSeriesDescription();
			if (currSeriesDesc.matches(T1MPR_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getErrorList().add("RULESERROR:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (final XnatMrscandata scan : subjectScans) {
				final String currSeriesDesc = scan.getSeriesDescription();
				if ((currSeriesDesc.matches(T1MPR_RGX) || currSeriesDesc.matches(T2SPC_RGX)) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isNormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					}
				}
			}
		}
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		// MRH 2022-03-17:  Change per Mike Harms.  We no longer care that T1/T2s have matching shims if we're not using the
		// fieldmaps.  MH explicitly prefers picking highest quality over matching shims per 03/16/22-03/17/22 e-mail thread.
		// We'll split shimGroup to t1ShimGroup and t2ShimGroup and just handle those separately.
		//char shimGroup = Character.MIN_VALUE;
		char t1ShimGroup = Character.MIN_VALUE;
		char t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : subjectScans) {
			String currSeriesDesc = scan.getSeriesDescription();
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (currSeriesDesc.matches(T1MPR_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				if (t1Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t1Count++;
				}
				if (t1Count!=0) {
					if (t1ShimGroup == Character.MIN_VALUE) {
						t1ShimGroup = getShimGroup(scan);
					}
					if (t1Count>2 || t1ShimGroup!=getShimGroup(scan)) {
						t1Count--;
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT1NormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					} else {
						final String part1 = "T1w_MPR";
						// NOTE:  DMCC Not collecting multiple structurals, so we won't include the count in the series description
						//final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t1Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t1Count);
						final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? currSeriesDesc.substring(part1.trim().length()+1) : "";
						scan.setDbdesc(part1+part2);
					    setRDFields(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT1NormalizedScan(nextScan)) {
							nextScan.setDbdesc(part1+part2 + "_Norm");
							nextScan.setDbtype(nextScan.getType() + "_Norm");
							setRDFields(nextScan);
						}
					}
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				if (t2Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t2Count++;
				}
				if (t2Count!=0) {
					if (t2ShimGroup == Character.MIN_VALUE) {
						t2ShimGroup = getShimGroup(scan);
					}
					if (t2Count>2 || t2ShimGroup!=getShimGroup(scan)) {
						t2Count--;
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT2NormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					} else {
						final String part1 = "T2w_SPC";
						// NOTE:  DMCC Not collecting multiple structurals, so we won't include the count in the series description
						//final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t2Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t2Count);
						final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? currSeriesDesc.substring(part1.trim().length()+1) : "";
						scan.setDbdesc(part1+part2);
					    setRDFields(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT2NormalizedScan(nextScan)) {
							nextScan.setDbdesc(part1+part2 + "_Norm");
							nextScan.setDbtype(nextScan.getType() + "_Norm");
							setRDFields(nextScan);
						}
					}
				}
			}
		}
		t1Count=0;
		t2Count=0;
		t1ShimGroup = Character.MIN_VALUE;
		t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			final String currSeriesDesc = scan.getSeriesDescription();
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (currSeriesDesc.matches(T1MPR_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1ShimGroup == Character.MIN_VALUE) {
					t1ShimGroup = getShimGroup(scan);
				}
				if (t1Count>2 || t1ShimGroup!=getShimGroup(scan)) {
					t1Count--;
					setNoRelease(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT1NormalizedScan(nextScan)) {
						setNoRelease(nextScan);
					}
				} else {
					String part1 = "T1w_MPR";
					// NOTE:  DMCC Not collecting multiple structurals, so we won't include the count in the series description
					//String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t1Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t1Count);
					String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? currSeriesDesc.substring(part1.trim().length()+1) : "";
					scan.setDbdesc(part1+part2);
				    setRDFields(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT1NormalizedScan(nextScan)) {
						nextScan.setDbdesc(part1+part2 + "_Norm");
						nextScan.setDbtype(nextScan.getType() + "_Norm");
						setRDFields(nextScan);
					}
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2ShimGroup == Character.MIN_VALUE) {
					t2ShimGroup = getShimGroup(scan);
				}
				if (t2Count>2 || t2ShimGroup!=getShimGroup(scan)) {
					t2Count--;
					setNoRelease(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT2NormalizedScan(nextScan)) {
						setNoRelease(nextScan);
					}
				} else {
					final String part1 = "T2w_SPC";
					// NOTE:  DMCC Not collecting multiple structurals, so we won't include the count in the series description
					//final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t2Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t2Count);
					final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? currSeriesDesc.substring(part1.trim().length()+1) : "";
					scan.setDbdesc(part1+part2);
				    setRDFields(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT2NormalizedScan(nextScan)) {
						nextScan.setDbdesc(part1+part2 + "_Norm");
						nextScan.setDbtype(nextScan.getType() + "_Norm");
						setRDFields(nextScan);
					}
				}
			}
		}
	}		
	
	private boolean scanShimMatchesFieldMaps(List<XnatMrscandata> subjectScans, XnatMrscandata comparescan) {
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) {
			if (scan.getSeriesDescription().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) &&
						scan.getParameters_sefieldmapgroup().equals(comparescan.getParameters_sefieldmapgroup())) {
				if (scan.getParameters_shimgroup().equals(comparescan.getParameters_shimgroup())) {
					return true;
				} else {
					return false;
				}
			} 
		}
		return false;
	}
	
	private boolean isTargetOrOverrideAll(XnatMrscandata scan) {
		if (scan.getSeriesDescription()!=null && (scan.getSeriesDescription().matches(STRUCTSCANDESC_RGX) &&
					!scan.getSeriesDescription().matches(VNAV_RGX))) { if (isTargetOrOverrideStruc(scan)) {
				return true;
			}
		} else if (scan.getTargetforrelease()!=null) {
			return scan.getTargetforrelease();
		}
		return false;
	}
	
	private boolean isTargetOrOverrideStruc(XnatMrscandata scan) {
		if ( (scan.getTargetforrelease()!=null && scan.getTargetforrelease() && (scan.getReleaseoverride()==null || !(scan.getReleaseoverride()<0))) ||
				(scan.getTargetforrelease()!=null && !scan.getTargetforrelease() && scan.getReleaseoverride()!=null && (scan.getReleaseoverride()>0))
			) {
			return true;
		}
		return false;
	}

	private void setNoRelease(XnatMrscandata scan) {
		// Do this instead of just setting targetforrelease to false to clear out other fields that
		// may have been set (Note that a few aren't included here that should remain)
		scan.setTargetforrelease(false);
		scan.setSubjectsessionnum(null);
		scan.setDatarelease(null);
		scan.setDbsession(null);
		scan.setDbid(null);
		scan.setDbtype(null);
		scan.setDbdesc(null);
		scan.setParameters_protocolphaseencodingdir(null);
		scan.setParameters_sefieldmapgroup(null);
		scan.setParameters_gefieldmapgroup(null);
		scan.setParameters_perotation(null);
		scan.setParameters_peswap(null);
		scan.setParameters_pedirection(null);
		scan.setParameters_readoutdirection(null);
		scan.setParameters_eprimescriptnum(null);
		scan.setParameters_biasgroup(null);
	}

	private char getShimGroup(XnatMrscandata scan) {
		try {
			if (scan.getParameters_shimgroup() == null || scan.getParameters_shimgroup().length()<1) {
				return '0';
			}
			return scan.getParameters_shimgroup().charAt(0);
		} catch (Exception e) {
			return '0';
		}
	}
	
	private void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			
			// BIAS FIELD GROUP
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && !inBiasGroup) {
				inBiasGroup = true;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				biasFieldGroup++;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
				inBiasGroup = false;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (scan.getReleasecountscan() && inBiasGroup && scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			// Update series description for BIAS Group
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getDbdesc() != null) {
				// MDP Difference.  MDP has one bias group per session.  Let's add Bias group to series description.
				String dbDesc = scan.getDbdesc()
					.replace("BIAS_", "BIAS" + biasFieldGroup + "_")
					;
				scan.setDbdesc(dbDesc);
			}
			
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && !scan.getType().matches(BIAS_RGX) &&
						isTargetOrOverrideAll(scan) && scan.getSubjectsessionnum().intValue() == seSession) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup && scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
			}
			
		}
		
	}		
	
	private void useLaterBiasGroupIfMissing(
			final List<XnatMrscandata> subjectScans) {
		// This method is to fix an issue first discovered with session 429040_3T where the initial set of bias scans
		// was unusable and a set was captured after the main scans.  These, having occurred in the same session,
		// should be usable, so when bias group is missing for a main scan, we'll use the later bias group in the
		// same session
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (mainNotVnav(scan,false) && isTargetOrOverrideAll(scan)) {
				final Integer currBias = scan.getParameters_biasgroup();
				if (currBias != null ) {
					continue;
				}
				for (final XnatMrscandata scan2 : subjectScans) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && (compareDbdesc.matches(BIAS_RGX) || compareDbdesc.equalsIgnoreCase("AFI")) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (compareBias!=null) {
							scan.setParameters_biasgroup(compareBias);
							break;
						}
					}
				}
			}
		}
	}
	
	private void excludeUnneededBiasScans(List<XnatMrscandata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if ((currDbdesc.matches(BIAS_RGX) || currDbdesc.equalsIgnoreCase("AFI")) && isTargetOrOverrideAll(scan)) {
				final Integer currBias = scan.getParameters_biasgroup();
				boolean hasMatch = false;
				for (final XnatMrscandata scan2 : subjectScans) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && mainNotVnav(scan2,false) && isTargetOrOverrideAll(scan2)) {
						final Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
			if (currDbdesc.matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan)) {
				final Integer currGE = scan.getParameters_gefieldmapgroup();
				boolean hasMatch = false;
				for (final XnatMrscandata scan2 : subjectScans) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && mainNotVnav(scan2,false) && isTargetOrOverrideAll(scan2)) {
						final Integer compareGE = scan2.getParameters_gefieldmapgroup();
						if (currGE!=null && compareGE!=null && compareGE.equals(currGE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
			if (currDbdesc.matches(SPINECHO_RGX) && isTargetOrOverrideAll(scan)) {
				final Integer currSE = scan.getParameters_sefieldmapgroup();
				boolean hasMatch = false;
				for (final XnatMrscandata scan2 : subjectScans) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && mainNotVnav(scan2,false) && isTargetOrOverrideAll(scan2)) {
						final Integer compareSE = scan2.getParameters_sefieldmapgroup();
						if (currSE!=null && compareSE!=null && compareSE.equals(currSE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}

	private void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DWI_RGX)) ? "DWI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                String scanComplete = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=260) ? "Y" : "N";
               		scanPct = (double)frames/260;
                } else if (currDbdesc.matches(DWI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		int dirNum = Integer.parseInt(dirVal);
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_StroopBas")) {
                		scanComplete = (frames>=540) ? "Y" : "N";
                		scanPct = (double)frames/540;
                	} else if (currDbdesc.contains("_AxcptBas")) {
                		scanComplete = (frames>=610) ? "Y" : "N";
                		scanPct = (double)frames/610;
                	} else if (currDbdesc.contains("_SternBas")) {
                		scanComplete = (frames>=600) ? "Y" : "N";
                		scanPct = (double)frames/600;
                	} else if (currDbdesc.contains("_CuedtsBas")) {
                		scanComplete = (frames>=650) ? "Y" : "N";
                		scanPct = (double)frames/650;
                	} else if (currDbdesc.contains("_CuedtsPro")) {
                		scanComplete = (frames>=650) ? "Y" : "N";
                		scanPct = (double)frames/650;
                	} else if (currDbdesc.contains("_StroopPro")) {
                		scanComplete = (frames>=540) ? "Y" : "N";
                		scanPct = (double)frames/540;
                	} else if (currDbdesc.contains("_AxcptPro")) {
                		scanComplete = (frames>=610) ? "Y" : "N";
                		scanPct = (double)frames/610;
                	} else if (currDbdesc.contains("_SternPro")) {
                		scanComplete = (frames>=600) ? "Y" : "N";
                		scanPct = (double)frames/600;
                	} else if (currDbdesc.contains("_CuedtsRea")) {
                		scanComplete = (frames>=650) ? "Y" : "N";
                		scanPct = (double)frames/650;
                	} else if (currDbdesc.contains("_StroopRea")) {
                		scanComplete = (frames>=590) ? "Y" : "N";
                		scanPct = (double)frames/590;
                	} else if (currDbdesc.contains("_AxcptRea")) {
                		scanComplete = (frames>=610) ? "Y" : "N";
                		scanPct = (double)frames/610;
                	} else if (currDbdesc.contains("_SternRea")) {
                		scanComplete = (frames>=600) ? "Y" : "N";
                		scanPct = (double)frames/600;
                	} else if (currDbdesc.contains("_StroopTest")) {
                		scanComplete = (frames>=10) ? "Y" : "N";
                		scanPct = (double)frames/10;
                	}
                }
                // Per Greg, tfMRI should have at least 75% of frames to have been considered usable.
                if (scanPct<.75) {
                	getErrorList().add("RULESERROR:  tfMRI, rfMRI and dMRI scans should have at least 75% of frames to have been marked usable (SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                }
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            }
            }
        }
	}
	
	private void setRDFields(XnatMrscandata scan) {
		try {
			final Float ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			if (scan.getParameters_orientation().equals("Sag") && scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
					ipe_rotation>=-0.53 && ipe_rotation<=0.53) {
				scan.setParameters_readoutdirection("+z");
				return;
			}
		} catch (Exception e) {
			// Do nothing
		}
		getWarningList().add("RULESWARNING:  Values do not match expected for setting READOUT DIRECTION (SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
	}
	
	protected void setPEFields(XnatMrscandata scan,String seriesDesc) {
		boolean hasNiftiResource = false;
		try {
			//Float ipe_rotation = null;
			// 2018-08-13 - We are now pulling PE direction from scan NIFTI JSON rather than calculating it.
			// Per e-mails with Mike Harms, Leah, et. al, we will throw an error if ipe_rotation is missing, but if we 
			// override the error, we will treat the ipe_rotation value as zero
			for (final XnatAbstractresourceI resourceI : scan.getFile()) {
				if (!(resourceI instanceof XnatResource && resourceI.getLabel().equalsIgnoreCase(CommonConstants.NIFTI_RESOURCE))) {
					continue;
				}
				final XnatResource resource = (XnatResource)resourceI;
				if (resource == null || resource.getFileResources(CommonConstants.ROOT_PATH) == null || resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
					continue;
				}
				hasNiftiResource = true;
				for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
					final File f = rFile.getF();
					if (!f.getName().toLowerCase().endsWith(".json")) {
						continue;
					}
					final String json = FileUtils.readFileToString(f);
					Map<String,Object> jsonMap = gson.fromJson(json, new TypeToken<Map<String,Object>>(){}.getType());
					for (final String phaseEncodingDirectionKey : PHASE_ENCODING_DIRECTION_KEYS) {
						if (jsonMap.containsKey(phaseEncodingDirectionKey)) {
							final Object peDirection = jsonMap.get(phaseEncodingDirectionKey);
							if (peDirection != null) {
								scan.setParameters_pedirection(peDirection.toString());
								checkPESeriesDesc(scan);
								return;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown retrieving PhaseEncodingDirection - " + e.toString());
		}
		if (scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(TFMRI_RGX) ||
				scan.getType().matches(RFMRI_RGX) || scan.getType().matches(DMRI_RGX)) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			//getErrorList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SESSION=" + 
			// Often, during development, we don't yet have NIFTI built.  Let's issue a warning in this
			// case, since this should be caught during production as a sanity check failure
			if (!hasNiftiResource) {
				getWarningList().add("RULESWARNING:  PHASE ENCODING DIRECTION IS NULL - NO NIFTI RESOURCE - " +
									"Assuming Development Phase (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_pedirection() + ")");
				return;
			}
			getErrorList().add("RULESERROR:  PHASE ENCODING DIRECTION IS NULL (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_pedirection() + ")");
		}
	}
	
	protected void checkPESeriesDesc(XnatMrscandata scan) {
		if (!(scan.getSeriesDescription().contains("_RL") ||
			scan.getSeriesDescription().contains("_LR") ||
			scan.getSeriesDescription().contains("_PA") ||
			scan.getSeriesDescription().contains("_AP"))) {
			return;
		}
		if (!(scan.getParameters_pedirection().equals("i") && scan.getSeriesDescription().contains("_RL") ||
			scan.getParameters_pedirection().equals("i-") && scan.getSeriesDescription().contains("_LR") ||
			scan.getParameters_pedirection().equals("j") && scan.getSeriesDescription().contains("_PA") ||
			scan.getParameters_pedirection().equals("j-") && scan.getSeriesDescription().contains("_AP"))) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			// DMCC change.  Make this a warning.
			getWarningList().add("RULESWARNING:  Series Description does not reflect PHASE ENCODING DIRECTION (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
					", PE=" + scan.getParameters_pedirection() + ")");
		}
	}

	public boolean isStructuralScan(XnatImagescandataI scan) {
		// NOTE:  Important here to include nav scans so override is considered for release
		if (scan instanceof XnatMrscandata && structNotVnav((XnatMrscandata)scan,true)) {
			return true;
		} 
		return false;
	}

	/*
	private boolean notStructuralSession(XnatImagesessiondata session) {
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			if (scan instanceof XnatMrscandata && structNotVnav((XnatMrscandata)scan,false)) {
				return false;
			}
		}
		return true;
	}
	*/
	
	private void hardCodedValues(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null || currSess.getLabel() == null || scan.getSeriesDescription() == null) {
				continue;
			}
		}
	}

	private void flagTaskError(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Integer> sessionMap = new HashMap<String,Integer>();
        final HashMap<String,String> dirMap = new HashMap<String,String>();
		for (final XnatMrscandata scan : subjectScans) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(TFMRI_RGX)) { 
               	final String part1=currDbdesc.replaceFirst(APPA_RGX,"");
               	// Too many scans to be released
                if (countMap.containsKey(part1) && !part1.contains("StroopTest")) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                	if (mapval>2) {
               			getErrorList().add("RULESERROR:  Session contains more than two task scans targeted for release (" + part1 + ")");
                	}
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
               	// Multiple sessions containing scans
                if (sessionMap.containsKey(part1) && scan.getSubjectsessionnum()!=null && !part1.contains("StroopTest")) {
                	if (!sessionMap.get(part1).equals(scan.getSubjectsessionnum())) {
               			getErrorList().add("RULESERROR:  Multiple sessions contain scans for the same task (" + part1 + ")");
                	}
                } else {
                	if (scan.getSubjectsessionnum()!=null) {
                		sessionMap.put(part1, scan.getSubjectsessionnum());
                	}
                }
               	// Multiple sessions containing same direction
                final String direction = (currDbdesc.matches(AP_RGX)) ? "AP" : "PA";
                if (dirMap.containsKey(part1) && direction!=null && !part1.contains("StroopTest")) {
                	if (dirMap.get(part1).equals(direction)) {
               			getErrorList().add("RULESERROR:  Multiple sessions for same phase encoding direction for task (" + part1 + ")");
                	}
                } else {
                	if (direction!=null) {
                		dirMap.put(part1,direction);
                	}
                }
            }
		}
	}

	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta, List<XnatMrscandata> scanList,
				HcpToolboxdata tboxexpt, NtScores ntexpt, HcpvisitHcpvisitdata visitexpt, boolean errorOverride) throws ReleaseRulesException {
		// No meta-data has been defined for this project.
	}
	
}

