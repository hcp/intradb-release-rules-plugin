package org.nrg.hcp.releaserules.projectutils.ccf_acp;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.dicomtools.utilities.DicomUtils;
import org.nrg.hcp.releaserules.pojo.DateInfo;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.utils.ResourceFile;

//@Slf4j
public class TemporalComparator_ACP implements Comparator<XnatImagescandata> {
	
	private final DateFormat TIME_FORMAT = CommonConstants.getFormatTIME();
	private List<XnatMrsessiondata> _sessionList;
	private Map<String, DateInfo> _cache = new HashMap<>();
	
	public TemporalComparator_ACP(List<XnatMrsessiondata> sessionList) {
		super();
		this._sessionList = sessionList;
	}

	@Override
	public int compare(XnatImagescandata arg0, XnatImagescandata arg1) {
		try {
			final Date d0 = getDateInfo(arg0).getDate();
			final Date d1 = getDateInfo(arg1).getDate();
			if ((d0 == null && d1 != null)) {
				return -1;
			} else if ((d1 == null && d0 != null)) {
				return 1;
			} else if ((d1 == null && d0 == null)) {
				return 0;
			} else if (d0.before(d1)) {
				return -1;
			} else if (d1.before(d0)) {
				return 1;
			} 
		} catch (Exception e) {
			// Do nothing for now
		}
		try {
			final Date t0 = TIME_FORMAT.parse(arg0.getStarttime().toString());
			final Date t1 = TIME_FORMAT.parse(arg1.getStarttime().toString());
			if (t0.before(t1)) {
				return -1;
			} else if (t1.before(t0)) {
				return 1;
			}
		} catch (Exception e) {
			// Do nothing for now
		}
		try {
			if (Integer.parseInt(arg0.getId()) <  Integer.parseInt(arg1.getId())) {
				return -1;
			} else if (Integer.parseInt(arg1.getId()) >  Integer.parseInt(arg0.getId())) {
				return 1;
			}
		} catch (Exception e) {
			// Do nothing for now
		}
		return 0;
	}

	protected XnatImagesessiondata getScanSession(XnatImagescandata arg0) {
		for (final XnatImagesessiondata sess : _sessionList) {
			if (sess.getId().equals(arg0.getImageSessionId())) {
				return sess;
			}
		}
		return null;
	}


	protected DateInfo getDateInfo(XnatImagescandata arg1) throws IOException {
		final String scanUID = arg1.getUid();
		if (_cache.containsKey(scanUID)) {
			return _cache.get(scanUID);
		}
		final DateInfo dateInfo = pullDateInfoFromScanDicom(arg1);
		_cache.put(scanUID, dateInfo);
		return dateInfo;
	}

	public static DateInfo pullDateInfoFromScanDicom(XnatImagescandata arg1) throws IOException {
		final DateInfo dateInfo = new DateInfo();
		final XnatResourcecatalog resource = ResourceUtils.getResource(arg1, CommonConstants.DICOM_RESOURCE);
		final List<ResourceFile> fileResources = resource.getFileResources(CommonConstants.ROOT_PATH);
		for (final ResourceFile fileResource : fileResources) {
			final File f = fileResource.getF();
			final DicomObject studyDateObj = DicomUtils.read(f, Tag.StudyDate);
			final Date studyDate = (studyDateObj != null) ? studyDateObj.getDate(Tag.StudyDate) : null;
			final DicomObject acqDateObj = DicomUtils.read(f, Tag.AcquisitionDate);
			final Date acqDate = (acqDateObj != null) ? acqDateObj.getDate(Tag.AcquisitionDate) : null;
			if (studyDate != null && acqDate != null && !studyDate.equals(acqDate)) {
				dateInfo.setDateMessage("DICOM StudyDate and AquisitionDate do not match (ScanID=" + arg1.getId() + 
						", ScanSD=" + arg1.getSeriesDescription() + ", StudyDate=" + studyDate +
						", AcquisitionDate=" + acqDate + ")");
				dateInfo.setDateProblem(true);
				dateInfo.setDate(studyDate);
				break;
			} else if (studyDate != null) {
				dateInfo.setDate(studyDate);
				break;
			} else if (acqDate != null) {
				dateInfo.setDate(acqDate);
				break;
			}
		}
		return dateInfo;
	}
	
}
