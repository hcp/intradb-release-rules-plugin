package org.nrg.hcp.releaserules.projectutils.ccf_hca;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_HCA Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_HCA extends AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler {
	
	{
		getPathMatchRegex().add("^.*/PHYSIO/.*csv");
		getPathMatchRegex().add("^.*/PSYCHOPY/.*mp4");
		getPathMatchRegex().add("^.*/PSYCHOPY/.*_design.csv");
		getPathMatchRegex().add("^.*/PSYCHOPY/.*_ts.csv");
		getPathMatchRegex().add("^.*/PSYCHOPY/.*_run[0-9]_wide.csv");
		getPathMatchRegex().add("^.*/PSYCHOPY/EVs/.*");
	}

	public ReleaseFileHandler_CCF_HCA(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_HCA(), user);
	}

}
