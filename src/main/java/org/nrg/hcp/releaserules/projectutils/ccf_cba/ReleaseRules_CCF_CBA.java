package org.nrg.hcp.releaserules.projectutils.ccf_cba;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;

import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_CBA Project Session Building Release Rules")
public class ReleaseRules_CCF_CBA extends ReleaseRules_CCF_HCA {
	
	public final String BIAS_RGX = "(?i)^BIAS_.*";
	private Object _lock = new Object();
	
	//private static final Logger _logger = LoggerFactory.getLogger(ReleaseRules_CCF_AABC.class);

	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	public ReleaseRules_CCF_CBA() {
		super();
		SELECTION_CRITERIA = new String[] {"PRIMARY","RETEST"};
		PHASE_ENCODING_DIRECTION_KEYS = new String[] { "PhaseEncodingDirection", "PhaseEncodingAxis" };
		structuralQualityRatings = new String[] {"excellent","good","fair","poor","usable","undetermined"};
	}
	

	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			clearWarningAndErrorLists();
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			updateT1T2InclusionAndDesc(subjectScans); 
			updateNonNormalizedInclusionAndQuality(subjectScans);
			flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			flagScansWithMissingFieldmapGroup(subjectScans);
			flagIfNotAllDmriInSameShimGroup(subjectScans);
			markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			useLaterBiasGroupIfMissing(subjectScans);
			excludeUnneededBiasScans(subjectScans);
			setScanOrderAndScanComplete(subjectScans); 
			//updatePCASLInclusion(subjectScans);
			removeFieldmapScansWithoutMainScan(subjectScans);
			//removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			//flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			checkSbrefScanUnusableIfMainScanUnusable(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride &&
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
		
	}
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: PRIMARY\n" +
				"    label: Select Visit\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	sessionLabel = sessionLabel.replace(/_(PRIMARY|RETEST)_/,'_' + newCriteria + '_');\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        'PRIMARY': 'PRIMARY'\n" +
				"        'RETEST': 'RETEST'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String selectionCriteria = params.get("selectionCriteria");
		log.debug("SelectionCriteria Parameter Value:  " + selectionCriteria);
		final List<String> validCriteria = Arrays.asList(SELECTION_CRITERIA);
		for (final XnatMrsessiondata exp : projectExpts) {
			for (final String criteria : validCriteria) {
				if (!criteria.equals(selectionCriteria)) {
					continue;
				}
				if (selectionCriteria != null && !selectionCriteria.equals("RETEST") && !exp.getLabel().toUpperCase().contains("RETEST")) {
					log.debug("Adding experiment to filteredExpts list:  " + exp.getLabel());
					filteredExpts.add(exp);
				} else if (selectionCriteria != null && selectionCriteria.equals("RETEST") && exp.getLabel().toUpperCase().contains("RETEST")) {
					log.debug("Adding experiment to filteredExpts list:  " + exp.getLabel());
					filteredExpts.add(exp);
				}
			}
			if (selectionCriteria == null || selectionCriteria.length()<1) {
				// Currently, let's default to V1 session if no parameter is passed.  Soon we may want to require the parameter.
				if (!exp.getLabel().toUpperCase().contains("RETEST")) {
					filteredExpts.add(exp);
				}
			} else if (!validCriteria.contains(selectionCriteria)) {
				throw new ReleaseRulesException("ERROR:  Invalid selection criteria value (" + selectionCriteria + ")");
			}
		}
		return filteredExpts;
	}
	
	
    @Override
	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					(scan.getQuality().equalsIgnoreCase("poor") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					scan.getQuality().equalsIgnoreCase("usable") ||
					scan.getQuality().equalsIgnoreCase("undetermined") ||
					scan.getQuality().equalsIgnoreCase("fair")) && ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan))
				) {
           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
           		scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
						(isMainStructuralScan(scan) && isNormalizedScan(scan))) {
					if (isT1NormalizedScan(scan)) {
						hasGoodT1 = true;
					} else if (isT2NormalizedScan(scan)) {
						hasGoodT2 = true;
					}
					//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
					//	prevScan.setTargetforrelease(true);
					//	prevScan.setQuality(scan.getQuality());
					//}
				}
			}
			if (!hasGoodT1 || !hasGoodT2) {
				boolean hasFairT1 = false, hasFairT2 = false;
				for (final XnatMrscandata scan : subjectScans) {
					if ((!hasGoodT1 && isT1NormalizedScan(scan)) || (!hasGoodT2 && isT2NormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							if (isT1NormalizedScan(scan)) {
								hasFairT1 = true;
							} else if (isT2NormalizedScan(scan)) {
								hasFairT2 = true;
							}
							scan.setTargetforrelease(true);
							//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
							//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
							//	prevScan.setTargetforrelease(true);
							//	prevScan.setQuality(scan.getQuality());
							//}
						}
					}
				}
				if (!(hasGoodT1 || hasFairT1) || !(hasGoodT2 || hasFairT2)) {
					for (final XnatMrscandata scan : subjectScans) {
						if ((!(hasGoodT1 || hasFairT1) && isT1NormalizedScan(scan)) || 
								(!(hasGoodT2 || hasFairT2) && isT2NormalizedScan(scan))) {
							// CBA difference:  Allowing poor scan ratings.  Let's not hold things up.
							if (scan.getQuality().equalsIgnoreCase("poor")
									|| scan.getQuality().equalsIgnoreCase("usable")
									|| scan.getQuality().equalsIgnoreCase("undetermined")) {
								scan.setTargetforrelease(true);
							}
						}
					}
				}
			}
		}
		/*
		if (!hasValidHiresScan(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("fair")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		if (!hasValidHiresScan(subjectScans)) {
			// 2018-08-13.  We'll release even poor HiRes scans if we don't have one of higher quality.
			for (final XnatMrscandata scan : subjectScans) {
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("poor")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		*/
		// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		if (!hasValidT1AndT2(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
           		setNoRelease(scan);
			}
		}
	}


	protected void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				log.error("ERROR: scan (key) should not be null!!!");
				log.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				log.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getSeriesDescription().matches(PHYSIO_RGX) ||
					(scan.getSeriesDescription().matches(SETTER_RGX) && !scan.getParameters_imagetype().contains("MOSAIC")) ||
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// Per M.Harms, 2018/01/12, let's only include specific scan types, so random ones that are sometimes collected
			// are excluded.
			if (!(isPcaslScan(scan) || isSetter(scan) || isHiresScan(scan) || isBiasScan(scan) || isCbaSpecificScan(scan) || 
					scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(MAINSCANDESC_RGX))) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  " + ((isMainStructuralScan(scan)) ? "Structural" : "TSE HiRes")  +
								" normalized scan quality rating is invalid or scan has not been rated (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Scan quality rating is invalid (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			// If scan specifically set to Don't release, then let's count it but not release it
			if (isDontRelease(scan)) {
				scan.setReleasecountscan(true);
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setReleasecountscan(true);
		}
	}
	
	public boolean isBiasScan(XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && (scan.getSeriesDescription().matches(BIAS_RGX))) {
			return true;
		} 
		return false;
	}
	
	public boolean isCbaSpecificScan(XnatImagescandataI scan) {
		final String sd = scan.getSeriesDescription();
		if (scan instanceof XnatMrscandata && (
				sd.contains("ASL") ||
				sd.contains("mIP_") ||
				sd.contains("Perfusion_Weighted") ||
				sd.contains("SWI_")
				)) {
			return true;
		} 
		return false;
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		final String sc = params.get("selectionCriteria");
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + (((sc != null && sc.length()>0 && 
					!sc.contains("RETEST")) || sc == null) ? "_MR" : "_MR_RT");
	}
	
	
	@Override
	protected boolean isT2Any(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T2_ANY) && !scan.getType().toUpperCase().contains("FLAIR")) {
			return true;
		}
		return false;
	}
	
	
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int geFieldmapGroup = 0;
		int seSession = 0;
		int geSession = 0;
		boolean inSEFieldmapGroup = false;
		boolean inGEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			// BIAS FIELD GROUP

			//if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && !inBiasGroup) {
			//	inBiasGroup = true;
			//	biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
			//	biasFieldGroup++;
			//	scan.setParameters_biasgroup(biasFieldGroup);
			//} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
			//	inBiasGroup = false;
			//	scan.setParameters_biasgroup(biasFieldGroup);
			//} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && inBiasGroup) {
			//	inBiasGroup = false;
			//} else if (scan.getReleasecountscan() && inBiasGroup && scan.getSubjectsessionnum().intValue() == biasSession) {
			//	scan.setParameters_biasgroup(biasFieldGroup);
			//}
			if (scan.getType().matches(BIAS_RGX) && isTargetOrOverrideAll(scan) && !inBiasGroup) {
				inBiasGroup = true;
				biasFieldGroup++;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getType().matches(BIAS_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
					 scan.getSubjectsessionnum().intValue() == biasSession) ||
							(biasSession == 0 && biasFieldGroup == 0))
					) {
				inBiasGroup = false;
				if (biasFieldGroup>0) {
					scan.setParameters_biasgroup(biasFieldGroup);
				} else {
					// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
					// to any fieldmaps being collected and the shim values don't normally match
					scan.setParameters_biasgroup(1);
				}
			} else if (!scan.getType().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inBiasGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getDbdesc() != null) {
				// MDP Difference.  MDP has one bias group per session.  Let's add Bias group to series description.
				String dbDesc = scan.getDbdesc()
					.replace("BIAS_", "BIAS" + biasFieldGroup + "_")
					;
				scan.setDbdesc(dbDesc);
			}
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
					 scan.getSubjectsessionnum().intValue() == seSession) ||
							(seSession == 0 && seFieldmapGroup == 0))
					) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				} else {
					// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
					// to any fieldmaps being collected and the shim values don't normally match
					scan.setParameters_sefieldmapgroup(1);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
			}
			// GE FIELDMAP GROUP
			if (scan.getType().matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inGEFieldmapGroup) {
				inGEFieldmapGroup = true;
				geFieldmapGroup++;
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_gefieldmapgroup(geFieldmapGroup);
			} else if (!scan.getType().matches(GEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
					 scan.getSubjectsessionnum().intValue() == geSession) ||
							(geSession == 0 && geFieldmapGroup == 0))
					) {
				inGEFieldmapGroup = false;
				if (geFieldmapGroup>0) {
					scan.setParameters_gefieldmapgroup(geFieldmapGroup);
				} else {
					// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
					// to any fieldmaps being collected and the shim values don't normally match
					scan.setParameters_gefieldmapgroup(1);
				}
			} else if (!scan.getType().matches(GEFIELDMAP_RGX) && inGEFieldmapGroup) {
				inGEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inGEFieldmapGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == geSession) {
				scan.setParameters_gefieldmapgroup(geFieldmapGroup);
			}
			// Update series description for GE FieldMap Group
			if (scan.getType().matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inGEFieldmapGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("FieldMap_", "FieldMap" + geFieldmapGroup + "_"));
			}
		}
	}		
	
	
	@Override
	protected void setPEFields(XnatMrscandata scan,String seriesDesc) {
		boolean hasNiftiResource = false;
		boolean skipPeCheck = false;
		try {
			//Float ipe_rotation = null;
			// 2018-08-13 - We are now pulling PE direction from scan NIFTI JSON rather than calculating it.
			// Per e-mails with Mike Harms, Leah, et. al, we will throw an error if ipe_rotation is missing, but if we 
			// override the error, we will treat the ipe_rotation value as zero
			for (final XnatAbstractresourceI resourceI : scan.getFile()) {
				if (!(resourceI instanceof XnatResource && resourceI.getLabel().equalsIgnoreCase(CommonConstants.NIFTI_RESOURCE))) {
					continue;
				}
				final XnatResource resource = (XnatResource)resourceI;
				if (resource == null || resource.getFileResources(CommonConstants.ROOT_PATH) == null || resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
					continue;
				}
				hasNiftiResource = true;
				for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
					final File f = rFile.getF();
					if (!f.getName().toLowerCase().endsWith(".json")) {
						continue;
					}
					final String json = FileUtils.readFileToString(f);
					Map<String,Object> jsonMap = gson.fromJson(json, new TypeToken<Map<String,Object>>(){}.getType());
					for (final String phaseEncodingDirectionKey : PHASE_ENCODING_DIRECTION_KEYS) {
						if (jsonMap.containsKey(phaseEncodingDirectionKey)) {
							if (phaseEncodingDirectionKey.contentEquals("PhaseEncodingAxis") && !jsonMap.containsKey("PhaseEncodingDirection")) {
								skipPeCheck = true;
								continue;
							}
							final Object peDirection = jsonMap.get(phaseEncodingDirectionKey);
							if (peDirection != null) {
								scan.setParameters_pedirection(peDirection.toString());
								checkPESeriesDesc(scan);
								return;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown retrieving PhaseEncodingDirection - " + e.toString());
		}
		if (scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(TFMRI_RGX) ||
				scan.getType().matches(RFMRI_RGX) || scan.getType().matches(DMRI_RGX) ||
				scan.getType().matches(PCASL_RGX) || scan.getType().matches(HIRESSCANTYPE_RGX)) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			//getErrorList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SESSION=" + 
			// Often, during development, we don't yet have NIFTI built.  Let's issue a warning in this
			// case, since this should be caught during production as a sanity check failure
			if (!hasNiftiResource) {
				getWarningList().add("RULESWARNING:  PHASE ENCODING DIRECTION IS NULL - NO NIFTI RESOURCE - " +
									"Assuming Development Phase (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_pedirection() + ")");
				return;
			}
			if (skipPeCheck) {
				return;
			}
			getErrorList().add("RULESERROR:  PHASE ENCODING DIRECTION IS NULL (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_pedirection() + ")");
		}
	}
	
	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(SEFIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					if (scan.getType().matches(SEFIELDMAP_RGX)) {
 						getErrorList().add("RULESERROR:  SE Fieldmap pair has non-matching shim values (" +
 							"SESSION=" + 
 								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
 								", FieldMapGroup=" + fmGroup +")");
 						continue;
					}
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
									"so a new SE fieldmap group has been chosen. (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
					}
					if (!newGroup) {
						// CBA often has missing shim values.  Let's make everything a warning.
						getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected " +
								"SE fieldmap (diffusion scan = warning only), and a new fieldmap group could not be chosen. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}
	
	@Override
	protected void fieldmapsMustBeReleasedInPairs(List<XnatMrscandata> subjectScans) {
		if (true) {
			//return;
		}
		final List<XnatMrscandata> apList = new ArrayList<>();
		final List<XnatMrscandata> paList = new ArrayList<>();
		boolean hasReleasableAP = false;
		boolean hasReleasablePA = false;
		boolean inFieldmapGroup = false;
		Iterator<XnatMrscandata> i = subjectScans.iterator();
		while (i.hasNext()) {
		    final XnatMrscandata scan = i.next();
			if (scan.getType().matches(SEFIELDMAP_RGX)) {
				inFieldmapGroup = true;
				if (scan.getSeriesDescription().matches(AP_RGX)) {
					apList.add(scan);
					if (willBeReleased(scan)) {
						hasReleasableAP=true;
					}
				}else if (scan.getSeriesDescription().matches(PA_RGX)) {
					paList.add(scan);
					if (willBeReleased(scan)) {
						hasReleasablePA=true;
					}
				}
			}
			if (inFieldmapGroup && (!scan.getType().matches(SEFIELDMAP_RGX) || !i.hasNext())) {
				inFieldmapGroup = false;
				if (hasReleasableAP!=hasReleasablePA) {
					if (hasReleasableAP) {
						for (final XnatMrscandata apScan : apList) {
							setNoRelease(apScan);
						}
					} else if (hasReleasablePA) {
						for (final XnatMrscandata paScan : paList) {
							setNoRelease(paScan);
						}
					}
				}
				hasReleasableAP = false;
				hasReleasablePA = false;
				apList.clear();
				paList.clear();
			}
		}
		return;
	}
	
	@Override
	protected void removeFieldmapScansWithoutMainScan(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(SEFIELDMAP_RGX) && willBeReleased(scan)) {
				boolean hasMain = false;
				final Integer fmFmg = scan.getParameters_sefieldmapgroup();
				for (final XnatMrscandata scan2 : subjectScans) {
					final Integer s2FmFmg = scan2.getParameters_sefieldmapgroup();
					if (!scan2.getType().matches(SEFIELDMAP_RGX) && willBeReleased(scan2) && 
							s2FmFmg!=null && s2FmFmg.equals(fmFmg)) {
						hasMain = true;
						break;
					}
				}
				if (!hasMain) {
					setNoRelease(scan);
				}
			}
		}
	}



	@Override
	protected void flagScansWithMissingFieldmapGroup(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		// SE FieldMap
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(SEFIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_sefieldmapgroup(thisFm);
						getWarningList().add("RULESWARNING:  Scan's nornally assigned SE fieldmap either doesn't exist or is unusable, " +
								"so a new SE fieldmap group has been chosen. (SESSION=" +
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					// CBA often has missing shim values.  Let's make everything a warning.
					getWarningList().add("RULESWARNING:  Scan's normally selected SE fieldmap either doesn't exist or is unusable " +
							"and a new SE fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				}
			}
		}
		// GE FieldMap
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(GEFIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_gefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_gefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_gefieldmapgroup(thisFm);
						//getWarningList().add("RULESWARNING:  Scan's nornally assigned GE fieldmap either doesn't exist or is unusable, " +
						//		"so a new GE fieldmap group has been chosen. (SESSION=" +
						//		((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
						//		", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					// CBA often has missing shim values.  Let's make everything a warning.
					//getWarningList().add("RULESWARNING:  Scan's normally selected GE fieldmap either doesn't exist or is unusable " +
					//		"and a new GE fieldmap group could not be chosen. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				}
			}
		}
		// BIAS
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(BIAS_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_biasgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_biasgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_biasgroup(thisFm);
						//getWarningList().add("RULESWARNING:  Scan's nornally assigned BIAS scan either doesn't exist or is unusable, " +
						//		"so a new bias group has been chosen. (SESSION=" +
						//		((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
						//		", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					// CBA often has missing shim values.  Let's make everything a warning.
					//getWarningList().add("RULESWARNING:  Scan's normally selected bias either doesn't exist or is unusable " +
					//		"and a new BIAS group could not be chosen. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				}
			}
		}
	}
	
	

	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			if (scan.getType().contains("rfMRI")) {
				scan.setDbdesc(scan.getDbdesc().replace("BOLD", "rfMRI"));
			} else if (scan.getType().contains("tfMRI")) {
				scan.setDbdesc(scan.getDbdesc().replace("BOLD", "tfMRI"));
			} else if (scan.getType().contains("dMRI")) {
				scan.setDbdesc(scan.getDbdesc().replace("DWI", "dMRI"));
			} else if (scan.getType().startsWith("FieldMap_")) {
				if (!scan.getSeriesDescription().contains("_")) {
					scan.setDbdesc(scan.getType());
				}
			} else if (scan.getSeriesDescription().startsWith("Perfusion_Weighted")) {
				scan.setDbdesc(scan.getSeriesDescription() + "_" + 
					scan.getParameters_tr().toString().replaceAll("\\..*$", ""));
			}
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				}
			}
			String dbDesc = scan.getDbdesc()
					.replaceAll("_WM[1234]_", "_WM_")
					.replaceAll("_MOTOR[1234]_", "_MOTOR_")
					;
			scan.setDbdesc(dbDesc);
		}
	}	
	

	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			boolean alreadyIncremented = false;
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
					alreadyIncremented=true;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
					} else {
						intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
						if (restCount>4) {
							getErrorList().add("RULESERROR:  CDB session contains more than four usable groups of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (intradbSessionRestCount>4) {
							getErrorList().add("RULESERROR:  Intradb session contains more than four usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (restGroupCount>2) {
							getErrorList().add("RULESERROR:  Intradb session group contains more than two groups of usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
					inRest=true;
				} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest && !alreadyIncremented) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest && !alreadyIncremented) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}
	
	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) {

    			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
                // ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=420) ? "Y" : "N";
                	scanPct = (double)frames10/(420-10);
                	scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
    					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_WM")) {
                		scanComplete = (frames>=372) ? "Y" : "N";
                		scanPct = (double)frames10/(372-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI WM scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_MOTOR")) {
                		scanComplete = (frames>=260) ? "Y" : "N";
                		scanPct = (double)frames10/(260-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI MOTOR/F scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.contains("_FACENAME")) {
                		scanComplete = (frames>=345) ? "Y" : "N";
                		//scanPct = (double)frames10/(345-10);
                		if (frames<315) {
                			getErrorList().add("RULESERROR:  tfMRI FACENAME scans should have at least 315 frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                }
                scan.setScancomplete(scanComplete);

            } else if (currDbdesc.matches(DMRI_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                //int frames10 = frames - 10;
                String scanComplete = null;
                //String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For FTD, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } 
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("CARIT")) {
                    continue;
            }
            if (currDbdesc.matches(DMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	
	
	protected void excludeUnneededBiasScans(List<XnatMrscandata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan)) {
				Integer currBias = scan.getParameters_biasgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}
	
	
	protected void useLaterBiasGroupIfMissing(List<XnatMrscandata> subjectScans) {
		// This method is to fix an issue first discovered with session 429040_3T where the initial set of bias scans
		// was unusable and a set was captured after the main scans.  These, having occurred in the same session,
		// should be usable, so when bias group is missing for a main scan, we'll use the later bias group in the
		// same session
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan)	) {
				Integer currBias = scan.getParameters_biasgroup();
				if (currBias != null ) {
					continue;
				}
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (compareBias!=null) {
							scan.setParameters_biasgroup(compareBias);
							break;
						}
					}
				}
			}
		}
	}
	
}

