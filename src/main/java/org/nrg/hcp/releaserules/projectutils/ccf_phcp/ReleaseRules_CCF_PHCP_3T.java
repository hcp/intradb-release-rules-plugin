package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_PHCP Project Session Building Release Rules")
public class ReleaseRules_CCF_PHCP_3T extends ReleaseRules_CCF_HCA {
	
	//private Object _lock = new Object();
	
	public ReleaseRules_CCF_PHCP_3T() {
		super();
		//SELECTION_CRITERIA = new String[] {"3T","7T","MRS"};
		SESSION_LABEL_EXT = "_V1_3T";
		SELECTION_CRITERIA = new String[] {"3T","7T"};
	    //SELECTION_CRITERIA = new String[] { "01" };
	}
	
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: 3T\n" +
				"    label: Select Session Type\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	if (newCriteria == '3T') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_7T)/,'_3T');\n" +
				"                 	}else if (newCriteria == '7T') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_3T)/,'_7T');\n" +
				"                 	}else if (newCriteria == 'MRS') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_3T|_7T)/,'_MRS');\n" +
				"                 	}\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        '3T': '3T'\n" +
				"        '7T': '7T'\n" +
				//"        'MRS': 'MRS'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		String sc = params.get("selectionCriteria");
		if (sc == null || sc.equals("ALL")) {
			sc="3T";
		}
		// Currently BWH isn't building longitudinal datasets, so this may be null.  They're building 01 datasets only.
		for (final XnatMrsessiondata session : projectExpts) {
			final String lcLabel = session.getLabel().toLowerCase();
			final String scanner = session.getScanner();
			final String sessionType = session.getSessionType();
			boolean isMRS = scanner.contains("7TAS") && (lcLabel.contains("mrs") || sessionType.endsWith("MRS"));
			boolean is7T = scanner.contains("7TAS") && !isMRS;
			boolean is3T = scanner.equals("TRIOC");
			if (!(is3T || is7T || isMRS)) {
				log.warn("WARNING:  Session " + session.getLabel() + " does not match any build type.");
				continue;
			}
			if ((sc.equals("3T") && is3T)) {
				//|| (sc.equals("7T") && is7T) || (sc.equals("MRS") && isMRS)) {
				filteredExpts.add(session);
			}
		}
		return filteredExpts;
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		String sc = params.get("selectionCriteria");
		if (sc == null) {
			sc = "3T";
		}
		String appendGroup = sc;
		if (sc.contains("_MRS_")) {
			appendGroup = sc.replace("_MRS_","_7") + "MRS";
		} else if (sc.contains("_7T_")) {
			appendGroup = sc.replace("_7T_", "_7");  
		}
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + "_" + appendGroup; 
	}
	
	@Override
	public boolean requireScanUidAnonymization() {
		return false;
	}
	
	@Override
	protected void flagIfNotAllDmriInSameShimGroup(List<XnatMrscandata> subjectScans) {
		// PHCP MODIFICATION!!!:  We're not flagging different shims for diffusion because for much of data collection 
		// dir98 and dir99 diffusion scans were collected in different sessions with different shims.
		return;
	}
	
	
	
	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            // NOTE HERE:  Let's not skip this code altogether so fieldmap still gets picked based on shims.  Let's just skip
            // outputting the error for non-rfMRI
			//if (currDbdesc == null || !(currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
			//	// PHCP MODIFICATION!!!!!  For PHCP, only shim values of non fMRI scans often don't match the fieldmaps
			//	continue;
			//}
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					if (scan.getType().matches(FIELDMAP_RGX)) {
 						getErrorList().add("RULESERROR:  Fieldmap pair has non-matching shim values (" +
 							"SESSION=" + 
 								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
 								", FieldMapGroup=" + fmGroup +")");
 						continue;
					}
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							// MRH 2020/10/23:  Removing skipping of checks for not-yet-built projects pending review.  We 
							// only want to skip checks when necessary.  We want to make sure shim issues are fully reviewed first.
							// I'm not sure these have been fully checked across all projects at this point.
							// PHCP MODIFICATION!!!!!  For PHCP, only shim values of non fMRI scans often don't match the fieldmaps
							//if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
								getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
										"so a new fieldmap group has been chosen. (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
										", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							//}
						}
					}
					if (!newGroup) {
						// MRH 2020/10/23:  Removing skipping of checks for not-yet-built projects pending review.  We 
						// only want to skip checks when necessary.  We want to make sure shim issues are fully reviewed first.
						// I'm not sure these have been fully checked across all projects at this point.
						// PHCP MODIFICATION!!!!!  For PHCP, only shim values of non fMRI scans often don't match the fieldmaps
						//if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
						if (currDbdesc != null && !(currDbdesc.matches(DMRI_RGX))) {
							getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						} else if (currDbdesc != null) {
							getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap (diffusion scan = warning only), and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							
						}
					}
				}
			}
		}
	}
	
}

