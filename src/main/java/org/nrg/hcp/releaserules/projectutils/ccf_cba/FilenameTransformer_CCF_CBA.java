package org.nrg.hcp.releaserules.projectutils.ccf_cba;

import java.io.File;

import org.nrg.ccf.sessionbuilding.releaserules.transformers.DefaultSeriesDescAndSessionLabelTransformer;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public class FilenameTransformer_CCF_CBA extends DefaultSeriesDescAndSessionLabelTransformer {
	
	@Override
	public String transformParentDirectoryPath(String fileDirectory, File sessionFile, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		fileDirectory = super.transformFilename(fileDirectory, srcSession, destSession, srcSessionScan, destSessionScan);
		final String fileName = sessionFile.getName();
		if (sessionFile.getPath() .contains("LINKED_DATA")) {
			if (fileName.endsWith(".txt") || fileName.endsWith(".edat2")) {
				fileDirectory = "TASK_TIMING";
			} else if (fileName.endsWith(".log")) {
				fileDirectory = "PHYSIO";
			}
			
		}
		return fileDirectory;
	}

}
