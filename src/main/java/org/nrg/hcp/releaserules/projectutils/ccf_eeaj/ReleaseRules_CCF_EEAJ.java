package org.nrg.hcp.releaserules.projectutils.ccf_eeaj;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.hcp.releaserules.projectutils.ccf_eeaj.ReleaseRules_CCF_EEAJ;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CcfReleaseRules(description = "CCF_EEAJ Project Session Building Release Rules")
public class ReleaseRules_CCF_EEAJ extends ReleaseRules_CCF_HCA {
	
	/**
	 * MRH  NOTE!!!!!  This class has had no work.  It's a placeholder for the EEAJ project.  The only reason it's 
	 * here is to note the following:
	 * 
	 * Per 02/19/2020 meeting with EEAJ, the EEAJ project will not be able to release their internal subject/session IDs.
	 * They are going to use a different study ID (NAS????) as the internal study ID.  They should be providing
	 * this on their GUID/VisitId/AgeInMonths spreadsheet.  This is what we'll use for staging IDs.
	 * 
	 * There should be a wiki page for this meeting with action items.
	 * 
	 */
	
	private static final Logger _logger = LoggerFactory.getLogger(ReleaseRules_CCF_EEAJ.class);
	public final String BIAS_RGX = "(?i)^BIAS_.*"; 
	public static final String UNKNOWN_LABEL = "Unknown";
	public static final String SESSION_BUILDING_LABEL = "session_building_label";
	public static final String EEAJTYPE_RGX = "(?i)((^IR)|(PCASL)|(ASL)|(PWI)|(MoCo)|(FLAIR)|(T2Star)|(CBF)|(PWI)|" 
			+ "(SWI)|(TOF)|(dMRI_TRACEW)|(dMRI_FA)|(dMRI_ADC)|(dMRI_EXP)|(TOF_MIP)|" +
			"(SWI_Images)|(SWI_mIP)|(FieldMap)|(Phase)|(T2_FLAIR)|(T1_Tra)|(T2_Tra)|(Matched_Bandwidth))";
	private Object _lock = new Object();
	// EEAJ difference (TEMPORARY):  Allowing usable and undetermined scan ratings - Hopefully all will get rated.
	
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		//rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	public ReleaseRules_CCF_EEAJ() {
		super();
		//SELECTION_CRITERIA = new String[] {"3T","7T","MRS"};
		SESSION_LABEL_EXT = "_MR";
		// Want to keep non-standard fieldmaps
		FIELDMAP_RGX = "^FieldMap_.*";
		SELECTION_CRITERIA = new String[] { "ALL" };
	    structuralQualityRatings = new String[] {"usable", "undetermined", "excellent","good","fair","poor"};
	}
	
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: PCP\n" +
				"    label: Select Session Type\n" +
				//"    after:\n" +
				//"        script:\n" +
				//"            tag: 'script'\n" +
				//"            content: >\n" +
				//"                 $('#selectionCriteria').change(function() {\n" +
				//"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				//"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				//"                 	if (newCriteria == 'ALL') {\n" +
				//"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_7T)/,'_3T');\n" +
				//"                 	}else if (newCriteria == '7T') {\n" +
				//"                 	   sessionLabel = sessionLabel.replace(/(_MRS|_3T)/,'_7T');\n" +
				//"                 	}else if (newCriteria == 'MRS') {\n" +
				//"                 	   sessionLabel = sessionLabel.replace(/(_3T|_7T)/,'_MRS');\n" +
				//"                 	}\n" +
				//"                 	$('#sessionLabel').val(sessionLabel);\n" +
				//"                 });\n" +
				"    options:\n" +
				"        'PCP': 'PCP'\n" +
				//"        '7T': '7T'\n" +
				//"        'MRS': 'MRS'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		String sc = params.get("selectionCriteria");
		if (sc == null) {
			sc = UNKNOWN_LABEL;
		}
		for (final XnatMrsessiondata session : projectExpts) {
			final Object groupObj = session.getFieldByName(SESSION_BUILDING_LABEL);
			final String group = (groupObj != null && groupObj instanceof String && 
						((String)groupObj).trim().length()>0) ? (String)groupObj : "";
			if (group.equals(sc)) {
				filteredExpts.add(session);
			}
		}
		return filteredExpts;
	}
	

	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			clearWarningAndErrorLists();
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			updateT1T2InclusionAndDesc(subjectScans); 
			updateNonNormalizedInclusionAndQuality(subjectScans);
			flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			flagScansWithMissingFieldmapGroup(subjectScans);
			flagIfNotAllDmriInSameShimGroup(subjectScans);
			markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			useLaterBiasGroupIfMissing(subjectScans);
			excludeUnneededBiasScans(subjectScans);
			setScanOrderAndScanComplete(subjectScans); 
			//updatePCASLInclusion(subjectScans);
			removeFieldmapScansWithoutMainScan(subjectScans);
			//removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			//flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride &&
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
		
	}
	
	public boolean isBiasScan(XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && (scan.getSeriesDescription().matches(BIAS_RGX))) {
			return true;
		} 
		return false;
	}
	

	@Override
	protected void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				log.error("ERROR: scan (key) should not be null!!!");
				log.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				log.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getSeriesDescription().matches(PHYSIO_RGX) ||
					(scan.getSeriesDescription().matches(SETTER_RGX) && !scan.getParameters_imagetype().contains("MOSAIC")) ||
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// Per M.Harms, 2018/01/12, let's only include specific scan types, so random ones that are sometimes collected
			// are excluded.
			if (!(isPcaslScan(scan) || isSetter(scan) || isHiresScan(scan) || 
					scan.getType().matches(FIELDMAP_RGX) ||
					scan.getType().matches(BIAS_RGX) ||
					scan.getType().matches(MAINSCANDESC_RGX) || 
					scan.getType().matches(EEAJTYPE_RGX) 
					)) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  " + ((isMainStructuralScan(scan)) ? "Structural" : "TSE HiRes")  +
								" normalized scan quality rating is invalid or scan has not been rated (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Scan quality rating is invalid (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			// If scan specifically set to Don't release, then let's count it but not release it
			if (isDontRelease(scan)) {
				scan.setReleasecountscan(true);
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setReleasecountscan(true);
		}
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				log.error("ERROR: scan (key) should not be null!!!");
				log.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				log.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
		}
	}
	
    @Override
	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					(scan.getQuality().equalsIgnoreCase("poor") ||
					// ACP difference:  Not mandating scan ratings.		
					 scan.getQuality().equalsIgnoreCase("usable") ||
					 scan.getQuality().equalsIgnoreCase("undetermined") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					scan.getQuality().equalsIgnoreCase("fair")) && (isMainStructuralScan(scan) && isNormalizedScan(scan))
				) {
           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
           		scan.setTargetforrelease(false);
				continue;
			}
			if (isDontRelease(scan)) { 
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
						(isMainStructuralScan(scan) && isNormalizedScan(scan))) {
					if (isT1NormalizedScan(scan)) {
						hasGoodT1 = true;
					} else if (isT2NormalizedScan(scan)) {
						hasGoodT2 = true;
					}
					//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
					//	prevScan.setTargetforrelease(true);
					//	prevScan.setQuality(scan.getQuality());
					//}
				}
			}
			if (!hasGoodT1 || !hasGoodT2) {
				boolean hasFairT1 = false, hasFairT2 = false;
				for (final XnatMrscandata scan : subjectScans) {
					if (isDontRelease(scan)) { 
						continue;
					}
					if ((!hasGoodT1 && isT1NormalizedScan(scan)) || (!hasGoodT2 && isT2NormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							if (isT1NormalizedScan(scan)) {
								hasFairT1 = true;
							} else if (isT2NormalizedScan(scan)) {
								hasFairT2 = true;
							}
							scan.setTargetforrelease(true);
							//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
							//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
							//	prevScan.setTargetforrelease(true);
							//	prevScan.setQuality(scan.getQuality());
							//}
						}
					}
				}
				if (!(hasGoodT1 || hasFairT1) || !(hasGoodT2 || hasFairT2)) {
					for (final XnatMrscandata scan : subjectScans) {
						if (isDontRelease(scan)) { 
							continue;
						}
						if ((!(hasGoodT1 || hasFairT1) && isT1NormalizedScan(scan)) || 
								(!(hasGoodT2 || hasFairT2) && isT2NormalizedScan(scan))) {
							// DMCC difference (TEMPORARY):  Allowing usable and undetermined scan ratings - Hopefully all will get rated.
							if (scan.getQuality().equalsIgnoreCase("poor")
									|| scan.getQuality().equalsIgnoreCase("usable")
									|| scan.getQuality().equalsIgnoreCase("undetermined")
									) {
								scan.setTargetforrelease(true);
							}
						}
					}
				}
			}
		}
		// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		// Releasing everything for EEAJ.
		//if (!hasValidT1AndT2(subjectScans)) {
		//	for (final XnatMrscandata scan : subjectScans) {
        //   		setNoRelease(scan);
		//	}
		//}
		
		// For one, let's alternate release all T1s and T2s not marked as _Alt	
		for (final XnatMrscandata scan : subjectScans) {
			if (isT1NormalizedScan(scan) || isT2NormalizedScan(scan)) {
				if (scan.getQuality()!=null && scan.getQuality().equalsIgnoreCase("unusable")) {
					continue;
				}
				if (!scan.getTargetforrelease() || 
						(scan.getReleaseoverride()!=null && scan.getReleaseoverride()<0)) {
					setAltRelease(scan);
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					if (prevScan !=null &&
						((isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) || 
						 (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)))) { 
							setAltRelease(prevScan);
						}
				}
			}
		}
	}
	

	@Override
	// This method is lifted from the BANDA class.  ACP is also not considering shims for T1/T2s
	protected void updateT1T2InclusionAndDesc(List<XnatMrscandata> subjectScans) {
		// Target T1, T2 from session that has both
		final ArrayList<Integer> t1List = new ArrayList<Integer>();
		final ArrayList<Integer> t2List = new ArrayList<Integer>();
		final ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getWarningList().add("RULESWARNING:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getType().matches(T1_NORM_RGX) || scan.getType().matches(T2_NORM_RGX)) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
						scan.setTargetforrelease(false);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isNormalizedScan(nextScan)) {
							setNoRelease(nextScan);
							nextScan.setTargetforrelease(false);
						}
					}
				}
			}
		}
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		// MRH 2022-03-17:  Change per Mike Harms.  We no longer care that T1/T2s have matching shims if we're not using the
		// fieldmaps.  MH explicitly prefers picking highest quality over matching shims per 03/16/22-03/17/22 e-mail thread.
		// We'll split shimGroup to t1ShimGroup and t2ShimGroup and just handle those separately.
		//char shimGroup = Character.MIN_VALUE;
		char t1ShimGroup = Character.MIN_VALUE;
		char t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getDbdesc()!=null && scan.getDbdesc().contains("_Alt")) {
				continue;
			}
			// Keeping only 1 T1/T2, Flag error if more than one 
			// BWH/BANDA CHANGE:  We're not ensuring T1/T2 shim groups match!
			// Keeping second only if in same shim group as first (NOT FOR BWH/BANDA!!!!)
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1Count!=0) {
					if (t1ShimGroup == Character.MIN_VALUE) {
						t1ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
					if (t1Count>1) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getWarningList().add("RULESWARNING:  There is currently more than one T1 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						setAltRelease(scan);
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setAltRelease(prevScan);
						}
					//if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
					//	if (t1ShimGroup == getShimGroup(scan)) {
					//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					//		getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
					//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					//	}
					//	t1Count--;
					//	setNoRelease(scan);
					//	if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
					//		setNoRelease(prevScan);
					//	}
					} else {
					    setRDFields(scan);
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2Count!=0) {
					if (t2ShimGroup == Character.MIN_VALUE) {
						t2ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
					if (t2Count>1) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getWarningList().add("RULESWARNING:  There is currently more than one T2 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						setAltRelease(scan);
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setAltRelease(prevScan);
						}
					//if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
					//	if (t2ShimGroup == getShimGroup(scan)) {
					//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					//		getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
					//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					//	}
					//	t2Count--;
					//	setNoRelease(scan);
					//	if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
					//		setNoRelease(prevScan);
					//	}
					} else {
					    setRDFields(scan);
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
		}
		t1Count=0;
		t2Count=0;
		t1ShimGroup = Character.MIN_VALUE;
		t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			if (scan.getDbdesc()!=null && scan.getDbdesc().contains("_Alt")) {
				continue;
			}
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1ShimGroup == Character.MIN_VALUE) {
					t1ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
				if (t1Count>1) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
							"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
								subjectSession.getLabel() +
							", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
						prevScan.setTargetforrelease(false);
					}
				//if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
				//	if (t1ShimGroup == getShimGroup(scan)) {
				//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				//		getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
				//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
				//					subjectSession.getLabel() +
				//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				//	}
				//	t1Count--;
				//	setNoRelease(scan);
				//	if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
				//		setNoRelease(prevScan);
				//	}
				} else {
				    setRDFields(scan);
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2ShimGroup == Character.MIN_VALUE) {
					t2ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
				if (t2Count>1) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
							"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
							", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
						prevScan.setTargetforrelease(false);
					}
				//if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
				//	if (t2ShimGroup == getShimGroup(scan)) {
				//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				//		getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
				//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
				//					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
				//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				//	}
				//	t2Count--;
				//	setNoRelease(scan);
				//	if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
				//		setNoRelease(prevScan);
				//	}
				} else {
				    setRDFields(scan);
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
		}
		// For ACP, since they always collected two sets of structurals, let's keep them but make them
		// alternate and put them in the "other_files" directory.
		// Only continue if some scans are set to release override
		boolean anyOverride = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (isStructuralScan(scan) && (scan.getReleaseoverride()!=null && scan.getReleaseoverride()>0)) {
				anyOverride = true;
			}
		}
		if (!anyOverride) {
			return;
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (isStructuralScan(scan) && !(scan.getReleaseoverride()!=null && scan.getReleaseoverride()>0)) {
				setAltRelease(scan);
			}
		}
	}		

	private void setAltRelease(XnatMrscandata scan) {
		if (scan.getReleaseoverride()!=null && scan.getReleaseoverride()>0) {
			// If release override, it is not an alternate
			return;
		}
		if (scan.getDbtype() == null || scan.getDbtype().trim().length()<1) {
			scan.setDbtype(scan.getType());
		}
		if (scan.getDbdesc() == null || scan.getDbdesc().trim().length()<1) {
			scan.setDbdesc(scan.getSeriesDescription());
		}
		scan.setDbtype(((scan.getDbtype()!=null && !scan.getDbtype().contains("_Alt")) ?
				scan.getDbtype() : scan.getType()) + "_Alt");
		scan.setDbdesc(((scan.getDbdesc()!=null && !scan.getDbtype().contains("_Alt")) ?
				scan.getDbdesc() : scan.getSeriesDescription()) + "_Alt");
		if (scan.getDbtype().contains("_Alt") && scan.getDbtype().contains("_Norm") && !scan.getDbdesc().contains("_Norm")) {
			scan.setDbdesc(scan.getDbdesc().replace("_Alt","_Norm_Alt"));
		}
		if (scan.getQuality() == null || !scan.getQuality().equalsIgnoreCase("unusable")) {
			scan.setTargetforrelease(true);
		}
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		String sc = params.get("selectionCriteria");
		if (sc == null || sc.trim().length()<1) {
			return UNKNOWN_LABEL;
		}
		final ArrayList<XnatSubjectassessordata> expts = subj.getExperiments_experiment(XnatMrsessiondata.SCHEMA_ELEMENT_NAME);
		for (final XnatSubjectassessordata expt : expts) {
			if (expt instanceof XnatMrsessiondata) {
				final XnatMrsessiondata mrSession = (XnatMrsessiondata)expt;
				final Object groupObj = mrSession.getFieldByName(SESSION_BUILDING_LABEL);
				final String group = (groupObj != null && groupObj instanceof String && 
						((String)groupObj).trim().length()>0) ? (String)groupObj : UNKNOWN_LABEL;
				if (group.equals(sc)) {
					return group;
				}
			}
		}
		return UNKNOWN_LABEL;
	}
	
	
	@Override
	protected void flagTaskError(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Integer> sessionMap = new HashMap<String,Integer>();
        final HashMap<String,String> dirMap = new HashMap<String,String>();
		for (final XnatMrscandata scan : subjectScans) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(TFMRI_RGX)) { 
               	final String part1=currDbdesc.replaceFirst(APPA_RGX,"");
               	// Too many scans to be released
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                	if (mapval>2) {
               			getWarningList().add("RULESWARNING:  Session contains more than two task scans targeted for release (" + part1 + ")");
                	}
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
               	// Multiple sessions containing scans
                if (sessionMap.containsKey(part1) && scan.getSubjectsessionnum()!=null) {
                	if (!sessionMap.get(part1).equals(scan.getSubjectsessionnum())) {
               			getWarningList().add("RULESWARNING:  Multiple sessions contain scans for the same task (" + part1 + ")");
                	}
                } else {
                	if (scan.getSubjectsessionnum()!=null) {
                		sessionMap.put(part1, scan.getSubjectsessionnum());
                	}
                }
               	// Multiple sessions containing same direction
                final String direction = (currDbdesc.matches(AP_RGX)) ? "AP" : "PA";
                if (dirMap.containsKey(part1) && direction!=null) {
                	if (dirMap.get(part1).equals(direction)) {
               			getWarningList().add("RULESWARNING:  Multiple sessions for same phase encoding direction for task (" + part1 + ")");
                	}
                } else {
                	if (direction!=null) {
                		dirMap.put(part1,direction);
                	}
                }
            }
		}
	}
	

	@Override
	protected void markAssociatedScansWithUnreleasedStructuralsAsUnreleased(
			List<XnatMrscandata> subjectScans) {
		// First find scans that will be released and keep their fieldmap values
		ArrayList<Integer> fmList = new ArrayList<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				Integer fieldMapGroup = scan.getParameters_sefieldmapgroup();
				if (!fmList.contains(fieldMapGroup)) { 
					fmList.add(fieldMapGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(STRUCT_RGX) && willBeReleased(scan) && 
					(scan.getParameters_sefieldmapgroup()==null || !fmList.contains(scan.getParameters_sefieldmapgroup()))) {
				// EEAJ Difference:  Release structurals without matching fieldmap
				// FYI:  This method doesn't seem to be doing what the method name would suggest.....  I'm effectively undoing it here.
				//setNoRelease(scan);
			}
		}
	}
	
	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan) && !isAlternateScan(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm") && !scan.getDbdesc().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				}
			}
			//if (scan.getDbdesc().contains("_Alt_Norm")) {
			//		scan.setDbdesc(scan.getDbdesc().replace("_Alt_Norm","_Norm_Alt"));
			//}
			String dbDesc = scan.getDbdesc()
					.replace("_Redo", "")
					.replace("_rerun", "")
					.replace("_repeat", "")
					.replace("REST0", "REST")
					.replace("_AP_2", "_AP")
					.replace("_PA_2", "_PA")
					.replace("_AP2", "_AP")
					.replace("_PA2", "_PA")
					.replace("resting_state_fMRI","rfMRI_REST")
					;
			scan.setDbdesc(dbDesc);
		}
	}
	
	
	private boolean isAlternateScan(XnatMrscandata intraScan) {
		final String type = intraScan.getDbtype();
		final String desc = intraScan.getDbdesc();
		if (type == null || desc == null) {
			return false;
		}
		return (type.endsWith("_Alt") && desc.endsWith("_Alt"));
	}
	
	@Override
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			
			// BIAS FIELD GROUP
			
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && !inBiasGroup) {
				inBiasGroup = true;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				biasFieldGroup++;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
				inBiasGroup = false;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (scan.getReleasecountscan() && inBiasGroup && scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
			}
		}
	}		
	
	
	
	@Override
	protected void setRDFields(XnatMrscandata scan) {
		// Like the ACP project, we're getting many missing readout direction values, so we won't set it.
		return;
	}
	

	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			boolean alreadyIncremented = false;
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
					alreadyIncremented=true;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
					} else {
						intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
						if (restCount>4) {
							getErrorList().add("RULESERROR:  CDB session contains more than four usable groups of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (intradbSessionRestCount>4) {
							getErrorList().add("RULESERROR:  Intradb session contains more than four usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (restGroupCount>2) {
							getErrorList().add("RULESERROR:  Intradb session group contains more than two groups of usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
					inRest=true;
				} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest && !alreadyIncremented) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest && !alreadyIncremented) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}

	@Override
	protected void checkPESeriesDesc(XnatMrscandata scan) {
		if (!(scan.getSeriesDescription().contains("_RL") ||
			scan.getSeriesDescription().contains("_LR") ||
			scan.getSeriesDescription().contains("_PA") ||
			scan.getSeriesDescription().contains("_AP"))) {
			return;
		}
		// EEAJ difference.  Lots of phase encoding issues.  we're keeping the data but renaming the scans
		if (!(scan.getParameters_pedirection().equals("i") && scan.getSeriesDescription().contains("_RL") ||
			scan.getParameters_pedirection().equals("i-") && scan.getSeriesDescription().contains("_LR") ||
			scan.getParameters_pedirection().equals("j") && scan.getSeriesDescription().contains("_PA") ||
			scan.getParameters_pedirection().equals("j-") && scan.getSeriesDescription().contains("_AP"))) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			getWarningList().add("RULESWARNING:  Series Description does not reflect PHASE ENCODING DIRECTION (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
					", PE=" + scan.getParameters_pedirection() + ")");
		}
	}


	@Override
	public boolean requireScanUidAnonymization() {
		return true;
	}

	protected void flagScansWithMissingFieldmapGroup(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				if (isBiasScan(scan)) {
					continue;
				}
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				final String currDbdesc = scan.getDbdesc();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_sefieldmapgroup(thisFm);
						getWarningList().add("RULESWARNING:  Scan's nornally assigned fieldmap either doesn't exist or is unusable, " +
								"so a new fieldmap group has been chosen. (SESSION=" +
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					if (currDbdesc != null && !(currDbdesc.matches(DMRI_RGX))) {
						getWarningList().add("RULESWARNING:  Scan's normally selected fieldmap either doesn't exist or is unusable, " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					} else if (currDbdesc != null) {
						getWarningList().add("RULESWARNING:  Scan's normally selected fieldmap either doesn't exist or is unusable " +
								"(diffusion scan = warning only), " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}

	
	//@Override
	//protected void flagIfNotAllDmriInSameShimGroup(List<XnatMrscandata> subjectScans) {
	//	// PHCP MODIFICATION!!!:  We're not flagging different shims for diffusion because for much of data collection 
	//	// dir98 and dir99 diffusion scans were collected in different sessions with different shims.
	//	return;
	//}
	
	
	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=420) ? "Y" : "N";
               		scanPct = (double)frames10/(420-10);
               		scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For EEAJ, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.toUpperCase().contains("_GAMB")) {
                		scanComplete = (frames>=228) ? "Y" : "N";
                		scanPct = (double)frames10/(228-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Gambling scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_WORKMEM")) {
                		scanComplete = (frames>=365) ? "Y" : "N";
                		scanPct = (double)frames10/(365-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI WorkMem scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_WM")) {
                		scanComplete = (frames>=365) ? "Y" : "N";
                		scanPct = (double)frames10/(365-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI WorkMem scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } 
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("CARIT")) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	


	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				String currDbdesc = scan.getDbdesc();
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					if (scan.getType().matches(FIELDMAP_RGX)) {
 						getWarningList().add("RULESERROR:  Fieldmap pair has non-matching shim values (" +
 							"SESSION=" + 
 								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
 								", FieldMapGroup=" + fmGroup +")");
 						continue;
					}
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							// NOTE:  For EEAJ, Structurals often don't have matching shims
							if (!isStructuralScan(scan)) { 
								getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
										"so a new fieldmap group has been chosen. (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
										", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							}
						}
					}
					// NOTE:  For EEAJ, Structurals often don't have matching shims
					if (!newGroup && !isStructuralScan(scan)) { 
						// For EEAJ, there are lots of shim problems.  Let's make all of them warnings.
						//getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
						//		"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
						//			((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
						//		", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected " +
								"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}
	
	
	@Override
	protected void flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(List<XnatMrscandata> subjectScans)
			throws ReleaseRulesException {
		Integer compFieldmapGroup = null;
		String compShimGroup = null;
		XnatMrscandata compScan = null;
		int releaseCount = 0;
		int requiresOverrideCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				releaseCount++;
				if (requiresOverride(scan)) {
					requiresOverrideCount++;
				}
				if (releaseCount>3) {
					getErrorList().add("RULESERROR:  Too many structural scans are marked for release.  Please " +
							"modify the release flag or usability for one or more scans.  This error should not be overridden.");
				}
				if (compFieldmapGroup == null || compShimGroup == null) {
					compFieldmapGroup = scan.getParameters_sefieldmapgroup();
					compShimGroup = scan.getParameters_shimgroup();
					compScan = scan;
				} else {
					final XnatMrsessiondata compSession = getScanSession(compScan, subjectSessions);
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					final Integer fmGroup = scan.getParameters_sefieldmapgroup();
					final String shimGroup = scan.getParameters_shimgroup();
					if (fmGroup != null && !fmGroup.equals(compFieldmapGroup)) {
						getWarningList().add("RULESERROR:  Structural scans to be released have different SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								scan.getParameters_sefieldmapgroup() + ")");
					} else if (fmGroup == null) {
						getWarningList().add("RULESERROR:  Structural scan to be released has null SeFieldmapGroup values (SESSIONS=" +
					
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - FIELDMAPGROUPS=" + compFieldmapGroup + "," +
								fmGroup + ")");
					}
					if (shimGroup != null && !shimGroup.equals(compShimGroup)) {
						// EEAJ Difference.  This happens often in EEAJ.  We'll just make it a warning. 
						getWarningList().add("RULESWARNING:  Structural scans to be released have different ShimGroup values (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								scan.getParameters_shimgroup() + ")");
					} else if (shimGroup == null) {
						getWarningList().add("RULESERROR:  Structural scan to be released has null ShimGroup value (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								shimGroup + ")");
					}
				}
			}
		}
		if (releaseCount<2) {
			getWarningList().add("RULESWARNING:  This session does not contain a complete T1/T2 pair marked for release.  " +
							"Note that this error requires a hard override in order to override.  Please edit scan " +
							"quality or use the release override flags to mark scans for release.");
		} else if (requiresOverrideCount>0) {
			getWarningList().add("RULESWARNING:  T1/T2 pair marked for release requires a release scan-level override flag to be released. " +
							"This could be due to insufficient scan quality or mismatched shim or fieldmap groups.  The override flag" +
					        " has been supplied but the session errorOverride flag should be used to submit this session so this notice is logged.");
		}
	}
	
	
	
	@Override
	protected void flagIfNotAllDmriInSameShimGroup(List<XnatMrscandata> subjectScans) {
		String compShimGroup = null;
		XnatMrscandata compScan = null;
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(DMRI_RGX)) { 
				if (compShimGroup == null) {
					compShimGroup = scan.getParameters_shimgroup();
					compScan = scan;
				} else {
					final XnatMrsessiondata compSession = getScanSession(compScan, subjectSessions);
					final XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
					final String shimGroup = scan.getParameters_shimgroup();
					if (shimGroup != null && !shimGroup.equals(compShimGroup)) {
						// EEAJ Difference:  This occurs regularly for EEAJ.  Let's make it a warning.
						getWarningList().add("RULESWARNING:  One or more diffusion scans to be released have different ShimGroup values (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								scan.getParameters_shimgroup() + ")");
					} else if (shimGroup == null) {
						getWarningList().add("RULESWARNING:  Diffusion scan to be released has null ShimGroup value (SESSIONS=" +
								((compSession!=null) ? compSession.getLabel() : compScan.getImageSessionId()) + "," +
								((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + "," + " - SCANS=" +
								compScan.getId() + "," + scan.getId() + " - SHIM=" + compShimGroup + "," +
								shimGroup + ")");
					}
				}
			}
		}
	}
	
	protected void excludeUnneededBiasScans(List<XnatMrscandata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan)) {
				Integer currBias = scan.getParameters_biasgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}
	
	
	protected void useLaterBiasGroupIfMissing(List<XnatMrscandata> subjectScans) {
		// This method is to fix an issue first discovered with session 429040_3T where the initial set of bias scans
		// was unusable and a set was captured after the main scans.  These, having occurred in the same session,
		// should be usable, so when bias group is missing for a main scan, we'll use the later bias group in the
		// same session
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan)	) {
				Integer currBias = scan.getParameters_biasgroup();
				if (currBias != null ) {
					continue;
				}
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (compareBias!=null) {
							scan.setParameters_biasgroup(compareBias);
							break;
						}
					}
				}
			}
		}
	}
	
	
	protected void setPEFields(XnatMrscandata scan,String seriesDesc) {
		boolean hasNiftiResource = false;
		try {
			//Float ipe_rotation = null;
			// 2018-08-13 - We are now pulling PE direction from scan NIFTI JSON rather than calculating it.
			// Per e-mails with Mike Harms, Leah, et. al, we will throw an error if ipe_rotation is missing, but if we 
			// override the error, we will treat the ipe_rotation value as zero
			for (final XnatAbstractresourceI resourceI : scan.getFile()) {
				if (!(resourceI instanceof XnatResource && resourceI.getLabel().equalsIgnoreCase(CommonConstants.NIFTI_RESOURCE))) {
					continue;
				}
				final XnatResource resource = (XnatResource)resourceI;
				if (resource == null || resource.getFileResources(CommonConstants.ROOT_PATH) == null || resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
					continue;
				}
				hasNiftiResource = true;
				for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
					final File f = rFile.getF();
					if (!f.getName().toLowerCase().endsWith(".json")) {
						continue;
					}
					final String json = FileUtils.readFileToString(f);
					Map<String,Object> jsonMap = gson.fromJson(json, new TypeToken<Map<String,Object>>(){}.getType());
					for (final String phaseEncodingDirectionKey : PHASE_ENCODING_DIRECTION_KEYS) {
						if (jsonMap.containsKey(phaseEncodingDirectionKey)) {
							final Object peDirection = jsonMap.get(phaseEncodingDirectionKey);
							if (peDirection != null) {
								scan.setParameters_pedirection(peDirection.toString());
								checkPESeriesDesc(scan);
								return;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown retrieving PhaseEncodingDirection - " + e.toString());
		}
		if (scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(TFMRI_RGX) ||
				scan.getType().matches(RFMRI_RGX) || scan.getType().matches(DMRI_RGX) ||
				scan.getType().matches(PCASL_RGX) || scan.getType().matches(HIRESSCANTYPE_RGX)) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			//getErrorList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SESSION=" + 
			// Often, during development, we don't yet have NIFTI built.  Let's issue a warning in this
			// case, since this should be caught during production as a sanity check failure
			getWarningList().add("RULESWARNING:  PHASE ENCODING DIRECTION IS NULL (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_pedirection() + ")");
		}
	}
	
	protected void flagDuplicateScans(List<XnatMrscandata> subjectScans) {
		final List<String> checkMultipleList = Arrays.asList(new String[] {
				"dMRI_dir98_AP", "dMRI_dir98_AP_TRACEW", "dMRI_dir98_PA", "dMRI_dir98_PA_TRACEW", "dMRI_dir99_AP", "dMRI_dir99_AP_TRACEW", "dMRI_dir99_PA", "dMRI_dir99_PA_TRACEW",
				"rfMRI_REST1_AP", "rfMRI_REST1_PA", "rfMRI_REST2_AP", "rfMRI_REST2_PA", "rfMRI_REST3_AP", "rfMRI_REST3_PA", "rfMRI_REST4_AP", "rfMRI_REST4_PA",
				"SpinEchoFieldMap1_AP", "SpinEchoFieldMap1_PA", "SpinEchoFieldMap2_AP", "SpinEchoFieldMap2_PA", "SpinEchoFieldMap3_AP", "SpinEchoFieldMap3_PA",
				"SpinEchoFieldMap4_AP", "SpinEchoFieldMap4_PA", "SpinEchoFieldMap5_AP", "SpinEchoFieldMap5_PA", "SpinEchoFieldMap6_AP", "SpinEchoFieldMap6_PA",
				"T1w_MPR", "T1w_MPR_Norm", "T2w_SPC", "T2w_SPC_Norm"
		});
		boolean keepgoing = true;
		//while (keepgoing) {
			//keepgoing = false;
			// Reverse the scans so later scans are incremented
			for (final XnatMrscandata scan : getReversedScanList(subjectScans)) {
				// field maps are excluded from this check, along with scans not planned for released, we do release multiple resting state
				// scans, but they will be named differently.
				//if (scan.getType().matches(ANYFIELDMAP_RGX) || !willBeReleased(scan) || scan.getSeriesDescription().matches(RFMRI_RGX)) {
				
				if (!willBeReleased(scan)) {
					continue;
				}
				int matchCount=0;
				for (final XnatMrscandata scan2 : subjectScans) {
					if (!scan.equals(scan2) && scan.getDbdesc() != null && scan2.getDbdesc() != null && 
							scan.getDbdesc().equals(scan2.getDbdesc()) &&
							scan.getType().equals(scan2.getType()) && willBeReleased(scan2)) {
						if (checkMultipleList.contains(scan.getDbdesc())) {
							getErrorList().add("RULESERROR:  Multiple matching scans slated for release (" +
									scan.getDbdesc() + ", " + getScanSession(scan, subjectSessions).getLabel() + ", " + scan.getId() + ", FRAMES=" + scan.getFrames() + ")");
						} else {
							//keepgoing = true;
							matchCount=matchCount+1;
							getWarningList().add("RULESWARNING:  Multiple matching scans slated for release (" +
									scan.getDbdesc() + ", " + getScanSession(scan, subjectSessions).getLabel() + ", " + scan.getId() + ", FRAMES=" + scan.getFrames() + ")");
						}
					}
				}
				if (matchCount>0) {
					incrementDbDesc(scan,matchCount+1);
				}
			}
		//}
	}


	private void incrementDbDesc(XnatMrscandata scan, int incValue) {
		scan.setDbdesc(incrementString(scan.getDbdesc(),incValue));
	}
	
	private String incrementString(String input,int incValue) {
	     // Regex to match an underscore followed by one or more digits at the end of the string
		if (input.contains("LANGUAGE")) {
	         return input.replaceAll("LANGUAGE(\\d+)*", "LANGUAGE" + incValue);
		} else if (input.contains("MOTOR_STRIP")) {
	         return input.replaceAll("MOTOR_STRIP(\\d+)*", "MOTOR_STRIP" + incValue);
		} else {
		     String regex = "_(\\d+)$";
		     // Try to match the input string against the pattern
		     java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(regex);
		     java.util.regex.Matcher matcher = pattern.matcher(input);
		     if (matcher.find()) {
		         // If we find a match, extract the number and increment it
		         //String numberStr = matcher.group(1);
		         //int number = Integer.parseInt(numberStr);
		         //number++;
		         // Reconstruct the string without the old number, then append the incremented number
		         return input.substring(0, matcher.start()) + "_" + incValue;
		     } else {
		         // No trailing number: Just append "_2"
		         return input + "_" + incValue;
		     }
	     }
	 }
	
}
