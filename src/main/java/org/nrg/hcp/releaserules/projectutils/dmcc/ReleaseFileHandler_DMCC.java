package org.nrg.hcp.releaserules.projectutils.dmcc;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.releaserules.transformers.DefaultSeriesDescAndSessionLabelTransformer;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "DMCC Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_DMCC extends AbstractNiftiWithLinkedDataReleaseFileHandler {
	
	{
		getPathMatchRegex().add("^.*/PHYSIO/.*.(txt|csv)");
		// FOR DMCC, some task files are misplaced
		getPathMatchRegex().add("^.*/LINKED_DATA/[^/]*.txt");
		getPathMatchRegex().add("^.*/EPRIME/.*.txt");
		// NOT TO BE SENT, Per JE
		//getPathMatchRegex().add("^.*/EYETRACKER/.*.edf");
		// NOT TO BE SENT, Per JE (possibly identifiable)
		//getPathMatchRegex().add("^.*/AUDIO/.*.wav");
	}

	public ReleaseFileHandler_DMCC(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_DMCC(), user);
	}

}
