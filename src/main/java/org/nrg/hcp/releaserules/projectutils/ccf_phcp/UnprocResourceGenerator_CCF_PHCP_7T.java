package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.UnprocResourceGenerator_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_PHCP Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_PHCP_7T extends UnprocResourceGenerator_CCF_HCA {
	
	public UnprocResourceGenerator_CCF_PHCP_7T(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
		biasScanTypes = new String[] {};
		tseHiresScanTypes = new String[] {};
		tseHiresSeriesDescriptions = new String[] {};
		tseHiresNormSeriesDescriptions = new String[] {};
		pcaslScanTypes = new String[] {};
		fieldmapSeriesDescriptions = new String[] { "BOLD_AP_SE", "BOLD_REV_AP_SE", "FieldMap_Magnitude", "FieldMap_Phase", "FieldMap", "BOLD_oppPE_AP" };
		fieldmapScanTypes = new String[] { "FieldMap_SE_EPI", "FieldMap_GE_EPI", "FieldMap_Magnitude", "FieldMap_Phase" };
		t1t2SeriesDescriptions = new String[] { "t1_mpr_tra_iso_ND" };
		fmriScanTypes = new String[] { "tfMRI" };
		dmriScanTypes = new String[] { };
	}
	
	@Override
	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					//scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					//scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		final Map<String, List<String>> strucSubdirMap = new HashMap<>();
		strucSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		strucSubdirMap.get("OTHER_FILES").add("(?i).*_iso[.].*$");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, strucSubdirMap, ci);
	}
	
	@Override
	public void generateResources() throws ReleaseResourceException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			createT1T2Resources(ci);
			createFmriResources(ci);
			//addReportToT1Resource(ci);
		} catch (Exception e) {
			throw new ReleaseResourceException("ERROR:  Could not create unproc resources", e);
		}
	}
	
	@Override
	public Collection<? extends XnatImagescandataI> getAssociatedStructuralScans(XnatImagescandataI scan) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : _combSession.getScans_scan()) {
			if (!mrScan.equals(cScan)) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				if (cScan.getType().startsWith(mrScan.getType()) && cScan.getSeriesDescription().equals("t1_mpr_tra_iso")) {
					returnList.add(cScan);
				}
			}
		}
		return returnList;
	}
	
	
	@Override
	public Collection<? extends XnatImagescandataI> getMatchingSefieldmapScansByType(XnatImagescandataI scan, String[] scanTypes) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		//final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : _combSession.getScans_scan()) {
			if (Arrays.asList(scanTypes).contains(cScan.getType())) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				final XnatMrscandata mrcScan = (XnatMrscandata)cScan;
				returnList.add(mrcScan);
			}
		}
		return returnList;
	}

}
