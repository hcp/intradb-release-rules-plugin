package org.nrg.hcp.releaserules.projectutils.ccf_mdp;

import java.util.List;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.UnprocResourceGenerator_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_MDP Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_MDP extends UnprocResourceGenerator_CCF_HCA {
	
	protected static String[] t1t2SeriesDescriptions = { "T1w_MPR", "T2w_SPC" };
	protected static String[] tseHiresScanTypes = { };
	
	public UnprocResourceGenerator_CCF_MDP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	}
	

	@Override
	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		// BANDA/BWH/ANXPE/MDP-specific change.  Shims don't match fieldmaps for BANDA/BWH/ANXPE/MDP, so we'll include them as "OTHER" files.
		final Map<String, List<String>> strucSubdirMap = new HashMap<>();
		strucSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(_setter|_InitialFrames).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(MPR|SPC)[.].*$");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, strucSubdirMap, ci);
	}
	
	@Override
	protected void createHiresResources(EventMetaI ci) throws ReleaseResourceException {
		// No HiRes Resources for MDP
		return;
	}
	
}
