package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_PHCP Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_PHCP extends AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler {
	
	{
		getPathMatchRegex().add("^.*/PHYSIO/.*csv");
		getPathMatchRegex().add("^.*/EYE_TRACKER/.*mp4");
		getPathMatchRegex().add("^.*/PSYCHOPY/.*txt");
		getPathMatchRegex().add("^.*/PSYCHOPY/.*log");
		getPathMatchRegex().add("^.*/PSYCHOPY/.*Report.xml");
		getPathMatchRegex().add("^.*/PSYCHOPY/EVs/.*");
	}
	
	// NOTE!!!!:    Per 01/30/2020 e-mail from Elijah, all DPX task data is recorded in one set of files.  We'll 
	// need to figure out how to organize and share this data
	public ReleaseFileHandler_CCF_PHCP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_PHCP() , user);
	}

}
