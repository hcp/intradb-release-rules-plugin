package org.nrg.hcp.releaserules.projectutils.ccf_mdp;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseFileHandler_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_MDP Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_MDP extends ReleaseFileHandler_CCF_HCA {
	
	{
		getPathMatchRegex().add("^.*/PHYSIO/.*.*csv");
		getPathMatchRegex().add("^.*/TASK_TIMING/.*.txt");
		getPathMatchRegex().add("^.*/TASK_TIMING/.*.mat");
		getPathMatchRegex().add("^.*/TASK_TIMING/.*.xml");
		getPathMatchRegex().add("^.*/TASK_TIMING/.*.txt");
		getPathMatchRegex().add("^.*/TASK_TIMING/EVs/.*");
		getPathMatchRegex().add("^.*/EYE_TRACKER/.*.mov");
		getPathMatchRegex().add("^.*/EYE_TRACKER/.*.mat");
		getPathMatchRegex().add("^.*/EYE_TRACKER/.*.mp4");
	}
	
	public ReleaseFileHandler_CCF_MDP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
	}

}
