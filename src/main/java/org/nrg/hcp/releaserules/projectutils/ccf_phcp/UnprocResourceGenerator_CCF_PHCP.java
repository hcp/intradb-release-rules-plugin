package org.nrg.hcp.releaserules.projectutils.ccf_phcp;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.UnprocResourceGenerator_CCF_HCA;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_PHCP Project Unprocessed Resource Generator")
public class UnprocResourceGenerator_CCF_PHCP extends UnprocResourceGenerator_CCF_HCA {
	
	// NOTE!!!!:    Per 01/30/2020 e-mail from Elijah, all DPX task data is recorded in one set of files.  We'll 
	// need to figure out how to organize and share this data
	public UnprocResourceGenerator_CCF_PHCP(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	}

}
