package org.nrg.hcp.releaserules.projectutils.ccf_cba;

import java.util.List;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.releaserules.abst.AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "CCF_CBA Project file handler - NIFTI and LinkedData")
public class ReleaseFileHandler_CCF_CBA extends AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler {	

	{
		getPathMatchRegex().add("^.*/LINKED_DATA/.*.txt");
		getPathMatchRegex().add("^.*/LINKED_DATA/.*.*.log");
		getPathMatchRegex().add("^.*/LINKED_DATA/.*.edat2");
	}
	
	public ReleaseFileHandler_CCF_CBA(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession,
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, new FilenameTransformer_CCF_CBA(), user);
	}
	
}
