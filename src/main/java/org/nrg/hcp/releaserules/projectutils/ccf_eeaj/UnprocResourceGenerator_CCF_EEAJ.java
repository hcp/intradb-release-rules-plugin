package org.nrg.hcp.releaserules.projectutils.ccf_eeaj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.hcp.releaserules.projectutils.ccf_mdp.UnprocResourceGenerator_CCF_MDP;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;

@CcfResourceGenerator(description = "CCF_EEAJ Project Unprocessed Resource Generator")
// NOTE:  Starting out with CCF_MDP project unprocessed generator since series descriptions are the same for EEAJ
public class UnprocResourceGenerator_CCF_EEAJ extends UnprocResourceGenerator_CCF_MDP {
	
	protected static String[] seFieldmapScanTypes = { "FieldMap_SE_EPI" };
	protected static String[] geFieldmapScanTypes = new String[] { "FieldMap_Magnitude", "FieldMap_Phase" };
	public static final String EEAJTYPE_RGX = "(?i)((^IR)|(PCASL)|(ASL)|(PWI)|(MoCo)|(FLAIR)|(T2Star)|(CBF)|(PWI)|" 
			+ "(SWI)|(TOF)|(dMRI_TRACEW)|(dMRI_FA)|(dMRI_ADC)|(dMRI_EXP)|(TOF_MIP)|" +
			"(SWI_Images)|(SWI_mIP)|(FieldMap)|(Phase)|(T2_FLAIR)|(T1_Tra)|(T2_Tra)|(Matched_Bandwidth))";
	//protected final static String[] t1t2SeriesDescriptions = { "T1w_MPR", "T2w_SPC" };
	
	public UnprocResourceGenerator_CCF_EEAJ(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, 
			CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	    t1t2SeriesDescriptions = new String[] { 
	    		"Basic_Sagittal", "MPRage-3T", "MPRAGE_GRAPPA2", "SAG_MPRAGE", "SAG_MPRAGE_2", 
	    		"t1_mp2rage_sag_p3_0.75mm_INV1", "t1_mp2rage_sag_p3_0.75mm_INV2", "t1_mp2rage_sag_p3_0.75mm_UNI_Images", 
	    		"T1w_MPR", "T2w_SPC" 
	    };
	    biasScanTypes = new String[] { "Bias_Receive", "BIAS_BC", "BIAS_32CH" };
	    fieldmapScanTypes = new String[] { "FieldMap_SE_EPI", "FieldMap_Magnitude", "FieldMap_Phase" };
	}
	
	@Override
	public void generateResources() throws ReleaseResourceException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			createT1T2Resources(ci);
			createFmriResources(ci);
			createDmriResources(ci);
			addReportToT1Resource(ci);
			createOtherScansResources(ci);
		} catch (Exception e) {
			throw new ReleaseResourceException("ERROR:  Could not create unproc resources", e);
		}
	}
	

	@Override
	protected void createT1T2Resources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String sd : Arrays.asList(t1t2SeriesDescriptions)) {
				final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, sd);
				if (sd.equals(scan.getSeriesDescription())) {
					scanList.add(scan);
					scanList.addAll(getAssociatedStructuralScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		// Shims often don't match fieldmaps for DMCC, so we'll include them as "OTHER" files.
		final Map<String, List<String>> strucSubdirMap = new HashMap<>();
		strucSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		strucSubdirMap.get("OTHER_FILES").add("(?i).*(MPR|SPC)(?!.*_Norm).*$");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, strucSubdirMap, ci);
	}

	@Override
	protected void createFmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(fmriScanTypes)) {
				if (st.equals(scan.getType())) {
					final List<XnatImagescandataI> scanList = getScanListFromMap(assignMap, scan.getSeriesDescription());
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, fieldmapScanTypes));
				}
			}
		}
		final Map<String, List<String>> fmriSubdirMap = new HashMap<>();
		fmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());
		fmriSubdirMap.get("OTHER_FILES").add("(?i).*(Bias).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, fmriSubdirMap, ci);
	}
	
	@Override
	protected void createDmriResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		List<XnatImagescandataI> scanList = new ArrayList<>(); 
		assignMap.put("Diffusion", scanList);
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			for (final String st : Arrays.asList(dmriScanTypes)) {
				if (st.equals(scan.getType())) {
					scanList.add(scan);
					scanList.addAll(getMatchingSbrefScans(scan));
					scanList.addAll(getMatchingSefieldmapScansByType(scan, seFieldmapScanTypes));
					scanList.addAll(getMatchingGefieldmapScansByType(scan, geFieldmapScanTypes));
					scanList.addAll(getMatchingBiasScansByType(scan, biasScanTypes));
				}
			}
		}
		final Map<String, List<String>> dmriSubdirMap = new HashMap<>();
		dmriSubdirMap.put("OTHER_FILES", new ArrayList<String>());		
		dmriSubdirMap.get("OTHER_FILES").add("(?i).*(FieldMap|Bias).*");
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, dmriSubdirMap, ci);
	}
	
	protected void createOtherScansResources(EventMetaI ci) throws ReleaseResourceException {
		final Map<String,List<XnatImagescandataI>> assignMap = new HashMap<>();
		List<XnatImagescandataI> scanList = new ArrayList<>(); 
		assignMap.put("OtherScans", scanList);
		for (final XnatImagescandataI scan : getSession().getScans_scan()) {
			final String sd = scan.getSeriesDescription();
			final String stype = scan.getType();
			if (sd != null && (
					sd.contains("ASL") ||
					sd.contains("mIP_") ||
					sd.contains("Perfusion") ||
					sd.contains("SWI")
					)) {
				scanList.add(scan);
			} else if (stype != null && (stype.matches(EEAJTYPE_RGX))) {
				scanList.add(scan);
			}
		}
		final Map<String, List<String>> otherSubdirMap = new HashMap<>();
		createSessionUnprocResourcesFromMap(assignMap, INCLUDED_RESOURCES, INCLUDED_RESOURCE_LABELS, otherSubdirMap, ci);
	}
	
}
