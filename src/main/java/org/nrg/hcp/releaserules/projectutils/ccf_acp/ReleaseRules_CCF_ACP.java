package org.nrg.hcp.releaserules.projectutils.ccf_acp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_ACP Project Session Building Release Rules")
public class ReleaseRules_CCF_ACP extends ReleaseRules_CCF_HCA {
	
	private Object _lock = new Object();
	public final String BIAS_RGX = "(?i)^BIAS_.*";
	public Map<List<XnatMrsessiondata>, TemporalComparator_ACP> _comparatorMap = new HashMap<>();
	
	public ReleaseRules_CCF_ACP() {
		super();
		SEFIELDMAP_RGX = "(?i)^FieldMap_SE_.*";
		AP_RGX = "(?i)^.*_AP(((_[23])|(_[23]_SBRef)|(_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_New)|(_dir[0-9][0-9]))?$"; 
		PA_RGX = "(?i)^.*_PA(((_[23])|(_[23]_SBRef)|(_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_New)|(_dir[0-9][0-9]))?$"; 
		SESSION_LABEL_EXT = "_MR";
		SELECTION_CRITERIA = new String[] {"PRIMARY","RETEST"};
		structuralQualityRatings = new String[] {"excellent","good","fair","poor","usable","undetermined"};
	    //SELECTION_CRITERIA = new String[] { "01" };
	}
	
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		//rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	@Override
	public List<String> getParametersYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"SelectionCriteria:\n" +
				"    id: selectionCriteria\n" +
				"    name: selectionCriteria\n" +
				"    kind: panel.select.single\n" +
				"    value: PRIMARY\n" +
				"    label: Select Visit\n" +
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#selectionCriteria').change(function() {\n" +
				"                 	var sessionLabel = $('#sessionLabel').val();\n" +
				"                 	var newCriteria = $('#selectionCriteria').val();\n" +
				"                 	if (newCriteria != 'RETEST') {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_RT)*_MR/,'_MR');\n" +
				"                 	} else {\n" +
				"                 	   sessionLabel = sessionLabel.replace(/(_RT)*_MR/,'_RT_MR');\n" +
				"                 	}\n" +
				"                 	$('#sessionLabel').val(sessionLabel);\n" +
				"                 });\n" +
				"    options:\n" +
				"        'PRIMARY': 'PRIMARY'\n" +
				"        'RETEST': 'RETEST'\n" +
				""
				;
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String sc = params.get("selectionCriteria");
		final boolean retestRequest = sc.equals("RETEST");
		// Currently BWH isn't building longitudinal datasets, so this may be null.  They're building 01 datasets only.
		for (final XnatMrsessiondata session : projectExpts) {
			final String lcLabel = session.getLabel().toLowerCase();
			final boolean retestSession = (lcLabel.contains("retest") && lcLabel.contains("calibration"));
			if ((!retestRequest && !retestSession) || (retestRequest && retestSession)) {
				filteredExpts.add(session);
			}
		}
		return filteredExpts;
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		final String sc = params.get("selectionCriteria");
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + 
				((sc == null || !sc.equals("RETEST")) ? SESSION_LABEL_EXT : "_RT_MR");
	}
	
	@Override
	public List<String> applyRulesToScans(
			final List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions,
			final Map<String,String> params, final boolean errorOverride)
			throws ReleaseRulesException {
		return this.applyRulesToScans(subjectScans, subjectSessions, errorOverride);
	}

	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			clearWarningAndErrorLists();
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			// Would need to be updated for _Pha scans...
			//fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			updateT1T2InclusionAndDesc(subjectScans); 
			updateNonNormalizedInclusionAndQuality(subjectScans);
			flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			flagScansWithMissingFieldmapGroup(subjectScans);
			flagIfNotAllDmriInSameShimGroup(subjectScans);
			markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			useLaterBiasGroupIfMissing(subjectScans);
			excludeUnneededBiasScans(subjectScans);
			setScanOrderAndScanComplete(subjectScans); 
			//updatePCASLInclusion(subjectScans);
			removeFieldmapScansWithoutMainScan(subjectScans);
			removeBiasScansWithoutMainScan(subjectScans);
			//removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			//flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride &&
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();

		}
		
	}

	@Override
	protected void setCountScanAndSession(List<XnatMrscandata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans) {
			if (scan == null) {
				log.error("ERROR: scan (key) should not be null!!!");
				log.error("subjectScans.keySet().size() - " + subjectScans.size());
				continue;
			}
			final XnatMrsessiondata currSess = getScanSession(scan, subjectSessions);
			if (currSess == null) {
				log.error("ERROR: currSess should not be null!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId());
				continue;
			} else {
				log.debug("OK: currSess is notnull!!! - " + scan.getId() + " - " +
							scan.getSeriesDescription() + " - " + scan.getImageSessionId() + 
							" - " + currSess.getLabel());
			}
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getSeriesDescription().matches(PHYSIO_RGX) ||
					(scan.getSeriesDescription().matches(SETTER_RGX) && !scan.getParameters_imagetype().contains("MOSAIC")) ||
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// Per M.Harms, 2018/01/12, let's only include specific scan types, so random ones that are sometimes collected
			// are excluded.
			if (!(isPcaslScan(scan) || isSetter(scan) || isHiresScan(scan) || isBiasScan(scan) ||
					scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(MAINSCANDESC_RGX))) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			if ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  " + ((isMainStructuralScan(scan)) ? "Structural" : "TSE HiRes")  +
								" normalized scan quality rating is invalid or scan has not been rated (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorList().add("RULESERROR:  Scan quality rating is invalid (SESSION=" +
							((subjectSession!=null) ? subjectSession.getLabel() : scan.getImageSessionId()) + ", SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			// If scan specifically set to Don't release, then let's count it but not release it
			if (isDontRelease(scan)) {
				scan.setReleasecountscan(true);
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setReleasecountscan(true);
		}
	}
	

	@Override
	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		
			// Sets targetForRelease and some other fields dependent on scanCount
			// Initial pass
			Integer prevSess = null;
			int ones = 0;
			for (final XnatMrscandata scan : subjectScans) {
				// 1)  COUNTSCAN - Exclude scans we're not counting
				if (!scan.getReleasecountscan()) { 
	           		setNoRelease(scan);
					scan.setDbid(null);
					continue;
							
				}
				if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
					ones = 0;
					prevSess = scan.getSubjectsessionnum();
				}
				ones++;
				scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
				if (
						// 2)  COUNTSCAN - Exclude scans with poor quality
						(scan.getQuality().equalsIgnoreCase("poor") ||
						// ACP difference:  Not mandating scan ratings.		
						 scan.getQuality().equalsIgnoreCase("usable") ||
						 scan.getQuality().equalsIgnoreCase("undetermined") ||
						// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
						scan.getQuality().equalsIgnoreCase("fair")) && ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan))
					) {
	           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
	           		scan.setTargetforrelease(false);
					continue;
				}
				scan.setTargetforrelease(true);
			}
			// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
			if (!hasValidT1AndT2(subjectScans)) {
				boolean hasGoodT1 = false, hasGoodT2 = false;
				for (final XnatMrscandata scan : subjectScans) {
					if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
							(isMainStructuralScan(scan) && isNormalizedScan(scan))) {
						if (isT1NormalizedScan(scan)) {
							hasGoodT1 = true;
						} else if (isT2NormalizedScan(scan)) {
							hasGoodT2 = true;
						}
						//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
						//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
						//	prevScan.setTargetforrelease(true);
						//	prevScan.setQuality(scan.getQuality());
						//}
					}
				}
				if (!hasGoodT1 || !hasGoodT2) {
					boolean hasFairT1 = false, hasFairT2 = false;
					for (final XnatMrscandata scan : subjectScans) {
						if ((!hasGoodT1 && isT1NormalizedScan(scan)) || (!hasGoodT2 && isT2NormalizedScan(scan))) {
							if (scan.getQuality().equalsIgnoreCase("fair")) {
								if (isT1NormalizedScan(scan)) {
									hasFairT1 = true;
								} else if (isT2NormalizedScan(scan)) {
									hasFairT2 = true;
								}
								scan.setTargetforrelease(true);
								//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
								//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
								//	prevScan.setTargetforrelease(true);
								//	prevScan.setQuality(scan.getQuality());
								//}
							}
						}
					}
					if (!(hasGoodT1 || hasFairT1) || !(hasGoodT2 || hasFairT2)) {
						for (final XnatMrscandata scan : subjectScans) {
							if ((!(hasGoodT1 || hasFairT1) && isT1NormalizedScan(scan)) || 
									(!(hasGoodT2 || hasFairT2) && isT2NormalizedScan(scan))) {
								// FTD difference (TEMPORARY):  Allowing usable and undetermined scan ratings - Hopefully all will get rated.
								if (scan.getQuality().equalsIgnoreCase("poor")
										|| scan.getQuality().equalsIgnoreCase("usable")
										|| scan.getQuality().equalsIgnoreCase("undetermined")
										) {
									scan.setTargetforrelease(true);
								}
							}
						}
					}
				}
			}
			/*
			if (!hasValidHiresScan(subjectScans)) {
				for (final XnatMrscandata scan : subjectScans) {
					if ((isHiresScan(scan) && isNormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							scan.setTargetforrelease(true);
						}
					}
				}
			}
			if (!hasValidHiresScan(subjectScans)) {
				// 2018-08-13.  We'll release even poor HiRes scans if we don't have one of higher quality.
				for (final XnatMrscandata scan : subjectScans) {
					if ((isHiresScan(scan) && isNormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("poor")) {
							scan.setTargetforrelease(true);
						}
					}
				}
			}
			*/
			// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
			if (!hasValidT1AndT2(subjectScans)) {
				for (final XnatMrscandata scan : subjectScans) {
	           		setNoRelease(scan);
				}
			}
			
		// SOME OTHER STUFF

		// For one, let's alternate release all T1s and T2s not marked as _Alt	
		for (final XnatMrscandata scan : subjectScans) {
			if (isT1NormalizedScan(scan) || isT2NormalizedScan(scan)) {
				if (scan.getQuality()!=null && scan.getQuality().equalsIgnoreCase("unusable")) {
					continue;
				}
				if (!scan.getTargetforrelease() || 
						(scan.getReleaseoverride()!=null && scan.getReleaseoverride()<0)) {
					setAltRelease(scan);
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					if (prevScan !=null &&
						((isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) || 
						 (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)))) { 
							setAltRelease(prevScan);
						}
				}
			}
		}

	    // ACP named their rest groups and sometimes hand no separation between collections.  Let's use their names.
		boolean hasRest2 = false;	
		for (final XnatMrscandata scan : subjectScans) {
			if ( scan.getType() == null ) { 
				setNoRelease(scan);
				scan.setReleasecountscan(false);
			}
			// Remove unwanted portions from series descrptions
			final String sd = scan.getSeriesDescription();
			if (sd == null) {
				continue;
			}
			if (sd.contains("_REST_") && (sd.contains("_2_") || sd.endsWith("_2"))) {
				hasRest2 = true;
				final String dbdesc = sd.replaceAll("_REST_","_REST2_").replaceAll("_2_","_").replaceAll("_2$","");
				scan.setDbdesc(dbdesc);
			}
		}
		if (hasRest2) {
			for (final XnatMrscandata scan : subjectScans) {
				if ( scan.getType() == null ) { 
					setNoRelease(scan);
					scan.setReleasecountscan(false);
				}
				// Remove unwanted portions from series descrptions
				final String sd = (scan.getDbdesc()!=null) ? scan.getDbdesc() : scan.getSeriesDescription();
				if (sd == null) {
					continue;
				}
				if (sd.contains("_REST_")) {
					final String dbdesc = sd.replaceAll("_REST_","_REST1_");
					scan.setDbdesc(dbdesc);
				}
			}
		}
		
		for (final XnatMrscandata scan : subjectScans) {
			// Removing fieldmap phpase scans and diffusion TRACEW scans
			if ( scan.getType() == null || scan.getType().equalsIgnoreCase("FieldMap_SE_EPI_Pha") ||  
					scan.getType().equalsIgnoreCase("dMRI_Trace")) { 
				setNoRelease(scan);
				scan.setReleasecountscan(false);
			}
			// Remove unwanted portions from series descrptions
			final String sd = (scan.getDbdesc()!=null) ? scan.getDbdesc() : scan.getSeriesDescription();
			if (sd == null) {
				continue;
			}
			final String dbdesc = sd.replaceAll("Working[ _]Memory[ _][1234]","Working_Memory")
					.replaceAll("Emotion[ _][1234]","Emotion")
					.replaceAll("[ _]AP[ _][1234]","_AP")
					.replaceAll("[ _]PA[ _][1234]","_PA")
					;
			scan.setDbdesc(dbdesc);
		}
		
	}
	

	protected void checkPESeriesDesc(XnatMrscandata scan) {
		if (!(scan.getSeriesDescription().contains("_RL") ||
			scan.getSeriesDescription().contains("_LR") ||
			scan.getSeriesDescription().contains("_PA") ||
			scan.getSeriesDescription().contains("_AP"))) {
			return;
		}
		if (!(scan.getParameters_pedirection().equals("i") && scan.getSeriesDescription().contains("_RL") ||
			scan.getParameters_pedirection().equals("i-") && scan.getSeriesDescription().contains("_LR") ||
			scan.getParameters_pedirection().equals("j") && scan.getSeriesDescription().contains("_PA") ||
			scan.getParameters_pedirection().equals("j-") && scan.getSeriesDescription().contains("_AP"))) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			getWarningList().add("RULESWARNING:  Series Description does not reflect PHASE ENCODING DIRECTION (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
					", PE=" + scan.getParameters_pedirection() + ")");
		}
	}
	
	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan) && !isAlternateScan(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm") && !scan.getDbdesc().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				}
			}
			//if (scan.getDbdesc().contains("_Alt_Norm")) {
			//		scan.setDbdesc(scan.getDbdesc().replace("_Alt_Norm","_Norm_Alt"));
			//}
			if (scan.getType().startsWith("tfMRI") && !scan.getDbdesc().startsWith("tfMRI")) {
					scan.setDbdesc("tfMRI_" + scan.getDbdesc());
					if (scan.getDbdesc().contains("Working_Memory")) {
						scan.setDbdesc(scan.getDbdesc().replace("Working_Memory","WorkingMemory"));
					}
			}
		}
	}
	
	private boolean isAlternateScan(XnatMrscandata intraScan) {
		final String type = intraScan.getDbtype();
		final String desc = intraScan.getDbdesc();
		if (type == null || desc == null) {
			return false;
		}
		return (type.endsWith("_Alt") && desc.endsWith("_Alt"));
	}
	
	@Override
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			//if ( scan.getType() == null || scan.getType().equalsIgnoreCase("FieldMap_SE_EPI_Pha") ||  
			//		scan.getType().equalsIgnoreCase("dMRI_Trace")) { 
			//	// Don't want to do anything based on these scans.
			//	continue;
			//}
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			
			// BIAS FIELD GROUP
			if (scan.getType().matches(BIAS_RGX) && isTargetOrOverrideAll(scan) && !inBiasGroup) {
				inBiasGroup = true;
				biasFieldGroup++;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getType().matches(BIAS_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
					 scan.getSubjectsessionnum().intValue() == biasSession) ||
							(biasSession == 0 && biasFieldGroup == 0))
					) {
				inBiasGroup = false;
				if (biasFieldGroup>0) {
					scan.setParameters_biasgroup(biasFieldGroup);
				} else {
					// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
					// to any fieldmaps being collected and the shim values don't normally match
					scan.setParameters_biasgroup(1);
				}
			} else if (!scan.getType().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inBiasGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getDbdesc() != null) {
				// MDP Difference.  MDP has one bias group per session.  Let's add Bias group to series description.
				String dbDesc = scan.getDbdesc()
					.replace("BIAS_", "BIAS" + biasFieldGroup + "_")
					;
				scan.setDbdesc(dbDesc);
			}
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
					 scan.getSubjectsessionnum().intValue() == seSession) ||
							(seSession == 0 && seFieldmapGroup == 0))
					) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				} else {
					// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
					// to any fieldmaps being collected and the shim values don't normally match
					scan.setParameters_sefieldmapgroup(1);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
				scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
			}
		}
	}		
	/*
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			if ( scan.getType() == null || scan.getType().equalsIgnoreCase("FieldMap_SE_EPI_Pha") ||  
					scan.getType().equalsIgnoreCase("dMRI_Trace")) { 
				// Don't want to do anything based on these scans.
				continue;
			}
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
					isTargetOrOverrideAll(scan) &&
					((scan.getSubjectsessionnum() != null &&
					 scan.getSubjectsessionnum().intValue() == seSession) ||
							(seSession == 0 && seFieldmapGroup == 0))
					) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				} else {
					// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
					// to any fieldmaps being collected and the shim values don't normally match
					scan.setParameters_sefieldmapgroup(1);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
					scan.getSubjectsessionnum() != null &&
					scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			// Update series description for SE FieldMap Group
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
				if (!isPcaslScan(scan)) {
					scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
				}
			}
		}
	}		
	*/
	
	
	@Override
	protected void setRDFields(XnatMrscandata scan) {
		// Per 2019-12-05 e-mail from M.Harms, we'll not set ReadoutDirection for ACP.  Many readout direction values are coming
		// out missing.  This is because of a missing value for "In-plane phase encoding rotation" value.  It's likely that
		// value is left out because it's zero, but we can't be sure.....
		return;
	}
	
	
	@Override
	// This method is lifted from the BANDA class.  ACP is also not considering shims for T1/T2s
	protected void updateT1T2InclusionAndDesc(List<XnatMrscandata> subjectScans) {
		// Target T1, T2 from session that has both
		final ArrayList<Integer> t1List = new ArrayList<Integer>();
		final ArrayList<Integer> t2List = new ArrayList<Integer>();
		final ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getWarningList().add("RULESWARNING:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getType().matches(T1_NORM_RGX) || scan.getType().matches(T2_NORM_RGX)) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
						scan.setTargetforrelease(false);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isNormalizedScan(nextScan)) {
							setNoRelease(nextScan);
							nextScan.setTargetforrelease(false);
						}
					}
				}
			}
		}
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		// MRH 2022-03-17:  Change per Mike Harms.  We no longer care that T1/T2s have matching shims if we're not using the
		// fieldmaps.  MH explicitly prefers picking highest quality over matching shims per 03/16/22-03/17/22 e-mail thread.
		// We'll split shimGroup to t1ShimGroup and t2ShimGroup and just handle those separately.
		//char shimGroup = Character.MIN_VALUE;
		char t1ShimGroup = Character.MIN_VALUE;
		char t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getDbdesc()!=null && scan.getDbdesc().contains("_Alt")) {
				continue;
			}
			// Keeping only 1 T1/T2, Flag error if more than one 
			// BWH/BANDA CHANGE:  We're not ensuring T1/T2 shim groups match!
			// Keeping second only if in same shim group as first (NOT FOR BWH/BANDA!!!!)
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1Count!=0) {
					if (t1ShimGroup == Character.MIN_VALUE) {
						t1ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
					if (t1Count>1) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getWarningList().add("RULESWARNING:  There is currently more than one T1 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						setAltRelease(scan);
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setAltRelease(prevScan);
						}
					//if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
					//	if (t1ShimGroup == getShimGroup(scan)) {
					//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					//		getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
					//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					//	}
					//	t1Count--;
					//	setNoRelease(scan);
					//	if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
					//		setNoRelease(prevScan);
					//	}
					} else {
					    setRDFields(scan);
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2Count!=0) {
					if (t2ShimGroup == Character.MIN_VALUE) {
						t2ShimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
					if (t2Count>1) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getWarningList().add("RULESWARNING:  There is currently more than one T2 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						setAltRelease(scan);
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setAltRelease(prevScan);
						}
					//if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
					//	if (t2ShimGroup == getShimGroup(scan)) {
					//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					//		getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
					//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					//	}
					//	t2Count--;
					//	setNoRelease(scan);
					//	if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
					//		setNoRelease(prevScan);
					//	}
					} else {
					    setRDFields(scan);
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
		}
		t1Count=0;
		t2Count=0;
		t1ShimGroup = Character.MIN_VALUE;
		t2ShimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			if (scan.getDbdesc()!=null && scan.getDbdesc().contains("_Alt")) {
				continue;
			}
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1ShimGroup == Character.MIN_VALUE) {
					t1ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
				if (t1Count>1) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
							"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
								subjectSession.getLabel() +
							", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
						prevScan.setTargetforrelease(false);
					}
				//if (t1Count>1 || t1ShimGroup!=getShimGroup(scan)) {
				//	if (t1ShimGroup == getShimGroup(scan)) {
				//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				//		getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
				//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
				//					subjectSession.getLabel() +
				//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				//	}
				//	t1Count--;
				//	setNoRelease(scan);
				//	if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
				//		setNoRelease(prevScan);
				//	}
				} else {
				    setRDFields(scan);
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2ShimGroup == Character.MIN_VALUE) {
					t2ShimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
				if (t2Count>1) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
							"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
							", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
						prevScan.setTargetforrelease(false);
					}
				//if (t2Count>1 || t2ShimGroup!=getShimGroup(scan)) {
				//	if (t2ShimGroup == getShimGroup(scan)) {
				//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				//		getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
				//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
				//					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
				//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				//	}
				//	t2Count--;
				//	setNoRelease(scan);
				//	if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
				//		setNoRelease(prevScan);
				//	}
				} else {
				    setRDFields(scan);
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
		}
		// For ACP, since they always collected two sets of structurals, let's keep them but make them
		// alternate and put them in the "other_files" directory.
		// Only continue if some scans are set to release override
		boolean anyOverride = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (isStructuralScan(scan) && (scan.getReleaseoverride()!=null && scan.getReleaseoverride()>0)) {
				anyOverride = true;
			}
		}
		if (!anyOverride) {
			return;
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (isStructuralScan(scan) && !(scan.getReleaseoverride()!=null && scan.getReleaseoverride()>0)) {
				setAltRelease(scan);
			}
		}
	}		

	private void setAltRelease(XnatMrscandata scan) {
		if (scan.getReleaseoverride()!=null && scan.getReleaseoverride()>0) {
			// If release override, it is not an alternate
			return;
		}
		if (scan.getDbtype() == null || scan.getDbtype().trim().length()<1) {
			scan.setDbtype(scan.getType());
		}
		if (scan.getDbdesc() == null || scan.getDbdesc().trim().length()<1) {
			scan.setDbdesc(scan.getSeriesDescription());
		}
		scan.setDbtype(((scan.getDbtype()!=null && !scan.getDbtype().contains("_Alt")) ?
				scan.getDbtype() : scan.getType()) + "_Alt");
		scan.setDbdesc(((scan.getDbdesc()!=null && !scan.getDbtype().contains("_Alt")) ?
				scan.getDbdesc() : scan.getSeriesDescription()) + "_Alt");
		if (scan.getDbtype().contains("_Alt") && scan.getDbtype().contains("_Norm") && !scan.getDbdesc().contains("_Norm")) {
			scan.setDbdesc(scan.getDbdesc().replace("_Alt","_Norm_Alt"));
		}
		if (scan.getQuality() == null || !scan.getQuality().equalsIgnoreCase("unusable")) {
			scan.setTargetforrelease(true);
		}
	}
	
	@Override
	protected void flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(List<XnatMrscandata> subjectScans)
			throws ReleaseRulesException {
		int releaseCount = 0;
		int requiresOverrideCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String scanType = scan.getType();
			if (isMainStructuralScan(scan) && !(scan.getDbdesc()!=null && scan.getDbdesc().contains("_Alt")) && isNormalizedScan(scan) && willBeReleased(scan)) {
				releaseCount++;
				if (releaseCount>=3) {
					hardErrorList.add("RULESERROR:  Too many structural scans are marked for release.  Please " +
							"modify the release flag or usability for one or more scans. This error should not be overridden.");
				}
				if (requiresOverride(scan)) {
					requiresOverrideCount++;
				}
				// NOTE: For ACP, we're not considering fieldmap groups or shim values for structurals.
			}
		}
		if (releaseCount<2) {
			hardErrorList.add("RULESERROR:  This session does not contain a complete T1/T2 pair marked for release.  " +
							"Note that this error cannot be overridden.  Please edit scan quality or use the release " +
							"override flags to mark scans for release.");
		} else if (requiresOverrideCount>0) {
			//getErrorList().add("RULESERROR:  T1/T2 pair marked for release requires a release scan-level override flag to be released. " +
			//		"This could be due to insufficient scan quality or mismatched shim or fieldmap groups.  The override flag" +
			//        " has been supplied but the session errorOverride flag should be used to submit this session so this notice is logged.");
			//
			// NOTE:  FOR ACP, not requiring error override when release flags are set.  We're making this a warning and building.
			//
			getWarningList().add("RULESWARNING:  T1/T2 pair marked for release requires a release scan-level override flag to be released. " +
					"This could be due to insufficient scan quality or mismatched shim or fieldmap groups.");
		}
	}
	
	
	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
            //final String currDbdesc = scan.getDbdesc();
            // NOTE HERE:  Let's not skip this code altogether so fieldmap still gets picked based on shims.  Let's just skip
            // outputting the error for non-rfMRI
			//if (currDbdesc == null || !(currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
			//	// For ACP, only shim values of resting state and task scans normally match fieldmaps.
			//	continue;
			//}
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					if (scan.getType().matches(FIELDMAP_RGX)) {
						// Turning this to warning for ACP session building
 						//getErrorList().add("RULESERROR:  Fieldmap pair has non-matching shim values (" +
 						getWarningList().add("RULESWARNING:  Fieldmap pair has non-matching shim values (" +
 							"SESSION=" + 
 								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
 								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() +
 								", FieldMapGroup=" + fmGroup +")");
 						continue;
					}
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							// MRH 2020/10/23:  Removing skipping of checks for not-yet-built projects pending review.  We 
							// only want to skip checks when necessary.  We want to make sure shim issues are fully reviewed first.
							// I'm not sure these have been fully checked across all projects at this point.
							//if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
								getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
										"so a new fieldmap group has been chosen. (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
										", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							//}
						}
					}
					if (!newGroup) {
						// MRH 2020/10/23:  Removing skipping of checks for not-yet-built projects pending review.  We 
						// only want to skip checks when necessary.  We want to make sure shim issues are fully reviewed first.
						// I'm not sure these have been fully checked across all projects at this point.
						//if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
						//if (currDbdesc != null && (!currDbdesc.matches(DMRI_RGX))) {
						//	getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
						//			"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
						//			((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
						//			", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						//} else if (currDbdesc != null) {
						// MRH:  2024/07/25 - Due to project descoping, all shim issues are warnings.
							getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap (diffusion scan = warning only), and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						//}
					}
				}
			}
		}
	}
	

	@Override
	protected void markAssociatedScansWithUnreleasedStructuralsAsUnreleased(
			List<XnatMrscandata> subjectScans) {
		// First find scans that will be released and keep their fieldmap values
		ArrayList<Integer> fmList = new ArrayList<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				Integer fieldMapGroup = scan.getParameters_sefieldmapgroup();
				if (!fmList.contains(fieldMapGroup)) { 
					fmList.add(fieldMapGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(STRUCT_RGX) && !(scan.getDbdesc()!=null && scan.getDbdesc().contains("_Alt")) && willBeReleased(scan) && 
					(scan.getParameters_sefieldmapgroup()==null || !fmList.contains(scan.getParameters_sefieldmapgroup()))) {
				setNoRelease(scan);
			}
		}
	}
	
	
	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=420) ? "Y" : "N";
               		scanPct = (double)frames10/(420-10);
               		scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA/HCD, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.toUpperCase().contains("_EMOTION")) {
                		scanComplete = (frames>=162) ? "Y" : "N";
                		scanPct = (double)frames10/(162-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Emotion scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_WORKING")) {
                		scanComplete = (frames>=374) ? "Y" : "N";
                		scanPct = (double)frames10/(374-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Working Memory scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } 
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("CARIT")) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	
	
	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		@SuppressWarnings("unused")
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			// For ACP, REST pairs are often captured together with only fieldmaps between them, so let's not skip
			// the fieldmaps here.
			if (currDbdesc != null && currDbdesc.matches(ANYFIELDMAP_RGX)) {
				if (inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				continue;
			} else if (currDbdesc == null) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (rarely occurrs).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
					} else {
						intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
						if (restCount>2) {
							getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						// I THINK THIS IS OKAY FOR THE ACP PROJECT
						//if (intradbSessionRestCount>2) {
						//	getErrorList().add("RULESERROR:  Intradb session contains more than two usable Resting State scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
						if (restGroupCount>1) {
							getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
					inRest=true;
				} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				// ACP DIFFERENCE:  Only change if hasn't already been labeled by study
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST_","rfMRI_REST" + Integer.toString(restCount) + "_"));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}
	

	@Override
	protected void flagScansWithMissingFieldmapGroup(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
			if (willBeReleased(scan)) {
				// MDP Difference:  No fieldmaps are fine for bias scans
				if (isBiasScan(scan)) {
					continue;
				}
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				final String currDbdesc = scan.getDbdesc();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_sefieldmapgroup(thisFm);
						getWarningList().add("RULESWARNING:  Scan's nornally assigned fieldmap either doesn't exist or is unusable, " +
								"so a new fieldmap group has been chosen. (SESSION=" +
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					if (currDbdesc != null && !(currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(STRUCT_RGX))) {
						getErrorList().add("RULESERROR:  Scan's normally selected fieldmap either doesn't exist or is unusable, " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					} else if (currDbdesc != null) {
						getWarningList().add("RULESWARNING:  Scan's normally selected fieldmap either doesn't exist or is unusable " +
								"(diffusion scan = warning only), " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}
	
	public boolean isBiasScan(XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && (scan.getSeriesDescription().matches(BIAS_RGX))) {
			return true;
		} 
		return false;
	}
	
	@Override
	public Comparator<XnatImagescandata> getTemporalComparator(List<XnatMrsessiondata> expList) {
		if (!_comparatorMap.containsKey(expList)) {
			_comparatorMap.put(expList, new TemporalComparator_ACP(expList));
		}
		return _comparatorMap.get(expList);
	}
	
	/*
	private void setDayValuesFromDicom(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) {
		final TemporalComparator_ACP comparator = (TemporalComparator_ACP)getTemporalComparator(subjectSessions);
		final List<Date> dates = new ArrayList<>();
		for (final XnatMrscandata scan : subjectScans) {
			try {
				final DateInfo dateInfo = comparator.getDateInfo(scan);
				if (dateInfo.getDateProblem()) {
					getErrorList().add("RULESERROR:  " + dateInfo.getDateMessage());
					getErrorList().add(dateInfo.getDateMessage());
				}
				final Date dateInfoDate = dateInfo.getDate();
				if (dateInfoDate == null) {
					scan.setSessionday("");
					continue;
				}
				if (!dates.contains(dateInfoDate)) {
					dates.add(dateInfoDate);
				}
				scan.setSessionday(new Integer(dates.indexOf(dateInfoDate)+1).toString());
			} catch (IOException e) {
				log.error("Exception thrown pulling dateInfo from scan (ID=" + scan.getId() + 
						", SD=" + scan.getSeriesDescription() + ")");
			}
		}
	}
	*/
	
	protected void excludeUnneededBiasScans(List<XnatMrscandata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan)) {
				Integer currBias = scan.getParameters_biasgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}
	
	
	protected void useLaterBiasGroupIfMissing(List<XnatMrscandata> subjectScans) {
		// This method is to fix an issue first discovered with session 429040_3T where the initial set of bias scans
		// was unusable and a set was captured after the main scans.  These, having occurred in the same session,
		// should be usable, so when bias group is missing for a main scan, we'll use the later bias group in the
		// same session
		for (XnatMrscandata scan : subjectScans) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(MAINSCANDESC_RGX) && isTargetOrOverrideAll(scan)	) {
				Integer currBias = scan.getParameters_biasgroup();
				if (currBias != null ) {
					continue;
				}
				for (XnatMrscandata scan2 : subjectScans) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(BIAS_RGX) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (compareBias!=null) {
							scan.setParameters_biasgroup(compareBias);
							break;
						}
					}
				}
			}
		}
	}
	
	protected void removeBiasScansWithoutMainScan(List<XnatMrscandata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(BIAS_RGX) && willBeReleased(scan)) {
				boolean hasMain = false;
				final Integer fmFmg = scan.getParameters_biasgroup();
				for (final XnatMrscandata scan2 : subjectScans) {
					final Integer s2FmFmg = scan2.getParameters_biasgroup();
					if (!scan2.getType().matches(BIAS_RGX) && willBeReleased(scan2) && 
							s2FmFmg!=null && s2FmFmg.equals(fmFmg)) {
						hasMain = true;
						break;
					}
				}
				if (!hasMain) {
					setNoRelease(scan);
				}
			}
		}
	}
	
}

