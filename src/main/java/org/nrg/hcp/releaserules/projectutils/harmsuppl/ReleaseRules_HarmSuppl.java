package org.nrg.hcp.releaserules.projectutils.harmsuppl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;


import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_AABC Project Session Building Release Rules")
public class ReleaseRules_HarmSuppl extends ReleaseRules_CCF_HCA {
	
	//private static final Logger _logger = LoggerFactory.getLogger(ReleaseRules_CCF_AABC.class);
	{
		rptColMap.clear();
		rptColMap.put("xnat:imageScanData/sessionDay","Session Day");
		rptColMap.put("xnat:imageScanData/startTime","Acquisition Time");
		rptColMap.put("ID","Scan Number");
		rptColMap.put("type","Scan Type");
		rptColMap.put("series_description","Series Description");
		rptColMap.put("xnat:imageScanData/frames","Frames");
		rptColMap.put("quality","Usability");
		rptColMap.put("xnat:imageScanData/subjectSessionNum","Session");
		rptColMap.put("xnat:imageScanData/releaseCountScan","CountScan");
		rptColMap.put("xnat:imageScanData/targetForRelease","Release");
		rptColMap.put("xnat:imageScanData/releaseOverride","ReleaseOverride");
		rptColMap.put("xnat:imageScanData/dbID","CDB_Scan");
		rptColMap.put("xnat:imageScanData/dbType","CDB_Type");
		rptColMap.put("xnat:imageScanData/dbDesc","CDB_Description");
		rptColMap.put("xnat:imageScanData/viewScan","View");
		rptColMap.put("xnat:mrScanData/parameters/shimGroup","Shim Group");
		//rptColMap.put("xnat:mrScanData/parameters/biasGroup","BiasField Group");
		rptColMap.put("xnat:mrScanData/parameters/seFieldMapGroup","SE_FieldMap Group");
		//rptColMap.put("xnat:mrScanData/parameters/geFieldMapGroup","GE_FieldMap Group");
		rptColMap.put("xnat:mrScanData/parameters/peDirection","PE Direction");
		rptColMap.put("xnat:mrScanData/parameters/readoutDirection","Readout Direction");
		//rptColMap.put("xnat:mrScanData/parameters/eprimeScriptNum","E-Prime Script");
		rptColMap.put("xnat:mrScanData/parameters/scanOrder","Scan Order");
		rptColMap.put("xnat:mrScanData/scanComplete","Scan Complete");
		rptColMap.put("xnat:mrScanData/pctComplete","Percent Complete");
		rptColMap.put("xnat:mrScanData/pctPairComplete","Percent Pair Complete");
		rptColMap.put("xnat:imageScanData/dataRelease","Data Release");
	}
	
	
	public ReleaseRules_HarmSuppl() {
		super();
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		final String selectionCriteria = params.get("selectionCriteria");
		log.debug("SelectionCriteria Parameter Value:  " + selectionCriteria);
		for (final XnatMrsessiondata exp : projectExpts) {
			if (exp.getLabel().contains("_" + selectionCriteria)) {
				log.debug("Adding experiment to filteredExpts list:  " + exp.getLabel());
				filteredExpts.add(exp);
			}
		}
		return filteredExpts;
	}
	
	
	@Override
	protected void setGroupValues(List<XnatMrscandata> subjectScans) {
		
		int seFieldmapGroup = 0;
		int pcaslFieldmapGroup = 90;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		boolean inPCASLFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans) {
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			// SE FIELDMAP GROUP
			if (!isPcaslScan(scan)) {
				if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
					inSEFieldmapGroup = true;
					seFieldmapGroup++;
					seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
						isTargetOrOverrideAll(scan) &&
						((scan.getSubjectsessionnum() != null &&
						 scan.getSubjectsessionnum().intValue() == seSession) ||
								(seSession == 0 && seFieldmapGroup == 0))
						) {
					inSEFieldmapGroup = false;
					if (seFieldmapGroup>0) {
						scan.setParameters_sefieldmapgroup(seFieldmapGroup);
					} else {
						// If we haven't hit any fieldmaps yet, put with group 1.  For ACP, the T1's are collected prior
						// to any fieldmaps being collected and the shim values don't normally match
						scan.setParameters_sefieldmapgroup(1);
					}
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
					inSEFieldmapGroup = false;
				} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup &&
						scan.getSubjectsessionnum() != null &&
						scan.getSubjectsessionnum().intValue() == seSession) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
				// Update series description for SE FieldMap Group
				if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && inSEFieldmapGroup) {
					if (!isPcaslScan(scan)) {
						scan.setDbdesc(scan.getDbdesc().replaceFirst("SpinEchoFieldMap_", "SpinEchoFieldMap" + seFieldmapGroup + "_"));
						scan.setDbdesc(scan.getDbdesc().replaceFirst("DistortionMap_", "DistortionMap" + seFieldmapGroup + "_"));
					}
				}
			} else {
				if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inPCASLFieldmapGroup) {
					inPCASLFieldmapGroup = true;
					pcaslFieldmapGroup++;
					seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
					scan.setParameters_sefieldmapgroup(pcaslFieldmapGroup);
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && 
						isTargetOrOverrideAll(scan) &&
						scan.getSubjectsessionnum() != null &&
						scan.getSubjectsessionnum().intValue() == seSession) {
					inPCASLFieldmapGroup = false;
					if (pcaslFieldmapGroup>0) {
						scan.setParameters_sefieldmapgroup(pcaslFieldmapGroup);
					}
				} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
					inPCASLFieldmapGroup = false;
				} else if (isTargetOrOverrideAll(scan) && inPCASLFieldmapGroup &&
						scan.getSubjectsessionnum() != null &&
						scan.getSubjectsessionnum().intValue() == seSession) {
					scan.setParameters_sefieldmapgroup(pcaslFieldmapGroup);
				}
			}
		}
	}		
	

	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            // NOTE HERE:  Let's not skip this code altogether so fieldmap still gets picked based on shims.  Let's just skip
            // outputting the error for non-rfMRI
			//if (currDbdesc == null || !(currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
			//	// For BANDA, only shim values of fmri scans normally match fieldmaps.
			//	continue;
			//}
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							// For BANDA, only shim values of fmri scans normally match fieldmaps.
							if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
								getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
										"so a new fieldmap group has been chosen. (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
										", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							}
						}
					}
					if (!newGroup) {
						if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
							getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						} else if (currDbdesc != null) {
							getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
					}
				}
			}
		}
	}

	@Override
	protected void flagScansWithMissingFieldmapGroup(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || !currDbdesc.matches(RFMRI_RGX)) {
				// For BANDA, only shim values of resting state scans normally match fieldmaps.
				continue;
			}
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_sefieldmapgroup(thisFm);
						getWarningList().add("RULESWARNING:  Scan's nornally assigned fieldmap either doesn't exist or is unusable, " +
								"so a new fieldmap group has been chosen. (SESSION=" +
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					if (currDbdesc != null && !(currDbdesc.equals(DMRI_RGX))) {
						getErrorList().add("RULESERROR:  Scan's normally selected fieldmap either doesn't exist or is unusable, " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					} else if (currDbdesc != null) {
						getWarningList().add("RULESWARNING:  Scan's normally selected fieldmap either doesn't exist or is unusable" +
								" (diffions scan = warning only), " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}
	

	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					if (scan.getDbdesc().contains("_4e")) {
						scan.setDbdesc(scan.getDbdesc().replace("_4e", "_Norm_4e"));
					} else {
						scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
					}
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("TSE_")) {
					scan.setDbdesc(scan.getDbdesc().replace("TSE_", "TSE_Norm_"));
				}
				// HarmSuppl difference:  Append _Norm to scans that need it but don't have it
				if (!scan.getDbdesc().contains("_Norm")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				}
			}
			// Insert dirValue into dMRI_b0
			if (scan.getSeriesDescription().contains("dMRI_b0")) {
				XnatMrscandata nextScan = getNextScan(subjectScans,scan);
				final String currentDesc = scan.getSeriesDescription();
				XnatMrscandata compareScan = scan;
				String newDesc = currentDesc;
				String dirValue = "";
				while (nextScan != null) {
					Pattern pattern = null;
					final String nextDesc = nextScan.getSeriesDescription();
					if (nextDesc.contains("_dMRI") && nextDesc.contains("_dir")) {
						pattern = Pattern.compile("_dir[^_]*");
					} else if (nextDesc.contains("_dMRI") && nextDesc.contains("dir_")) {
						pattern = Pattern.compile("[^_]*dir_*");
					}
					compareScan = nextScan;
					if (pattern != null) {
						Matcher matcher = pattern.matcher(nextDesc);
						if (matcher.find()) {
							dirValue=matcher.group();
							if (dirValue.endsWith("_")) {
								dirValue = "_" + dirValue.subSequence(0, dirValue.length()-1);
							}
							break;
						}
					}
					nextScan = getNextScan(subjectScans,compareScan);
				}
				int index = currentDesc.indexOf("_b0_");
				if (index >=0) {
					newDesc = new StringBuilder(currentDesc).insert(index, dirValue).toString();
				}
				if (!(newDesc.equals(currentDesc))) {
					scan.setDbdesc(newDesc);
				}
			}
		}
	}
	
	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false, multiGroupAP = false, multiGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		// HarmSuppl difference.  Only expect one group of resting state scans.......
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					restGroupCount=0;
					multiGroupAP = false;
					multiGroupPA = false;
					groupHasAP = false;
					groupHasPA = false;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
					} else {
						intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (groupHasAP) {
								multiGroupAP = true;
							}
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (groupHasPA) {
								multiGroupPA = true;
							}
							groupHasPA = true;
						}
						if ((groupHasAP && groupHasPA) || multiGroupAP || multiGroupPA) {
							restGroupCount++;
						}
						// HarmSuppl difference:  Only expecting one group of resting state scans in this project.
						if (restCount>1) {
							getErrorList().add("RULESERROR:  CDB session contains more than one usable group of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (intradbSessionRestCount>2) {
							getErrorList().add("RULESERROR:  Intradb session contains more than two usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						if (restGroupCount>1) {
							getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
					}
					inRest=true;
				} else if ((prevRestSession!=null && !prevRestSession.equals(scan.getSubjectsessionnum())) && inRest) {
					inRest=false;
					restCount++;
					restGroupCount=0;
					groupHasAP = false;
					groupHasPA = false;
					multiGroupAP = false;
					multiGroupPA = false;
				}
				// HarmSuppl difference:  Since we're only expecting one group of scans, let's not number them.
				//scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST[0-9]?","rfMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
				multiGroupAP = false;
				multiGroupPA = false;
			}
		}
	}
	
}

