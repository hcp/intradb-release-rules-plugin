package org.nrg.hcp.releaserules.projectutils.ccf_banda;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_hca.ReleaseRules_CCF_HCA;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;

import com.google.common.collect.ImmutableList;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

//@HcpReleaseRules
@Slf4j
@CcfReleaseRules(description = "CCF_BANDA Project Session Building Release Rules")
public class ReleaseRules_CCF_BANDA extends ReleaseRules_CCF_HCA {
	
	public final String SESSION_LABEL_EXT = "_MR";
	
	public String FMRI_RGX = "(?i)^.*fMRI.*"; 
	public String CONFLICTFMRI_RGX = "(?i)^.*fMRI.*conflict.*";
	
	
	private Object _lock = new Object();
	
	public ReleaseRules_CCF_BANDA() {
		super();
		RFMRI_RGX = "(?i)^.*fMRI.*_rest.*"; 
		TFMRI_RGX = "(?i)^.*fMRI.*(gamb|facem|conf).*";
		MAINSCAN_RGX = "(?i)^.*((T[12]w)|(fMRI)|(dMRI))";
		MAINSCANDESC_RGX = "(?i)^.*((T[12]w)|(fMRI)|(dMRI)).*$";
	    SELECTION_CRITERIA = new String[] { "01" };
	}

	@Override
	public List<String> getParametersYaml() {
		return ImmutableList.of();
	}
	
	@Override
	public List<XnatMrsessiondata> filterExptList(final List<XnatMrsessiondata> projectExpts, Map<String,String> params, UserI user) throws ReleaseRulesException {
		final List<XnatMrsessiondata> filteredExpts = new ArrayList<>();
		// Currently just one MR per subject for BANDA.  We'll just add them all.
		filteredExpts.addAll(projectExpts);
		return filteredExpts;
	}
	
	@Override
	public List<String> applyRulesToScans(
			final List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions,
			final Map<String,String> params, final boolean errorOverride)
			throws ReleaseRulesException {
		return this.applyRulesToScans(subjectScans, subjectSessions, errorOverride);
	}
	
	@Override
	public String getDefaultSessionLabel(final XnatProjectdata proj, final XnatSubjectdata subj, final Map<String,String> params) {
		final String sc = params.get("selectionCriteria");
		return CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + 
				(((sc != null && sc.length()>0 && !sc.equalsIgnoreCase("ALL")) ? "_" + sc : "") +
						SESSION_LABEL_EXT);
	}

	@Override
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, boolean errorOverride) throws ReleaseRulesException {
		
		// Needs to be synchronized because of use of subjectSessions and error and warning list fields
		synchronized (_lock) {
			
			clearWarningAndErrorLists();
		
			this.subjectSessions.clear();
			this.subjectSessions.addAll(subjectSessions);
			
			// NOTE:  Order of these calls is VERY important
			this.errorOverride = errorOverride;
			nullPriorValues(subjectScans); 
			setCountScanAndSession(subjectScans); 
			checkForNormalizedScans(subjectScans); 
			setTargetForReleaseAndScanID(subjectScans); 
			setTypeDescriptionAndOthers(subjectScans); 
			fieldmapsMustBeReleasedInPairs(subjectScans);
			setGroupValues(subjectScans); 
			updateT1T2InclusionAndDesc(subjectScans); 
			updateNonNormalizedInclusionAndQuality(subjectScans);
			flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(subjectScans);
			flagScansInDifferentShimGroupThanFieldmap(subjectScans);
			flagScansWithMissingFieldmapGroup(subjectScans);
			flagIfNotAllDmriInSameShimGroup(subjectScans);
			markAssociatedScansWithUnreleasedStructuralsAsUnreleased(subjectScans);
			// Remove this requirement for now
			//removeHiresScansWithoutT1T2(subjectScans);
			// Not requiring HiRes scans with T1/T2
			//flagT1T2withoutAssociatedHiresScan(subjectScans);
			updaterfMRIDescAndFlagError(subjectScans); 
			setScanOrderAndScanComplete(subjectScans); 
			//updatePCASLInclusion(subjectScans);
			removeFieldmapScansWithoutMainScan(subjectScans);
			//removeSettersAndOtherStructuralsWithoutMainStructural(subjectScans);
			flagTaskError(subjectScans); 
			//flagIfNoHiresScanMarkedForRelease(subjectScans);
			flagDuplicateScans(subjectScans); 
			fixBackslashProblem(subjectScans);
			if (!(hardErrorList.isEmpty() || (errorOverride && 
					_sessionBuildingPreferences.getCcfSessionBuildingHardOverride(subjectSessions.get(0).getProject())))) {
				throw new ReleaseRulesException(getWarningAndErrorSummaryList());
			}
			return getWarningAndErrorSummaryList();
			
		}
		
	}
	
	@Override
	public Comparator<XnatImagescandata> getTemporalComparator(List<XnatMrsessiondata> expList) {
		return new TemporalComparator_BANDA(expList);
	}
	
	

	@Override
	protected void setTargetForReleaseAndScanID(List<XnatMrscandata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
           		setNoRelease(scan);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					(scan.getQuality().equalsIgnoreCase("poor") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					scan.getQuality().equalsIgnoreCase("fair")) && ((isMainStructuralScan(scan) || isHiresScan(scan)) && isNormalizedScan(scan))
				) {
           		// NOTE:  Don't use setNoRelease method here.  May still revert the values set here, so we don't want other fields cleared.
           		scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) &&
						(isMainStructuralScan(scan) && isNormalizedScan(scan))) {
					if (isT1NormalizedScan(scan)) {
						hasGoodT1 = true;
					} else if (isT2NormalizedScan(scan)) {
						hasGoodT2 = true;
					}
					//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
					//	prevScan.setTargetforrelease(true);
					//	prevScan.setQuality(scan.getQuality());
					//}
				}
			}
			if (!hasGoodT1 || !hasGoodT2) {
				for (final XnatMrscandata scan : subjectScans) {
					if ((!hasGoodT1 && isT1NormalizedScan(scan)) || (!hasGoodT2 && isT2NormalizedScan(scan))) {
						if (scan.getQuality().equalsIgnoreCase("fair")) {
							scan.setTargetforrelease(true);
							//final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
							//if (isStructuralScan(prevScan) && !isNormalizedScan(prevScan)) {
							//	prevScan.setTargetforrelease(true);
							//	prevScan.setQuality(scan.getQuality());
							//}
						}
					}
				}
			}
		}
		if (!hasValidHiresScan(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("fair")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		if (!hasValidHiresScan(subjectScans)) {
			// 2018-08-13.  We'll release even poor HiRes scans if we don't have one of higher quality.
			for (final XnatMrscandata scan : subjectScans) {
				if ((isHiresScan(scan) && isNormalizedScan(scan))) {
					if (scan.getQuality().equalsIgnoreCase("poor")) {
						scan.setTargetforrelease(true);
					}
				}
			}
		}
		// BANDA wants to release sessions that don't have a T2, so we have to drop this section
		//// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		//if (!hasValidT1AndT2(subjectScans)) {
		//	for (final XnatMrscandata scan : subjectScans) {
        //   		setNoRelease(scan);
		//	}
		//}
	}
	
	
	

	@Override
	protected void setTypeDescriptionAndOthers(List<XnatMrscandata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Initially set to current description (do null check to allow child classes to initialize this earlier).
			if (scan.getDbtype() == null) {
				scan.setDbtype(scan.getType());
			}
			if (scan.getDbdesc() == null) {
				scan.setDbdesc(scan.getSeriesDescription());
			}
			setPEFields(scan, scan.getSeriesDescription());
			// Add _norm to series descriptions for normalized scans
			if (scan.getType().contains("_Norm")) {
				if (scan.getType().contains("T1w")) {
					if (scan.getDbdesc().contains("_4e")) {
						scan.setDbdesc(scan.getDbdesc().replace("_4e", "_Norm_4e"));
					} else {
						scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
					}
				} else if (scan.getType().contains("T2w")) {
					scan.setDbdesc(scan.getDbdesc().concat("_Norm"));
				} else if (scan.getType().contains("TSE_")) {
					scan.setDbdesc(scan.getDbdesc().replace("TSE_", "TSE_Norm_"));
				}
			}
			// BANDA SPECIFIC CHANGES
			if (scan.getDbtype().contains("_Setter")) {
				scan.setDbtype(scan.getDbtype().replace("_Setter", "_setter"));
			}
			String dbDesc = scan.getDbdesc()
					.replace("HCP_MGH_", "")
					.replace("HCP_CMRR_", "")
					.replace("ABCD_", "")
					.replace("_gambling", "_GAMBLING")
					.replace("_conflict", "_CONFLICT")
					.replace("_facematching", "_FACEMATCHING")
					.replace("_faceMatching", "_FACEMATCHING")
					.replace("_rest", "_REST")
					.replace("_MPR_vNav_setter", "_setter")
					.replace("_SPC_vNav_setter", "_setter")
					;
			if (dbDesc.startsWith("fMRI_REST")) {
				dbDesc = "r" + dbDesc;
			} else if (dbDesc.startsWith("fMRI")) {
				dbDesc = "t" + dbDesc;
			}
			scan.setDbdesc(dbDesc);
		}
	}

	
	@Override
	protected void updateT1T2InclusionAndDesc(List<XnatMrscandata> subjectScans) {
		// Target T1, T2 from session that has both
		final ArrayList<Integer> t1List = new ArrayList<Integer>();
		final ArrayList<Integer> t2List = new ArrayList<Integer>();
		final ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getErrorList().add("RULESERROR:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (final XnatMrscandata scan : subjectScans) {
				if ((scan.getType().matches(T1_NORM_RGX) || scan.getType().matches(T2_NORM_RGX)) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isNormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					}
				}
			}
		}
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		char shimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : subjectScans) {
			// Keeping only 1 T1/T2, Flag error if more than one 
			// BWH/BANDA CHANGE:  We're not ensuring T1/T2 shim groups match!
			// Keeping second only if in same shim group as first (NOT FOR BWH/BANDA!!!!)
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (t1Count!=0) {
					if (shimGroup == Character.MIN_VALUE) {
						shimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
					if (t1Count>1) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setNoRelease(prevScan);
						}
					//if (t1Count>1 || shimGroup!=getShimGroup(scan)) {
					//	if (shimGroup == getShimGroup(scan)) {
					//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					//		getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
					//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					//	}
					//	t1Count--;
					//	setNoRelease(scan);
					//	if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
					//		setNoRelease(prevScan);
					//	}
					} else {
					    setRDFields(scan);
						if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (t2Count!=0) {
					if (shimGroup == Character.MIN_VALUE) {
						shimGroup = getShimGroup(scan);
					}
					final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
					// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
					if (t2Count>1) {
						final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
								"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setNoRelease(prevScan);
						}
					//if (t2Count>1 || shimGroup!=getShimGroup(scan)) {
					//	if (shimGroup == getShimGroup(scan)) {
					//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					//		getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
					//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
					//				((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
					//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					//	}
					//	t2Count--;
					//	setNoRelease(scan);
					//	if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
					//		setNoRelease(prevScan);
					//	}
					} else {
					    setRDFields(scan);
						if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
							setRDFields(prevScan);
						}
					}
				}
			}
		}
		t1Count=0;
		t2Count=0;
		shimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : getReversedScanList(subjectScans)) { 
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (scan.getType().matches(T1_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
				if (t1Count>1) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
							"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
								subjectSession.getLabel() +
							", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
					}
				//if (t1Count>1 || shimGroup!=getShimGroup(scan)) {
				//	if (shimGroup == getShimGroup(scan)) {
				//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				//		getErrorList().add("RULESERROR:  There is currently more than one T1 scan available for release.  Please set " +
				//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + subjectSession.getId() +
				//					subjectSession.getLabel() +
				//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				//	}
				//	t1Count--;
				//	setNoRelease(scan);
				//	if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
				//		setNoRelease(prevScan);
				//	}
				} else {
				    setRDFields(scan);
					if (isT1Type(prevScan,false) && !isT1NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
			if (scan.getType().matches(T2_NORM_RGX) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				final XnatMrscandata prevScan = getPreviousScan(subjectScans,scan);
				// Section MODIFIED FOR BWH/BANDA!!!! NOT CONSIDERING SHIMS!!!
				if (t2Count>1) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
						getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
							"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
							", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setNoRelease(prevScan);
					}
				//if (t2Count>1 || shimGroup!=getShimGroup(scan)) {
				//	if (shimGroup == getShimGroup(scan)) {
				//		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				//		getErrorList().add("RULESERROR:  There is currently more than one T2 scan available for release.  Please set " +
				//				"the \"Don't Release\" flag for non-preferred scan. (SESSION=" + 
				//					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
				//				", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
				//	}
				//	t2Count--;
				//	setNoRelease(scan);
				//	if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
				//		setNoRelease(prevScan);
				//	}
				} else {
				    setRDFields(scan);
					if (isT2Type(prevScan,false) && !isT2NormalizedScan(prevScan)) {
						setRDFields(prevScan);
						setRDEchoScans(subjectScans, prevScan);
					}
				}
			}
		}
	}		
	

	@Override
	protected void updaterfMRIDescAndFlagError(List<XnatMrscandata> subjectScans) {
		boolean inRest = false, restGroupHasAP = false, restGroupHasPA = false, multiRestGroupAP = false, multiRestGroupPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		//int intradbSessionRestCount = 0;
		//int restGroupCount = 0;
		boolean inConflict = false, conflictGroupHasAP = false, conflictGroupHasPA = false, multiConflictGroupAP = false, multiConflictGroupPA = false;
		int conflictCount = 1;
		Integer prevConflictSession = null;
		//int intradbSessionConflictCount = 0;
		//int conflictGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || currDbdesc.matches(ANYFIELDMAP_RGX)) {
				continue;
			}
			XnatMrsessiondata scanSession = getScanSession(scan, subjectSessions);
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (inRest && ((currDbdesc.matches(AP_RGX) && restGroupHasAP) || (currDbdesc.matches(PA_RGX) && restGroupHasPA))) {
					// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
					// Need to move this outside of SBRef check so the restCount changes when it hits the first scan
					restCount++;
					//restGroupCount=0;
					multiRestGroupAP = false;
					multiRestGroupPA = false;
					restGroupHasAP = false;
					restGroupHasPA = false;
				} 
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						//intradbSessionRestCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (restGroupHasAP) {
								multiRestGroupAP = true;
							}
							restGroupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (restGroupHasPA) {
								multiRestGroupPA = true;
							}
							restGroupHasPA = true;
						}
						if ((restGroupHasAP && restGroupHasPA) || multiRestGroupAP || multiRestGroupPA) {
							//restGroupCount++;
						}
					} else {
						//intradbSessionRestCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (restGroupHasAP) {
								multiRestGroupAP = true;
							}
							restGroupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (restGroupHasPA) {
								multiRestGroupPA = true;
							}
							restGroupHasPA = true;
						}
						if ((restGroupHasAP && restGroupHasPA) || multiRestGroupAP || multiRestGroupPA) {
							//restGroupCount++;
						}
						if (restCount>2) {
							getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						//if (intradbSessionRestCount>2) {
						//	getErrorList().add("RULESERROR:  Intradb session contains more than two usable Resting State scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
						//if (restGroupCount>1) {
						//	getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Resting State scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
					}
					inRest=true;
				} else if ((restGroupHasAP && restGroupHasPA) && inRest) {
					inRest=false;
					restCount++;
					//restGroupCount=0;
					restGroupHasAP = false;
					restGroupHasPA = false;
					multiRestGroupAP = false;
					multiRestGroupPA = false;
				}
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)fMRI_rest","fMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX)) && inRest) {
				inRest=false;
				restCount++;
				//restGroupCount=0;
				restGroupHasAP = false;
				restGroupHasPA = false;
				multiRestGroupAP = false;
				multiRestGroupPA = false;
			}
			if (currDbdesc.matches(CONFLICTFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (inConflict && ((currDbdesc.matches(AP_RGX) && conflictGroupHasAP) || (currDbdesc.matches(PA_RGX) && conflictGroupHasPA))) {
						// Handle case of multiple CONFLICT groups with no intervening scans (not sure that this would ever occur).
						conflictCount++;
						//conflictGroupCount=0;
						multiConflictGroupAP = false;
						multiConflictGroupPA = false;
						conflictGroupHasAP = false;
						conflictGroupHasPA = false;
					} 
					if (prevConflictSession == null || !scan.getSubjectsessionnum().equals(prevConflictSession)) {
						prevConflictSession = scan.getSubjectsessionnum();
						//intradbSessionConflictCount=1;
						if (currDbdesc.matches(AP_RGX)) {
							if (conflictGroupHasAP) {
								multiConflictGroupAP = true;
							}
							conflictGroupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (conflictGroupHasPA) {
								multiConflictGroupPA = true;
							}
							conflictGroupHasPA = true;
						}
						if ((conflictGroupHasAP && conflictGroupHasPA) || multiConflictGroupAP || multiConflictGroupPA) {
							//conflictGroupCount++;
						}
					} else {
						//intradbSessionConflictCount++;
						if (currDbdesc.matches(AP_RGX)) {
							if (conflictGroupHasAP) {
								multiConflictGroupAP = true;
							}
							conflictGroupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							if (conflictGroupHasPA) {
								multiConflictGroupPA = true;
							}
							conflictGroupHasPA = true;
						}
						if ((conflictGroupHasAP && conflictGroupHasPA) || multiConflictGroupAP || multiConflictGroupPA) {
							//conflictGroupCount++;
						}
						if (conflictCount>2) {
							getErrorList().add("RULESERROR:  CDB session contains more than two usable groups of Conflict Task scans (SESSION=" + 
									((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						}
						//if (intradbSessionConflictCount>2) {
						//	getErrorList().add("RULESERROR:  Intradb session contains more than two usable Conflict Task scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
						//if (conflictGroupCount>1) {
						//	getErrorList().add("RULESERROR:  Intradb session group contains more than one group of usable Conflict Task scans (SESSION=" + 
						//			((scanSession!=null) ? scanSession.getLabel() : scan.getImageSessionId()) + ")");
						//}
					}
					inConflict=true;
				} else if ((conflictGroupHasAP && conflictGroupHasPA) && inConflict) {
					inConflict=false;
					conflictCount++;
					//conflictGroupCount=0;
					conflictGroupHasAP = false;
					conflictGroupHasPA = false;
					multiConflictGroupAP = false;
					multiConflictGroupPA = false;
				}
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)fMRI_conflict","fMRI_CONFLICT" + Integer.toString(conflictCount)));
			} else if ((!currDbdesc.matches(CONFLICTFMRI_RGX)) && inConflict) {
				inConflict=false;
				conflictCount++;
				//conflictGroupCount=0;
				conflictGroupHasAP = false;
				conflictGroupHasPA = false;
				multiConflictGroupAP = false;
				multiConflictGroupPA = false;
			}
		}
	}
	

	@Override
	protected void flagScansInDifferentShimGroupThanFieldmap(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            // NOTE HERE:  Let's not skip this code altogether so fieldmap still gets picked based on shims.  Let's just skip
            // outputting the error for non-rfMRI
			//if (currDbdesc == null || !(currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
			//	// For BANDA, only shim values of fmri scans normally match fieldmaps.
			//	continue;
			//}
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup==null || shimGroup==null) {
					continue;
				}
				if (fieldmapShimMap.containsKey(fmGroup) && !shimGroup.equals(fieldmapShimMap.get(fmGroup))) {
					final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
					boolean newGroup = false;
					for (final Integer thisFm : fieldmapShimMap.keySet()) {
						if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
							newGroup = true;
							scan.setParameters_sefieldmapgroup(thisFm);
							// For BANDA, only shim values of fmri scans normally match fieldmaps.
							if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
								getWarningList().add("RULESWARNING:  Scan's shim group doesn't match the shim group of its normally selected fieldmap, " +
										"so a new fieldmap group has been chosen. (SESSION=" +
										((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
										", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
							}
						}
					}
					if (!newGroup) {
						// For BANDA, only shim values of fmri scans normally match fieldmaps.
						if (currDbdesc != null && (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX))) {
							getErrorList().add("RULESERROR:  Scan's shim group doesn't match the shim group of its normally selected " +
									"fieldmap, and a new fieldmap group could not be chosen. (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
						}
					}
				}
			}
		}
	}
	

	@Override
	protected void flagScansWithMissingFieldmapGroup(List<XnatMrscandata> subjectScans) {
		final Map<Integer,String> fieldmapShimMap = new HashMap<>();
		for (final XnatMrscandata scan : subjectScans) {
			if (scan.getType().matches(FIELDMAP_RGX) && willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (fmGroup!=null && !fieldmapShimMap.containsKey(fmGroup) && shimGroup!=null) {
					fieldmapShimMap.put(fmGroup, shimGroup);
				}
			}
		}
		for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || !currDbdesc.matches(RFMRI_RGX)) {
				// For BANDA, only shim values of resting state scans normally match fieldmaps.
				continue;
			}
			if (willBeReleased(scan)) {
				final Integer fmGroup = scan.getParameters_sefieldmapgroup();
				final String shimGroup = scan.getParameters_shimgroup();
				if (!(fmGroup==null || fmGroup.toString().length()==0)) {
					continue;
				}
				final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
				boolean newGroup = false;
				for (final Integer thisFm : fieldmapShimMap.keySet()) {
					if (fieldmapShimMap.get(thisFm).equals(shimGroup)) {
						newGroup = true;
						scan.setParameters_sefieldmapgroup(thisFm);
						getWarningList().add("RULESWARNING:  Scan's nornally assigned fieldmap either doesn't exist or is unusable, " +
								"so a new fieldmap group has been chosen. (SESSION=" +
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
				if (!newGroup) {
					if (currDbdesc != null && !(currDbdesc.equals(DMRI_RGX))) {
						getErrorList().add("RULESERROR:  Scan's normally selected fieldmap either doesn't exist or is unusable, " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					} else if (currDbdesc != null) {
						getWarningList().add("RULESWARNING:  Scan's normally selected fieldmap either doesn't exist or is unusable" +
								" (diffions scan = warning only), " +
								"and a new fieldmap group could not be chosen. (SESSION=" + 
								((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
								", SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		}
	}
	
	
	@Override
	protected void flagT1T2InDifferentShimOrFieldmapGroupsOrTooManyScans(List<XnatMrscandata> subjectScans)
			throws ReleaseRulesException {
		int releaseCount = 0;
		int requiresOverrideCount = 0;
		for (final XnatMrscandata scan : subjectScans) {
			if (isMainStructuralScan(scan) && isNormalizedScan(scan) && willBeReleased(scan)) {
				releaseCount++;
				if (requiresOverride(scan)) {
					requiresOverrideCount++;
				}
				if (releaseCount>=3) {
					hardErrorList.add("RULESERROR:  Too many structural scans are marked for release.  Please " +
							"modify the release flag or usability for one or more scans. This error should not be overridden.");
				}
				// NOTE: For BWH/BANDA, we're not considering fieldmap groups or shim values for structurals.
			}
		}
		if (releaseCount<2) {
			hardErrorList.add("RULESERROR:  This session does not contain a complete T1/T2 pair marked for release.  " +
							"Note that this error cannot be overridden.  Please edit scan quality or use the release " +
							"override flags to mark scans for release.");
			
		} else if (requiresOverrideCount>0) {
			getErrorList().add("RULESERROR:  T1/T2 pair marked for release requires a release scan-level override flag to be released. " +
					"This could be due to insufficient scan quality or mismatched shim or fieldmap groups.  The override flag" +
					" has been supplied but the session errorOverride flag should be used to submit this session so this notice is logged.");
		}
	}
	
	@Override
	public void setScanOrderAndScanComplete(List<XnatMrscandata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) ||
            		currDbdesc.matches(ANYFIELDMAP_RGX)) {
                    continue;
            }
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DMRI_RGX)
            		|| currDbdesc.matches(PCASL_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DMRI_RGX)) ? "dMRI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                int frames10 = frames - 10;
                String scanComplete = null;
                String scan100 = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=420) ? "Y" : "N";
               		scanPct = (double)frames10/(420-10);
               		scan100 = (frames10>=100) ? "Y" : "N";
                	if (scan100.equals("N")) {
                		getErrorList().add("RULESERROR:  rfMRI scans should have at least 100 frames not counting the 10 initial " +
                				"frames (110 total) to have been marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                				", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                	}
                } else if (currDbdesc.matches(DMRI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		// For HCA/HCD, it looks like we're collecting 1 more frame than directions.
                		int dirNum = Integer.parseInt(dirVal)+1;
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.toUpperCase().contains("_CONFLICT")) {
                		scanComplete = (frames>=280) ? "Y" : "N";
                		scanPct = (double)frames10/(280-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Conflict scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_GAMBLING")) {
                		scanComplete = (frames>=215) ? "Y" : "N";
                		scanPct = (double)frames10/(215-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI Gambling scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} else if (currDbdesc.toUpperCase().contains("_FACEMATCHING")) {
                		scanComplete = (frames>=338) ? "Y" : "N";
                		scanPct = (double)frames10/(338-10);
                		if (scanPct<.75) {
                			getErrorList().add("RULESERROR:  tfMRI FaceMatching scans should have at least 75% of frames to have been marked usable (SESSION=" +
                					((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
                					", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                		}
                	} 
                } 
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        final Map<String,Double> dPairMap = new HashMap<>();
        for (final XnatMrscandata scan : subjectScans) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX) || currDbdesc.contains("CARIT")) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            final String dbMin = currDbdesc.replace("_AP","").replace("_PA","");
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            } else {
	            	scan.setPctpaircomplete((double) 0);
	            }
	            if (currDbdesc.matches(DMRI_RGX)) { 
	            	if (!dPairMap.containsKey(dbMin)) {
	            		dPairMap.put(dbMin, scan.getPctpaircomplete());
	            	}
	            }
            }
        }
        Double dTotal = 0.0;
        for (final Double val : dPairMap.values()) {
        	if (val == null) {
        		continue;
        	}
        	dTotal = dTotal + val;
        }
        log.debug("Diffusion pair total = " + dTotal);
        if (dTotal<100) {
        	for (final XnatMrscandata scan : subjectScans) {
        		final String currDbdesc = scan.getDbdesc();
        		if (currDbdesc == null || !currDbdesc.matches(DMRI_RGX) || 
        				!isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
        		}
        		final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
              	getErrorList().add("RULESERROR:  Insufficient dMRI frames to be marked usable (SESSION=" +
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
              			", SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ").  Need sum of pctPairComplete for pairs to be >= 100.");
        	}
        }
	}
	
	
	protected void setPEFields(XnatMrscandata scan,String seriesDesc) {
		try {
			//Float ipe_rotation = null;
			// 2018-08-13 - We are now pulling PE direction from scan NIFTI JSON rather than calculating it.
			// Per e-mails with Mike Harms, Leah, et. al, we will throw an error if ipe_rotation is missing, but if we 
			// override the error, we will treat the ipe_rotation value as zero
			for (final XnatAbstractresourceI resourceI : scan.getFile()) {
				if (!(resourceI instanceof XnatResource && resourceI.getLabel().equalsIgnoreCase(CommonConstants.NIFTI_RESOURCE))) {
					continue;
				}
				final XnatResource resource = (XnatResource)resourceI;
				if (resource == null || resource.getFileResources(CommonConstants.ROOT_PATH) == null || 
						resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
					continue;
				}
				for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
					final File f = rFile.getF();
					if (!f.getName().toLowerCase().endsWith(".json")) {
						continue;
					}
					final String json = FileUtils.readFileToString(f);
					Map<String,Object> jsonMap = gson.fromJson(json, new TypeToken<Map<String,Object>>(){}.getType());
					for (final String phaseEncodingDirectionKey : PHASE_ENCODING_DIRECTION_KEYS) {
						if (jsonMap.containsKey(phaseEncodingDirectionKey)) {
							final Object peDirection = jsonMap.get(phaseEncodingDirectionKey);
							if (peDirection != null) {
								scan.setParameters_pedirection(peDirection.toString());
								checkPESeriesDesc(scan);
								return;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown retrieving PhaseEncodingDirection - " + e.toString());
		}
		if (scan.getType().matches(FIELDMAP_RGX) || scan.getType().matches(FMRI_RGX) || scan.getType().matches(DMRI_RGX)) {
			final XnatMrsessiondata subjectSession = getScanSession(scan, subjectSessions);
			//getErrorList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SESSION=" + 
			getErrorList().add("RULESERROR:  PHASE ENCODING DIRECTION IS NULL (SESSION=" + 
									((subjectSession != null) ? subjectSession.getLabel() : scan.getImageSessionId()) + 
									", SCAN=" + scan.getId() + ", DESC=" + seriesDesc +
									", PE=" + scan.getParameters_phaseencodingdirection() + ")");
		}
	}
	
}

